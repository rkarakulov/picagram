import React from 'react';
import {Provider} from 'react-redux';
import Router from './router';
import {PersistGate} from 'redux-persist/integration/react';
import {configureStore} from './src/redux/configure-store';

const {store, persistor} = configureStore();

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Router />
      </PersistGate>
    </Provider>
  );
}
