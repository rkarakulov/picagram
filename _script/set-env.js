#!/bin/node
const fs = require('fs');
const _ = require('lodash');
//Obtain the environment string passed to the node script
let env_type = process.argv[2] || 'test';
console.log('env_type:', env_type);

const basePath = `src/libs/config/domain/env/env`;

if (basePath) {
  fs.copyFile(`${basePath}.${env_type}.ts`, `${basePath}.ts`, (err) => {
    if (err) throw err;
  });
}
