import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform,
  SafeAreaView,
  TouchableNativeFeedback,
  TouchableNativeFeedbackComponent,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import BackIcon from '../assets/icons/ArrowBack.svg';
import MinusIcon from '../assets/icons/minusIcon.svg';
import PlusIcon from '../assets/icons/plusIcon.svg';

import { COLOR_PRIMARY_FOTOS } from '../assets/colors/colors';
import Modal from 'react-native-modal';
import CloseButton from '../assets/icons/modalClose.svg';
import { connect } from 'react-redux';

import { updateCart, updateCartSelected, updateCartPhotos, updateCartOptions } from '../libs/card/domain/CartActions';
import { getPhotoIndexByPath, getPhotoCount, getPhotoCountSelection } from '../helpers/PhotoFunctions';

class EntwurftPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 1,
      cart: [],
      modal: false,
      selectedItem: null,
    };
  }

  componentDidMount() {
    // this.unsubscribe = this.props.navigation.addListener('focus', () => {
    //   this.loadPropsData();
    // });
    // this.loadPropsData();
  }

  onBackClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  updateCart = (data) => {
    this.props.updateCartPhotos(data);
  };

  openCartItem = (item, index) => {
    const cart = this.props.cart.cart;
    let selectedItem = null;
    let selectedIndex = null;

    //const searchName = album.title + ' ' + item.productName;

    //const index = cart.findIndex(obj => obj.albumName === searchName);
    if (index != -1) {
      selectedItem = cart[index];
      selectedIndex = index;
    }

    if (cart.length > 0 && selectedIndex === null) {
      selectedIndex = cart.length;
    }

    if (!selectedItem) {
    } else {
      this.props.updateCartSelected(selectedItem, selectedIndex ? selectedIndex : 0);

      this.props.navigation.navigate('CreatePhotoPageV2', {
        item: selectedItem.albumItem,
        album: selectedItem.album,
      });
      // this.props.navigation.navigate('AlbumPage');
    }
  };
  removeItem = () => {
    if (this.state.selectedItem >= 0) {
      let carts = [...this.props.cart.cart];
      carts.splice(this.state.selectedItem, 1);
      this.updateCart(carts);
    }
    this.setState({
      modal: false,
      selectedItem: null,
    });
  };

  onPlusPress = (index) => {
    let carts = [...this.props.cart.cart];
    if (carts[index]) {
      let qty = carts[index] && carts[index].quantity ? carts[index].quantity : 1;
      qty++;
      if (qty > 0) {
        carts[index].quantity = qty;
        this.updateCart(carts);
      }
    }
  };

  onMinusPress = (index) => {
    let carts = [...this.props.cart.cart];
    if (carts[index]) {
      let qty = carts[index] && carts[index].quantity ? carts[index].quantity : 1;
      qty--;
      if (qty > 0) {
        carts[index].quantity = qty;
        this.updateCart(carts);
      } else {
        this.setState({
          modal: true,
          selectedItem: index,
        });
      }
    }
  };

  onAddProductClick = () => {
    this.props.navigation.navigate('HomePage');
  };

  onButtonClick = () => {
    if (this.props.login && this.props.login.isAuthenticated) {
      this.props.navigation.navigate('UploadPage');
    } else {
      this.props.navigation.navigate('SignInPage', { fromPage: 'cart' });
    }
  };

  render() {
    // const cart = [...this.state.cart];
    const cart = this.props.cart.cart ? this.props.cart.cart : [];
    // console.log('CART', cart);
    let sumTotal = 0;

    if (cart && cart.length > 0) {
      cart.map((item, key) => {
        if (!item.finished) {
          let price = item.albumItem && item.albumItem.price ? item.albumItem.price : 0;
          let qty = item.quantity ? item.quantity : 1;
          let total = price * qty;
          sumTotal += total;
        }
      });
      sumTotal = sumTotal.toFixed(2);
    }
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    const backgroundModalButton = TouchableNativeFeedback.Ripple('#C7AEB8', true);
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => this.onBackClick()} style={styles.backButton}>
            <BackIcon />
          </TouchableOpacity>
          <View style={styles.header}>
            <Text style={styles.titleHeader}>ENTWURF</Text>
          </View>
        </View>

        <ScrollView>
          {cart &&
            cart.length > 0 &&
            cart.map((item, key) => {
              // console.log('ITEM', item);
              if (!item.finished) {
                let price = item.albumItem && item.albumItem.price ? item.albumItem.price : 0;
                let qty = item.quantity ? item.quantity : 1;
                let total = price * qty;
                let image = null;
                if (item.selected.length > 0 && item.selected[0].path) image = { uri: item.selected[0].path };
                return (
                  <View key={'cart' + key} style={styles.containerInfo}>
                    <View style={styles.innerContainerInfo}>
                      <View>
                        <Image
                          style={{ width: 125, height: 128 }}
                          source={
                            image
                              ? image
                              : item.album && item.album.image
                              ? item.album.image
                              : require('../assets/images/warenkorbImage.png')
                          }
                        />

                        <TouchableOpacity onPress={() => this.openCartItem(item, key)}>
                          <Text style={styles.bearbeiten}>BEARBEITEN</Text>
                        </TouchableOpacity>
                      </View>

                      <View
                        style={{
                          flexDirection: 'column',
                          width: '40%',
                          height: '90%',
                        }}
                      >
                        <ScrollView contentContainerStyle={{ flex: 1 }}>
                          <Text style={styles.titleContainer}>{item?.albumItem?.productName}</Text>
                        </ScrollView>
                        <View>
                          <Text style={{ fontWeight: 'bold', fontSize: 13 }}>
                            {item.album && item.album.shortDescription ? item.album.shortDescription : ''}
                          </Text>
                        </View>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'flex-end',
                            justifyContent: 'flex-end',
                          }}
                        >
                          <TouchableOpacity onPress={() => this.onMinusPress(key)}>
                            <Image
                              source={require('../assets/icons/minusIcon.png')}
                              style={{ width: 25, height: 25 }}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </View>
                );
              }
            })}
        </ScrollView>

        <Modal
          transparent={true}
          isVisible={this.state.modal}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <View style={[styles.modalContent, { justifyContent: 'flex-end' }]}>
            <View style={styles.innerModalContent}>
              <TouchableOpacity
                onPress={() => this.setState({ modal: false, selectedItem: null })}
                style={styles.closeButton}
              >
                <CloseButton />
              </TouchableOpacity>
              <Text style={styles.modalTitle}>
                Möchten Sie dieses Album wirklich aus {'\n'}Ihrem Entwurf entfernen?
              </Text>

              <View style={[styles.modalButton, { borderRadius: 20, overflow: 'hidden' }]}>
                <TouchableNativeFeedback useForeground={true} background={background} onPress={() => this.removeItem()}>
                  <View style={{ padding: 15, width: '100%' }}>
                    <Text style={styles.modalButtonText}>Löschen</Text>
                  </View>
                </TouchableNativeFeedback>
              </View>
            </View>
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    // marginTop: Platform.OS === 'ios' ? 20 : 0,
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '5%',
    backgroundColor: '#D38074',
  },
  backButton: {
    padding: 20,
    position: 'absolute',
    zIndex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
  },
  titleHeader: {
    color: '#FFFFFF',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 2,
  },
  textButton: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  containerInfo: {
    borderWidth: 3,
    borderColor: '#D38074',
    borderRadius: 20,
    width: '90%',
    alignSelf: 'center',
    marginVertical: 20,
    height: 194,
  },
  innerContainerInfo: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  bearbeiten: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    color: '#12336B',
    fontSize: 13,
    textDecorationLine: 'underline',
    marginTop: 10,
  },
  titleContainer: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  addproduct: {
    borderWidth: 1,
    borderColor: '#B42762',
    borderRadius: 20,
    alignSelf: 'center',
    alignItems: 'center',
    width: '90%',
  },
  textproduct: {
    color: '#B42762',
    textAlign: 'center',
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  footer: {
    borderTopWidth: 4,
    borderTopColor: '#B42762',
    bottom: '5%',
    backgroundColor: '#ffffff',
  },
  footerText: {
    fontSize: 20,
    color: '#B42762',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    margin: 20,
  },

  modalContent: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerModalContent: {
    backgroundColor: '#D38074',
    width: '90%',
    height: '30%',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalTitle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
  },
  modalButton: {
    borderWidth: 1,
    width: 193,
    borderColor: '#FFFFFF',
    borderRadius: 20,
    marginVertical: 10,
  },
  modalButtonText: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  closeButton: {
    position: 'relative',
    alignSelf: 'flex-end',
    padding: 20,
    bottom: '15%',
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
    product: state.product,
    login: state.login,
  };
}

export default connect(mapStateToProps, {
  updateCartPhotos,
  updateCartOptions,
  updateCartSelected,
})(EntwurftPage);
