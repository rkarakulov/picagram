import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
  Dimensions,
  SafeAreaView,
  ScrollView
} from 'react-native';
import UberIcon from '../assets/icons/uberIcon.svg';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class UberUnsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() { }

  onBackClick = () => {
    this.props.navigation.navigate('ProfilPage');
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={{ flex: 1 }}>
          <View style={styles.header}>
            <TouchableOpacity
              onPress={() => this.onBackClick()}
              style={styles.backButton}>
              <ArrowBack />
            </TouchableOpacity>
            <View style={styles.headerIconAndTitle}>
              <UberIcon />
              <Text style={styles.headerTitle}>Über uns</Text>
            </View>
          </View>

          <View style={{ zIndex: 999, flex: 1 }}>

            <ScrollView contentContainerStyle={{ paddingVertical: 30 }}>
              <View style={styles.innerContentContainer}>
                <View style={{ padding: 20 }}>
                <Text style={styles.textTitle}>
                WIR DRUCKEN DEINE SMARTPHONE-FOTOS{'\n'}
                  </Text>
                  <Text style={styles.textTitle}>
                    Unsere Mission: Das schönste Format für die schönsten Erinnerungen
                  </Text>
                  <Text style={styles.textStyle}>
                    Bilder auf dem Smartphone – das ist bequem und supereasy.
                    Aber das gedruckte Format hat viele unschlagbare Vorteile.
                    Nichts geht mehr ans Herz, als mit der besten Freundin auf dem Bett zu sitzen und durch ein Fotobuch zu blättern.
                    Oder die tollsten Erinnerungen aus einer Fotobox zu holen, wenn die Familie zu Gast ist. Oder den perfekten Moment als Postkarte zu verschicken.
                    Es gibt so viele tolle Ideen.
                    Kreativ, stilvoll, witzig, mit Charme. Weil wir wissen, dass nichts das Herz mehr berührt als wertvolle Erinnerungen, wollen wir ebendiese im perfekten Format präsentieren.
                    So wie sie es verdient haben.
                    Unsere Mission ist es also nicht, Bilder zu drucken.
                    Sondern vielmehr Erinnerungen den schönsten Rahmen zu geben.
                    Und damit Menschen glücklich zu machen.
                    Welch schönere Aufgabe kann es geben?{'\n'}
                  </Text>
                  <Text style={styles.textTitle}>
                  WIR LASSEN MENSCHEN KREATIV WERDEN
                  </Text>
                  <Text style={styles.textStyle}>
                    Das Fotobuch zum Blättern und Schwelgen ist fast schon ein Klassiker.
                    Doch Deiner Kreativität sind keine Grenzen gesetzt. Die stylishe, individuell designte Fotobox? Die ganz persönliche Postkarte.
                    Das Fanbook mit den schönsten Erinnerungen Deines Lieblingsstars? Wir wollen, dass unsere Kunden kreativ werden.
                    Und Ideen zum Leben erwecken.
                    Das Fotobuch zum Blättern und Schwelgen ist fast schon ein Klassiker.
                    Doch Deiner Kreativität sind keine Grenzen gesetzt. Die stylishe, individuell designte Fotobox? Die ganz persönliche Postkarte.
                    Das Fanbook mit den schönsten Erinnerungen Deines Lieblingsstars? Wir wollen, dass unsere Kunden kreativ werden.
                    Und Ideen zum Leben erwecken.

                  </Text>
                </View>
              </View>
            </ScrollView>
          </View>
          <Image
            source={require('../assets/images/footerImageEdited.png')}
            style={styles.imageYellow}
          />

        </LinearGradient>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  header: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '3%',
    zIndex: 1,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  headerIconAndTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  imageYellow: {
    alignSelf: 'center',
    zIndex: 0,
    bottom: 0,
    position: 'absolute',
  },
  innerContentContainer: {
    borderRadius: 20,
    backgroundColor: '#FFFFFF',
    alignSelf: 'center',
    width: '90%',
    alignItems: 'center',
  },

  textTitle: {
    color: '#D38074',
    fontSize: 20,
    textAlign: 'center',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  textStyle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    marginVertical: 10,
    textAlign: 'center'
  },
});