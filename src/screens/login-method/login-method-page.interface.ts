import { INavigation, IRoute } from '../../libs/common';
import { loginUserThunk } from '../../libs/auth/domain/_thunk/login-user';
import { registrationThunk } from '../../libs/auth/domain/_thunk/registration/registration.thunk';
import { addNotificationThunk } from '../../libs/notification/domain';

export type ILoginMethodPageProps = ILoginMethodPageOwnProps &
  ILoginMethodPageStateProps &
  ILoginMethodPageDispatchProps;

export interface ILoginMethodPageOwnProps {
  navigation: INavigation;
  route: IRoute;
}

export interface ILoginMethodPageStateProps {
  contextToken: string;
}

export interface ILoginMethodPageDispatchProps {
  loginUser: typeof loginUserThunk;
  register: typeof registrationThunk;
  addNotification: typeof addNotificationThunk;
}

export interface ILoginMethodPageState {
  email: string;
  AnmeldenButtonPressed: boolean;
  modalAppInfo: boolean;
}
