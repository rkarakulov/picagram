import * as React from 'react';
import {
  BackHandler,
  Dimensions,
  Image,
  Platform,
  SafeAreaView,
  Text,
  TextInput,
  TouchableNativeFeedback,
  TouchableOpacity,
  View,
} from 'react-native';
import OderLines from '../../assets/images/oderLines.svg';
import FacebookIconBlack from '../../assets/icons/facebookIconBlack.svg';
import AppleIcon from '../../assets/icons/appleIcon.svg';
import GoogleIcon from '../../assets/icons/googleIcon.svg';
import Modal from 'react-native-modal';
import ArrowBack from '../../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';
import CloseButton from '../../assets/icons/modalClose.svg';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { loginUserThunk } from '../../libs/auth/domain/_thunk/login-user';
import { ILoginMethodPageProps, ILoginMethodPageState } from './login-method-page.interface';
import { noop } from 'lodash';
import { registrationThunk } from '../../libs/auth/domain/_thunk/registration/registration.thunk';
import { styles } from './login-method-page.css';
import { loginMethodPagePropsSelector } from './login-method-page.props-selector';
import { addNotificationThunk } from '../../libs/notification/domain';
import { t } from '../../libs/i18n/domain';
import { Screen } from '../../libs/common';

const height = Dimensions.get('window').height;

class LoginMethodPage extends React.Component<ILoginMethodPageProps, ILoginMethodPageState> {
  private unsubscribe: any;
  private scroll: any;
  private PasswortInputRef: TextInput;

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      AnmeldenButtonPressed: true,
      modalAppInfo: false,
    };
    this._scrollToInput = this._scrollToInput.bind(this);
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.unsubscribe();
  }

  handleBackButtonClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', noop);
  }

  renderFooterIcons() {
    if (Platform.OS === 'ios') {
      return (
        <View style={styles.footerIcons}>
          <TouchableOpacity>
            <FacebookIconBlack />
          </TouchableOpacity>
          <TouchableOpacity>
            <AppleIcon />
          </TouchableOpacity>
          <TouchableOpacity>
            <GoogleIcon />
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View style={styles.footerIcons}>
          <TouchableOpacity>
            <FacebookIconBlack />
          </TouchableOpacity>
          <TouchableOpacity>
            <GoogleIcon />
          </TouchableOpacity>
        </View>
      );
    }
  }

  onAnmeldenClick = () => {
    const { email } = this.state;

    if (email === '') {
      this.props.addNotification({
        message: t('$FillAllFields'),
      });
    } else {
      this.setState({ AnmeldenButtonPressed: false });
    }
  };

  onModalButtonsClick = () => {
    this.props.navigation.navigate(Screen.ProfilePage);
  };

  onRegistrationClick = () => {
    const { email } = this.state;
    const { navigation, contextToken } = this.props;

    if (email === '') {
      this.props.addNotification({
        message: t('$FillAllFields'),
      });
    } else {
      this.props.register({
        email,
        navigation,
        token: contextToken,
      });
    }
  };

  _scrollToInput = (reactNode: React.ReactNode) => {
    // Add a 'scroll' ref to your ScrollView
    this.scroll.props.scrollToFocusedInput(reactNode);
  };

  onBackClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  render() {
    const backgroundModalButtons = TouchableNativeFeedback.Ripple('#C7AEB8', true);
    const background = TouchableNativeFeedback.Ripple('#fff', true);

    return (
      <SafeAreaView style={styles.container}>
        {this.state.AnmeldenButtonPressed ? (
          <View style={{ flex: 1 }}>
            <TouchableOpacity
              style={{ padding: 20, alignSelf: 'flex-end' }}
              onPress={() => this.props.navigation.navigate(Screen.RegistrationPage)}
            >
              <CloseButton />
            </TouchableOpacity>
            <View
              style={{
                position: 'absolute',
                bottom: 0,
                justifyContent: 'center',
                alignSelf: 'center',
                width: '100%',
              }}
            >
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'row',
                  alignSelf: 'center',
                }}
              >
                <Text style={{ fontSize: 13, fontFamily: 'Lato-Bold' }}>Du hast kein Konto?</Text>
                <TouchableOpacity
                  style={{ padding: 10 }}
                  onPress={() => this.props.navigation.navigate(Screen.RegistrationPage)}
                >
                  <Text
                    style={{
                      fontSize: 13,
                      fontFamily: 'Lato-Bold',
                      color: '#B42762',
                    }}
                  >
                    Zum Registrieren
                  </Text>
                </TouchableOpacity>
              </View>
              <Image style={styles.image} source={require('../../assets/images/footerImageEdited.png')} />
            </View>
            <KeyboardAwareScrollView
              style={{ height: height }}
              contentContainerStyle={{}}
              innerRef={(ref) => {
                this.scroll = ref ? ref['getScrollResponder']() : null;
              }}
            >
              <View
                style={{
                  alignItems: 'center',
                  paddingBottom: 20,
                  paddingTop: 30,
                }}
              >
                <Text style={styles.textWelcome}>WILLKOMMEN ZURÜCK!</Text>
                <Text style={styles.textSubtitle}>Wähle bitte eine Anmeldemethode</Text>
              </View>

              <View style={styles.inputsBlock}>
                <View style={this.state.email !== '' ? styles.input : styles.inputError}>
                  <TextInput
                    style={styles.textInput}
                    selectionColor="#000000"
                    placeholderTextColor="#707070"
                    placeholder="Gib deine E-Mailadresse ein"
                    autoCorrect={false}
                    autoCapitalize="none"
                    value={this.state.email}
                    onChangeText={(Email) => this.setState({ email: Email })}
                    blurOnSubmit={false}
                    returnKeyType="next"
                    onSubmitEditing={() => this.PasswortInputRef.focus()}
                  />
                </View>
              </View>

              <View style={{ zIndex: 9999 }}>
                <View style={[styles.registrierenButton, { borderRadius: 20, overflow: 'hidden', paddingBottom: 3 }]}>
                  <TouchableNativeFeedback
                    useForeground={true}
                    background={background}
                    onPress={() => this.onAnmeldenClick()}
                  >
                    <View>
                      <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        colors={['#FFD06C', '#D78275', '#B42762']}
                        style={styles.LinearGradient}
                      >
                        <Text style={styles.textRegistrieren}>ANMELDEN</Text>
                      </LinearGradient>
                    </View>
                  </TouchableNativeFeedback>
                </View>
              </View>
            </KeyboardAwareScrollView>
            <View
              style={{
                alignSelf: 'center',
                paddingTop: 20,
                position: 'absolute',
                bottom: 0,
              }}
            >
              <View style={styles.oder}>
                <OderLines />
                <Text
                  style={{
                    fontSize: 12,
                    paddingHorizontal: 20,
                    fontFamily: 'Lato-Heavy',
                  }}
                >
                  ODER
                </Text>
                <OderLines />
              </View>

              {this.renderFooterIcons()}
            </View>
          </View>
        ) : (
          <View style={{ flex: 1 }}>
            <TouchableOpacity onPress={() => this.setState({ AnmeldenButtonPressed: true })} style={styles.backButton}>
              <ArrowBack />
            </TouchableOpacity>
            <TouchableOpacity
              style={{ padding: 20, alignSelf: 'flex-end' }}
              onPress={() => this.props.navigation.navigate(Screen.RegistrationPage)}
            >
              <CloseButton />
            </TouchableOpacity>

            <Image style={styles.imageFooter} source={require('../../assets/images/footerImageEdited.png')} />

            <View style={{ alignItems: 'center', paddingBottom: 20, paddingTop: 30 }}>
              <Text style={styles.textWelcome}>CHECK DEINE E-MAILS</Text>
              <Text style={styles.textSubtitle}>
                Wir haben dir einen Link an mustermann@hotmail.com{'\n'}
                gesendet. Öffne den Link und bestätige deine E-Mailadresse.
              </Text>
            </View>

            <View style={{ zIndex: 9999 }}>
              <View style={[styles.registrierenButton, { borderRadius: 20, overflow: 'hidden', paddingBottom: 3 }]}>
                <TouchableNativeFeedback
                  useForeground={true}
                  background={background}
                  onPress={() => this.setState({ modalAppInfo: true })}
                >
                  <View>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={['#FFD06C', '#D78275', '#B42762']}
                      style={styles.LinearGradient}
                    >
                      <Text style={styles.textRegistrieren}>E-MAIL ÖFFNEN</Text>
                    </LinearGradient>
                  </View>
                </TouchableNativeFeedback>
              </View>

              <Modal
                isVisible={this.state.modalAppInfo}
                style={{
                  flex: 1,
                  margin: 0,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <View style={styles.modalContent}>
                  <View style={styles.innerModalContent}>
                    <View style={styles.modalTitleContainer}>
                      <View></View>
                      <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ modalAppInfo: false })}>
                        <CloseButton />
                      </TouchableOpacity>
                    </View>

                    <Text style={styles.modalSubtitle}>
                      Möchtest du ab sofort über Geschenke oder{'\n'}
                      neue Produkte oder Rabatte informiert werden?
                    </Text>
                    <View style={{ paddingTop: 30, flexDirection: 'row' }}>
                      <View style={[styles.modalButtons, { overflow: 'hidden', borderRadius: 20 }]}>
                        <TouchableNativeFeedback
                          background={backgroundModalButtons}
                          useForeground={true}
                          onPress={() => this.onModalButtonsClick()}
                        >
                          <View style={{ width: '100%', padding: 15 }}>
                            <Text style={styles.buttonText}>NEIN</Text>
                          </View>
                        </TouchableNativeFeedback>
                      </View>
                      <View style={[styles.modalButtons, { overflow: 'hidden', borderRadius: 20 }]}>
                        <TouchableNativeFeedback
                          useForeground={true}
                          background={backgroundModalButtons}
                          onPress={() => this.onModalButtonsClick()}
                        >
                          <View style={{ width: '100%', padding: 15 }}>
                            <Text style={styles.buttonText}>JA</Text>
                          </View>
                        </TouchableNativeFeedback>
                      </View>
                    </View>
                  </View>
                </View>
              </Modal>
            </View>
          </View>
        )}
      </SafeAreaView>
    );
  }
}

export default connect(loginMethodPagePropsSelector, {
  register: registrationThunk,
  loginUser: loginUserThunk,
  addNotification: addNotificationThunk,
})(LoginMethodPage);
