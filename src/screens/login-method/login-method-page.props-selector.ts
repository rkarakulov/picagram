import { createSelector } from 'reselect';
import { createPathSelector } from 'reselect-utils';
import { loginSegmentSelector } from '../../libs/auth/domain';
import { ILoginMethodPageStateProps } from './login-method-page.interface';

export const loginMethodPagePropsSelector = createSelector(
  [createPathSelector(loginSegmentSelector).results.contextToken()],
  (contextToken): ILoginMethodPageStateProps => {
    return {
      contextToken,
    };
  },
);
