import React, { Component } from 'react';
import {
  BackHandler,
  Dimensions,
  Image,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  TouchableOpacity,
  View,
} from 'react-native';
import Header from '../components/Header';
import EinstellungeIcon from '../assets/icons/einstellungeIcon.svg';
import KontaktIcon from '../assets/icons/kontaktIcon.svg';
import GiftIcon from '../assets/icons/giftIcon.svg';
import AdresseIcon from '../assets/icons/adresseIcon.svg';
import EuroIcon from '../assets/icons/euroIcon.svg';
import UberIcon from '../assets/icons/uberIcon.svg';
import LinearGradient from 'react-native-linear-gradient';
import LogoutIcon from '../assets/icons/logoutIcon.svg';
import Footer from '../components/Footer';

import { connect } from 'react-redux';

import { getProfile, loginUser, logOut } from '../libs/auth/domain/LoginActions';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

class ProfilPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fromPage: '',
    };
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.unsubscribe();
  }

  handleBackButtonClick = () => {
    if (this.props.login.isAuthenticated) {
      const { goBack } = this.props.navigation;
      goBack();
    } else {
      this.props.navigation.navigate('HomePage');
    }
    return true;
  };

  getData = () => {
    if (this.props.login && this.props.login.isAuthenticated) {
      if (this.props.login.results && this.props.login.results.contextToken) {
        this.props
          .getProfile(this.props.login.results.contextToken)
          .then((response) => {
            if (response) {
              //this.props.navigation.navigate('ProfilPage');
            }
          })
          .catch((error) => {
            //  console.log('profile error', error);
          });
      }
    }
  };
  checkUser = () => {
    //console.log('checkUser');
    // console.log(this.props.login);
    if (this.props.login && this.props.login.isAuthenticated) {
    } else {
      if (this.state.fromPage === 'profile') {
        this.setState({ fromPage: '' }, () => {
          this.props.navigation.navigate('HomePage', { fromPage: 'profile' });
        });
      } else {
        this.setState({ fromPage: 'profile' }, () => {
          this.props.navigation.navigate('SignInPage', { fromPage: 'profile' });
        });
      }
    }
  };

  componentDidMount() {
    this.checkUser();
    this.getData();
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      this.checkUser();
    });
  }

  onSettingsClick = () => {
    this.props.navigation.navigate('SettingsPage');
  };

  onKontaktClick = () => {
    this.props.navigation.navigate('HelpAndContactPage');
  };
  onBestellungenClick = () => {
    this.props.navigation.navigate('OrdersPage');
  };
  onBringAFriendClick = () => {
    this.props.navigation.navigate('BringAFriendPage');
  };
  onGiftCardClick = () => {
    this.props.navigation.navigate('GiftCardPage');
  };
  onMeineAdresseClick = () => {
    this.props.navigation.navigate('AdressePage');
  };
  onMeineZahlungsmethodenClick = () => {
    this.props.navigation.navigate('MyPaymentMethodPage');
  };
  onAusloggenClick = () => {
    this.props.logOut();
    this.props.navigation.navigate('HomePage');
  };
  onUberUnsClick = () => {
    this.props.navigation.navigate('UberUnsPage');
  };

  render() {
    const firstName =
      this.props.login.profile && this.props.login.profile.firstName ? this.props.login.profile.firstName : '';

    const background = TouchableNativeFeedback.Ripple('#fff', true);

    return (
      <SafeAreaView style={styles.container}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={{ flex: 1 }}
        >
          <Header />

          {!this.props.login.requesting && this.props.login.isAuthenticated && (
            <ScrollView style={{ zIndex: 1 }}>
              <View style={styles.contentContainer}>
                <View
                  style={{
                    position: 'relative',
                    width: '90%',
                    flexDirection: 'column',
                    justifyContent: 'space-evenly',
                    height: '100%',
                  }}
                >
                  <View>
                    <Text style={styles.willkommen}>
                      HERZLICH WILLKOMMEN{'\n'}
                      {firstName}!
                    </Text>
                    <Text style={styles.price}>0,00 EURO</Text>
                  </View>
                  <View style={[styles.buttonMeine, { borderRadius: 20, overflow: 'hidden' }]}>
                    <TouchableNativeFeedback
                      useForeground={true}
                      background={background}
                      onPress={() => this.onBestellungenClick()}
                    >
                      <View
                        style={{
                          height: '100%',
                          width: '100%',
                          justifyContent: 'center',
                        }}
                      >
                        <Text style={styles.buttonsText}>MEINE BESTELLUNGEN</Text>
                      </View>
                    </TouchableNativeFeedback>
                  </View>
                  <View style={[styles.buttonBringaFriend, { borderRadius: 20, overflow: 'hidden' }]}>
                    <TouchableNativeFeedback
                      useForeground={true}
                      background={background}
                      onPress={() => this.onBringAFriendClick()}
                    >
                      <View
                        style={{
                          height: '100%',
                          width: '100%',
                          justifyContent: 'center',
                        }}
                      >
                        <Text style={styles.buttonsText}>BRING A FRIEND</Text>
                      </View>
                    </TouchableNativeFeedback>
                  </View>
                  <View style={styles.containerMeinProfil}>
                    <View style={styles.titleContainer}>
                      <Text style={styles.titleTextStyle}>MEIN PROFIL</Text>
                    </View>
                    <TouchableOpacity onPress={() => this.onSettingsClick()}>
                      <View style={styles.containers}>
                        <View style={styles.innerContainers}>
                          <EinstellungeIcon />
                          <View style={{ width: '70%' }}>
                            <Text style={styles.innerContainersText}>Einstellunge</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onKontaktClick()}>
                      <View style={styles.containers}>
                        <View style={styles.innerContainers}>
                          <KontaktIcon />
                          <View style={{ width: '70%' }}>
                            <Text style={styles.innerContainersText}>Hilfe und Kontakt</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onGiftCardClick()}>
                      <View style={styles.containers}>
                        <View style={styles.innerContainers}>
                          <GiftIcon />
                          <View style={{ width: '70%' }}>
                            <Text style={styles.innerContainersText}>Geschenkkarte einlösen</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onMeineAdresseClick()}>
                      <View style={styles.containers}>
                        <View style={styles.innerContainers}>
                          <AdresseIcon />
                          <View style={{ width: '70%' }}>
                            <Text style={styles.innerContainersText}>Meine Adresse</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onMeineZahlungsmethodenClick()}>
                      <View style={styles.containers}>
                        <View style={styles.innerContainers}>
                          <EuroIcon />
                          <View style={{ width: '70%' }}>
                            <Text style={styles.innerContainersText}>Meine Zahlungsmethoden</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onUberUnsClick()}>
                      <View style={styles.containers}>
                        <View style={styles.innerContainers}>
                          <UberIcon />
                          <View style={{ width: '70%' }}>
                            <Text style={styles.innerContainersText}>Über uns </Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onAusloggenClick()}>
                      <View style={styles.containers}>
                        <View style={styles.innerContainers}>
                          <LogoutIcon />
                          <View style={{ width: '70%' }}>
                            <Text style={styles.innerContainersText}>Ausloggen</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <View style={{ padding: '4%' }}>
                      <Text></Text>
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>
          )}

          <Image source={require('../assets/images/footerImageEdited.png')} style={styles.imageYellow} />

          {!this.props.login.requesting && this.props.login.isAuthenticated && (
            <Footer activePage="ProfilPage" navigation={this.props.navigation} />
          )}
        </LinearGradient>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  // image: {
  //   position: 'absolute',
  //   width: width,
  //   height: height,
  //   zIndex: 0,
  // },
  imageYellow: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    zIndex: 0,
  },
  contentContainer: {
    alignItems: 'center',
    alignSelf: 'center',
    zIndex: 1,
    width: width,
    height: height,
  },
  willkommen: {
    fontSize: 25,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
  },
  price: {
    color: '#FFFFFF',
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
  },
  buttonMeine: {
    borderWidth: 1,
    borderColor: '#FFFFFF',
    borderRadius: 20,
    height: 52,
  },
  buttonBringaFriend: {
    borderRadius: 20,
    height: 52,
    backgroundColor: '#5D6DA2',
  },
  buttonsText: {
    color: '#FFFFFF',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    textAlign: 'center',
  },
  containerMeinProfil: {
    borderRadius: 20,
    backgroundColor: '#FFFFFF',
    width: '100%',
  },
  titleContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#B42762',
    padding: '4%',
  },
  titleTextStyle: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
  },
  containers: {
    borderBottomWidth: 1,
    borderBottomColor: '#B42762',
    flexDirection: 'row',
    padding: '4%',
  },
  innerContainers: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 20,
  },
  innerContainersText: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '2%',
    backgroundColor: '#FFFFFF',
  },
  icons: {
    paddingLeft: 10,
    paddingRight: 10,
  },
});

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

export default connect(mapStateToProps, { loginUser, logOut, getProfile })(ProfilPage);
