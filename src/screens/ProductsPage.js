import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import FotosComponent from '../components/Products/FotosComponent';

import {getItems} from '../helpers/PhotoFunctions';

import {connect} from 'react-redux';

class ProductsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      this.pageCheck();
    });
    this.pageCheck();
  }

  pageCheck = () => {
    var item =
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.item
        ? this.props.route.params.item
        : null;
    var album =
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.album
        ? this.props.route.params.album
        : null;

    if (!item) {
      const items = getItems();
      if (
        this.props.cart.selectedAlbum &&
        this.props.cart.selectedAlbum.album &&
        this.props.cart.selectedAlbum.album.id
      ) {
        let indexAlbum = items.findIndex(
          obj => obj.id === this.props.cart.selectedAlbum.album.id,
        );

        if (indexAlbum > -1) {
          let indexProduct = items[indexAlbum].products.findIndex(
            obj => obj.id === this.props.cart.selectedAlbum.albumItem.id,
          );

          if (indexProduct > -1) {
            album = items[indexAlbum];
            item = items[indexAlbum].products[indexProduct];
          }
        }
      }
    }

    if (!item) {
      this.props.navigation.navigate('HomePage');
    }
  };

  openPage = page => {
    this.props.navigation.navigate(page);
  };
  /* 
  onBackClick = () => {
    this.props.navigation.navigate('HomePage');
    
  };*/

  onBackClick = () => {
    const {goBack} = this.props.navigation;
    goBack();
    return true;
  };

  render() {
    var item =
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.item
        ? this.props.route.params.item
        : null;
    var album =
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.album
        ? this.props.route.params.album
        : null;

    if (!item) {
      const items = getItems();
      if (
        this.props.cart.selectedAlbum &&
        this.props.cart.selectedAlbum.album &&
        this.props.cart.selectedAlbum.album.id
      ) {
        let indexAlbum = items.findIndex(
          obj => obj.id === this.props.cart.selectedAlbum.album.id,
        );

        if (indexAlbum > -1) {
          let indexProduct = items[indexAlbum].products.findIndex(
            obj => obj.id === this.props.cart.selectedAlbum.albumItem.id,
          );

          if (indexProduct > -1) {
            album = items[indexAlbum];
            item = items[indexAlbum].products[indexProduct];
          }
        }
      }
    }
    if (item)
      return (
        <FotosComponent
          navigation={this.props.navigation}
          item={item}
          album={album}
          title={album.title}
          openPage={this.openPage}
          onBack={this.onBackClick}
        />
      );
    else
      return (
        <View>
          <Text>Component not created yet!</Text>
        </View>
      );
  }
}

const styles = StyleSheet.create({});

function mapStateToProps(state) {
  return {
    cart: state.cart,
  };
}

export default connect(mapStateToProps, {})(ProductsPage);
