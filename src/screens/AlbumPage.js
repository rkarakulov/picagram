import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  BackHandler,
  PermissionsAndroid,
  Platform,
  Image,
  SafeAreaView,
  TouchableNativeFeedback,
} from 'react-native';

import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';
import GalleryIcon from '../assets/icons/galleryIcon';
import CameraRoll from '@react-native-community/cameraroll';
import SocialTabs from '../components/SocialTabs';

export default class AlbumPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    this.unsubscribe();
  }

  async componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    if (Platform.OS === 'android') {
      const result = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'Permission Explanation',
          message: 'ReactNativeForYou would like to access your photos!',
        },
      );
      if (result !== 'granted') {
        //  console.log('Access to pictures was denied');
        return;
      }
    } else {
    }
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      //this.getAlbumData();
      BackHandler.addEventListener(
        'hardwareBackPress',
        this.handleBackButtonClick,
      );
    });
    this.getAlbumData();
  }

  getAlbumData = async (force = false) => {
    // console.log('geet lbum data');
    const data = [];
    //let data = [...this.state.data];
    //CameraRoll.get;

    CameraRoll.getAlbums({
      //first: 1,
      // groupTypes: Platform.OS === 'ios' ? 'Album' : undefined,
      //assetType: Platform.OS === 'ios' ? undefined : 'Photos',
      assetType: 'Photos',
      groupTypes: Platform.OS === 'ios' ? undefined : 'Album',
      albumType: 'All',
    })

      .then(r => {
        r.map((item, key) => {
          CameraRoll.getPhotos({
            first: 1,
            groupTypes: item.type,
            groupName: item.title,
            subType: item.subType,
          })
            .then(res => {
              //console.log('slike ' + item.title);
              data.push({
                title: item.title,
                count: item.count,
                groupTypes: item.type,
                subType: item.subType,
                pictures: res && res.edges ? res.edges : [],
              });

              this.setState({
                data: data,
              });
              // console.log('data', data);
            })
            .catch(error => {
              // console.log('greska', error);
            });
        });
      })
      .catch(err => {
        // console.log('greska2', err);
        //Error Loading Images
      });
  };

  handleBackButtonClick = () => {
    const {goBack} = this.props.navigation;
    goBack();
    return true;
  };

  onBackClick = () => {
    this.props.navigation.navigate('ProductsPage');
  };

  onButtonClick = () => {
    //this.props.navigation.navigate('ChoosePicturePage');

    let item = null;
    if (this.state.data && this.state.data.length > 0) {
      let item = {...this.state.data[0]};
      this.props.navigation.navigate('ChoosePicturePage', {
        item: item,
        type: 'local',
      });
    } else {
      this.props.navigation.navigate('ChoosePicturePage');
    }
  };

  render() {
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity
            onPress={() => this.onBackClick()}
            style={styles.backButton}>
            <ArrowBack />
          </TouchableOpacity>
          <View style={styles.headerIconAndTitle}>
            <GalleryIcon />
            <Text style={styles.headerTitle}>BILDAUSWAHL</Text>
          </View>
        </View>

        <SocialTabs type="local" navigation={this.props.navigation} />

        <View style={styles.containerTitlesAlbum}>
          <Text style={styles.textMeine}>MEINE ALBEN</Text>
          <TouchableOpacity
            style={styles.albumRefresh}
            onPress={() => this.getAlbumData(true)}>
            <Text style={styles.textAlle}>AKTUALISIEREN</Text>
          </TouchableOpacity>
        </View>
        {this.state.data && this.state.data.length > 0 && (
          <FlatList
            style={{flex: 1}}
            columnWrapperStyle={{justifyContent: 'flex-start'}}
            keyExtractor={album =>
              album && album.title ? album.title : 'album0'
            }
            data={this.state.data}
            numColumns={2}
            contentContainerStyle={{
              paddingBottom: 30,
            }}
            /* ListFooterComponent={() => {
              return (
                //Z {this.state.data.length > 0 && (
                <TouchableOpacity
                  style={styles.albumRefresh}
                  onPress={() => this.getAlbumData()}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 0}}
                    colors={['#FFD06C', '#D78275', '#B42762']}
                    style={styles.LinearGradient}>
                    <Text style={styles.textFooter}>
                      Albumliste aktualisieren
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
                //)}
              );
            }}
            */
            renderItem={({item}) => {
              if (item && item.title) {
                let img = null;
                if (item && item.pictures.length > 0)
                  img = item.pictures[0].node;
                return (
                  <View style={{flex: 1 / 2}}>
                    <TouchableOpacity
                      style={{margin: 3, alignItems: 'center'}}
                      onPress={() =>
                        this.props.navigation.navigate('ChoosePicturePage', {
                          item: item,
                          type: 'local',
                        })
                      }>
                      <View style={{paddingBottom: 20}}>
                        <Image
                          style={styles.albumImage}
                          source={{uri: img ? img.image.uri : ''}}
                        />
                        <Text style={styles.imageDetail}>{item.title}</Text>
                        <Text style={styles.imageDetail}>{item.count}</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                );
              }
            }}
          />
        )}

        <View
          style={{
            marginTop: this.state.data && this.state.data.length > 0 ? 0 : 30,
          }}>
          {/* <View>
            <Text style={styles.whiteFooter} />
          </View> */}

          {this.state.data && this.state.data.length > 0 && (
            <View
              style={[
                styles.footer,
                {borderRadius: 20, overflow: 'hidden', paddingBottom: 5},
              ]}>
              <TouchableNativeFeedback
                background={background}
                useForeground={true}
                onPress={() => this.onButtonClick()}>
                <View>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 0}}
                    colors={['#FFD06C', '#D78275', '#B42762']}
                    style={styles.LinearGradient}>
                    <Text style={styles.textButton}>GESTALTUNG STARTEN</Text>
                  </LinearGradient>
                </View>
              </TouchableNativeFeedback>
            </View>
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 15,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerIconAndTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  containerTitlesAlbum: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 20,
    marginHorizontal: 25,
  },
  textMeine: {
    color: '#D38074',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  textAlle: {
    color: '#12336B',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    alignSelf: 'flex-end',
  },
  albumImage: {
    borderRadius: 20,
    marginBottom: 10,
    height: 172,
    width: 172,
  },
  imageDetail: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
  },
  footer: {
    alignSelf: 'center',
    bottom: 30,
    width: '90%',
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.17,
    shadowRadius: 2.49,
    elevation: 2,
  },
  textButton: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  whiteFooter: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    padding: '5%',
  },
});
