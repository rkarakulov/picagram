import React, {Component, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Alert,
  BackHandler,
  Platform,
  Image,
  SafeAreaView,
  Keyboard,
  KeyboardAvoidingView,
  ScrollView,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';
import LogoHeader from '../assets/images/logo.svg';
import LinearGradient from 'react-native-linear-gradient';
import FooterImage from '../assets/images/FooterImageYellow.svg';
import SignInHeader from '../components/SignInHeader';

var height = Dimensions.get('window').height;

export default class ResetPasswordCodePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TextInputValue: '',
      ErrorStatus: true,
    };
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    this.unsubscribe();
  }
  handleBackButtonClick = () => {
    const {goBack} = this.props.navigation;
    goBack();
    return true;
  };

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => {});
  }

  onButtonClick = () => {
    const {TextInputValue} = this.state;
    if (TextInputValue == '') {
      Alert.alert('Please enter the code to proceed');
    } else {
      this.props.navigation.navigate('ResetPasswordPage');
    }
    //console.log(TextInputValue);
  };

  onEnterText = TextInputValue => {
    if (TextInputValue.trim() != 0) {
      this.setState({TextInputValue: TextInputValue, ErrorStatus: true});
    } else {
      this.setState({TextInputValue: TextInputValue, ErrorStatus: false});
    }
  };

  onBackClick = () => {
    const {goBack} = this.props.navigation;
    goBack();
    return true;
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <SignInHeader onBackClick={this.onBackClick} />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          keyboardVerticalOffset={30}
          style={styles.container}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <ScrollView contentContainerStyle={{flexGrow: 1}}>
              <View style={{flex: 1, height: height}}>
                <View style={{flex: 1}}>
                  <View style={styles.logo}>
                    <LogoHeader />
                  </View>

                  <View>
                    <Text style={styles.passworthilfe}>PASSWORTHILFE</Text>
                    <Text style={styles.textStyle}>
                      Du hast einen Sicherheitscode bekommen.{'\n'}
                      Bitte gib ihn hier ein.
                    </Text>
                  </View>

                  <View style={styles.inputContainer}>
                    <TextInput
                      style={styles.textInput}
                      placeholderTextColor="#707070"
                      autoCorrect={false}
                      autoCapitalize="none"
                      selectionColor="black"
                      selectionColor="#000000"
                      onChangeText={TextInputValue =>
                        this.onEnterText(TextInputValue)
                      }
                    />
                  </View>
                </View>
                <View
                  style={{
                    width: '100%',
                  }}>
                  <Image
                    source={require('../assets/images/footerImageEdited.png')}
                    style={styles.imageYellow}
                  />

                  <View
                    style={{
                      position: 'absolute',
                      bottom: 0,
                      width: '100%',
                      height: 90,
                    }}>
                    <TouchableOpacity
                      activeOpacity={0.9}
                      style={styles.footer}
                      onPress={() => this.onSubmit()}>
                      <LinearGradient
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 0}}
                        colors={['#FFD06C', '#D78275', '#B42762']}
                        style={styles.LinearGradient}>
                        <Text style={styles.textFooter}>WEITER</Text>
                      </LinearGradient>
                    </TouchableOpacity>
                    <Text style={styles.whiteFooter} />
                  </View>
                </View>
              </View>
            </ScrollView>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  backButton: {
    padding: 20,
    position: 'absolute',
    zIndex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
  },
  headerTitle: {
    color: '#FFFFFF',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  imageYellow: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    zIndex: 0,
  },
  logo: {
    alignItems: 'center',
    marginVertical: 30,
    height: '10%',
  },
  passworthilfe: {
    fontSize: 25,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    alignSelf: 'center',
  },
  textStyle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    alignSelf: 'center',
    textAlign: 'center',
    marginVertical: 15,
    fontSize: 13,
  },
  textInput: {
    padding: '4%',
    color: 'black',
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },

  whiteFooter: {
    backgroundColor: 'white',
    position: 'absolute',
    padding: '5%',
    width: '100%',
    bottom: 0,
    zIndex: 1,
  },
  footer: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: 30,
    width: '100%',
    zIndex: 99,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 5,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  image: {
    alignSelf: 'center',
    bottom: '5%',
  },
  inputContainer: {
    margin: 20,
    borderColor: 'gray',
    borderRadius: 20,
    borderWidth: 1,
  },
});
