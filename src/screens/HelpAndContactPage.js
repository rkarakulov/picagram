import React, { Component } from 'react';

import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
  ScrollView,
  Dimensions,
  SafeAreaView,
  TouchableNativeFeedback,
  TouchableNativeFeedbackBase
} from 'react-native';
import KontaktIcon from '../assets/icons/kontaktIcon.svg';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';
import Accordion from 'react-native-collapsible/Accordion';
import * as Animatable from 'react-native-animatable';
import Accordian from '../components/Accordian';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;



export default class HelpAndContactPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu :[
        { 
          title: 'Hier steht eine Frage?', 
          data: 'Lorem ipsum...'
        },
        { 
          title: 'Hier steht eine Frage?',
          data: 'Lorem ipsum...'
        },
        { 
         title: 'Hier steht eine Frage?',
         data: 'Lorem ipsum...'
        },
        { 
          title: 'Hier steht eine Frage?',
          data: 'Lorem ipsum...'
        },
      ]
    };
   
  
  }
  

  componentDidMount() {

  }

  renderAccordians=(item)=> {
    const items = [];
    for (item of this.state.menu) {
        items.push(
            <Accordian 
                title = {item.title}
                data = {item.data}
            />
        );
    }
    return items;
}


 
  onBackClick = () => {
    this.props.navigation.navigate('ProfilPage');
  };
  onButtonClick = () => {
    this.props.navigation.navigate('HelpAndContactFaqPage');
  };
  onEmailButtonClick = () => {
    this.props.navigation.navigate('HelpAndContactSendMessagePage');
  };

  onUnsereAgbsClick = () => {
    this.props.navigation.navigate('UnsereAgbsPage');
  };
  onDatenerklarungClick = () => {
    this.props.navigation.navigate('DatenerklarungPage');
  };
  render() {
    const background = TouchableNativeFeedback.Ripple('#fff',true)
    return (
      <SafeAreaView style={styles.container}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={{ flex: 1 }}>
          <View style={styles.header}>
            <TouchableOpacity
              onPress={() => this.onBackClick()}
              style={styles.backButton}>
              <ArrowBack />
            </TouchableOpacity>
            <View style={styles.headerIconAndContainer}>
              <KontaktIcon />
              <Text style={styles.headerTitle}>HILFE UND KONTAKT</Text>
            </View>
          </View>

          <Image
            source={require('../assets/images/footerImageEdited.png')}
            style={styles.imageYellow}
          />
          <ScrollView >
            <View style={{ paddingTop: 20 }}>
            
              <View style={styles.containerMeinProfil}>
                <View style={styles.titleContainer}>
                  <Text style={styles.titleTextStyle}>
                    HÄUFIG GESTELLTE FRAGEN
                  </Text>
                </View>       
                { this.renderAccordians() }
              </View>
    
              <View
                style={{
                  width: '100%',
                  zIndex: 2
                }}>
                  <View style={[styles.button,{ width: '90%', alignSelf: 'center' ,borderRadius:20, overflow:'hidden'}]}>
                <TouchableNativeFeedback
                  useForeground={true}
                  background={background}
                  onPress={() => this.onButtonClick()}>
                  <View style={{width:'100%',height:'100%',justifyContent:'center'}}>
                    <Text style={styles.buttonText}>BESUCHE UNSERE FAQ</Text>
                  </View>
                </TouchableNativeFeedback>
                </View>
                <View style={styles.infoContainer}>

                  <TouchableOpacity
                    style={{ padding: 10 }}
                    onPress={() => this.onUnsereAgbsClick()}>
                    <Text style={styles.infoText}>UNSERE AGBS</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{ padding: 10 }}
                    onPress={() => this.onDatenerklarungClick()}>
                    <Text style={styles.infoText}>DATENERKLÄRUNG</Text>
                  </TouchableOpacity>

                </View>
              </View>
            </View>
          </ScrollView>

          <View style={styles.feedbackContainer}>
            <Text style={styles.feedbackText}>
              Hast du Fragen oder sonstiges Feedback?{'\n'}
              Wir freuen uns von dir zu hören!
            </Text>
          </View>
          <Text style={styles.whiteFooter} /> 
          <View style={[styles.footer,{borderRadius:20,overflow:'hidden',paddingBottom:5}]}>
            
            <TouchableNativeFeedback
            useForeground={true}
            background={background}
              onPress={() => this.onEmailButtonClick()}>
                <View>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={['#FFD06C', '#D78275', '#B42762']}
                style={styles.LinearGradient}>
                <Text style={styles.textFooter}>E-MAIL SENDEN</Text>
              </LinearGradient>
              </View>
            </TouchableNativeFeedback>
          </View>

        </LinearGradient>
      </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '3%',
    zIndex: 9999,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  headerIconAndContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  imageYellow: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    zIndex: 0
  },
  contentContainer: {
    width: '100%',
    backgroundColor:'red'
  },
  containerMeinProfil: {
    backgroundColor: '#FFFFFF',
    alignSelf: 'center',
    width: '90%',
    borderRadius:20,
    padding:10
  },
  titleContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#B42762',
    padding: '4%',
  },
  titleTextStyle: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
  },
  innerContainers: {
    borderBottomWidth: 1,
    borderBottomColor: '#B42762',
    flexDirection: 'row',
    padding: '4%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  innerContainersText: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
  },
  button: {
    borderWidth: 1,
    borderColor: '#FFFFFF',
    borderRadius: 20,
    height:52,
    marginTop: 15,
  },
  buttonText: {
    color: '#FFFFFF',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    textAlign: 'center',
  },
  infoContainer: {
    width: '90%',
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  infoText: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    color: '#FFFFFF',
    textDecorationLine: 'underline',
    fontSize: 13,
  },
  feedbackContainer: {
    alignSelf: 'center',
    position: 'relative',
    width: '100%',
    zIndex: 0,
    paddingBottom: 50
  },
  feedbackText: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
    textAlign: 'center',
  },
  whiteFooter: {
    width: '100%',
    backgroundColor: 'white',
    padding: '5%',
    position: 'absolute',
    bottom: 0,
    zIndex: 1,
  },
  footer: {
    alignSelf: 'center',
    width: '90%',
    zIndex: 999,
    bottom:30
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49, 
    elevation: 2,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
});
