import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  BackHandler,
  Alert,
  Image,
  Platform,
  Dimensions,
  SafeAreaView,
  ImageBackground,
  TouchableNativeFeedback,
} from 'react-native';

import Header from '../components/Header';
import Footer from '../components/Footer';

// import {
//   COLOR_PRIMARY_FOTOS,
//   COLOR_PRIMARY_FOTOBUCH,
//   COLOR_PRIMARY_FOTOBOX,
//   COLOR_PRIMARY_POSTKARTEN,
//   COLOR_PRIMARY_FANBOOK,
// } from '../assets/colors/colors';

// import LayoutIcon from '../assets/icons/layoutIcon';
// import ShippingIcon from '../assets/icons/shippingIcon';
// import LoveIcon from '../assets/icons/loveIcon';
// import VacationIcon from '../assets/icons/vacationIcon';
// import DesignIcon from '../assets/icons/designIcon';
// import FormatIcon from '../assets/icons/formatIcon';

import { getProducts } from '../libs/product/domain/ProductActions';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

import { connect } from 'react-redux';
import { getItems } from '../helpers/PhotoFunctions';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { abs } from 'react-native-reanimated';

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      backClickCount: 0,
    };
  }

  exitApp = () => {
    if (this.state.backClickCount === 1) {
      Alert.alert(
        'Exit Application',
        'Do you want to exit the app?',
        [
          {
            text: 'No',
            onPress: () => {
              this.setState({ backClickCount: 0 });
            },
          },
          {
            text: 'Yes',
            onPress: () => {
              BackHandler.exitApp();
            },
          },
        ],
        { cancelable: true },
      );
    } else {
      this.setState({ backClickCount: 1 });

      setTimeout(() => {
        this.setState({ backClickCount: 0 });
      }, 500);
    }
    return true;
  };

  removeHandlerBack = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.exitApp);
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.exitApp);
    this.unsubscribe();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.exitApp);

    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      BackHandler.addEventListener('hardwareBackPress', this.exitApp);
      this.loadData();
    });
    this.loadData();
  }

  loadData = () => {
    let blnReload = false;
    //console.log('load data');
    //console.log(this.state.items.length);
    if (this.state.items.length > 0) {
      if (this.state.items[0].products && this.state.items[0].products.length === 0) {
        blnReload = true;
      }
    } else {
      blnReload = true;
    }
    if (blnReload) {
      //  console.log('reload data');
      const items = getItems();
      this.setState({
        items: items,
      });

      this.props.getProducts(items);
    }
  };

  onProductClick(item) {
    // console.log(item);
    BackHandler.removeEventListener('hardwareBackPress', this.exitApp);

    if (item.type === 'album') {
      this.props.navigation.navigate('DetailPage', {
        album: item,
      });
    } else {
      this.props.navigation.navigate('GeschenkkartePage', {
        album: item,
      });
    }
  }

  render() {
    const items = [...this.state.items];
    const background = TouchableNativeFeedback.Ripple('#707070', true);

    return (
      <SafeAreaView style={styles.container}>
        <Header />
        <Image style={styles.image} source={require('../assets/images/footerHomeImage.png')} />

        <ScrollView style={{ zIndex: 0 }}>
          {items.map((item, key) => {
            return (
              <View key={'albumcat' + key} style={styles.innerContainerImages}>
                <View style={{ borderRadius: 20, overflow: 'hidden' }}>
                  <TouchableNativeFeedback
                    background={background}
                    useForeground={true}
                    onPress={() => this.onProductClick(item)}
                  >
                    <View style={{ height: 300, width: 354 }}>
                      <Image
                        source={item.image}
                        style={{
                          width: 354,
                          height: 270,
                          borderBottomRightRadius: 20,
                          borderBottomLeftRadius: 20,
                        }}
                      />
                      <Text style={[styles.title, { backgroundColor: item.color }]}>{item.title}</Text>
                    </View>
                  </TouchableNativeFeedback>
                </View>
              </View>
            );
          })}
        </ScrollView>

        <Footer
          activePage="HomePage"
          removeHandlerBack={this.removeHandlerBack}
          navigation={this.props.navigation}
          color="#F5C25B"
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    // marginTop: Platform.OS === 'ios' ? 30 : 0,
  },

  innerContainerImages: {
    // width:'100%',
    alignSelf: 'center',
    marginBottom: 30,
    marginVertical: 20,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.2,
    shadowRadius: 7.49,

    elevation: 12,
  },

  title: {
    color: '#FFFFFF',
    fontSize: 25,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    position: 'absolute',
    alignSelf: 'center',
    bottom: 0,
    padding: '4%',
    width: 355,
    textAlign: 'center',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  image: {
    width: width,
    position: 'absolute',
    zIndex: 0,
    bottom: 0,
  },
  // child: {
  //   flex: 1,
  //   backgroundColor: 'rgba(4,3,2,0.1)',
  //   borderTopRightRadius: 30,
  //   borderTopLeftRadius: 30,
  // }
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
    product: state.product,
  };
}

export default connect(mapStateToProps, { getProducts })(HomePage);
