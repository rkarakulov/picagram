import { INavigation, IRoute } from '../../libs/common';
import { loginUserThunk } from '../../libs/auth/domain/_thunk/login-user';
import { registrationThunk } from '../../libs/auth/domain/_thunk/registration/registration.thunk';
import { addNotificationThunk } from '../../libs/notification/domain';

export type IRegistrationPageProps = IRegistrationPageOwnProps &
  IRegistrationPageStateProps &
  IRegistrationPageDispatchProps;

export interface IRegistrationPageOwnProps {
  navigation: INavigation;
  route: IRoute;
}

export interface IRegistrationPageStateProps {
  isAuthenticated: boolean;
  contextToken: string;
}

export interface IRegistrationPageDispatchProps {
  loginUser: typeof loginUserThunk;
  register: typeof registrationThunk;
  addNotification: typeof addNotificationThunk;
}

export interface IRegistrationPageState {
  user: string;
  password: string;
  emailError: boolean;
  passwordError: boolean;
}
