import React, { Component, ReactNode } from 'react';
import {
  BackHandler,
  Dimensions,
  findNodeHandle,
  Image,
  SafeAreaView,
  Text,
  TextInput,
  TouchableNativeFeedback,
  TouchableOpacity,
  View,
} from 'react-native';
import LogoHeader from '../../assets/images/logo.svg';
import OderLines from '../../assets/images/oderLines.svg';
import LinearGradient from 'react-native-linear-gradient';
import SignInHeader from '../../components/SignInHeader';
import Icon from 'react-native-vector-icons/AntDesign';
import { noop } from 'lodash';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { registrationPagePropsSelector } from './registration-page.props-selector';
import { loginUserThunk } from '../../libs/auth/domain';
import { styles } from './registration-page.css';
import { IRegistrationPageProps } from './registration-page.interface';
import { RegistrationFooterIcons } from './footer-icons/registration-footer-icons.ios';
import { registrationThunk } from '../../libs/auth/domain/_thunk/registration/registration.thunk';
import { addNotificationThunk } from '../../libs/notification/domain';
import { t } from '../../libs/i18n/domain';

const height = Dimensions.get('window').height;

interface IRegistrationPageState {
  firstName: string;
  lastName: string;
  password: string;
  confirmPassword: string;
  email: string;
  secureTextEntry: boolean;
  icon: string;
  modalAppInfo: boolean;
}

class RegistrationPage extends Component<IRegistrationPageProps, IRegistrationPageState> {
  private scroll: any;
  private EmailInputRef: any;
  private LastNameRef: any;
  private PasswortInputRef: any;
  private PasswortConfirmInputRef: any;

  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      password: '',
      confirmPassword: '',
      email: '',
      secureTextEntry: true,
      icon: 'eye',
      modalAppInfo: false,
    };
    this._scrollToInput = this._scrollToInput.bind(this);
  }

  onChangeIcon() {
    this.setState((prevState) => ({
      icon: prevState.icon === 'eyeo' ? 'eye' : 'eyeo',
      secureTextEntry: !prevState.secureTextEntry,
    }));
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  private unsubscribe: any;

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.unsubscribe();
  }

  handleBackButtonClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', noop);
  }

  onAnmeldenClick = () => {
    this.props.navigation.navigate('SignInPage');
  };

  onRegistrationClick = () => {
    const { firstName, lastName, email, password, confirmPassword } = this.state;
    const { navigation, contextToken } = this.props;

    if (firstName === '' || password === '' || email === '' || confirmPassword === '') {
      this.props.addNotification({ message: t('$FillAllFields') });
    } else if (password !== confirmPassword) {
      this.props.addNotification({ message: t('$WrongPassword') });
    } else {
      this.props.register({
        firstName,
        lastName,
        email,
        password,
        navigation,
        token: contextToken,
      });
    }
  };

  _scrollToInput = (reactNode: ReactNode) => {
    // Add a 'scroll' ref to your ScrollView
    this.scroll.props.scrollToFocusedInput(reactNode);
  };

  onBackClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  render() {
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    const backgroundPurple = TouchableNativeFeedback.Ripple('#B42762', true);
    return (
      <SafeAreaView style={styles.container}>
        <SignInHeader onBackClick={this.onBackClick} />
        <Image style={styles.image} source={require('../../assets/images/footerImageEdited.png')} />
        <KeyboardAwareScrollView
          style={{ height: height }}
          contentContainerStyle={{}}
          innerRef={(ref) => {
            this.scroll = ref ? ref['getScrollResponder']() : null;
          }}
        >
          <View style={styles.logo}>
            <LogoHeader />
          </View>

          <View style={{ alignItems: 'center', paddingBottom: 20 }}>
            <Text style={styles.textWelcome}>SCHÖN, DASS DU DA BIST!</Text>
          </View>

          <View style={styles.inputsBlock}>
            <View style={this.state.firstName !== '' ? styles.input : styles.inputError}>
              <TextInput
                onFocus={(event) => {
                  // `bind` the function if you're using ES6 classes
                  this._scrollToInput(findNodeHandle(event.target));
                }}
                style={styles.textInput}
                selectionColor="#000000"
                placeholderTextColor="#707070"
                placeholder="Vorname"
                autoCorrect={false}
                autoCapitalize="none"
                value={this.state.firstName}
                onChangeText={(FirstName) => this.setState({ firstName: FirstName })}
                blurOnSubmit={false}
                returnKeyType="next"
                onSubmitEditing={() => this.LastNameRef.focus()}
              />
            </View>
            <View style={this.state.lastName !== '' ? styles.input : styles.inputError}>
              <TextInput
                onFocus={(event) => {
                  // `bind` the function if you're using ES6 classes
                  this._scrollToInput(findNodeHandle(event.target));
                }}
                style={styles.textInput}
                selectionColor="#000000"
                ref={(ref) => {
                  this.LastNameRef = ref;
                }}
                placeholderTextColor="#707070"
                placeholder="Nachname"
                autoCorrect={false}
                autoCapitalize="none"
                value={this.state.lastName}
                onChangeText={(LastName) => this.setState({ lastName: LastName })}
                blurOnSubmit={false}
                returnKeyType="next"
                onSubmitEditing={() => this.EmailInputRef.focus()}
              />
            </View>
            <View style={this.state.email !== '' ? styles.input : styles.inputError}>
              <TextInput
                style={styles.textInput}
                selectionColor="#000000"
                placeholderTextColor="#707070"
                placeholder="Gib deine E-Mailadresse ein"
                autoCorrect={false}
                autoCapitalize="none"
                value={this.state.email}
                onChangeText={(Email) => this.setState({ email: Email })}
                blurOnSubmit={false}
                returnKeyType="next"
                ref={(ref) => {
                  this.EmailInputRef = ref;
                }}
                onSubmitEditing={() => this.PasswortInputRef.focus()}
              />
            </View>
            <View
              style={
                this.state.password !== '' && this.state.password === this.state.confirmPassword
                  ? styles.input
                  : styles.inputError
              }
            >
              <TextInput
                style={styles.textInputPassword}
                selectionColor="#000000"
                placeholderTextColor="#707070"
                placeholder="Passwort"
                autoCorrect={false}
                autoCapitalize="none"
                secureTextEntry={this.state.secureTextEntry}
                onChangeText={(Password) => this.setState({ password: Password })}
                value={this.state.password}
                blurOnSubmit={false}
                returnKeyType="next"
                ref={(ref) => {
                  this.PasswortInputRef = ref;
                }}
                onSubmitEditing={() => this.PasswortConfirmInputRef.focus()}
              />
              <TouchableOpacity onPress={() => this.onChangeIcon()} style={{ padding: 10 }}>
                <Icon name={this.state.icon} size={24} />
              </TouchableOpacity>
            </View>

            <View
              style={
                this.state.password !== '' && this.state.password === this.state.confirmPassword
                  ? styles.input
                  : styles.inputError
              }
            >
              <TextInput
                style={styles.textInputPassword}
                selectionColor="#000000"
                placeholderTextColor="#707070"
                placeholder="Passwort bestätigen"
                autoCorrect={false}
                autoCapitalize="none"
                secureTextEntry={this.state.secureTextEntry}
                value={this.state.confirmPassword}
                onChangeText={(confirmPassword) => this.setState({ confirmPassword })}
                returnKeyType="done"
                ref={(ref) => {
                  this.PasswortConfirmInputRef = ref;
                }}
              />
              <TouchableOpacity onPress={() => this.onChangeIcon()} style={{ padding: 10 }}>
                <Icon active name={this.state.icon} size={24} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ zIndex: 9999 }}>
            <View style={[styles.registrierenButton, { borderRadius: 20, overflow: 'hidden', paddingBottom: 3 }]}>
              <TouchableNativeFeedback
                useForeground={true}
                background={background}
                onPress={() => this.onRegistrationClick()}
              >
                <View>
                  <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    colors={['#FFD06C', '#D78275', '#B42762']}
                    style={styles.LinearGradient}
                  >
                    <Text style={styles.textRegistrieren}>REGISTRIEREN</Text>
                  </LinearGradient>
                </View>
              </TouchableNativeFeedback>
            </View>

            <View style={{ alignItems: 'center', paddingVertical: 10 }}>
              <Text style={{ fontSize: 12, fontFamily: 'Lato-Regular' }}>Du hast bereits ein Konto?</Text>
            </View>

            <View style={[styles.anmeldenButton, { borderRadius: 20, overflow: 'hidden' }]}>
              <TouchableNativeFeedback
                useForeground={true}
                background={backgroundPurple}
                onPress={() => this.onAnmeldenClick()}
              >
                <View
                  style={{
                    height: '100%',
                    width: '100%',
                    justifyContent: 'center',
                  }}
                >
                  <Text style={styles.textAnmelden}>ANMELDEN</Text>
                </View>
              </TouchableNativeFeedback>
            </View>

            <View
              style={{
                alignSelf: 'center',
                paddingTop: 20,
              }}
            >
              <View style={styles.oder}>
                <OderLines />
                <Text
                  style={{
                    fontSize: 12,
                    paddingHorizontal: 20,
                    fontFamily: 'Lato-Heavy',
                  }}
                >
                  ODER
                </Text>
                <OderLines />
              </View>
              <RegistrationFooterIcons styles={styles.footerIcons} />
            </View>
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

export default connect(registrationPagePropsSelector, {
  register: registrationThunk,
  addNotification: addNotificationThunk,
  loginUser: loginUserThunk,
})(RegistrationPage);
