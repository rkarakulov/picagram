import { createSelector } from 'reselect';
import { createPathSelector } from 'reselect-utils';
import { loginSegmentSelector } from '../../libs/auth/domain';
import { IRegistrationPageStateProps } from './registration-page.interface';

export const registrationPagePropsSelector = createSelector(
  [
    createPathSelector(loginSegmentSelector).isAuthenticated(),
    createPathSelector(loginSegmentSelector).results.contextToken(),
  ],
  (isAuthenticated, contextToken): IRegistrationPageStateProps => {
    return {
      isAuthenticated,
      contextToken,
    };
  },
);
