import { TouchableOpacity, View } from 'react-native';
import FacebookIconBlack from '../../../assets/icons/facebookIconBlack.svg';
import * as React from 'react';
import GoogleIcon from '../../../assets/icons/googleIcon.svg';
import { IRegistrationFooterIconsProps } from './registration-footer-icons.interface';

export const RegistrationFooterIcons: React.FC<IRegistrationFooterIconsProps> = (props) => {
  return (
    <View style={props.styles}>
      <TouchableOpacity>
        <FacebookIconBlack />
      </TouchableOpacity>
      <TouchableOpacity>
        <GoogleIcon />
      </TouchableOpacity>
    </View>
  );
};
