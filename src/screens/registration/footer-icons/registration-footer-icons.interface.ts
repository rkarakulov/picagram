import { StyleProp, ViewStyle } from 'react-native';

export interface IRegistrationFooterIconsProps {
  styles: StyleProp<ViewStyle>;
}
