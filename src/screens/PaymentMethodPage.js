import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Platform, SafeAreaView, TouchableNativeFeedback} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import BackIcon from '../assets/icons/ArrowBack.svg';
import Cards from '../assets/images/cardsLogo.svg';
import Paypal from '../assets/images/paypalLogo.svg';
import ApplePay from '../assets/images/applepayLogo.svg';
import Sofort from '../assets/images/sofortLogo.svg';
import GiroPay from '../assets/images/giropayLogo.svg';
import BouncyCheckbox from 'react-native-bouncy-checkbox';

export default class PaymentMethodPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  onBackClick = () => {
    this.props.navigation.navigate('MyPaymentMethodPage');
  };

  render() {
    const background = TouchableNativeFeedback.Ripple('#fff',true)

    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity
            onPress={() => this.onBackClick()}
            style={styles.backButton}>
            <BackIcon />
          </TouchableOpacity>
          <View style={styles.header}>
            <Text style={styles.titleHeader}>ZAHLUNGSART</Text>
          </View>
        </View>

        <View style={{flexDirection: 'column', justifyContent: 'space-evenly'}}>
          <View style={styles.contentContainer}>
            <View style={styles.checkboxItems}>
              <BouncyCheckbox fillColor="#D38074" />
              <Text style={styles.checkboxTitle}>Zahlungskarte</Text>
            </View>
            <Cards style={{marginHorizontal: 20}} />
          </View>
          <View style={styles.contentContainer}>
            <View style={styles.checkboxItems}>
              <BouncyCheckbox fillColor="#D38074" />
              <Text style={styles.checkboxTitle}>Paypal</Text>
            </View>
            <Paypal style={{marginHorizontal: 20}} />
          </View>
          <View style={styles.contentContainer}>
            <View style={styles.checkboxItems}>
              <BouncyCheckbox fillColor="#D38074" />

              <Text style={styles.checkboxTitle}>Apple Pay</Text>
            </View>
            <ApplePay style={{marginHorizontal: 20}} />
          </View>
          <View style={styles.contentContainer}>
            <View style={styles.checkboxItems}>
              <BouncyCheckbox fillColor="#D38074" />
              <Text style={styles.checkboxTitle}>Sofort</Text>
            </View>
            <Sofort style={{marginHorizontal: 20}} />
          </View>
          <View style={styles.contentContainer}>
            <View style={styles.checkboxItems}>
              <BouncyCheckbox fillColor="#D38074" />
              <Text style={styles.checkboxTitle}>Giropay</Text>
            </View>
            <GiroPay style={{marginHorizontal: 20}} />
          </View>
        </View>
   <View style={[styles.Button,{borderRadius:20,overflow:'hidden',paddingBottom:5}]}>
        <TouchableNativeFeedback 
        useForeground={true}
        background={background}>
          <View>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#FFD06C', '#D78275', '#B42762']}
            style={styles.LinearGradient}>
            <Text style={styles.textButton}>BESTÄTIGEN</Text>
          </LinearGradient>
          </View>
        </TouchableNativeFeedback>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    // justifyContent:'space-between'
  },
  headerContainer: {
    marginTop: (Platform.OS === 'ios') ? 20 : 0,
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '5%',
    backgroundColor: '#D38074',
  },
  backButton: {
    padding: 20,
    position: 'absolute',
    zIndex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
  },
  titleHeader: {
    color: '#FFFFFF',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  Button: {
    alignSelf: 'center',
    width: '90%',
    // paddingTop: '15%',
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49, 
    elevation: 2,
  },
  textButton: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  contentContainer: {
    flexDirection: 'row',
    width: '90%',
    height: '15%',
    borderWidth: 3,
    borderColor: '#D38074',
    borderRadius: 20,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'space-between',
    // marginBottom: 20,
  },

  checkboxItems: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
  },
  checkboxTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
  },
});
