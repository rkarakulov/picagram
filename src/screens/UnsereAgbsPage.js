import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
  Dimensions,
  SafeAreaView,
  ScrollView
} from 'react-native';
import UberIcon from '../assets/icons/uberIcon.svg';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class UnsereAgbsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() { }

  onBackClick = () => {
    const {goBack} = this.props.navigation;
    goBack();
    return true;
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={{ flex: 1 }}>
          <View style={styles.header}>
            <TouchableOpacity
              onPress={() => this.onBackClick()}
              style={styles.backButton}>
              <ArrowBack />
            </TouchableOpacity>
            <View style={styles.headerTitle}>
              <Text style={styles.headerTitleText}>UNSERE AGBS</Text>
            </View>
          </View>

          <View style={{ zIndex: 999, flex: 1 }}>

            <ScrollView contentContainerStyle={{ paddingVertical: 30 }}>
              <View style={styles.innerContentContainer}>
                <View style={{ padding: 20 }}>
                  <Text style={styles.textTitle}>
                  Allgemeine Geschäftsbedingungen{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Dies sind die Allgemeinen Geschäftsbedingungen zur Nutzung des Angebotes „PICAMORY“ (Mobile- oder{'\n'}
Web-App) der LAPIXA GmbH, Dircksenstraße 40, DE-10178 Berlin (nachfolgend als „PICOMORY“, „wir“ oder{'\n'}
„uns“ bezeichnet). Diese Allgemeinen Geschäftsbeziehungen werden als „AGB“ bezeichnet.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  1. Geltungsbereich{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  1.1 Diese AGB gelten für die Nutzung unserer Angebote durch dich als unseren Kunden (nachfolgend „Kunde“{'\n'}
bzw. „Kunden“ bzw. „du“ „dein(e)“) in ihrer zum Zeitpunkt der Bestellung auf unserer Webseite abrufbaren{'\n'}
Fassung. Kunden im Sinne dieser AGB sind sowohl Verbraucher (§ 13 BGB) als auch Unternehmer (§ 14 BGB).{'\n\n'}
1.2 Kunden können über unsere Webseite www.picamory.com und die dort verfügbare Web-App als auch über{'\n'}
unsere Mobile-App kreative Fotoprodukte entwerfen und diese anschließend bei uns bestellen.{'\n\n'}
1.3 Wir bedienen uns zur Erfüllung von Bestellaufträgen externer Dienstleister. An diese Dienstleister werden{'\n'}
die entsprechenden Fotodaten und auch die zugehörigen Kundendaten übersandt. Diese Dienstleister wurden{'\n'}
vertraglich auf die Einhaltung datenschutzrechtlicher Bestimmungen und auf die ausschließlich{'\n'}
weisungsgebundene Nutzung von personenbezogenen Daten verpflichtet.{'\n\n'}
1.3 Abweichende Geschäftsbedingungen von Kunden erkennen wir ohne ausdrückliche schriftliche Zustimmung{'\n'}
nicht an. Wenn und soweit in diesen AGB nicht ausdrücklich etwas anderes bestimmt ist, gelten diese{'\n'}
gleichermaßen für den Geschäftsverkehr mit Verbrauchern und Unternehmern.{'\n\n'}
1.4 Wir erbringen unsere Leistungen ausschließlich auf Basis dieser AGB. Daher finden die{'\n'}
Geschäftsbedingungen des Kunden oder Dritter auch dann keine Anwendung, wenn wir ihrer Geltung im{'\n'}
Einzelfall nicht gesondert widersprechen oder wenn wir auf ein Schreiben Bezug nehmen, das{'\n'}
Geschäftsbedingungen des Kunden oder eines Dritten enthält oder auf solche verweist.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  2. Registrierung, Pflichten des Kunden{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  2.1 Bestimmte Angebote in unserer App oder auf unserer Webseite, wie etwa der Upload von Fotos und der{'\n'}
digitale Entwurf eines Fotobuchs, sind für Kunden frei zugänglich und kostenfrei nutzbar.{'\n\n'}
2.2 Die vollumfängliche Nutzung unserer Angebote, insbesondere die Bestellung von Fotobüchern, setzt deine{'\n'}
Registrierung als Kunde voraus. Die Registrierung erfolgt durch die Eröffnung eines Kundenkontos unter{'\n'}
Zustimmung zu diesen AGB und unserer Datenschutzerklärung{'\n'}
https://www.picamory.com/datenschutzerklarung . Mit der erfolgreichen Registrierung kannst du bei uns z.B.{'\n'}
deine Fotobuch-Projekte speichern und Bestellungen ohne erneute Adresseingabe ausführen. Wenn du ein{'\n'}
Kundenkonto für ein Unternehmen eröffnest, sichert du zu, dass du zur Vertretung dieses Unternehmens{'\n'}
berechtigt bist.{'\n\n'}
2.2 Im Rahmen der Registrierung bist du verpflichtet, die von uns angefragten Daten wahrheitsgemäß und{'\n'}
vollständig anzugeben. Du hast dein von dir gewähltes Passwort geheim zu halten und den Zugang zu deinem{'\n'}
Kundenkonto sorgfältig zu sichern. Du verpflichtest dich, uns umgehend zu informieren, sofern es{'\n'}
Anhaltspunkte dafür gibt, dass dein Kundenkonto von Dritten missbraucht wurde.{'\n\n'}
2.3 Wir behalten uns vor, deine Registrierung ohne Angabe von Gründen abzulehnen oder dich nachträglich von{'\n'}
der Nutzung unserer Angebote auszuschließen.{'\n\n'}
2.4 Ein von dir erstelltes persönliches Kundenkonto ist nicht auf Dritte übertragbar.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  3. Bestellung, Vertragsabschluss{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  3.1 Bei unseren Angeboten auf unserer Webseite oder in unserer App handelt es sich noch nicht um ein{'\n'}
verbindliches Angebot im Rechtssinne, sondern um eine Einladung an Kunden, ein Angebot zum Abschluss{'\n'}
eines Vertrages abzugeben. Du gibst ein Angebot zum Abschluss eines Vertrages mit uns ab, indem Du einen{'\n'}
oder mehrere von Dir entworfene digitale Fotobücher in den virtuellen Warenkorb auf unserer Webseite oder{'\n'}
in unserer App legst und am Ende des Bestellvorgangs den Button „Jetzt Kaufen” anklickst. Im Anschluss{'\n'}
erhältst Du von uns eine Bestätigungsmail, mit welcher wir dein Angebot auf Abschluss eines Vertrages{'\n'}
annehmen.{'\n\n'}
3.3 Nach dem Zustandekommen des Vertrages ist eine nachträgliche Änderung, Zusammenführung oder{'\n'}
Löschung deiner Bestellungen - vorbehaltlich eines dir etwaig zustehenden Widerrufsrechts - nicht mehr{'\n'}
möglich.{'\n'}
                  </Text>

                  <Text style={styles.textSubtitle}>
                  4. Preise{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  4.1 Die Lieferung der Waren erfolgt zu den am Tag der Auftragserteilung gültigen, in der Webseite oder App von{'\n'}
uns angegebenen Preisen in Euro. Sämtliche Preise setzen sich aus dem Auftragswert der jeweiligen Bestellung,{'\n'}
den Versandkosten und etwaigen sonstigen Preisbestandteilen zusammen. Sämtliche Preise beinhalten die{'\n'}
jeweils geltende gesetzliche Umsatzsteuer.{'\n\n'}
4.2 Versandkosten werden gesondert berechnet und richten sich nach Art und Umfang der Bestellung sowie{'\n'}
dem von dir angegebenen Lieferort. Die dir entstehenden Versandkosten einschließlich der voraussichtlichen{'\n'}
Lieferzeit werden im Rahmen des Online-Bestellprozesses ausgewiesen.{'\n\n'}
4.3 Im Falle des Versandes in Länder außerhalb der europäischen Union können zusätzlich Zollgebühren{'\n'}
anfallen. Diese sind nicht im Preis enthalten und von dir gesondert zu tragen.{'\n\n'}
4.4 Du kannst für die Bezahlung unter den jeweils zur Auswahl stehenden Zahlungsmethoden auswählen. Im{'\n'}
Falle der Bezahlung per Lastschrift wird der Rechnungsbetrag von dem im Rahmen deiner Bestellung{'\n'}
angegebenen Bankkonto abgebucht. Falls eine Abbuchung von deinem Konto aus von dir zu vertretenden{'\n'}
Gründen scheitert, z.B. wegen nicht ausreichender Kontodeckung, so hast Du uns die durch die Rückbelastung{'\n'}
entstehenden Bankbearbeitungsgebühren zu erstatten.{'\n\n'}
4.5 Rechnungen für deine Bestellungen bei uns werden dir als PDF-Dokument per E-Mail zur Verfügung gestellt.{'\n\n'}
4.6 Du kannst nur mit einer unbestrittenen oder rechtskräftig festgestellten oder durch uns anerkannten{'\n'}
Forderung aufrechnen. Du kannst ein Zurückbehaltungsrecht nur ausüben, wenn dein Gegenanspruch auf{'\n'}
demselben Vertragsverhältnis beruht.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  5. Upload von Bildmaterial{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  5.1 Du hast über unsere Webseite oder App die Möglichkeit, Fotodateien oder sonstige Grafiken (nachfolgend{'\n'}
„Bildmaterial“) hochzuladen, dieses zur Erstellung eines Fotobuchs zu nutzen und im Rahmen deiner Bestellung{'\n'}
an uns zu übermitteln.{'\n\n'}
5.2 Du bist alleine dafür verantwortlich, dass die Nutzung des von dir hochgeladenen bzw. uns übermittelten{'\n'}
Bildmaterials im Rahmen der Erstellung eines Fotoproduktes und dessen Bestellung rechtmäßig erfolgt. Du{'\n'}
bestätigst insbesondere durch das Hochladen und / oder Übermitteln des Bildmaterials an uns, dass du über die{'\n'}
erforderlichen Rechte verfügst, um das Bildmaterial einschließlich seiner Inhalte (z.B. Darstellung von{'\n'}
bestimmten Personen) zu vervielfältigen bzw. durch Dritte vervielfältigen zu lassen.{'\n\n'}
5.3 Du garantierst, dass das hochgeladene und / oder an uns übermittelte Bildmaterial nicht gegen geltendes{'\n'}
Recht verstößt, insbesondere, dass mit dem Bildmaterial keine Inhalte hochgeladen und / oder übermittelt{'\n'}
werden, die Gesetze zum Schutze der Jugend oder Strafgesetze verletzen, die Urheber-, Marken- oder sonstigen{'\n'}
Schutzrechte, das allgemeine Persönlichkeitsrecht oder sonstige Rechte Dritter verletzen oder gegen{'\n'}
vertragliche oder gesetzliche Geheimhaltungspflichten verstoßen.{'\n\n'}
5.4 Werden wir wegen einer Rechtsverletzung von Dritten in Anspruch genommen, für die du verantwortlich{'\n'}
bist, bist du verpflichtet, uns insoweit von solchen Ansprüchen Dritter sowie von den uns entstandenen Kosten{'\n'}
und Aufwendungen, einschließlich der Kosten für die erforderliche Rechtsverteidigung, vollumfänglich{'\n'}
freizustellen bzw. uns die im Zusammenhang mit einer solchen Rechtsverletzung entstanden Kosten auf{'\n'}
Nachweis zu erstatten.{'\n\n'}
5.5 Du bist zudem verpflichtet, sicher zu stellen, dass das von Dir hochgeladene Bildmaterial keine Viren,{'\n'}
Trojaner, Würmer und sonstige Schadsoftware enthält.{'\n\n'}
5.6 Wir sind berechtigt, aber nicht verpflichtet, den Inhalt des von dir hochgeladenen Bildmaterials auf seine{'\n'}
Rechtmäßigkeit hin zu überprüfen. Werden uns Verstöße gegen gesetzliche Bestimmungen, insbesondere, aber{'\n'}
nicht ausschließlich, gegen Strafvorschriften über den Besitz oder die Verbreitung kinder-, jugend- oder{'\n'}
tierpornographischer Abbildungen (§§ 184 bis 184c StGB) bekannt, so werden wir unverzüglich die{'\n'}
Strafverfolgungsbehörden einschalten. Diese Überprüfung kann auch durch unsere externen Dienstleister{'\n'}
stattfinden.{'\n\n'}
5.7 Mit dem Hochladen des Bildmaterials räumst du uns zugleich das Recht ein, das zur Verfügung gestellte{'\n'}
Bildmaterial im Rahmen des notwendigen Umfangs zur Erstellung der bestellten Fotoprodukte zu nutzen.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  6. Widerrufsrecht{'\n'}

                  </Text>
                  <Text style={styles.textSubtitle}>
                  Unsere Fotoprodukte sind Produkte, die nicht vorgefertigt sind und für deren Herstellung eine individuelle{'\n'}
Auswahl oder Bestimmung durch dich maßgeblich ist. Das Widerrufsrecht ist für solche Produkte gemäß §{'\n'}
312g Abs. 1 Ziffer 1 BGB ausgeschlossen.{'\n'}
                    </Text> 
                    <Text style={styles.textSubtitle}>
                    7. Mängelgewährleistung{'\n'}
                      </Text>  
                      <Text style={styles.textStyle}>
                      7.1 Sollte ein von uns bzw. einem Dienstleister geliefertes Fotobuch einen Mangel (z.B. Beschädigungen,{'\n'}
laborbedingte Fehler) aufweisen, hast du Anspruch auf Nacherfüllung, nach deiner Wahl entweder in Form der{'\n'}
Nachbesserung oder Lieferung einer neuen mangelfreien Sache (Nachlieferung). Wir sind berechtigt, die{'\n'}
gewählte Art der Nacherfüllung zu verweigern, wenn diese objektiv unmöglich ist oder nur mit{'\n'}
unverhältnismäßigen Kosten durchgeführt werden kann. Wird die Nacherfüllung vollständig verweigert oder ist{'\n'}
die Nacherfüllung fehlgeschlagen, kannst Du nach deiner Wahl den Rücktritt vom Vertrag erklären und den{'\n'}
Kaufpreis mindern.{'\n\n'}
7.2 Im Übrigen gelten hinsichtlich der Gewährleistung die gesetzlichen Bestimmungen und für Kaufleute{'\n'}
zusätzlich die besonderen kaufmännischen Untersuchungs- und Rügeverpflichten.{'\n\n'}
7.3 Über die gesetzlichen Gewährleistungsbestimmungen hinaus werden keine Garantien hinsichtlich der{'\n'}
gelieferten Waren oder Dienstleistungen übernommen, außer solche Garantien werden ausdrücklich schriftlich{'\n'}
eingeräumt.{'\n\n'}
7.4 Von dir im Rahmen der Bestellung gemachte Schreibfehler, vom dir genutztes qualitativ unzureichendes{'\n'}
Bildmaterial (z.B. schlechte Auflösung) oder von dir verschuldete Gestaltungsfehler bei der Erstellung eines{'\n'}
Fotobuches (falsches Bildformat, falsche Bildausrichtung usw.) sind von der Reklamation / Gewährleistung{'\n'}
grundsätzlich ausgeschlossen.{'\n'}
                        </Text>  
                        <Text style={styles.textSubtitle}>
                        8. Haftung{'\n'}
                          </Text>  
                          <Text style={styles.textStyle}>
                          8.1 Unsere Haftung, gleich aus welchem Rechtsgrund, ist ausgeschlossen, sofern der Schaden nicht auf Vorsatz{'\n'}
oder grober Fahrlässigkeit oder auf der einfach fahrlässigen Verletzung wesentlicher Vertragspflichten beruht.{'\n'}
„Wesentliche Vertragspflichten“ sind solche Pflichten, die vertragswesentliche Rechtspositionen des Bestellers{'\n'}
schützen, die ihm der Vertrag seinem Inhalt nach gerade zu gewähren hat; wesentlich sind ferner solche{'\n'}
Vertragspflichten, deren Erfüllung die ordnungsgemäße Durchführung des Vertrages überhaupt erst ermöglicht{'\n'}
und auf deren Einhaltung der Auftragnehmer regelmäßig vertraut und vertrauen darf.{'\n\n'}
8.2 In Fällen einfacher Fahrlässigkeit ist die Höhe eines etwaigen Schadensersatzanspruches auf den{'\n'}
vertragstypischen, vorhersehbaren Schaden begrenzt.{'\n\n'}
8.3 Die vorstehenden Bestimmungen zur Haftungsbeschränkung gelten entsprechend für eine Haftung unserer{'\n'}
Vertreter / Organe und Mitarbeiter / Erfüllungsgehilfen.{'\n\n'}
8.4 Die vorstehenden Haftungsbeschränkungen gelten nicht für deine Ansprüche im Fall der Verletzung von{'\n'}
Leben, Körper und Gesundheit sowie für deine Ansprüche nach dem Produkthaftungsgesetz.{'\n'}
                            </Text> 
                            <Text style={styles.textSubtitle}>
                            9. Schlussbestimmungen{'\n'}
                              </Text> 
                              <Text style={styles.textStyle}>
                              9.1 Für alle auf Grundlage dieser AGB geschlossenen Verträge gilt das Recht der Bundesrepublik Deutschland.{'\n'}
Die Anwendung des UN-Kaufrechts über den Kauf beweglicher Sachen sowie der Kollisionsregeln des deutschen{'\n'}
internationalen Privatrechts sind ausgeschlossen. Unberührt bleiben zwingende Bestimmungen des Staates, in{'\n'}
dem du als Kunde deinen gewöhnlichen Aufenthalt hast.{'\n\n'}
9.2 Sollten einzelne Bestimmungen dieser AGB unwirksam sein oder werden und/oder den gesetzlichen{'\n'}
Regelungen widersprechen, so wird hierdurch die Wirksamkeit der vertraglichen Bestimmungen im Übrigen{'\n'}
nicht berührt. Die unwirksame Bestimmung wird von den Parteien einvernehmlich durch eine solche{'\n'}
Bestimmung ersetzt, welche dem wirtschaftlichen Sinn und Zweck der unwirksamen Bestimmung in{'\n'}
rechtswirksamer Weise am Nächsten kommt. Die vorstehende Regelung gilt entsprechend bei Regelungslücken.{'\n'}
9.3 Jegliche Änderungen, Ergänzungen oder Aufhebungen dieser AGB bedürfen Schriftform. Dies gilt auch für{'\n'}
die Aufhebung des Schriftformerfordernisses.{'\n\n'}
9.4 Ausschließlicher Gerichtsstand für Streitigkeiten ist Berlin, sofern der Kunde keinen allgemeinen{'\n'}
Gerichtsstand im Inland hat, nach Vertragsschluss seinen Wohnsitz ins Ausland verlegt hat, der Wohnsitz zum{'\n'}
Zeitpunkt der Klageerhebung nicht bekannt ist oder der Kunde Kaufmann ist und in dieser Eigenschaft eine{'\n'}
Bestellung bei uns abgegeben hat.{'\n'}
                                </Text>  
                                <Text style={{textAlign:'center',fontSize: 13,fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold'}}>
                                Stand: 03.09.2021{'\n'}
                                Version 1.0
                                  </Text> 
                </View>
              </View>
            </ScrollView>
          </View>
          <Image
            source={require('../assets/images/footerImageEdited.png')}
            style={styles.imageYellow}
          />

        </LinearGradient>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  header: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '3%',
    zIndex: 1,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerTitleText: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  headerTitle: {
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  imageYellow: {
    alignSelf: 'center',
    zIndex: 0,
    bottom: 0,
    position: 'absolute',
  },
  innerContentContainer: {
    borderRadius: 20,
    backgroundColor: '#FFFFFF',
    alignSelf: 'center',
    width: '90%',
    alignItems: 'center',
  },
  textTitle: {
    color: '#D38074',
    fontSize: 20,
    textAlign: 'center',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  textStyle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    marginVertical: 10,
    textAlign: 'left'
  },
  textSubtitle: {
    color: '#000000',
    fontSize: 15,
    textAlign: 'left',
    fontWeight:"700",
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
});