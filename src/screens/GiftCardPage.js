import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
  Dimensions,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  SafeAreaView,
  TouchableNativeFeedback,
} from 'react-native';

import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';
import GiftImage from '../assets/images/giftImage';
import {TextInput} from 'react-native-gesture-handler';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class GiftCardPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
    };
  }

  componentDidMount() {}

  onBackClick = () => {
    this.props.navigation.navigate('ProfilPage');
  };

  onSubmit = () => {
    const code = this.state.code;

    //console.log('code:', code);
  };

  render() {
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    return (
      <SafeAreaView style={{flex: 1}}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={{flex: 1}}>
          <Image
            source={require('../assets/images/blueBackground.png')}
            style={styles.blueImage}
          />
          <Image
            source={require('../assets/images/footerImageEdited.png')}
            style={styles.imageYellow}
          />

          <KeyboardAvoidingView
            behavior={Platform.OS === 'android' ? '' : 'padding'}
            keyboardVerticalOffset={Platform.OS !== 'android' ? 0 : 30}
            style={styles.container}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <ScrollView contentContainerStyle={{flexGrow: 1}}>
                <View style={{flex: 1, height: height}}>
                  <TouchableOpacity
                    style={styles.arrowStyle}
                    onPress={() => this.onBackClick()}>
                    <ArrowBack />
                  </TouchableOpacity>

                  <View style={styles.contentContainer}>
                    <View>
                      <GiftImage style={{alignSelf: 'center'}} />
                    </View>
                    <View style={{padding: 10}}>
                      <Text style={styles.textTitle}>GESCHENKKARTEN</Text>
                    </View>
                    <View style={{padding: 10}}>
                      <Text style={styles.textSubtitle}>
                        Gib deinen Geschenkkarten-Code unten ein
                      </Text>
                    </View>
                    <View style={{width: '70%', alignSelf: 'center'}}>
                      <TextInput
                        style={styles.input}
                        autoCorrect={false}
                        autoCapitalize="none"
                        selectionColor="#000000"
                        value={this.state.code}
                        onChangeText={code => this.setState({code})}
                      />
                    </View>
                  </View>
                  <Text style={styles.whiteFooter} />
                  <View
                    style={[
                      styles.footer,
                      {borderRadius: 20, overflow: 'hidden', paddingBottom: 5},
                    ]}>
                    <TouchableNativeFeedback
                      useForeground={true}
                      background={background}
                      onPress={() => this.onSubmit()}>
                      <View>
                        <LinearGradient
                          start={{x: 0, y: 0}}
                          end={{x: 1, y: 0}}
                          colors={['#FFD06C', '#D78275', '#B42762']}
                          style={styles.LinearGradient}>
                          <Text style={styles.textFooter}>
                            GUTHABEN AUFLADEN
                          </Text>
                        </LinearGradient>
                      </View>
                    </TouchableNativeFeedback>
                  </View>
                </View>
              </ScrollView>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </LinearGradient>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageYellow: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    zIndex: 0,
  },
  whiteFooter: {
    backgroundColor: 'white',
    position: 'absolute',
    padding: '5%',
    width: '100%',
    bottom: 0,
    zIndex: 1,
  },
  footer: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: 30,
    width: '90%',
    zIndex: 99,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 2,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  arrowStyle: {
    position: 'absolute',
    padding: 20,
  },
  blueImage: {
    position: 'absolute',
    alignSelf: 'center',
    zIndex: 0,
  },
  contentContainer: {
    flexDirection: 'column',
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'space-between',
    paddingTop: 50,
    flex: 1,
  },
  textTitle: {
    fontSize: 20,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    color: '#FFFFFF',
    alignSelf: 'center',
  },
  textSubtitle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    textAlign: 'center',
  },
  input: {
    padding: '4%',
    borderRadius: 20,
    color: '#000000',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
    backgroundColor: '#FFFFFF',
  },
});
