import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform,
  BackHandler,
  Dimensions,
  SafeAreaView,
  TouchableNativeFeedback,
} from 'react-native';
import axios from 'axios';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';
import InstagramLogo from '../assets/images/instagramLOGO.svg';
import FacebookIconBlack from '../assets/icons/facebookIconBlack.svg';
import OderLines from '../assets/images/oderLines.svg';
import AppleIcon from '../assets/icons/appleIcon.svg';
import GoogleIcon from '../assets/icons/googleIcon.svg';
import Apple from '../assets/icons/apple.svg';
import GalleryIcon from '../assets/icons/galleryIcon';
import InstagramLogin from 'react-native-instagram-login';
import CookieManager from '@react-native-community/cookies';
import SocialTabs from '../components/SocialTabs';

import { connect } from 'react-redux';
import { settingsInstagram } from '../libs/settings/domain/SettingsActions';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

class InstagramPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: null,
      media: [],
    };
  }

  setIgToken = (data) => {
    this.props.settingsInstagram(data);
    this.setState({ token: data.access_token });

    /*
     //const response = await axios(config);
     let url =
     'https://graph.instagram.com/v12.0/' +
     data.user_id +
     '?fields=instagram_graph_user_media&access_token=' +
     data.access_token;

     //v12.0/me/media?fields=id,media_url,media_type,caption&access_token=
     url =
     'https://graph.instagram.com/v12.0/' +
     data.user_id +
     '/media?fields=id,media_url,media_type,caption&access_token=' +
     data.access_token;

     url =
     'https://graph.instagram.com/v12.0/me' +
     '?fields=id,username&access_token=' +
     data.access_token;
     */
    let url =
      'https://graph.instagram.com/v12.0/me/media' +
      '?fields=id,media_url,media_type,caption&access_token=' +
      data.access_token;

    axios
      .get(url)
      .then((response) => {
        this.props.navigation.navigate('ChoosePicturePage', {
          item: response.data,
          type: 'instagram',
        });
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  onClear() {
    CookieManager.clearAll(true).then((res) => {
      this.setState({ token: null });
    });
    this.props.settingsInstagram(null);
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.unsubscribe();
  }

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => {});
  }

  handleBackButtonClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };
  onBackClick = () => {
    this.props.navigation.navigate('ProductsPage');
  };

  renderFooterIcons() {
    if (Platform.OS === 'ios') {
      return (
        <View style={styles.footerIcons}>
          <TouchableOpacity>
            <FacebookIconBlack />
          </TouchableOpacity>
          <TouchableOpacity>
            <AppleIcon />
          </TouchableOpacity>
          <TouchableOpacity>
            <GoogleIcon />
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View style={styles.footerIcons}>
          <TouchableOpacity>
            <FacebookIconBlack />
          </TouchableOpacity>
          <TouchableOpacity>
            <GoogleIcon />
          </TouchableOpacity>
        </View>
      );
    }
  }

  onButtonClick = () => {
    this.props.navigation.navigate('InstagramSignInPage');
  };

  render() {
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    const token =
      this.props.settings && this.props.settings.results && this.props.settings.results.access_token
        ? this.props.settings.results.access_token
        : null;
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => this.onBackClick()} style={styles.backButton}>
            <ArrowBack />
          </TouchableOpacity>
          <View style={styles.headerIconAndTitle}>
            <GalleryIcon />
            <Text style={styles.headerTitle}>BILDAUSWAHL</Text>
          </View>
        </View>

        <SocialTabs type="instagram" navigation={this.props.navigation} />

        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <View style={{ flex: 1 }}>
            <View
              style={{
                alignContent: 'center',
                flex: 1,
                paddingTop: 50,
              }}
            >
              <View style={{ alignSelf: 'center' }}>
                <InstagramLogo />
              </View>
              {!token && (
                <View>
                  <Text style={styles.textStyle}>
                    Lade deine schönsten Fotos von deinem{'\n'}
                    Instagram Account hoch.{'\n'}
                    Jetzt einloggen und loslegen!
                  </Text>
                </View>
              )}

              <View>
                <View style={[styles.footer, { overflow: 'hidden', borderRadius: 20, paddingBottom: 5 }]}>
                  {!token && (
                    <TouchableNativeFeedback
                      useForeground={true}
                      background={background}
                      onPress={() => this.instagramLogin.show()}
                    >
                      <View>
                        <LinearGradient
                          start={{ x: 0, y: 0 }}
                          end={{ x: 1, y: 0 }}
                          colors={['#FFD06C', '#D78275', '#B42762']}
                          style={styles.LinearGradient}
                        >
                          <Text style={styles.textFooter}>ÜBER INSTAGRAM ANMELDEN</Text>
                        </LinearGradient>
                      </View>
                    </TouchableNativeFeedback>
                  )}
                  {token && (
                    <TouchableNativeFeedback
                      useForeground={true}
                      background={background}
                      onPress={() => {
                        let url =
                          'https://graph.instagram.com/v12.0/me/media' +
                          '?fields=id,media_url,media_type,caption&access_token=' +
                          token;

                        axios
                          .get(url)
                          .then((response) => {
                            this.props.navigation.navigate('ChoosePicturePage', {
                              item: response.data,
                              type: 'instagram',
                            });
                          })
                          .catch(function (error) {
                            // handle error
                            console.log(error);
                          });
                      }}
                    >
                      <View>
                        <LinearGradient
                          start={{ x: 0, y: 0 }}
                          end={{ x: 1, y: 0 }}
                          colors={['#FFD06C', '#D78275', '#B42762']}
                          style={styles.LinearGradient}
                        >
                          <Text style={styles.textFooter}>Load Photos</Text>
                        </LinearGradient>
                      </View>
                    </TouchableNativeFeedback>
                  )}
                </View>
                <View style={[styles.buttonCenter, { overflow: 'hidden', borderRadius: 20, paddingBottom: 5 }]}>
                  <TouchableNativeFeedback
                    useForeground={true}
                    background={background}
                    onPress={() => {
                      this.props.navigation.navigate('InstagramPageWeb');
                    }}
                  >
                    <View>
                      <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        colors={['#FFD06C', '#D78275', '#B42762']}
                        style={styles.LinearGradient}
                      >
                        <Text style={styles.textFooter}>Find more on Instagram</Text>
                      </LinearGradient>
                    </View>
                  </TouchableNativeFeedback>
                </View>
              </View>
              {token && (
                <View>
                  <Text style={styles.textStyle}>
                    Lade deine schönsten Fotos von deinem{'\n'}
                    Instagram Account hoch.{'\n'}
                  </Text>
                  <View style={[styles.footer, { overflow: 'hidden', borderRadius: 20, paddingBottom: 5 }]}>
                    <TouchableNativeFeedback
                      useForeground={true}
                      background={background}
                      onPress={() => this.onClear()}
                    >
                      <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        colors={['#FFD06C', '#D78275', '#B42762']}
                        style={styles.LinearGradient}
                      >
                        <Text style={styles.textFooter}>LOGOUT FROM INSTAGRAM</Text>
                      </LinearGradient>
                    </TouchableNativeFeedback>
                  </View>
                </View>
              )}
              <InstagramLogin
                ref={(ref) => (this.instagramLogin = ref)}
                appId="1017733855675230"
                appSecret="9583e1ad79c4308e9ac434345ca80246"
                redirectUrl="https://picamory.devla.dev/moorl/sign-in/login"
                scopes={['user_profile', 'user_media', 'instagram_graph_user_media', 'instagram_graph_user_profile']}
                onLoginSuccess={this.setIgToken}
                onLoginFailure={(data) => console.log(data)}
              />
            </View>

            <View style={{}}>
              <Image style={styles.image} source={require('../assets/images/footerImageEdited.png')} />
              <View style={styles.footerImageContent}>
                <View style={styles.oder}>
                  <OderLines />
                  <Text
                    style={{
                      fontSize: 12,
                      paddingHorizontal: 20,
                      fontFamily: 'Lato-Heavy',
                    }}
                  >
                    ODER
                  </Text>
                  <OderLines />
                </View>
                {this.renderFooterIcons()}
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  buttonCenter: {
    marginTop: 5,
    width: '90%',
    alignSelf: 'center',
  },
  headerContainer: {
    // marginTop: Platform.OS === 'ios' ? 20 : 0,
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 15,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerIconAndTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  subHeader: {
    backgroundColor: '#D38074',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '70%',
    alignSelf: 'center',
  },
  icons: {
    position: 'absolute',
    alignSelf: 'center',
  },

  textStyle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
    fontSize: 13,
    margin: 10,
  },
  footer: {
    alignSelf: 'center',
    width: '90%',
    zIndex: 1,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 5,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  footerIcons: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footerImageContent: {
    alignSelf: 'center',
    position: 'absolute',
    paddingTop: 50,
  },
  oder: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
  },
  apple: {
    alignSelf: 'center',
    position: 'absolute',
  },
  image: {
    width: width,
    alignSelf: 'center',
    zIndex: 0,
  },
});

function mapStateToProps(state) {
  return {
    settings: state.settings,
  };
}

export default connect(mapStateToProps, { settingsInstagram })(InstagramPage);
