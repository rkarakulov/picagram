import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
  FlatList,
  SafeAreaView
} from 'react-native';

import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';
import {COLOR_PRIMARY_FOTOS} from '../assets/colors/colors';
import Footer from '../components/Footer';

export default class OrdersPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  onBackClick = () => {
    this.props.navigation.navigate('ProfilPage');
  };

  renderOrders(item) {
    return (
      <SafeAreaView style={styles.contentContainer}>
        <View style={styles.box}>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              justifyContent: 'space-evenly',
              alignSelf: 'center',
              marginVertical: 20,
            }}>
            <View>
              <Image
                source={require('../assets/images/fotobuch.png')}
                style={{width: 125, height: 128}}
              />
            </View>
            <View style={{width:'45%'}}>
              <Text style={styles.textStyle}>Nr. 0000000000000000</Text>
              <Text style={styles.title}>FOTOBUCH</Text>
              <Text style={styles.textStyle}>
                Quadratisch, 14x14 cm{'\n'}
                24 Seiten
              </Text>
              <View style={{marginVertical: 5}}>
                <Text style={[styles.title, {color: COLOR_PRIMARY_FOTOS}]}>
                  XX, XX EURO
                </Text>
              </View>
              <View>
                <Text style={styles.textStyle}>Bestellt am:</Text>
                <Text style={[styles.textStyle, {color: COLOR_PRIMARY_FOTOS}]}>
                  00.00.2021{'\n'}
                </Text>
                <Text style={styles.textStyle}>Zahlungsstatus:</Text>
                <Text style={[styles.textStyle, {color: COLOR_PRIMARY_FOTOS}]}>
                  bezahlt mit Kreditkarte{'\n'}
                </Text>
                <Text style={styles.textStyle}>Versandstatus:</Text>
                <Text style={[styles.textStyle, {color: COLOR_PRIMARY_FOTOS}]}>
                  in Bearbeitung{'\n'}
                </Text>
              </View>
              <TouchableOpacity>
                <Text style={styles.textLink}>LIEFERUNG VERFOLGEN</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }

  render() {
    const Orders = [{name: 'user1'}, {name: 'user2'}, {name: 'user3'}];
    return (
      <View style={styles.container}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={{flex: 1}}>
          <View style={styles.header}>
            <TouchableOpacity
              onPress={() => this.onBackClick()}
              style={styles.backButton}>
              <ArrowBack />
            </TouchableOpacity>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                width: '100%',
                justifyContent: 'center',
              }}>
              <Text style={styles.headerTitle}>MEINE BESTELLUNGEN </Text>
            </View>
          </View>

          <FlatList
            style={{zIndex: 1}}
            keyExtractor={Orders => Orders.index}
            data={Orders}
            renderItem={({item}) => this.renderOrders(item)}
          />

          <Image
            source={require('../assets/images/footerImageEdited.png')}
            style={styles.imageYellow}
          />

          <Footer activePage="ProfilPage" navigation={this.props.navigation} />
        </LinearGradient>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  // image: {
  //   position: 'absolute',
  //   width: '100%',
  //   zIndex: 0,
  // },
  imageYellow: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    zIndex: 0,
  },
  header: {
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '3%',
    zIndex: 1,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '2%',
    backgroundColor: '#FFFFFF',
  },
  icons: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  contentContainer: {
    flexDirection: 'column',
    position: 'relative',
    alignSelf: 'center',
    width: '100%',
    zIndex: 2,
  },
  box: {
    backgroundColor: '#FFFFFF',
    width: '90%',
    borderWidth: 3,
    borderColor: '#D38074',
    borderRadius: 20,
    flexDirection: 'row',
    alignSelf: 'center',
    marginVertical: 10,
  },

  textStyle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
  },
  title: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  textLink: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    color: '#12336B',
    fontSize: 13,
    textDecorationLine: 'underline',
  },
});
