import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert,
  Platform,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Dimensions,
  ScrollView,
  SafeAreaView,
  TouchableNativeFeedback,
  TouchableNativeFeedbackBase,
} from 'react-native';
import KontaktIcon from '../assets/icons/kontaktIcon.svg';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import CloseButton from '../assets/icons/close.svg';
import {TextInput} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class HelpAndContactSendMessagePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      message: '',
    };
  }

  onBackClick = () => {
    this.props.navigation.navigate('HelpAndContactPage');
  };
  onEmailButtonClick = () => {
    const email = this.state.email;
    const message = this.state.message;
    //console.log(email, message);
    if ((email == '') | (message == '')) {
      Alert.alert('Field can not be empty');
    }
  };

  render() {
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    return (
      <SafeAreaView style={styles.container}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={styles.container}>
          <View style={styles.header}>
            <TouchableOpacity
              onPress={() => this.onBackClick()}
              style={styles.backButton}>
              <ArrowBack />
            </TouchableOpacity>
            <View style={styles.headerIconAndTitle}>
              <KontaktIcon />
              <Text style={styles.headerTitle}>HILFE UND KONTAKT</Text>
            </View>
          </View>
          <Image
            source={require('../assets/images/footerImageEdited.png')}
            style={styles.imageYellow}
          />
          <KeyboardAvoidingView
            behavior={Platform.OS !== 'android' ? '' : 'height'}
            style={{flex: 1}}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <ScrollView contentContainerStyle={{flexGrow: 1}}>
                <View
                  style={{
                    justifyContent: 'flex-start',
                    flex: 1,
                    paddingTop: 20,
                  }}>
                  <View style={styles.contentContainer}>
                    <View style={styles.innerContentContainer}>
                      <Text style={styles.title}>
                        WIE KÖNNEN WIR DIR HELFEN?
                      </Text>

                      <View style={styles.emailContainer}>
                        <TextInput
                          style={styles.textInputEmail}
                          placeholder="maxmustermann@gmail.de"
                          placeholderTextColor="#707070"
                          autoCorrect={false}
                          autoCapitalize="none"
                          selectionColor="#000000"
                          value={this.state.email}
                          onChangeText={email => this.setState({email})}
                          returnKeyType="next"
                          blurOnSubmit={false}
                          onSubmitEditing={() =>
                            this.DeineNachrichtInput.focus()
                          }
                        />
                        <TouchableOpacity
                          style={{padding: 10}}
                          onPress={() => this.setState({email: ''})}>
                          <CloseButton />
                        </TouchableOpacity>
                      </View>

                      <View style={styles.messageContainer}>
                        <TextInput
                          style={styles.textInput}
                          placeholder="Deine Nachricht"
                          placeholderTextColor="#707070"
                          autoCorrect={false}
                          autoCapitalize="none"
                          selectionColor="#000000"
                          // multiline={true}
                          value={this.state.message}
                          onChangeText={message => this.setState({message})}
                          ref={ref => {
                            this.DeineNachrichtInput = ref;
                          }}
                        />
                      </View>
                    </View>
                  </View>
                </View>

                <View style={{}}>
                  <View style={styles.feedbackContainer}>
                    <Text style={styles.feedbackText}>
                      Hast du Fragen oder sonstiges Feedback?{'\n'}
                      Wir freuen uns von dir zu hören!
                    </Text>
                  </View>
                  <Text style={styles.whiteFooter} />

                  <View
                    style={[
                      styles.footer,
                      {borderRadius: 20, overflow: 'hidden', paddingBottom: 5},
                    ]}>
                    <TouchableNativeFeedback
                      useForeground={true}
                      background={background}
                      onPress={() => this.onEmailButtonClick()}>
                      <View>
                        <LinearGradient
                          start={{x: 0, y: 0}}
                          end={{x: 1, y: 0}}
                          colors={['#FFD06C', '#D78275', '#B42762']}
                          style={styles.LinearGradient}>
                          <Text style={styles.textFooter}>E-MAIL SENDEN</Text>
                        </LinearGradient>
                      </View>
                    </TouchableNativeFeedback>
                  </View>
                </View>

                {/* <View style={{backgroundColor:'green'}}>
              
                  <View style={styles.feedbackContainer}>
                    <Text style={styles.feedbackText}>
                      Hast du Fragen oder sonstiges Feedback?{'\n'}
                      Wir freuen uns von dir zu hören!
                    </Text>
                  </View>
                    <Image
                      source={require('../assets/images/footerImageEdited.png')}
                      style={styles.imageYellow}
                    />
                       <View
                    style={{
                      position: 'relative',
                      bottom: 0,
                      width: '100%',
                      height: 90,
                    }}>
                    <TouchableOpacity
                      style={styles.footer}
                      onPress={() => this.onEmailButtonClick()}>
                      <LinearGradient
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 0}}
                        colors={['#FFD06C', '#D78275', '#B42762']}
                        style={styles.LinearGradient}>
                        <Text style={styles.textFooter}>E-MAIL SENDEN</Text>
                      </LinearGradient>
                    </TouchableOpacity>
                    <Text style={styles.whiteFooter} />
                  </View>
               </View>  */}
              </ScrollView>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </LinearGradient>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '3%',
    zIndex: 1,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  headerIconAndTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  imageYellow: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    zIndex: 0,
  },
  contentContainer: {
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 20,
  },
  innerContentContainer: {
    width: '90%',
    alignSelf: 'center',
    paddingVertical: 10,
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  emailContainer: {
    borderWidth: 1,
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginVertical: 20,
  },
  messageContainer: {
    borderWidth: 1,
    borderRadius: 20,
    paddingHorizontal: 20,
    minHeight: 250,
  },
  textInputEmail: {
    color: 'black',
    width: 250,
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  textInput: {
    color: 'black',
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },

  feedbackContainer: {
    alignSelf: 'center',
    position: 'relative',
    width: '100%',
    zIndex: 0,
    paddingBottom: 50,
  },
  feedbackText: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
    textAlign: 'center',
  },
  whiteFooter: {
    width: '100%',
    backgroundColor: 'white',
    padding: '5%',
    position: 'absolute',
    bottom: 0,
    zIndex: 1,
  },
  footer: {
    alignSelf: 'center',
    width: '90%',
    zIndex: 3,
    bottom: 30,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 2,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
});
