import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform,
  BackHandler,
  FlatList,
  SafeAreaView,
} from 'react-native';
import { WebView } from 'react-native-webview';
import { captureScreen, captureRef } from 'react-native-view-shot';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import BackgroundColor from '../assets/icons/backgroundColor.svg';
import Smartphone from '../assets/icons/smartphone-call.svg';
import SmartphoneIcon from '../assets/icons/smartphoneIcon.svg';
import InstagramIcon from '../assets/icons/instagramIcon.svg';
import LinearGradient from 'react-native-linear-gradient';
import FacebookLogo from '../assets/images/facebookLOGO.svg';
import InstagramIconBlack from '../assets/icons/instagramIconBlack.svg';
import OderLines from '../assets/images/oderLines.svg';
import AppleIcon from '../assets/icons/appleIcon.svg';
import Apple from '../assets/icons/apple.svg';
import GalleryIcon from '../assets/icons/galleryIcon';
import { LoginManager, Profile, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk-next';
import SocialTabs from '../components/SocialTabs';
import ImagePicker from 'react-native-image-crop-picker';

import { connect } from 'react-redux';
import Icon from '../assets/icons/icon.svg';

import { updateCartPhotos } from '../libs/card/domain/CartActions';
import { getPhotoIndexByPath, getPhotoCount, getPhotoCountSelection } from '../helpers/PhotoFunctions';

class FacebookPageWeb extends Component {
  constructor(props) {
    super(props);

    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
    const selected =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];

    this.state = {
      isLoggedIn: false,
      user: null,
      authenticated: false,
      data: selected,
      selected: selected,
      showWebView: true,
      showIcons: false,
      pictureSelectLimit: 24,
      source: 'facebook_grab',
      selectedDimensions: {
        width: 1080,
        height: 1350,
      },
      selectedType: 'feed',
      showFrame: false,
    };

    this.viewRef2 = null;
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.unsubscribe();
  }

  nextPage = (type) => {
    this.setState(
      {
        modal: false,
      },
      () => {
        this.props.navigation.navigate('CreatePhotoPage', {
          selected: this.state.selected,
          type: type,
        });
      },
    );
  };

  selectAllPhotos = () => {
    let data = [...this.state.data];
    let selected = [...this.state.selected];
    let numSelected = getPhotoCountSelection(selected, 'facebook_grab');
    const value = value;
    data.map((item, key) => {
      if (item.source === 'facebook_grab' && numSelected < this.state.pictureSelectLimit) {
        var index = getPhotoIndexByPath(selected, item.path, 'facebook_grab');

        if (index !== -1) {
          selected[index].selected = true;
          numSelected++;
        } else {
          selected.push({
            path: item.path,
            source: 'facebook_grab',
            selected: true,
            qty: 1,
          });
          numSelected++;
        }
      }
    });
    this.setState({ selected: selected });
    this.updateCartPropsPhotos(selected);
  };

  DeselectAllPhotos = () => {
    let selected = [...this.state.selected];
    selected.map((item, key) => {
      var index = getPhotoIndexByPath(selected, item.path);
      if (index !== -1) {
        selected[index].selected = false;
      }
    });
    this.setState({ selected: selected });
    this.updateCartPropsPhotos(selected);
  };

  updateCartPropsPhotos = (selected) => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    if (!cart[cartIndex].selected) cart[cartIndex].selected = [];
    cart[cartIndex].selected = selected;
    this.props.updateCartPhotos(cart);
  };

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      //console.log('fokus');
      //this.setState({
      //showWebView: true,
      //});

      const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
      const selected =
        this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
          ? this.props.cart.cart[selectedIndex].selected
          : [];

      this.setState({
        selected: selected,
      });
    });
  }

  handleBackButtonClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  onBackClick = () => {
    this.props.navigation.navigate('FacebookPage');
  };

  render() {
    const pictureSelectLimit = this.state.pictureSelectLimit;
    const count = getPhotoCountSelection(this.state.selected);
    // console.log('count facebook web', count);
    const countActive = getPhotoCount(this.state.selected, 'facebook_grab');

    const selectedPhotos =
      this.state.selected && this.state.selected.length > 0
        ? this.state.selected.filter(function (item) {
            return item.source === 'facebook_grab';
          })
        : [];
    return (
      <SafeAreaView style={styles.container}>
        <View
          style={{
            display: this.state.showWebView ? 'flex' : 'none',
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: this.state.showWebView ? '100%' : 0,
            zIndex: 99996,
          }}
        >
          <WebView
            ref={(ref) => (this.viewRef2 = ref)}
            //source={{uri: 'https://www.instagram.com/'}}
            source={{ uri: 'https://www.facebook.com/' }}
            style={{
              height: 100,
            }}
          />
        </View>

        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => this.onBackClick()}>
            <View style={{ padding: 10 }}>
              <ArrowBack />
            </View>
          </TouchableOpacity>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <GalleryIcon />
            <Text style={styles.headerTitle}>
              {count}/{pictureSelectLimit}
            </Text>
          </View>
          <TouchableOpacity onPress={() => this.nextPage('data')}>
            <Text style={styles.speichern}>BILDAUSWAHL</Text>
          </TouchableOpacity>
        </View>

        <SocialTabs type="facebook" navigation={this.props.navigation} />

        {!this.state.showWebView && (
          <View style={styles.containerTitlesAlbum}>
            {count < pictureSelectLimit && (
              <TouchableOpacity
                onPress={() => {
                  this.selectAllPhotos();
                }}
              >
                <Text style={styles.textAlle}>{'ALLE AUSWÄHLEN'}</Text>
              </TouchableOpacity>
            )}
            {this.state.selected && count > 0 && (
              <TouchableOpacity
                onPress={() => {
                  this.DeselectAllPhotos();
                }}
              >
                <Text style={styles.textAlle}>{'ABWÄHLEN'}</Text>
              </TouchableOpacity>
            )}
          </View>
        )}
        {!this.state.showWebView && (
          <FlatList
            style={{
              alignSelf: 'center',
              position: this.state.showWebView ? 'absolute' : 'relative',
              bottom: this.state.showWebView ? 0 : 'auto',
              height: this.state.showWebView ? 60 : 'auto',
              zIndex: this.state.showWebView ? 99996 : 99996,
              width: '100%',
            }}
            keyExtractor={(album) => {
              //  console.log(album);
              return album && album.id ? album.id : 'album0';
            }}
            data={selectedPhotos}
            numColumns={2}
            contentContainerStyle={{
              paddingBottom: 30,
              justifyContent: 'center',
              flexDirection: 'row',
              flexWrap: 'wrap',
            }}
            renderItem={({ item }) => {
              // console.log(item);
              // console.log('end item');
              if (item && item) {
                let img = item.path;
                //let count = 0;

                let blnSelected = false;

                var index = getPhotoIndexByPath([...this.state.selected], item.path, 'facebook_grab');
                //var index = this.state.selected.indexOf(item.node.image.uri);
                if (index !== -1) {
                  if (this.state.selected[index].selected) blnSelected = true;
                }
                //  console.log(img, count);
                return (
                  <TouchableOpacity
                    style={{ margin: 10 }}
                    onPress={
                      () => {
                        let selected = [...this.state.selected];

                        var index = getPhotoIndexByPath(selected, item.path, 'facebook_grab');
                        if (index !== -1) {
                          // console.log('ima u nizu');

                          if (count >= pictureSelectLimit) {
                            //  console.log('count', count, pictureSelectLimit);
                            if (!selected[index].selected) {
                              alert('Max number of photos ' + pictureSelectLimit + ' is selected!');
                            } else {
                              selected[index].selected = false;
                            }
                          } else {
                            selected[index].selected = selected[index].selected ? false : true;
                          }

                          //selected.splice(index, 1);
                        } else {
                          if (count >= pictureSelectLimit) {
                            alert('Max number of photos ' + pictureSelectLimit + ' is selected!');
                          } else {
                            selected.push({
                              path: item.path,
                              source: 'facebook_grab',
                              selected: true,
                              qty: 1,
                            });
                          }
                        }
                        this.setState({ selected: selected });
                        this.updateCartPropsPhotos(selected);
                      }

                      /*
                       this.props.navigation.navigate('EditPhotoPage', {
                       selected: this.state.selected,
                       item: item.path,
                       })
                       */
                    }
                  >
                    <View
                      style={{
                        position: 'relative',
                        justifyContent: 'center',
                      }}
                    >
                      {blnSelected && (
                        <View
                          style={{
                            position: 'absolute',
                            zIndex: 9998,
                            width: '100%',
                            height: '100%',
                            margin: 5,
                            left: 0,
                            top: 0,
                            backgroundColor: 'rgba(255,255,255,0.4)',
                          }}
                        ></View>
                      )}
                      {blnSelected && (
                        <Icon
                          style={{
                            position: 'absolute',
                            zIndex: 9999,
                            alignSelf: 'flex-end',
                            bottom: 0,
                          }}
                        />
                      )}
                      <Image
                        style={styles.albumImage}
                        source={{
                          uri: img ? img : '',
                        }}
                      />
                    </View>
                  </TouchableOpacity>
                );
              }
            }}
          />
        )}

        {this.state.showWebView && this.state.showIcons && (
          <TouchableOpacity
            style={styles.buttonFeed}
            onPress={() => {
              this.setState(
                {
                  selectedDimensions: {
                    width: 1080,
                    height: 1350,
                  },
                  selectedType: 'feed',
                },
                () => {
                  captureRef(this.viewRef2, {
                    // Either png or jpg (or webm Android Only), Defaults: png
                    format: 'jpg',
                    // Quality 0.0 - 1.0 (only available for jpg)
                    quality: 0.8,
                  }).then(
                    //callback function to get the result URL of the screnshot
                    (uri) => {
                      //console.log(uri);
                      //const photos = [...this.state.photos];
                      //photos.push(uri);
                      //console.log(photos);
                      //this.setState({photos: photos});

                      ImagePicker.openCropper({
                        path: uri,
                        width: this.state.selectedDimensions.width,
                        height: this.state.selectedDimensions.height,
                        freeStyleCropEnabled: true,
                      })
                        .then((image) => {
                          //  console.log(image);
                          const photos = [...this.state.selected];
                          photos.push({
                            path: image.path,
                            source: 'facebook_grab',
                            selected: false,
                            qty: 1,
                          });
                          //   console.log(photos);
                          this.setState({ selected: photos, showIcons: false });
                          this.updateCartPropsPhotos(photos);
                        })
                        .catch((error) => {
                          //  console.log('error');
                        });
                    },
                    (error) => console.error('Oops, Something Went Wrong', error),
                  );
                },
              );
            }}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={['#FFD06C', '#D78275', '#B42762']}
              style={styles.ButtonGradient}
            >
              <Text style={styles.buttonText}>Feed</Text>
            </LinearGradient>
          </TouchableOpacity>
        )}
        {this.state.showWebView && this.state.showIcons && (
          <TouchableOpacity
            style={styles.buttonStory}
            onPress={() => {
              this.setState(
                {
                  selectedDimensions: {
                    width: 1080,
                    height: 1620,
                  },
                  selectedType: 'story',
                },
                () => {
                  captureRef(this.viewRef2, {
                    // Either png or jpg (or webm Android Only), Defaults: png
                    format: 'jpg',
                    // Quality 0.0 - 1.0 (only available for jpg)
                    quality: 0.8,
                  }).then(
                    //callback function to get the result URL of the screnshot
                    (uri) => {
                      //console.log(uri);
                      //const photos = [...this.state.photos];
                      //photos.push(uri);
                      //console.log(photos);
                      //this.setState({photos: photos});

                      ImagePicker.openCropper({
                        path: uri,
                        width: this.state.selectedDimensions.width,
                        height: this.state.selectedDimensions.height,
                        freeStyleCropEnabled: true,
                      })
                        .then((image) => {
                          //  console.log(image);
                          const photos = [...this.state.selected];
                          photos.push({
                            path: image.path,
                            source: 'facebook_grab',
                            selected: false,
                            qty: 1,
                          });
                          //   console.log(photos);
                          this.setState({ selected: photos, showIcons: false });
                          this.updateCartPropsPhotos(photos);
                        })
                        .catch((error) => {
                          //  console.log('error');
                        });
                    },
                    (error) => console.error('Oops, Something Went Wrong', error),
                  );
                },
              );
            }}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={['#FFD06C', '#D78275', '#B42762']}
              style={styles.ButtonGradient}
            >
              <Text style={styles.buttonText}>Story</Text>
            </LinearGradient>
          </TouchableOpacity>
        )}

        {this.state.showWebView && (
          <TouchableOpacity
            style={styles.buttonCenter}
            onPress={() => {
              this.setState({
                showIcons: !this.state.showIcons,
              });
            }}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={['#FFD06C', '#D78275', '#B42762']}
              style={styles.ButtonGradient}
            >
              <InstagramIcon />
            </LinearGradient>
          </TouchableOpacity>
        )}
        <TouchableOpacity
          style={styles.buttonFixedSwitch}
          onPress={() => {
            let blnShow = this.state.showWebView;
            this.setState({
              showWebView: blnShow ? false : true,
            });
          }}
        >
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={['#FFD06C', '#D78275', '#B42762']}
            style={styles.LinearGradient}
          >
            <Text style={styles.textFooter}>
              {this.state.showWebView ? 'Show photos (' + countActive + ')' : 'Go back'}
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    marginTop: Platform.OS === 'ios' ? 20 : 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
    marginHorizontal: 10,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerIconAndTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  subHeader: {
    backgroundColor: '#D38074',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '70%',
    alignSelf: 'center',
  },
  icons: {
    position: 'absolute',
    alignSelf: 'center',
  },
  textStyle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
    fontSize: 13,
  },

  buttonCenter: {
    position: 'absolute',
    zIndex: 99999,
    left: 30,
    bottom: 80,
    width: 80,
    height: 80,
  },
  buttonText: {
    color: '#ffffff',
    fontSize: 11,
  },
  buttonStory: {
    position: 'absolute',
    zIndex: 99999,
    left: 30,
    bottom: 120,
    width: 80,
    height: 80,
  },
  layoutDimension: {
    position: 'absolute',
    zIndex: 99999,
    left: 30,
    backgroundColor: 'rgba(255,255,255,0.9)',
    bottom: 250,
    padding: 5,
  },
  textDimension: {
    color: '#333',
    fontSize: 10,
  },
  buttonFeed: {
    position: 'absolute',
    zIndex: 99999,
    left: 30,
    bottom: 160,
    width: 80,
    height: 80,
  },
  buttonFixedSwitch: {
    position: 'absolute',
    zIndex: 99999,
    right: 10,
    bottom: 90,
    width: 250,
    height: 60,
  },
  footer: {
    alignSelf: 'center',
    width: '100%',
    zIndex: 1,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '90%',
    alignSelf: 'center',
  },
  ButtonGradient: {
    borderRadius: 20,
    padding: 5,
    alignSelf: 'center',
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  footerIcons: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footerImageContent: {
    alignSelf: 'center',
    position: 'absolute',
    top: 50,
    // alignSelf: 'center',
    // position: 'relative',
    // bottom: '15%',
    // width: '100%',
  },
  oder: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    width: '90%',
    justifyContent: 'space-between',
  },
  apple: {
    alignSelf: 'center',
    position: 'absolute',
  },
  image: {
    width: '100%',
    zIndex: 0,
  },

  albumImage: {
    zIndex: 9,
    width: 114,
    height: 114,
    margin: 5,
  },
  imageDetail: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
    width: 150,
  },
  containerTitlesAlbum: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 20,
    marginHorizontal: 25,
  },
  textMeine: {
    color: '#D38074',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  textAlle: {
    color: '#12336B',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
    alignSelf: 'flex-end',
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
  };
}

export default connect(mapStateToProps, { updateCartPhotos })(FacebookPageWeb);
