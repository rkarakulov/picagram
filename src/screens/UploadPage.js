import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  BackHandler,
  Platform,
  TouchableNativeFeedback,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import UploadedImage from '../assets/images/uploadedImage.svg';
import Icon from '../assets/icons/icon.svg';
import Line from '../assets/icons/uploadLine.svg';

import { connect } from 'react-redux';

import { updateCartPhotos, updateCartOptions } from '../libs/card/domain/CartActions';
import { getPhotoIndexByPath, getPhotoCount, getPhotoCountSelection } from '../helpers/PhotoFunctions';

class UploadPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 1,
      selected: [],
      selectedOptions: [],
      type: null,
      font: null,
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      this.loadPropsData();
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    });
    this.loadPropsData();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.unsubscribe();
  }

  onBackClick = () => {
    this.props.navigation.navigate('AlbumPage');
  };
  onButtonClick = () => {
    this.props.navigation.navigate('SummaryPage');
  };

  handleBackButtonClick = () => {
    if (this.props.login.isAuthenticated) {
      this.props.navigation.navigate('WarenkorbPage');
    } else {
      const { goBack } = this.props.navigation;
      goBack();
    }
    return true;
  };

  loadPropsData = () => {
    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
    const selected =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];

    const font =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].font
        ? this.props.cart.cart[selectedIndex].font
        : 'CourierPrime-Regular';

    //const selected = this.props.route.params.selected;
    //const type = this.props.route.params.selected;
    this.setState({
      selected: selected,
      selectedOptions: [],
      font: font,
    });
  };

  render() {
    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
    const cart = this.props.cart.cart[selectedIndex] ? this.props.cart.cart[selectedIndex] : null;

    const background = TouchableNativeFeedback.Ripple('#fff', true);

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <View style={styles.header}>
            <Text style={styles.titleHeader}>HOCHLADEN</Text>
            <TouchableOpacity>
              <Text style={styles.abbrechen}>ABBRECHEN</Text>
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView>
          <View style={styles.contentContainer}>
            {this.state.selected.map((item, key) => {
              return (
                <View style={styles.innerContentContainer}>
                  <Image style={{ width: 120, height: 120 }} source={{ uri: item.path }} />
                  <View style={{ flexDirection: 'column', width: '65%' }}>
                    <Text style={styles.containerTitle}>
                      {cart && cart.albumItem ? cart.albumItem.productName : ''}
                    </Text>
                    <Text style={styles.containerSubtitle}>Foto hochladen{'\n'}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Line />
                      <Icon style={{ marginHorizontal: 10 }} />
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        </ScrollView>
        <View style={[styles.Button, { borderRadius: 20, overflow: 'hidden', paddingBottom: 5 }]}>
          <TouchableNativeFeedback useForeground={true} background={background} onPress={() => this.onButtonClick()}>
            <View>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={['#FFD06C', '#D78275', '#B42762']}
                style={styles.LinearGradient}
              >
                <Text style={styles.textButton}>100%</Text>
              </LinearGradient>
            </View>
          </TouchableNativeFeedback>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  headerContainer: {
    // marginTop: Platform.OS === 'ios' ? 20 : 0,
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '5%',
    backgroundColor: '#D38074',
    justifyContent: 'flex-end',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '70%',
    alignSelf: 'center',
    justifyContent: 'space-between',
  },
  titleHeader: {
    color: '#FFFFFF',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  Button: {
    alignSelf: 'center',
    width: '90%',
    bottom: 20,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    // marginTop: '7%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 2,
  },
  textButton: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  abbrechen: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textDecorationLine: 'underline',
    color: 'white',
    fontSize: 13,
  },
  containerTitle: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  containerSubtitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
  },
  contentContainer: {
    width: '90%',
    marginVertical: 20,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  innerContentContainer: {
    marginBottom: 10,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
    login: state.login,
  };
}

export default connect(mapStateToProps, { updateCartPhotos, updateCartOptions })(UploadPage);
