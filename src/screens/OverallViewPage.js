import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  Image,
  Dimensions,
  ScrollView,
  SafeAreaView,
  Platform,
  TouchableNativeFeedback,
} from 'react-native';
import FotobuchIcon from '../assets/icons/fotobuchIcon';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import CloseButton from '../assets/icons/modalClose.svg';
import { DraggableGrid } from 'react-native-draggable-grid';

import { connect } from 'react-redux';

import { updateCartPhotos, updateCartOptions } from '../libs/card/domain/CartActions';
import { getPhotoIndexByPath, getPhotoCount, getPhotoCountSelection } from '../helpers/PhotoFunctions';

class OverallViewPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      albumData: [],
      pages: [],
      scrollEnabled: true,
      type: 'manual',
      modal: true,
      modalSpeichern: false,
    };
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.unsubscribe();
  }

  handleBackButtonClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      this.loadPropsData();
    });
    this.loadPropsData();
  }

  loadPropsData = () => {
    const type = this.props.route.params.selected;

    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
    const selected =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];

    const pages =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].pages
        ? this.props.cart.cart[selectedIndex].pages
        : [];

    let arySelected = [];
    selected.map((item, key) => {
      arySelected.push({
        ...item,
        key: item.key ? item.key : 'image' + key,
        image: item.path ? item.path : item.path,
        name: item.name ? item.name : key + '',
        itemPosition: item.itemPosition ? item.itemPosition : key,
      });
    });
    this.setState({
      albumData: arySelected,
      type: type,
      pages: pages,
    });
    //  console.log('aryselected');
    //  console.log(arySelected);
  };

  onBackClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };
  onButtonClick = () => {
    this.props.navigation.navigate('WarenkorbPage');
  };
  onErstellenClick = () => {
    this.props.navigation.navigate('CreatePhotoPageV2', {
      // selected: this.state.selected,
      // item: item,
    });
  };

  updateCart = (data) => {
    //console.log('data', data);
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    if (!cart[cartIndex].selected) cart[cartIndex].selected = [];
    cart[cartIndex].selected = data;
    this.props.updateCartPhotos(cart);
  };

  renderItem = (item, key) => {
    // console.log('item', item);
    //  console.log('item', key);

    let pages = [...this.state.pages];

    let itemWidth = 80;
    let itemHeight = 80;

    let pageIndex = pages.findIndex((obj) => obj.index === key);
    if (pageIndex != -1) {
      itemWidth = pages[pageIndex].width - 15;
      itemHeight = pages[pageIndex].height - 15;
    }

    return (
      <View
        key={item.key}
        style={{
          marginLeft: key % 2 === 0 ? 10 : 0,
        }}
      >
        <View
          style={{
            flexDirection: 'row',
            marginBottom: 20,
          }}
        >
          <View style={{ flexDirection: 'column' }}>
            <View style={[styles.imageBox, { alignSelf: 'center' }]}>
              <Image style={{ width: itemWidth, height: itemHeight }} source={{ uri: item.image }} />
            </View>
            <Text style={styles.number}>{key}</Text>
          </View>
        </View>
      </View>
    );
  };

  render() {
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    const backgroundButtonsSubheader = TouchableNativeFeedback.Ripple('#D38074', true);
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => this.onBackClick()}>
            <View style={{ padding: 10 }}>
              <ArrowBack />
            </View>
          </TouchableOpacity>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <FotobuchIcon />
            <Text style={styles.titleHeader}>FOTOBUCH</Text>
          </View>
          <TouchableOpacity onPress={() => this.setState({ modalSpeichern: true })}>
            <Text style={styles.speichern}>SPEICHERN</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.Buttons}>
          <View style={[styles.innerButtons, { backgroundColor: '#FFFFFF', overflow: 'hidden' }]}>
            <TouchableNativeFeedback
              useForeground={true}
              background={backgroundButtonsSubheader}
              onPress={() => this.onErstellenClick()}
            >
              <View style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                <Text style={[styles.buttonText, { color: '#D38074' }]}>ERSTELLEN</Text>
              </View>
            </TouchableNativeFeedback>
          </View>

          <View style={[styles.innerButtons, { backgroundColor: '#D38074' }]}>
            <Text style={[styles.buttonText, { color: '#FFFFFF' }]}>GESAMTANSICHT</Text>
          </View>
        </View>
        <Modal
          onRequestClose={() => this.setState({ modal: false })}
          transparent={true}
          isVisible={this.state.modal}
          style={{
            flex: 1,
            justifyContent: 'flex-end',
            alignItems: 'center',
            // bottom: -200,
          }}
        >
          <View style={[styles.modalContent, { justifyContent: 'flex-end' }]}>
            <View style={styles.innerModalContent}>
              <TouchableOpacity onPress={() => this.setState({ modal: false })} style={styles.closeButton}>
                <CloseButton />
              </TouchableOpacity>
              <Text style={styles.modalTitle}>
                Drücke länger auf die Seite,{'\n'}
                um sie verschieben zu können.
              </Text>

              <View style={[styles.modalButton, { borderRadius: 20, overflow: 'hidden' }]}>
                <TouchableNativeFeedback
                  useForeground={true}
                  background={background}
                  onPress={() => this.setState({ modal: false })}
                >
                  <View style={{ padding: 15, width: '100%' }}>
                    <Text style={styles.modalButtonText}>OK</Text>
                  </View>
                </TouchableNativeFeedback>
              </View>
            </View>
          </View>
        </Modal>
        <Modal
          onRequestClose={() => this.setState({ modalSpeichern: false })}
          transparent={true}
          isVisible={this.state.modalSpeichern}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <View style={styles.modalContent}>
            <View style={styles.innerModalContent}>
              <TouchableOpacity
                onPress={() => this.setState({ modalSpeichern: false })}
                style={styles.closeButtonSpeichern}
              >
                <CloseButton />
              </TouchableOpacity>
              <Text style={styles.modalTitleSpeichern}>SPEICHERN?</Text>
              <Text style={styles.modalSubTitle}>
                Bist du bereit, deinen Entwurf zu sichern,{'\n'}um ihn später zu beenden?
              </Text>

              <View style={[styles.modalButton, { borderRadius: 20, overflow: 'hidden' }]}>
                <TouchableNativeFeedback
                  useForeground={true}
                  background={background}
                  onPress={() => this.setState({ modalSpeichern: false })}
                >
                  <View style={{ padding: 15, width: '100%' }}>
                    <Text style={styles.modalButtonText}>NACH DATUM</Text>
                  </View>
                </TouchableNativeFeedback>
              </View>

              <TouchableOpacity>
                <Text style={styles.Löschen}>Löschen</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <ScrollView
          scrollEnabled={this.state.scrollEnabled}
          style={{
            minHeight: Dimensions.get('window').height * 0.55,
          }}
        >
          <View style={styles.wrapper}>
            <DraggableGrid
              numColumns={4}
              itemHeight={Dimensions.get('window').height * 0.16}
              renderItem={this.renderItem}
              data={this.state.albumData}
              onDragRelease={(data) => {
                this.setState({ albumData: data, scrollEnabled: true }); // need reset the props data sort after drag
                // release
                this.updateCart(data);
              }}
              onDragStart={(data) => {
                this.setState({ scrollEnabled: false }); // need reset the props data sort after drag release
              }}
            />
          </View>
        </ScrollView>
        <View style={[styles.Button, { borderRadius: 20, overflow: 'hidden', paddingBottom: 5 }]}>
          <TouchableNativeFeedback useForeground={true} background={background} onPress={() => this.onButtonClick()}>
            <View>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={['#FFD06C', '#D78275', '#B42762']}
                style={styles.LinearGradient}
              >
                <Text style={styles.textButton}>ZUM WARENKORB{'\n'}HINZUFÜGEN</Text>
              </LinearGradient>
            </View>
          </TouchableNativeFeedback>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  wrapper: {
    paddingVertical: 10,
    width: '90%',
    height: '100%',
    alignSelf: 'center',
  },
  headerContainer: {
    // marginTop: Platform.OS === 'ios' ? 20 : 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
    marginHorizontal: 10,
    backgroundColor: '#FFFFFF',
  },
  titleHeader: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  speichern: {
    color: '#12336B',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textDecorationLine: 'underline',
    fontSize: 13,
  },
  Buttons: {
    borderWidth: 1,
    borderColor: '#D38074',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    alignSelf: 'center',
  },
  innerButtons: {
    width: '50%',
    height: 40,
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
  },
  imageBox: {
    borderWidth: 1,
    borderColor: '#D38074',
    height: 90,
    width: 90,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 0,
  },
  // innerImageBox: {
  //   borderWidth: 1,
  //   borderColor: '#D38074',
  //   height: 70,
  //   width: 70,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   borderStyle: 'dashed',
  //   borderRadius: 1,
  // },
  Button: {
    alignSelf: 'center',
    width: '90%',
    bottom: 0,
    bottom: 20,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 4,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 2,
  },
  textButton: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  number: {
    alignSelf: 'center',
    fontSize: 12,
    fontFamily: 'Lato-Regular',
  },
  contentContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginHorizontal: 20,
  },
  modalContent: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerModalContent: {
    backgroundColor: '#D38074',
    width: '90%',
    // height: '30%',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalSubTitle: {
    fontSize: 12,
    textAlign: 'center',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    paddingVertical: 10,
  },
  modalTitle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
  },
  modalButton: {
    borderWidth: 1,
    width: 193,
    alignItems: 'center',
    borderColor: '#FFFFFF',
    borderRadius: 20,
    marginVertical: 10,
  },
  modalButtonText: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  closeButton: {
    position: 'relative',
    alignSelf: 'flex-end',
    padding: 20,
    // bottom: '15%',
  },
  Löschen: {
    textDecorationLine: 'underline',
    color: '#FFFFFF',
    marginBottom: 20,
    fontSize: 12,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  modalTitleSpeichern: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  closeButtonSpeichern: {
    position: 'relative',
    alignSelf: 'flex-end',
    padding: 20,
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
  };
}

export default connect(mapStateToProps, { updateCartPhotos, updateCartOptions })(OverallViewPage);
