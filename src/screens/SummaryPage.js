import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Platform,
  Dimensions,
  SafeAreaView,
  Alert,
  Image,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  TouchableNativeFeedback,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import BackIcon from '../assets/icons/ArrowBack.svg';
import EditIcon from '../assets/icons/iconForEdit';
import RemoveIcon from '../assets/icons/removeIcon';
import ImageSold from '../assets/images/imageSold';
import Modal from 'react-native-modal';
import CloseButton from '../assets/icons/modalClose.svg';
import { COLOR_PRIMARY_FOTOS, COLOR_PRIMARY_FOTOBUCH } from '../assets/colors/colors';
import { TextInput } from 'react-native-gesture-handler';
import { connect } from 'react-redux';

import { updateCart, updateCartSelected, updateCartPhotos, updateCartOptions } from '../libs/card/domain/CartActions';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

class SummaryPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      selectedItem: null,
      cart: [],
      nachname: 'Mustermann',
      name: 'Max ',
      land: '00000 Stadt, Deutschland',
      adresse: 'Maxstraße 120',
      modalData: null,
      data: [
        {
          name: '',
          nachname: '',
          adresse: '',
          land: '',
        },
      ],
    };
  }

  componentDidMount() {}

  onBackClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  onEditClick(item) {
    if (item.type === 'gift') {
      this.props.navigation.navigate('GeschenkkartePage');
    } else {
      this.props.navigation.navigate('AlbumPage');
    }
    return true;
  }

  openPage = () => {
    this.props.navigation.navigate('CheckOutPage');
  };

  onSubmit = () => {
    const nachname = this.state.nachname;
    const name = this.state.name;
    const land = this.state.land;
    const adresse = this.state.adresse;
    let obj = {
      nachname: nachname,
      name: name,
      land: land,
      adresse: adresse,
    };
    const data = [...this.state.data];

    //   this.setState({data: data, modal: true, nachname: '',
    //   name: '',
    //   land: '',
    //   adresse: '',
    //   modalData: null
    // });

    if ((nachname == '') | (name == '') | (land == '') | (adresse == '')) {
      Alert.alert('Field can not be empty');
    } else {
      data.push(obj);
      this.setState({ modal: false });
    }
  };

  addNewAddress = () => {
    this.setState({
      nachname: '',
      name: '',
      land: '',
      adresse: '',
      modalData: null,
      modal: true,
    });
  };

  passDataToModal = () => {
    this.setState({
      modal: true,
      name: this.state.name,
      nachname: this.state.nachname,
      land: this.state.land,
      adresse: this.state.adresse,
    });
  };
  removeItem = () => {
    if (this.state.selectedItem >= 0) {
      let carts = [...this.props.cart.cart];
      carts.splice(this.state.selectedItem, 1);
      this.updateCart(carts);
    }
    this.setState({
      selectedItem: null,
    });
  };
  updateCart = (data) => {
    this.props.updateCartPhotos(data);
  };
  openCartItem = (item, index) => {
    const cart = this.props.cart.cart;
    let selectedItem = null;
    let selectedIndex = null;

    //const searchName = album.title + ' ' + item.productName;

    //const index = cart.findIndex(obj => obj.albumName === searchName);
    if (index != -1) {
      selectedItem = cart[index];
      selectedIndex = index;
    }

    if (cart.length > 0 && selectedIndex === null) {
      selectedIndex = cart.length;
    }

    if (!selectedItem) {
    } else {
      this.props.updateCartSelected(selectedItem, selectedIndex ? selectedIndex : 0);

      this.props.navigation.navigate('CreatePhotoPageV2', {
        item: selectedItem.albumItem,
        album: selectedItem.album,
      });
      // this.props.navigation.navigate('AlbumPage');
    }
  };

  render() {
    const item = this.props.item;
    const cart = this.props.cart.cart ? this.props.cart.cart : [];
    const nachname = this.state.nachname;
    const name = this.state.name;
    const land = this.state.land;
    const adresse = this.state.adresse;
    const background = TouchableNativeFeedback.Ripple('#fff', true);

    return (
      <SafeAreaView style={styles.container}>
        {/* <KeyboardAvoidingView
         behavior={Platform.OS !== 'android' ? 'padding' : 'height'}
         keyboardVerticalOffset={Platform.OS !== 'android' ? 0 : 30}
         style={{flex: 1}}>
         <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
         <View> */}

        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => this.onBackClick()} style={styles.backButton}>
            <BackIcon />
          </TouchableOpacity>
          <View style={styles.header}>
            <Text style={styles.titleHeader}>ZUSAMMENFASSUNG</Text>
          </View>
        </View>

        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <View style={styles.contentContainer}>
            {
              <View style={styles.containerInfo}>
                <View style={styles.innerContainerInfo}>
                  <Text style={styles.containerTitle}>LIEFERADRESSE</Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 12,
                        fontFamily: 'Lato-Regular',
                        width: '70%',
                      }}
                    >
                      {name} {nachname}
                      {'\n'}
                      {adresse}
                      {'\n'}
                      {land}
                    </Text>

                    <TouchableOpacity
                      onPress={() => this.passDataToModal(name, nachname, adresse, land)}
                      style={{ alignSelf: 'flex-end' }}
                    >
                      <EditIcon />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            }

            <View style={styles.containerInfo}>
              <View style={styles.innerContainerInfo}>
                <Text style={styles.containerTitle}>LIEFERUNGSART AUSWÄHLEN</Text>
                <Text style={{ fontSize: 12, fontFamily: 'Lato-Regular' }}>
                  DHL{'\n'}Voraussichtlicher Leifertermin:
                </Text>
                <Text style={{ fontWeight: 'bold', fontSize: 12 }}>Donnerstag 10 Juni - Donnerstag 17 Juni</Text>
              </View>
            </View>

            {cart &&
              cart.length > 0 &&
              cart.map((item, key) => {
                // console.log('ITEM', item);
                let image = null;
                let price = null;
                if (item.selected.length > 0 && item.selected[0].path) image = { uri: item.selected[0].path };
                return (
                  <View key={'cart' + key} style={styles.containerInfo}>
                    <View style={styles.innerContainerInfo}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-around',
                        }}
                      >
                        <Image
                          style={{ width: 125, height: 156 }}
                          source={
                            image
                              ? image
                              : item.album && item.album.image
                              ? item.album.image
                              : require('../assets/images/geschenkkarte.jpg')
                          }
                        />
                        <View style={{ flexDirection: 'column', width: '45%' }}>
                          <Text style={styles.containerTitle}>{item?.albumItem?.productName}</Text>
                          <Text
                            style={{
                              fontSize: 12,
                              fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
                            }}
                          >
                            Quadratisch, 14x14 cm{'\n'}
                            24 Seiten{'\n'}
                          </Text>
                          <Text style={[styles.containerTitle, { color: COLOR_PRIMARY_FOTOS }]}>
                            {item?.albumItem?.price} EURO{'\n'}
                          </Text>
                          <Text
                            style={{
                              fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
                              fontSize: 12,
                            }}
                          >
                            Anzahl: 1
                          </Text>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignSelf: 'flex-end',
                              width: 70,
                              justifyContent: 'space-between',
                            }}
                          >
                            <TouchableOpacity onPress={() => this.removeItem()}>
                              <RemoveIcon />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.onEditClick(item)}>
                              <EditIcon />
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                );
              })}

            {/* <View style={styles.containerInfo}>
             <View style={styles.innerContainerInfo}>
             <View style={{ flexDirection: 'row' }}>
             <ImageSold style={{ height: 156, width: 125 }} />
             <View style={{ flexDirection: 'column', width: '45%' }}>
             <Text style={styles.containerTitle}>FOTOBUCH</Text>
             <Text
             style={{
             fontSize: 12,
             fontFamily:
             Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
             }}>
             Quadratisch, 14x14 cm{'\n'}
             24 Seiten{'\n'}
             </Text>
             <Text
             style={[
             styles.containerTitle,
             { color: COLOR_PRIMARY_FOTOS },
             ]}>
             XX,XX EURO{'\n'}
             </Text>
             <Text
             style={{
             fontFamily:
             Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
             fontSize: 12,
             }}>
             Anzahl: 1
             </Text>
             <View style={{ flexDirection: 'row', alignSelf: 'flex-end', width: 70, justifyContent: 'space-between' }}>
             <TouchableOpacity>
             <RemoveIcon />
             </TouchableOpacity>
             <TouchableOpacity>
             <EditIcon />
             </TouchableOpacity>
             </View>
             </View>
             </View>
             </View>
             </View> */}
          </View>
        </ScrollView>

        <View style={styles.footer}>
          <TextInput
            style={styles.input}
            placeholder="RABATTCODE HINZUFÜGEN"
            placeholderTextColor="#707070"
            autoCapitalize="none"
            autoCorrect={false}
          />
          <View
            style={{
              flexDirection: 'column',
              width: '95%',
              alignSelf: 'center',
              paddingVertical: 10,
            }}
          >
            <View style={styles.footerTextContainer}>
              <Text style={styles.footerText}>Produkt</Text>
              <Text style={styles.footerPrice}>XX,XX €</Text>
            </View>
            <View style={styles.footerTextContainer}>
              <Text style={styles.footerText}>Versand</Text>
              <Text style={styles.footerPrice}>X,XX €</Text>
            </View>
            <View style={styles.footerTextContainer}>
              <Text style={[styles.footerText, { color: COLOR_PRIMARY_FOTOBUCH }]}>Gesamtbetrag</Text>
              <Text style={[styles.footerPrice, { color: COLOR_PRIMARY_FOTOBUCH }]}>XX,XX €</Text>
            </View>
          </View>
          <View
            style={{
              borderRadius: 20,
              overflow: 'hidden',
              width: '90%',
              paddingBottom: 5,
              alignSelf: 'center',
            }}
          >
            <TouchableNativeFeedback useForeground={true} background={background} onPress={() => this.openPage()}>
              <View>
                <LinearGradient
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                  colors={['#FFD06C', '#D78275', '#B42762']}
                  style={styles.LinearGradient}
                >
                  <Text style={styles.textButton}>ZAHLUNG HINZUFÜGEN</Text>
                </LinearGradient>
              </View>
            </TouchableNativeFeedback>
          </View>
        </View>

        <Modal
          onRequestClose={() => this.setState({ modal: false })}
          transparent={true}
          isVisible={this.state.modal}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            width: '100%',
          }}
        >
          <View style={styles.modalContent}>
            <View style={styles.innerModalContent}>
              <View style={styles.closeButton}>
                <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ modal: false })}>
                  <CloseButton />
                </TouchableOpacity>
              </View>

              <TextInput
                style={styles.inputModal}
                placeholder="Name"
                returnKeyType="next"
                placeholderTextColor="#707070"
                autoCorrect={false}
                autoCapitalize="none"
                selectionColor="#000000"
                onSubmitEditing={() => this.NachnameInput.focus()}
                blurOnSubmit={false}
                onChangeText={(name) => this.setState({ name })}
                value={this.state.name}
              />
              <TextInput
                style={styles.inputModal}
                placeholder="Nachname"
                placeholderTextColor="#707070"
                autoCorrect={false}
                autoCapitalize="none"
                returnKeyType="next"
                selectionColor="#000000"
                blurOnSubmit={false}
                ref={(ref) => {
                  this.NachnameInput = ref;
                }}
                onSubmitEditing={() => this.AdresseInput.focus()}
                value={this.state.nachname}
                onChangeText={(nachname) => this.setState({ nachname })}
              />
              <TextInput
                style={styles.inputModal}
                placeholder="Adresse, PLZ"
                placeholderTextColor="#707070"
                autoCorrect={false}
                returnKeyType="next"
                autoCapitalize="none"
                selectionColor="#000000"
                blurOnSubmit={false}
                ref={(ref) => {
                  this.AdresseInput = ref;
                }}
                onSubmitEditing={() => this.LandInput.focus()}
                value={this.state.adresse}
                onChangeText={(adresse) => this.setState({ adresse })}
              />
              <TextInput
                style={styles.inputModal}
                placeholder="Ort, Land"
                placeholderTextColor="#707070"
                autoCorrect={false}
                autoCapitalize="none"
                returnKeyType="next"
                blurOnSubmit={false}
                selectionColor="#000000"
                value={this.state.land}
                ref={(ref) => {
                  this.LandInput = ref;
                }}
                onChangeText={(land) => this.setState({ land })}
              />
              <View style={[styles.footerModal, { borderRadius: 20, overflow: 'hidden' }]}>
                <TouchableNativeFeedback useForeground={true} background={background} onPress={() => this.onSubmit()}>
                  <View>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={['#FFD06C', '#D78275', '#B42762']}
                      style={styles.LinearGradient}
                    >
                      <Text style={styles.textFooter}>EINREICHEN</Text>
                    </LinearGradient>
                  </View>
                </TouchableNativeFeedback>
              </View>
            </View>
          </View>
        </Modal>

        {/* </View>
         </TouchableWithoutFeedback>
         </KeyboardAvoidingView> */}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    marginTop: Platform.OS === 'ios' ? 20 : 0,
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '5%',
    backgroundColor: '#D38074',
  },
  backButton: {
    padding: 20,
    position: 'absolute',
    zIndex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
  },
  titleHeader: {
    color: '#FFFFFF',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 2,
  },
  textButton: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  contentContainer: {
    width: '100%',
    paddingBottom: 260,
  },
  containerInfo: {
    borderWidth: 3,
    borderRadius: 20,
    borderColor: '#D38074',
    backgroundColor: '#FFFFFF',
    margin: 10,
  },
  innerContainerInfo: {
    margin: 15,
  },
  containerTitle: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  footer: {
    borderTopWidth: 3,
    borderTopColor: '#B42762',
    position: 'absolute',
    bottom: 0,
    paddingBottom: 20,
    alignSelf: 'center',
    backgroundColor: '#FFFFFF',
  },
  input: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 20,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    marginVertical: 10,
    width: '90%',
    alignSelf: 'center',
    padding: 15,
    color: 'black',
    fontSize: 13,
  },
  footerText: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
    width: '65%',
  },
  footerPrice: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
    width: '35%',
    textAlign: 'right',
  },
  footerTextContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  modalContent: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputModal: {
    padding: '4%',
    borderWidth: 1,
    borderRadius: 20,
    marginBottom: 20,
    color: 'black',
    fontSize: 13,
    width: '90%',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  innerModalContent: {
    backgroundColor: '#FFFFFF',
    width: '90%',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  closeButton: {
    position: 'relative',
    alignSelf: 'flex-end',
    padding: 20,
  },
  footerModal: {
    alignSelf: 'center',
    width: '90%',
    paddingBottom: 20,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
    product: state.product,
    login: state.login,
  };
}

export default connect(mapStateToProps, {
  updateCartPhotos,
  updateCartOptions,
  updateCartSelected,
})(SummaryPage);
