import React, { Component } from 'react';
import {
  Alert,
  Dimensions,
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableNativeFeedback,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import EinstellungeIcon from '../assets/icons/einstellungeIcon.svg';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import Facebook from '../assets/icons/fbicon.svg';
import Instagram from '../assets/icons/igicon.svg';
import Footer from '../components/Footer';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';

import { getProfile, loginUser, logOut, saveProfile } from '../libs/auth/domain/LoginActions';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

class SettingsPage extends Component {
  constructor(props) {
    super(props);

    const name =
      this.props.login && this.props.login.profile && this.props.login.profile.firstName
        ? this.props.login.profile.firstName
        : '';
    const nachname =
      this.props.login && this.props.login.profile && this.props.login.profile.lastName
        ? this.props.login.profile.lastName
        : '';
    const email =
      this.props.login && this.props.login.profile && this.props.login.profile.email
        ? this.props.login.profile.email
        : '';

    const city =
      this.props.login && this.props.login.profile && this.props.login.profile.defaultBillingAddress
        ? this.props.login.profile.defaultBillingAddress.city
        : '';
    const country =
      this.props.login && this.props.login.profile && this.props.login.profile.defaultBillingAddress
        ? this.props.login.profile.defaultBillingAddress.country
        : '';
    const street =
      this.props.login && this.props.login.profile && this.props.login.profile.defaultBillingAddress
        ? this.props.login.profile.defaultBillingAddress.street
        : '';
    const zip =
      this.props.login && this.props.login.profile && this.props.login.profile.defaultBillingAddress
        ? this.props.login.profile.defaultBillingAddress.zipcode
        : '';

    this.state = {
      nachname: nachname,
      name: name,
      email: email,
      city: city,
      street: street,
      country: country,
      zip: zip,
      modalData: null,
      data: [],
    };
  }

  onSubmit = () => {
    const nachname = this.state.nachname;
    const name = this.state.name;
    const city = this.state.land;
    const street = this.state.street;
    const country = this.state.country;
    const zip = this.state.zip;
    const email = this.state.email;
    let obj = {
      nachname: nachname,
      name: name,
      email: email,
      city: city,
      street: street,
      country: country,
      zip: zip,
    };

    const data = [...this.state.data];

    if ((nachname == '') | (name == '') | (email == '') | (city == '')) {
      Alert.alert('Bitte überprüfen Sie das Formular und versuchen Sie es erneut!');
    } else {
      this.props
        .saveProfile(nachname, name, email, city, street, country, zip, this.props.login.results.contextToken)
        .then((response) => {
          if (response) {
            this.props
              .getProfile(this.props.login.results.contextToken)
              .then((resp) => {
                if (resp) {
                  Alert.alert('Profildetails werden gespeichert!');
                }
              })
              .catch((error) => {
                // console.log('profile error', error);
              });
            //
          }
        })
        .catch((error) => {
          console.error('error');
          console.error(error);
        });
    }
  };

  componentDidMount() {}

  onBackClick = () => {
    this.props.navigation.navigate('ProfilPage');
  };

  render() {
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    return (
      <SafeAreaView style={styles.container}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={{ flex: 1 }}
        >
          <View style={styles.header}>
            <TouchableOpacity onPress={() => this.onBackClick()} style={styles.backButton}>
              <ArrowBack />
            </TouchableOpacity>
            <View style={styles.headerIconAndTitle}>
              <EinstellungeIcon />
              <Text style={styles.headerTitle}>EINSTELLUNGEN</Text>
            </View>
          </View>
          <Image source={require('../assets/images/footerImageEdited.png')} style={styles.imageYellow} />
          <KeyboardAvoidingView behavior={Platform.OS !== 'android' ? '' : 'height'} style={{ flex: 1 }}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <ScrollView contentContainerStyle={{ flexGrow: 1, paddingVertical: 20 }}>
                <View style={{ justifyContent: 'flex-start' }}>
                  <View style={styles.inputsContainer}>
                    <View style={styles.innerInputsContainer}>
                      <Text style={styles.titleTextStyle}>NUTZERDETAILS</Text>
                      <TextInput
                        style={styles.input}
                        placeholder="Name"
                        returnKeyType="next"
                        placeholderTextColor="#707070"
                        autoCorrect={false}
                        autoCapitalize="none"
                        selectionColor="#000000"
                        onSubmitEditing={() => this.NachnameInput.focus()}
                        blurOnSubmit={false}
                        onChangeText={(name) => this.setState({ name })}
                        value={this.state.name}
                      />
                      <TextInput
                        style={styles.input}
                        placeholder="Nachname"
                        placeholderTextColor="#707070"
                        autoCorrect={false}
                        autoCapitalize="none"
                        returnKeyType="next"
                        selectionColor="#000000"
                        blurOnSubmit={false}
                        ref={(ref) => {
                          this.NachnameInput = ref;
                        }}
                        onSubmitEditing={() => this.EmailInput.focus()}
                        value={this.state.nachname}
                        onChangeText={(nachname) => this.setState({ nachname })}
                      />
                      <TextInput
                        style={styles.input}
                        placeholder="E-Mailadresse"
                        placeholderTextColor="#707070"
                        autoCorrect={false}
                        autoCapitalize="none"
                        returnKeyType="next"
                        selectionColor="#000000"
                        blurOnSubmit={true}
                        ref={(ref) => {
                          this.EmailInput = ref;
                        }}
                        // onSubmitEditing={() => this.AdresseInput.focus()}
                        value={this.state.email}
                        onChangeText={(email) => this.setState({ email })}
                      />
                      {/*
                       <TextInput
                       style={styles.input}
                       placeholder="Adresse"
                       placeholderTextColor="#707070"
                       autoCorrect={false}
                       returnKeyType="next"
                       autoCapitalize="none"
                       selectionColor="#000000"
                       blurOnSubmit={false}
                       ref={ref => {
                       this.AdresseInput = ref;
                       }}
                       onSubmitEditing={() => this.ZipInput.focus()}
                       value={this.state.street}
                       onChangeText={street => this.setState({street})}
                       />
                       <TextInput
                       style={styles.input}
                       placeholder="PLZ"
                       placeholderTextColor="#707070"
                       autoCorrect={false}
                       returnKeyType="next"
                       autoCapitalize="none"
                       selectionColor="#000000"
                       blurOnSubmit={false}
                       ref={ref => {
                       this.ZipInput = ref;
                       }}
                       onSubmitEditing={() => this.CityInput.focus()}
                       value={this.state.zip}
                       onChangeText={zip => this.setState({zip})}
                       />
                       <TextInput
                       style={styles.input}
                       placeholder="Ort"
                       placeholderTextColor="#707070"
                       autoCorrect={false}
                       returnKeyType="done"
                       autoCapitalize="none"
                       selectionColor="#000000"
                       blurOnSubmit={true}
                       ref={ref => {
                       this.CityInput = ref;
                       }}
                       value={this.state.city}
                       onChangeText={city => this.setState({city})}
                       />
                       */}
                      <View
                        style={[
                          styles.detailsButton,
                          {
                            borderRadius: 20,
                            overflow: 'hidden',
                            paddingBottom: 5,
                          },
                        ]}
                      >
                        <TouchableNativeFeedback
                          useForeground={true}
                          background={background}
                          onPress={() => this.onSubmit()}
                        >
                          <View>
                            <LinearGradient
                              start={{ x: 0, y: 0 }}
                              end={{ x: 1, y: 0 }}
                              colors={['#FFD06C', '#D78275', '#B42762']}
                              style={styles.LinearGradient}
                            >
                              <Text style={styles.textFooter}>SPEICHERN</Text>
                            </LinearGradient>
                          </View>
                        </TouchableNativeFeedback>
                      </View>
                    </View>
                  </View>
                </View>

                <View style={styles.contentContainer}>
                  <View style={styles.containerAccounts}>
                    <View style={styles.titleContainer}>
                      <Text style={styles.titleTextStyle}>VORHANDENE KONTEN</Text>
                    </View>
                    <View style={styles.innerContainers}>
                      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Facebook />
                        <Text style={styles.innerContainersText}>Facebook</Text>
                      </View>
                      <TouchableOpacity>
                        <Text style={styles.ausloggen}>AUSLOGGEN</Text>
                      </TouchableOpacity>
                    </View>
                    <View style={styles.instagramContainer}>
                      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Instagram />
                        <Text style={styles.innerContainersText}>Instagram</Text>
                      </View>
                      <TouchableOpacity>
                        <Text style={styles.ausloggen}>AUSLOGGEN</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </ScrollView>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>

          <View style={{ position: 'relative', bottom: 0, width: '100%', zIndex: 2 }}>
            <Footer activePage="ProfilPage" navigation={this.props.navigation} />
          </View>
        </LinearGradient>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '3%',
    zIndex: 1,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  headerIconAndTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  imageYellow: {
    alignSelf: 'center',
    width: '100%',
    position: 'absolute',
    bottom: 0,
    zIndex: 0,
  },
  contentContainer: {
    width: '100%',
    zIndex: 999,
    marginTop: 10,
    justifyContent: 'flex-start',
  },
  containerAccounts: {
    borderRadius: 20,
    backgroundColor: '#FFFFFF',
    alignSelf: 'center',
    width: '90%',
  },
  titleContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#B42762',
    padding: '4%',
  },
  titleTextStyle: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
  },
  innerContainers: {
    borderBottomWidth: 1,
    borderBottomColor: '#B42762',
    flexDirection: 'row',
    padding: '3%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  innerContainersText: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 12,
    paddingLeft: 25,
  },
  instagramContainer: {
    flexDirection: 'row',
    padding: '3%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '2%',
    backgroundColor: '#FFFFFF',
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
  icons: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  ausloggen: {
    color: '#12336B',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textDecorationLine: 'underline',
    fontSize: 13,
  },
  input: {
    padding: '3%',
    borderWidth: 1,
    borderRadius: 20,
    color: 'black',
    fontSize: 13,
    marginVertical: 5,
    width: '100%',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  detailsButton: {
    alignSelf: 'center',
    marginVertical: 10,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 5,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  inputsContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 20,
    width: '90%',
    alignSelf: 'center',
    alignItems: 'center',
  },
  innerInputsContainer: {
    width: '90%',
    alignItems: 'center',
    paddingVertical: 10,
  },
});

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

export default connect(mapStateToProps, {
  loginUser,
  logOut,
  getProfile,
  saveProfile,
})(SettingsPage);
