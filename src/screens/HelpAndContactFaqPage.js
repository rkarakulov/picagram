import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
  Dimensions,
  SafeAreaView,
  ScrollView
} from 'react-native';
import KontaktIcon from '../assets/icons/kontaktIcon.svg';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class HelpAndContactFaqPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() { }

  onBackClick = () => {
    this.props.navigation.navigate('HelpAndContactPage');
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={{ flex: 1 }}>
          <View style={styles.header}>
            <TouchableOpacity
              onPress={() => this.onBackClick()}
              style={styles.backButton}>
              <ArrowBack />
            </TouchableOpacity>
            <View style={styles.headerIconAndTitle}>
              <KontaktIcon />
              <Text style={styles.headerTitle}>HILFE UND KONTAKT</Text>
            </View>
          </View>

          <View style={{ zIndex: 999, flex: 1 }}>

            <ScrollView contentContainerStyle={{ paddingVertical: 30 }}>
              <View style={styles.innerContentContainer}>
                <View style={{ padding: 20 }}>
                  <Text style={styles.textTitle}>
                    WIE WIRD MEINE{'\n'}
                    BESTELLUNG GELIEFERT?
                  </Text>
                  <Text style={styles.textStyle}>
                    Occaernatur? Aximaxim nonsed eiur alisciis adit et as exeria que
                    nonsers perisin net quibus ma quo to bla pligenis possitiandae
                    dolorer orporepernam et et hitem aut pos repere pero ea volut
                    quidustia con et voluptis eum, sunt quamus, sed que voluptus era
                    nobistrunt earupis tiscit, od qui iur, eaqui nis sequid quunt
                    rest eosa aut laut rehendi dolute cus, iureicidi ut la sin et
                    harum assuntem rem et, coribus dandigent la quissumque volupta
                    vid ut imolorist eossectur restior itiust lab idigendit quo vit
                    laudanti as sit exeribus, ut quidign ientenda volore denihitio
                    in parchit atemperit, ut esciunto te et de plibus, nonsequia
                    vendundessed mo to totatet, omnis de sus ipit il int is ventis
                    quo bernatur, id ma pero coriorem doluptur, omnimenitate pos aut
                    mi, officienihil molutestrum re ditae omnim la alitatibus. Accum
                    vellate mpedite eatures susant pre nist, si optatiam, ad eni
                    vendessinis eum, ommoluptaes millandiciis eius, qui doles
                  </Text>
                </View>
              </View>
            </ScrollView>
          </View>

          <Image
            source={require('../assets/images/footerImageEdited.png')}
            style={styles.imageYellow}
          />

        </LinearGradient>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  header: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '3%',
    zIndex: 1,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  headerIconAndTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  imageYellow: {
    alignSelf: 'center',
    zIndex: 0,
    bottom: 0,
    position: 'absolute',
  },
  innerContentContainer: {
    borderRadius: 20,
    backgroundColor: '#FFFFFF',
    alignSelf: 'center',
    width: '90%',
    alignItems: 'center',
  },

  textTitle: {
    color: '#D38074',
    fontSize: 20,
    textAlign: 'center',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  textStyle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    marginVertical: 10,
    textAlign: 'center'
  },
});
