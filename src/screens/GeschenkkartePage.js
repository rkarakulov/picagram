import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Dimensions,
  Platform,
  Image,
  SafeAreaView,
  TouchableNativeFeedback,
  TouchableOpacity,
} from 'react-native';
import GeschenkkarteImage from '../assets/images/GeschenkkarteImage.svg';
import CloseButton from '../assets/icons/buttonX.svg';

import LinearGradient from 'react-native-linear-gradient';
import Slider from '@react-native-community/slider';
import { connect } from 'react-redux';

import { updateCart, updateCartPhotos } from '../libs/card/domain/CartActions';

class GeschenkkartePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 10,
    };
  }

  handleBackButtonClick = () => {
    const { goBack } = this.props.navigation;
    goBack(null);
    return true;
  };

  onMinusPress() {
    let value = this.state.value;
    this.setState({ value: this.state.value - 5 });
    if (value < 15) {
      {
        this.setState({
          value: this.state.value,
        });
      }
    }
  }

  onPlusPress() {
    let value = this.state.value;
    this.setState({ value: this.state.value + 5 });
    if (value > 195) {
      {
        this.setState({
          value: this.state.value,
        });
      }
    }
  }

  onButtonClick = () => {
    const cart = this.props.cart.cart;
    let cartData = [...cart];
    let selectedItem = {
      albumSize: '',
      albumName: 'GIFT',
      albumItem: {
        price: this.state.value,
        description: 'GIFT',
        shortDescription: '',
        productName: 'GIFT',
        image: null,
        id: 99999,
      },
      album: null,
      selected: [],
      finished: true,
      type: 'gift',
    };
    cartData.push(selectedItem);
    this.props.updateCart(cartData);

    this.props.navigation.navigate('WarenkorbPage');
  };

  componentDidMount() {}

  render() {
    const originalWidth = 390;
    const originalHeight = 562;
    const aspectRatio = originalWidth / originalHeight;
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    const backgroundButtons = TouchableNativeFeedback.Ripple('#D38074', true);
    let value = this.state.value;

    return (
      <SafeAreaView style={styles.container}>
        <TouchableOpacity onPress={() => this.handleBackButtonClick()} style={styles.closeButtonPreis}>
          <CloseButton />
        </TouchableOpacity>
        <ScrollView>
          <View style={{ aspectRatio, alignItems: 'center' }}>
            <GeschenkkarteImage width="100%" height="100%" viewBox={`0 0 ${originalWidth} ${originalHeight}`} />
            <View style={styles.price}>
              <Text adjustsFontSizeToFit style={styles.priceValue}>
                {value} €
              </Text>
            </View>
          </View>
          <View style={styles.slider}>
            <View style={styles.sliderButtonsAnimation}>
              <TouchableNativeFeedback
                background={backgroundButtons}
                useForeground={true}
                onPress={() => this.onMinusPress()}
              >
                <View>
                  <Text style={styles.sliderButtons}>-</Text>
                </View>
              </TouchableNativeFeedback>
            </View>
            <Slider
              onValueChange={(value) => this.setState({ value })}
              step={5}
              value={this.state.value}
              style={{ width: '80%' }}
              minimumValue={10}
              maximumValue={200}
              minimumTrackTintColor="#D38074"
              maximumTrackTintColor="#D38074"
              thumbTintColor="#D38074"
            />
            <View style={styles.sliderButtonsAnimation}>
              <TouchableNativeFeedback
                background={backgroundButtons}
                useForeground={true}
                onPress={() => this.onPlusPress()}
              >
                <View>
                  <Text style={styles.sliderButtons}>+</Text>
                </View>
              </TouchableNativeFeedback>
            </View>
          </View>
          <View style={{ width: '90%', alignSelf: 'center' }}>
            <Text style={styles.textTitlesContainer}>WÄHLE EINEN WERT AUS</Text>
            <View style={{ paddingBottom: 50 }}>
              <Text style={styles.textStyle}>
                Tada! Hier ist bereits die Geschenkkarte. Leite diese E-Mail an den glücklichen Empfänger weiter, oder
                noch besser: Drucke die angefügte Geschenkkarte aus und übergebe sie persönlich.{' '}
              </Text>
            </View>
          </View>
        </ScrollView>
        <View
          style={{
            borderRadius: 20,
            overflow: 'hidden',
            width: '90%',
            alignSelf: 'center',
            bottom: 30,
            paddingBottom: 5,
          }}
        >
          <TouchableNativeFeedback background={background} useForeground={true} onPress={() => this.onButtonClick()}>
            <View>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={['#FFD06C', '#D78275', '#B42762']}
                style={styles.LinearGradient}
              >
                <Text style={styles.textFooter}>ZUM WARENKORB HINZUFÜGEN</Text>
              </LinearGradient>
            </View>
          </TouchableNativeFeedback>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.17,
    shadowRadius: 2.49,
    elevation: 2,
  },
  titleContainer: {
    paddingTop: 20,
    width: '90%',
  },
  textTitlesContainer: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    paddingVertical: 10,
  },
  textStyle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    width: '90%',
  },
  sliderButtons: {
    fontFamily: 'Lato-Bold',
    fontSize: 25,
    color: '#D38074',
    textAlign: 'center',
  },
  slider: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  price: {
    width: 100,
    height: 50,
    backgroundColor: '#fff',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'flex-end',
    position: 'absolute',
    bottom: 0,
  },
  priceValue: {
    fontFamily: 'Lato-Bold',
    fontSize: 25,
    color: '#D38074',
    width: 70,
    textAlign: 'center',
  },
  sliderButtonsAnimation: {
    overflow: 'hidden',
    borderRadius: 20,
    height: 40,
    width: 40,
    justifyContent: 'center',
  },
  closeButtonPreis: {
    position: 'absolute',
    top: 40,
    right: 20,
    zIndex: 9999,

    borderRadius: 40,
    padding: 15,
    backgroundColor: 'rgba(255,255,255,0.5)',
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
  };
}

export default connect(mapStateToProps, { updateCart, updateCartPhotos })(GeschenkkartePage);
