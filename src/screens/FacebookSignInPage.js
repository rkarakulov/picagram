import React, { Component } from 'react';
import {
  Alert,
  BackHandler,
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import BackgroundColor from '../assets/icons/backgroundColor.svg';
import Smartphone from '../assets/icons/smartphone-call.svg';
import SmartphoneIcon from '../assets/icons/smartphoneIcon.svg';
import Facebook from '../assets/icons/facebook.svg';
import FacebookLogo from '../assets/images/facebookLOGO.svg';
import InstagramIcon from '../assets/icons/instagramIcon.svg';
import LinearGradient from 'react-native-linear-gradient';
import InstagramIconBlack from '../assets/icons/instagramIconBlack.svg';
import OderLines from '../assets/images/oderLines.svg';
import AppleIcon from '../assets/icons/appleIcon.svg';
import Apple from '../assets/icons/apple.svg';
import GalleryIcon from '../assets/icons/galleryIcon';

export default class FacebookSignInPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: '',
      password: '',
    };
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.unsubscribe();
  }

  handleBackButtonClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => {});
  }

  onBackClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  onAnmeldenClick = () => {
    this.props.navigation.navigate('SignInPage');
  };

  onRegistrierenClick = () => {
    this.props.navigation.navigate('RegistrationPage');
  };

  onPasswortVergessenClick = () => {
    this.props.navigation.navigate('ResetPasswordEmailPage');
  };

  onClick = () => {
    const user = this.state.user;
    const password = this.state.password;
    // console.log('User:', user, 'Password:', password);
    if ((user == '') | (password == '')) {
      Alert.alert('Field can not be empty');
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => this.onBackClick()} style={styles.backButton}>
            <ArrowBack />
          </TouchableOpacity>
          <View style={styles.headerIconAndTitle}>
            <GalleryIcon />
            <Text style={styles.headerTitle}>BILDAUSWAHL</Text>
          </View>
        </View>

        <View
          style={{
            width: '100%',
            backgroundColor: '#D38074',
            alignSelf: 'center',
          }}
        >
          <View style={styles.subHeader}>
            <View>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('AlbumPage')}>
                <View style={{ justifyContent: 'center' }}>
                  <SmartphoneIcon />
                  <Smartphone style={styles.icons} />
                </View>
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('InstagramPage')}>
                <InstagramIcon />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>
                <View style={{ justifyContent: 'center' }}>
                  <BackgroundColor />
                  <Facebook style={styles.icons} />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <ScrollView>
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : 'position'}
            keyboardVerticalOffset={-250}
            style={{ flex: 1 }}
          >
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <View>
                <View style={styles.facebookLogo}>
                  <FacebookLogo />
                </View>

                <View style={styles.inputsBlock}>
                  <TextInput
                    style={styles.input}
                    placeholder="Telefonnummer, E-Mailadresse oder Benutzername"
                    placeholderTextColor="#707070"
                    autoCorrect={false}
                    autoCapitalize="none"
                    selectionColor="#000000"
                    onChangeText={(user) => this.setState({ user })}
                    value={this.state.user}
                  />
                  <TextInput
                    style={styles.input}
                    placeholder="Passwort"
                    placeholderTextColor="#707070"
                    autoCorrect={false}
                    autoCapitalize="none"
                    secureTextEntry={true}
                    selectionColor="#000000"
                    onChangeText={(password) => this.setState({ password })}
                    value={this.state.password}
                  />
                </View>
                <View style={styles.passwortVergessenContainer}>
                  <TouchableOpacity onPress={() => this.onPasswortVergessenClick()}>
                    <Text style={styles.textPasswort}>Passwort vergessen?</Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    zIndex: 1,
                    height: 150,
                    justifyContent: 'space-between',
                    bottom: -50,
                  }}
                >
                  <TouchableOpacity onPress={this.onClick}>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={['#FFD06C', '#D78275', '#B42762']}
                      style={styles.LinearGradient}
                    >
                      <Text style={styles.textAnmelden}>ANMELDEN</Text>
                    </LinearGradient>
                  </TouchableOpacity>

                  <View style={{ alignItems: 'center' }}>
                    <Text
                      style={{
                        fontSize: 13,
                        fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
                      }}
                    >
                      Du hast bereits ein Konto?
                    </Text>
                  </View>

                  <TouchableOpacity style={styles.registrierenButton} onPress={() => this.onRegistrierenClick()}>
                    <Text style={styles.textRegistrieren}>REGISTRIEREN</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ height: 200, backgroundColor: 'green' }}>
                  <Image style={styles.image} source={require('../assets/images/footerImageYellow.png')} />

                  <View style={styles.footerImageContent}>
                    <View style={styles.oder}>
                      <OderLines />
                      <Text
                        style={{
                          fontSize: 12,
                          fontFamily: 'Lato-Heavy',
                        }}
                      >
                        ODER
                      </Text>
                      <OderLines />
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                      }}
                    >
                      <TouchableOpacity>
                        <View>
                          <InstagramIconBlack />
                        </View>
                      </TouchableOpacity>
                      <View>
                        <TouchableOpacity>
                          <View style={{ justifyContent: 'center' }}>
                            <AppleIcon />
                            <Apple style={styles.apple} />
                          </View>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    marginTop: Platform.OS === 'ios' ? 20 : 0,
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    padding: 15,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerIconAndTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  subHeader: {
    backgroundColor: '#D38074',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '70%',
    alignSelf: 'center',
  },
  facebookLogo: {
    alignSelf: 'center',
    marginTop: '20%',
    // marginBottom: '7%',
  },
  icons: {
    position: 'absolute',
    alignSelf: 'center',
  },
  inputsBlock: {
    width: '90%',
    alignSelf: 'center',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: 130,
    marginTop: 10,
  },
  input: {
    padding: '4%',
    borderWidth: 1,
    borderRadius: 20,
    // marginBottom: 20,
    color: '#000000',
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  textPasswort: {
    fontSize: 13,
    color: '#B42762',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    alignSelf: 'center',
  },
  oder: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    width: '90%',
    justifyContent: 'space-between',
  },
  apple: {
    alignSelf: 'center',
    position: 'absolute',
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '90%',
    alignSelf: 'center',
  },
  textAnmelden: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  registrierenButton: {
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#B42762',
    borderRadius: 20,
    width: '90%',
    alignSelf: 'center',
    padding: 15,
  },
  textRegistrieren: {
    alignItems: 'center',
    textAlign: 'center',
    color: '#B42762',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 15,
  },
  image: {
    width: '100%',
    zIndex: 0,
  },
  passwortVergessenContainer: {
    padding: 10,
    marginBottom: '10%',
  },
  footerImageContent: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: '25%',
  },
});
