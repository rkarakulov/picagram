import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  PermissionsAndroid,
  FlatList,
  BackHandler,
  AsyncStorage,
  Platform,
  SafeAreaView,
  Dimensions,
  TouchableNativeFeedback,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import CloseButton from '../assets/icons/modalClose.svg';
import CutIcon from '../assets/icons/cutIcon.svg';
import EffectIcon from '../assets/icons/effectIcon.svg';
import RotationIcon from '../assets/icons/rotationIcon.svg';
import ReturnIcon from '../assets/icons/returnIcon.svg';
import ImagePicker from 'react-native-image-crop-picker';

import PhotoEditor from '@baronha/react-native-photo-editor';

//import PhotoEditor from 'react-native-photo-editor';

import { connect } from 'react-redux';

var RNFS = require('react-native-fs');

import { updateCartPhotos } from '../libs/card/domain/CartActions';
import { getPhotoIndexByPath, getPhotoCount, getPhotoCountSelection } from '../helpers/PhotoFunctions';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

class EditPhotoPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: [],
      itemBackup: null,
      item: null,
      index: null,
      type: 'manual',
      selectedOptions: [],
      rotate: '0deg',
    };
  }

  updateCartPropsPhotos = (selected) => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    if (!cart[cartIndex].selected) cart[cartIndex].selected = [];
    cart[cartIndex].selected = selected;
    this.props.updateCartPhotos(cart);
  };

  onRotatePhotoClick = (item) => {
    let rotate = '90deg';
    if (this.state.rotate === '90deg') {
      rotate = '90deg';
    }
    if (this.state.rotate === '90deg') {
      rotate = '180deg';
    }
    if (this.state.rotate === '180deg') {
      rotate = '270deg';
    }
    if (this.state.rotate === '270deg') {
      rotate = '360deg';
    }
    if (this.state.rotate === '360def') {
      rotate = '90deg';
    }

    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
    const selected =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];

    const index = this.state.index;
    if (selected[index]) {
      selected[index].rotate = rotate;
    }

    this.setState({ rotate: rotate });
    this.updateCartPropsPhotos(selected);
    /*
     let selectedItem = false;
     const index = this.state.selectedOptions.findIndex(
     obj => obj.image === item,
     );
     if (index > -1) {
     selectedItem = true;
     }
     let selectedOptions = [...this.state.selectedOptions];
     if (!selectedItem) {
     selectedOptions.push({
     image: item,
     rotate: '90deg',
     });
     console.log('item add', selectedOptions);

     this.setState({
     selectedOptions: selectedOptions,
     });
     } else {
     selectedOptions[index].rotate = '90deg';
     }
     */
  };

  onEditEffects = async (uriPhoto) => {
    try {
      let imagePATH = '';
      let photo = uriPhoto;

      if (Platform.OS === 'ios') {
        if (uriPhoto.startsWith('ph://')) {
          imagePATH = uriPhoto.substring(5, 41);
          let photoPATH = `assets-library://asset/asset.JPG?id=${imagePATH}&ext=JPG`;

          const dest = `${RNFS.TemporaryDirectoryPath}${Math.random().toString(36).substring(7)}.jpg`;

          RNFS.copyAssetsFileIOS(photoPATH, dest, 500, 500, 1.0, 1.0, 'contain').then((data) => {
            //console.log(dest);

            PhotoEditor.Edit({
              //path:
              // 'https://images.unsplash.com/photo-1627532383712-981b37b4085c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80',
              path: dest,
              onDone: (res) => {
                // console.log('on done');
                // console.log(res);
                const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
                const selected =
                  this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
                    ? this.props.cart.cart[selectedIndex].selected
                    : [];

                const index = this.state.index;
                if (selected[index]) {
                  selected[index].originalPath = selected[index].path;
                  selected[index].path = data;
                }

                this.setState(
                  {
                    item: data,
                    photo: data,
                  },
                  () => this.updateCartPropsPhotos(selected),
                );
              },
            });
          });
        } else {
          PhotoEditor.Edit({
            //path:
            // 'https://images.unsplash.com/photo-1627532383712-981b37b4085c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80',
            path: photo,
            onDone: (res) => {
              //  console.log('on done2');
              //  console.log(res);
              const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
              const selected =
                this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
                  ? this.props.cart.cart[selectedIndex].selected
                  : [];

              const index = this.state.index;
              if (selected[index]) {
                selected[index].originalPath = selected[index].path;
                selected[index].path = res;
              }

              this.setState(
                {
                  item: res,
                  photo: res,
                },
                () => this.updateCartPropsPhotos(selected),
              );
            },
          });
        }
      } else {
        try {
          //ssssphoto = '/' + photo;
          photo = photo.replace('file://', '');
          console.log('photo url: ' + photo);
          const result = await PhotoEditor.open({
            path: photo,
          });
          console.log('resultEdit: ', result);
          const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
          const selected =
            this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
              ? this.props.cart.cart[selectedIndex].selected
              : [];

          const index = this.state.index;
          if (selected[index]) {
            selected[index].originalPath = selected[index].path;
            selected[index].path = 'file://' + result;
          }

          this.setState(
            {
              item: 'file://' + result,
              photo: 'file://' + result,
            },
            () => this.updateCartPropsPhotos(selected),
          );
        } catch (e) {
          console.log('error', e);
        } finally {
          console.log('finally');
        }
      }
    } catch (error) {
      //console.log('error efekti', error);
      //     alert('Failed to fetch the data from storage')
      //   }
    }

    //ovdje
  };
  onEditEffectsOld = async (photo) => {
    PhotoEditor.open({
      // path:
      // 'https://images.unsplash.com/photo-1627532383712-981b37b4085c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80',
      path: photo,
    })
      .then((path) => {
        const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
        const selected =
          this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
            ? this.props.cart.cart[selectedIndex].selected
            : [];

        const index = this.state.index;
        if (selected[index]) {
          selected[index].originalPath = selected[index].path;
          selected[index].path = path;
        }

        this.setState(
          {
            item: path,
            photo: path,
          },
          () => this.updateCartPropsPhotos(selected),
        );

        //   console.log('resultEdit', path);
      })
      .catch((error) => {
        //  console.log('error efekti', error);
        //PhotoEditor.removeEventListener();
      });
    //ovdje
  };

  onZuschneidenClick = (item) => {
    ImagePicker.openCropper({
      path: item,
      freeStyleCropEnabled: true,
    })
      .then((image) => {
        // console.log(image);

        const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
        const selected =
          this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
            ? this.props.cart.cart[selectedIndex].selected
            : [];

        const index = this.state.index;
        if (selected[index]) {
          selected[index].originalPath = selected[index].path;
          selected[index].path = image.path;
        }

        this.setState(
          {
            item: image.path,
            photo: image.path,
          },
          () => this.updateCartPropsPhotos(selected),
        );
      })
      .catch((error) => {
        //if (error.code !== 'E_PICKER_CANCELLED') {
        //   console.log(error);
        //   console.log('greska cut');
        //}
      });

    //.PhotoEditor.Edit({
    //   path: path,
    // });
    /*
     PhotoEditorMain.PhotoEditor.Edit({
     path: path,
     hiddenControls: ['text', 'sticker', 'share', 'draw', 'clear'],
     onDone: () => {
     console.log('on done');
     },
     onCancel: () => {
     console.log('on cancel');
     },
     });
     */
  };

  onEffekteClick = () => {
    // console.log('eeedit', this.state.item);
    let path = this.state.item;
    path = path.replace('file:///', '');
    this.onEditEffects(path);
  };

  onDrehenClick = () => {};

  onZurücksetzenClick = () => {
    // console.log('eeedit', this.state.item);

    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
    const selected =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];
    this.setState({
      rotate: '0deg',
    });

    const index = this.state.index;
    if (selected[index]) {
      if (selected[index].originalPath) {
        selected[index].modifiedBackup = selected[index].path;
        selected[index].path = selected[index].originalPath;
        selected[index].rotate = '0deg';
        this.setState({
          item: selected[index].originalPath,
          rotate: '0deg',
        });
        this.updateCartPropsPhotos(selected);
      }
    }

    // let path = this.state.item;
    // path = path.replace('file:///','');
    // PhotoEditor.Edit({
    //   path: path,
    //   hiddenControls:['text','sticker','share','draw','crop']
    // });
  };

  UNSAFE_componentWillMount() {
    //  console.log(' will mount');
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    // console.log(' unmount ');
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.unsubscribe();
  }

  handleBackButtonClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      this.loadPropsData();
    });
    this.loadPropsData();
  }

  loadPropsData = () => {
    // console.log(' load props data');

    //const selected = this.props.route.params.selected;
    const index = this.props.route.params.index;

    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
    const selected =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];

    const item = selected[index].path;
    const rotate = selected[index].rotate ? selected[index].rotate : '0deg';

    //console.log('load props data');
    this.setState({
      selected: selected,
      item: item,
      index: index,
      rotate: rotate,
    });
  };

  onBackClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  onButtonClick = () => {
    this.props.navigation.navigate('CreatePhotoPageV2');
  };
  //  readData = async () => {
  //   try {
  //     const item = await AsyncStorage.getItem(item)

  //     if (item !== null) {
  //       setState(item)
  //     }
  //   } catch (e) {
  //     alert('Failed to fetch the data from storage')
  //   }
  // }
  // getData = async () => {
  //   try {
  //     const value = await AsyncStorage.getItem(item)
  //     if(value != nul){

  //     }
  //   } catch(e) {

  //   }
  // }

  render() {
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.headerContainer}>
          <View>
            <Text></Text>
          </View>
          <View>
            <Text style={styles.titleHeader}>FOTO BEARBEITEN</Text>
          </View>
          <TouchableOpacity onPress={() => this.onBackClick()} style={{ padding: 10 }}>
            <CloseButton />
          </TouchableOpacity>
        </View>
        {/* <ImageCrop
         imageWidth={200}
         imageHeight={200}
         ref={(ref)=>this.imageCrop = ref}
         source={{uri:this.state.item}} /> */}
        <View style={{ flex: 1 }}>
          <View
            style={{
              justifyContent: 'center',
              flex: 1,
              backgroundColor: '#000',
            }}
          >
            <Image
              style={{
                width: '100%',
                height: '100%',
                alignSelf: 'center',
                resizeMode: 'contain',
                //crop: {left: 150, top: 250, width: 10, height: 10},

                transform: [{ rotate: this.state.rotate }],
              }}
              source={{ uri: this.state.item }}
            />
          </View>
          {/* <FlatList
           keyExtractor={(item, index) => index.toString()}
           data={this.state.selected}
           numColumns={2}
           columnWrapperStyle={'row'}
           contentContainerStyle={{flexGrow: 1, alignItems: 'center'}}
           renderItem={({item}) => {
           console.log('item', item);
           return (

           <Image
           style={{width: '100%', height: '100%'}}
           source={{uri: this.state.item}}
           />

           );
           }}
           /> */}

          <View style={{ width: width, alignSelf: 'center', paddingBottom: 20 }}>
            <View style={styles.footer}>
              <TouchableOpacity onPress={() => this.onZuschneidenClick(this.state.item)} style={styles.icons}>
                <View style={{ alignItems: 'center' }}>
                  <CutIcon />
                  <Text style={styles.iconsTitle}>ZUSCHNEIDEN</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity style={styles.icons} onPress={() => this.onEffekteClick(this.state.item)}>
                <View style={{ alignItems: 'center' }}>
                  <EffectIcon />
                  <Text style={styles.iconsTitle}>EFFEKTE</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => this.onRotatePhotoClick(this.state.item)} style={styles.icons}>
                <View style={{ alignItems: 'center' }}>
                  <RotationIcon />
                  <Text style={styles.iconsTitle}>DREHEN</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => this.onZurücksetzenClick(this.state.item)} style={styles.icons}>
                <View style={{ alignItems: 'center' }}>
                  <ReturnIcon />
                  <Text style={styles.iconsTitle}>ZURÜCKSETZEN</Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={[styles.Button, { borderRadius: 20, overflow: 'hidden', paddingBottom: 5 }]}>
              <TouchableNativeFeedback
                useForeground={true}
                background={background}
                onPress={() => this.onButtonClick(this.state.item)}
              >
                <View>
                  <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    colors={['#FFD06C', '#D78275', '#B42762']}
                    style={styles.LinearGradient}
                  >
                    <Text style={styles.textButton}>BESTÄTIGEN</Text>
                  </LinearGradient>
                </View>
              </TouchableNativeFeedback>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    // marginTop: Platform.OS === 'ios' ? 20 : 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
  },
  titleHeader: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: 'lightgrey',
  },
  icons: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  iconsTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
  },
  Button: {
    alignSelf: 'center',
    width: '90%',
    paddingBottom: 20,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 2,
  },
  textButton: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
  };
}

export default connect(mapStateToProps, { updateCartPhotos })(EditPhotoPage);
