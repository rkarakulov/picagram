import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
  Dimensions,
  SafeAreaView,
  ScrollView
} from 'react-native';
import UberIcon from '../assets/icons/uberIcon.svg';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class DatenerklarungPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() { }

  onBackClick = () => {
    const {goBack} = this.props.navigation;
    goBack();
    return true;
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={{ flex: 1 }}>
          <View style={styles.header}>
            <TouchableOpacity
              onPress={() => this.onBackClick()}
              style={styles.backButton}>
              <ArrowBack />
            </TouchableOpacity>
            <View style={styles.headerTitle}>
              <Text style={styles.headerTitleText}>DATENERKLÄRUNG</Text>
            </View>
          </View>

          <View style={{ zIndex: 999, flex: 1 }}>

            <ScrollView contentContainerStyle={{ paddingVertical: 30 }}>
              <View style={styles.innerContentContainer}>
                <View style={{ padding: 20 }}>
                  <Text style={styles.textTitle}>
                  Wir bringen Deine Erinnerungen in Dein Lieblingsformat!{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Wir von PICAMORY freuen uns, dass du unsere App „PICAMORY“ heruntergeladen hast oder unsere Website www.picamory.com besuchst und mit unserer Mobile- oder Web-App kreative PICAMORY-Fotobücher erstellen möchtest. 
                  PICAMORY ist ein Angebot der LAPIXA GmbH, Dircksenstrasse 40, DE-10178 Berlin (nachfolgend „wir“ oder „uns“ bzw. „unsere“ genannt).{'\n\n'}
                  Der Schutz deiner personenbezogenen Daten bei der Erhebung, Verarbeitung und Nutzung anlässlich deines Besuchs auf unserer Webseite oder der Nutzung unserer App ist uns ein wichtiges Anliegen und erfolgt im Rahmen der gesetzlichen Vorschriften, insbesondere der Datenschutz-Grundverordnung (DSGVO) und dem Bundesdatenschutzgesetz (BDSG), über die du dich z.B. unter www.bfd.bund.de informieren kannst.{'\n\n'}
                  Diese Datenschutzerklärung legt fest, auf welche Weise wir deine personenbezogenen Daten erfassen, verarbeiten und übermitteln und welche Rechte dir diesbezüglich zustehen.{'\n\n'}
                  Diese Datenschutzerklärung gilt für unsere Webseite www.picamory.com, für unserer App „PICAMORY“ sowie für sämtliche sonstigen Anwendungen und Dienste von uns, welche auf diese Datenschutzerklärung verweisen.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Definitionen{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  „Personenbezogene Daten“ sind alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person (im Folgenden „betroffene Person“) beziehen; als identifizierbar wird eine natürliche Person angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung (z.B. Cookie) oder zu einem oder mehreren besonderen Merkmalen identifiziert werden kann, die Ausdruck der physischen, physiologischen, genetischen, psychischen, wirtschaftlichen, kulturellen oder sozialen Identität dieser natürlichen Person sind.{'\n\n'}
                  „Verarbeitung“ ist jeder mit oder ohne Hilfe automatisierter Verfahren ausgeführte Vorgang oder jede solche Vorgangsreihe im Zusammenhang mit personenbezogenen Daten. Der Begriff reicht weit und umfasst praktisch jeden Umgang mit Daten.Als „Verantwortlicher“ wird die natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, die allein oder gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten entscheidet, bezeichnet.„Auftragsverarbeiter“ eine natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, die personenbezogene Daten im Auftrag des Verantwortlichen verarbeitet.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Verantwortlicher{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Verantwortlicher im Sinne der Art. 4 Ziffer 7 der DSGVO und anderer nationaler Datenschutzgesetze der Mitgliedsstaaten ist die:{'\n'}
LAPIXA GmbH{'\n'}
Dircksenstrasse 40{'\n'}
DE-10178 Berlin{'\n'}
E-Mail: kontakt@lapixa.de {'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Umfang der erhobenen Daten, sowie Art und Zweck von deren Verwendung:{'\n\n'}
                  Beim Besuch unserer Website{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Bei jedem Zugriff auf unsere Webseite werden durch den auf deinem Endgerät (Computer, Laptop, Tablet, Smartphone, etc.) zum Einsatz kommenden Internet-Browser automatisch Informationen an den Server unserer Webseite gesendet. Diese Informationen werden temporär in einem sog. Logfile (Protokolldatei) gespeichert.{'\n\n'}
                  Folgende Daten werden dabei ohne dein Zutun erfasst und bis zur automatisierten Löschung gespeichert:{'\n\n'}
                  ·         die IP-Adresse des anfragenden Rechners, sowie Geräte-ID oder individuelle Geräte-Kennung und Gerätetyp;{'\n'}
·         Name der abgerufenen Datei und übertragene Datenmenge, sowie Datum und Uhrzeit des Abrufs;{'\n'}
·         Meldung über erfolgreichen Abruf;{'\n'}
·         anfragende Domain;{'\n'}
·         Beschreibung des Typs des verwendeten Internet-Browsers und ggf. des Betriebssystems Ihres Endgeräts sowie der Name deines Access-Providers;{'\n'}
·         deine Browser-Verlaufsdaten sowie deine standardmäßigen Weblog-Informationen;·  Standortdaten, einschließlich Standortdaten von deinem Mobilgerät;{'\n\n'}
Bitte beachte, dass Du bei vielen Endgeräten die Verwendung von Standortservices im Einstellungsmenü steuern oder deaktivieren kannst.{'\n\n'}
Rechtsgrundlage vorstehender Verarbeitung ist Art. 6 Abs. 1 S. 1 lit. f DSGVO.{'\n\n'}
Unser berechtigtes Interesse zur Erhebung der Daten beruht auf den folgenden Zwecken: Gewährleistung eines reibungslosen Verbindungsaufbaus und einer komfortablen Nutzung der Webseite, Auswertung der Systemsicherheit und -stabilität, Werbung- und Marketing für unsere Produkte.{'\n\n'}
In keinem Fall verwenden wir die hier erhobenen Daten zu dem Zweck, Rückschlüsse auf deine Person zu ziehen.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Bei der Installation und dem Aufruf unserer App{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Bei jedem Aufruf unserer App werden vom Computersystem des aufrufenden Endgerätes (Smartphone, Tablet etc.) folgende Daten an unsere technischen Partner gesendet und in einem Logfile gespeichert:{'\n'}
                  ·         Name der aufgerufenen App;{'\n'}
·         Datum und Uhrzeit deines Aufrufs;{'\n'}
·         übertragene Datenmenge;{'\n'}
·         Meldung über erfolgreichen Abruf;{'\n'}
·         Typ Deines Browsers und verwendete Version,{'\n'}
·         deine IP-Adresse;Rechtsgrundlage für die Erhebung der Daten und deren Speicherung in Logfiles ist Art. 6 Abs. 1 S. 1 lit. f) DSGVO.{'\n\n'}
Rechtsgrundlagen für vorstehende Verarbeitungen sind Art. 6 Abs. 1 S. 1 lit. b) und lit. f) DSGVO.{'\n\n'}
Die Erhebung der Daten durch das System ist erforderlich, um die Installation unserer App auf deinem Endgerät zu ermöglichen.{'\n\n'}
Weiter dient sie der komfortablen Nutzung unserer App sowie Gewährleistung der Systemsicherheit und -stabilität.Durch den Aufruf der App wird dir zudem die Möglichkeit eröffnet, dein PICAMORY-Fotobuch zu erstellen.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Bei der Registrierung oder Anmeldung auf unserer Website oder in unserer App{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Um ein PICAMORY-Fotobuch erstellen zu können, musst Du dich einmalig auf unserer Website oder in unserer App registrieren. Bei der Registrierung werden von Dir folgende Daten erhoben:{'\n'}
 ·         die von dir angegebene E-Mail-Adresse;{'\n'}
·         deinen Vor- und Nachnamen;{'\n'}
·         dein selbst erstelltes Passwort;{'\n'}
·         deine IP-Adresse;{'\n'}
·         das Datum und die Uhrzeit deiner Registrierung;{'\n\n'}
Sofern du bereits registriert bist, werden von Dir bei der Anmeldung folgende Daten erhoben:{'\n\n'}
•  die von dir angegebene Email-Adresse;{'\n'}
•       dein selbst erstelltes Passwort;{'\n'}
•       deine IP-Adresse;{'\n'}
•       das Datum und die Uhrzeit deines Logins; {'\n\n'}
Im Falle einer Registrierung oder Anmeldung über deinen Apple Account, werden uns von der Apple Inc., One Apple Park Way, Cupertino, CA 95014, Vereinigte Staaten folgende deiner Daten zur Verfügung gestellt:{'\n\n'}
•         dein Vor- und Nachname;{'\n'}
•         deine E-Mail-Adresse;{'\n\n'}
Im Falle einer Registrierung oder Anmeldung über Deinen Facebook oder Instagram-Account, werden uns von der Facebook Ireland Limited, 4 Grand Canal Square, Dublin 2, Ireland folgende deiner Daten zur Verfügung gestellt:{'\n\n'}
•         dein Vor- und Nachname;{'\n'}
•         deine E-Mail-Adresse;{'\n\n'}
Die Verarbeitung der deiner Daten erfolgt zum Anlegen und zum Unterhalten deines Kundenkontos bei uns und zur Durchführung der Bestellung Deines PICAMORY-Fotobuchs.{'\n\n'}
Die Rechtsgrundlage vorstehender Verarbeitungen ist Art. 6 Abs. 1 S. 1 lit. b DSGVO.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Im Falle der Bestellung eines PICAMORY-Fotobuchs{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Im Rahmen der Erstellung eines PICAMORY-Fotobuches kann es sein, dass du uns folgende (personenbezogenen) Daten zur Verfügung stellst, die wir in unserer Bestell-Datenbank speichern:{'\n'}
                  ·         Fotos von Personen, die eine unmittelbare oder mittelbare Identifikation dieser Personen ermöglichen;{'\n'}
·         Sonstige von dir zur Verfügung gestellte Inhalte (z.B. Texte), die eine unmittelbare oder mittelbare Identifikation von natürlichen Personen ermöglichen;Rechtsgrundlage für die Erhebung und Verarbeitung der vorstehenden Daten zur Anfertigung eines PICAMORY-Fotobuches ist Art. 6 Abs. 1 Satz 1 lit. b) DSGVO.Für den Fall, dass du ein von dir angefertigtes PICAMORY-Fotobuch bestellen möchtest, werden wir zudem folgende weiteren personenbezogenen Daten von dir anfragen und verarbeiten:{'\n\n'}
·         dein Vor- und Nachname;{'\n'}
·         die Adresse, an die deine Bestellung geliefert werden soll;{'\n'}
·         die Adresse, die auf deiner Rechnung erscheinen soll;Rechtsgrundlage für die Erhebung und Verarbeitung der vorstehenden Daten im Rahmen der Abwicklung deiner Bestellung ist Art. 6 Abs. 1 Satz 1 lit. b) DSGVO.Für die im Rahmen der Bestellung notwendige Zahlungsabwicklung werden überdies folgende Daten erhoben und verarbeitet:{'\n\n'}
·         deine Kreditkarteninformationen;{'\n'}
·         deine Bank-Kontoinformationen;{'\n'}
·         deine Daten zur Nutzung sonstiger Zahlungsdienstleister;{'\n\n'}
Rechtsgrundlage für die Erhebung und Verarbeitung der vorstehenden Daten im Rahmen der Zahlungsabwicklung für deine Bestellung ist Art. 6 Abs. 1 Satz 1 lit. b) DSGVO.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Im Falle der Kontaktaufnahme per E-Mail{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Im Falle der Kontaktaufnahme über die auf unserer Webseite oder in unserer App angegebene E-Mail-Adresse wird deine E-Mail-Adresse sowie die von dir weiter übermittelten personenbezogenen Daten gespeichert und zur Beantwortung deiner Anfrage verarbeitet.Die Rechtsgrundlage vorstehender Verarbeitung ist Art. 6 Abs. 1 lit. f DSGVO. Unser berechtigtes Interesse erfolgt aus der Bearbeitung der uns von dir zugesandten Nachricht.Die im Rahmen der Kontaktaufnahme übermittelten personenbezogenen Daten werden nach Erledigung der von Dir gestellten Anfrage automatisch gelöscht, soweit Du nicht als Kunde Kontakt zu uns aufgenommen hast.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Weitergabe personenbezogener Daten an Dritte und Auftragsverarbeiter{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Grundsätzlich geben wir ohne deine ausdrückliche Einwilligung keine personenbezogenen Daten an Dritte weiter. Sofern wir im Rahmen der Verarbeitung deine Daten dennoch gegenüber Dritten offenbaren, sie an diese übermitteln oder Dritten sonstwie Zugriff auf deine Daten gewähren, erfolgt dies ausschließlich auf Grundlage einer der bereits vorstehend genannten Rechtsgrundlagen. Wenn wir gesetzlich oder per Gerichtsbeschluss dazu verpflichtet sind, müssen wir deine Daten auch an auskunftsberechtigte Stellen übermitteln. Teilweise bedienen wir uns zur Verarbeitung Ihrer Daten sorgfältig ausgewählter externer Dienstleister.Sollten im Rahmen einer sogenannten Auftragsverarbeitung deine Daten an Dienstleister weitergegeben, diesen offenbart oder sonstwie zugänglich gemacht werden, so erfolgt dies auf Grundlage des Art. 28 DSGVO. Sämtliche unserer Auftragsverarbeiter sind sorgfältig ausgewählt, an unsere Weisungen gebunden und werden regelmäßig von uns kontrolliert. Wir beauftragen nur solche Auftragsverarbeiter, die hinreichend Garantien dafür bieten, dass geeignete technische und organisatorische Maßnahmen so getroffen werden, dass die Verarbeitung deiner Daten im Einklang mit den Anforderungen von DSGVO und dem BDSG erfolgt und den Schutz deiner Rechte gewährleistet ist.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Datenübermittlung in Drittstaaten{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Die DSGVO gewährleistet ein innerhalb der Europäischen Union gleich hohes Datenschutzniveau. Bei der Auswahl unserer Dienstleister und Kooperationspartner setzen wir daher nach Möglichkeit auf europäische Partner, wenn Ihre personenbezogenen Daten verarbeitet werden sollen. Nur in Ausnahmefällen werden wir Daten außerhalb der Europäischen Union oder des Europäischen Wirtschaftsraums im Rahmen der Inanspruchnahme von Diensten Dritter verarbeiten lassen.{'\n\n'}
                  Wir lassen eine Verarbeitung Ihrer Daten in einem Drittland nur zu, wenn die besonderen Voraussetzungen der Art. 44 ff. DSGVO erfüllt sind. Das bedeutet, dass die Verarbeitung deiner Daten dann nur auf Grundlage besonderer Garantien erfolgen darf, wie etwa die von der EU-Kommission offiziell anerkannte Feststellung eines der EU entsprechenden Datenschutzniveaus oder die Beachtung offiziell anerkannter spezieller vertraglicher Verpflichtungen, der sogenannten „Standardvertragsklauseln“.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Betroffenenrechte{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Auf Anfrage werden wir dich gern informieren, ob und welche personenbezogenen Daten zu deiner Person gespeichert sind (Art. 15 DSGVO), insbesondere über die Verarbeitungszwecke, die Kategorie der personenbezogenen Daten, die Kategorien von Empfängern, gegenüber denen deine Daten offengelegt wurden oder werden, die geplante Speicherdauer, das Bestehen eines Rechts auf Berichtigung, Löschung, Einschränkung der Verarbeitung oder Widerspruch, das Bestehen eines Beschwerderechts, die Herkunft deiner Daten, sofern diese nicht bei uns erhoben wurden, sowie über das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling.{'\n\n'}
                  Dir steht zudem das Recht zu, etwaig unrichtig erhobene personenbezogene Daten berichtigen oder unvollständig erhobene Daten vervollständigen zu lassen (Art. 16 DSGVO).Ferner hast Du das Recht, von uns die Einschränkung der Verarbeitung deiner Daten zu verlangen, sofern die gesetzlichen Voraussetzungen hierfür vorliegen (Art. 18 DSGVO).Du hast das Recht, die dich betreffenden personenbezogenen Daten in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten oder die Übermittlung an einen anderen Verantwortlichen zu verlangen (Art. 20 DSGVO).Darüber hinaus steht Dir das sogenannte „Recht auf Vergessenwerden“ zu, d.h. Du kannst von uns die Löschung deiner personenbezogenen Daten verlangen, sofern hierfür die gesetzlichen Voraussetzungen vorliegen (Art. 17 DSGVO).{'\n\n'}
                  Unabhängig davon werden deine personenbezogenen Daten automatisch von uns gelöscht, wenn der Zweck der Datenerhebung weggefallen oder die Datenverarbeitung unrechtmäßig erfolgt ist.Gemäß Art. 7 Abs. 3 DSGVO hast Du das Recht deine einmal erteilte Einwilligung jederzeit gegenüber uns zu widerrufen. Dies hat zur Folge, dass wir die Datenverarbeitung, die auf dieser Einwilligung beruhte, für die Zukunft nicht mehr fortführen dürfen.Du hast zudem das Recht, jederzeit gegen die Verarbeitung deiner personenbezogenen Daten Widerspruch zu erheben, sofern ein Widerspruchsrecht gesetzlich vorgesehen ist. Im Falle eines wirksamen Widerrufs werden deine personenbezogenen Daten ebenfalls automatisch durch uns gelöscht (Art. 21 DSGVO).Möchtest Du von deinem Widerrufs- oder Widerspruchsrecht Gebrauch machen, genügt eine E-Mail an: privacy@picamory.comDu hast zudem das Recht, dich bei einer Datenschutz-Aufsichtsbehörde über die Verarbeitung deiner personenbezogenen Daten durch uns zu beschweren. Die für uns zuständige Datenschutzbehörde ist der / die:{'\n\n'}
                  Berliner Beauftragte für Datenschutz und Datensicherheit{'\n'}
Friedrichstraße 219{'\n'}
10969 Berlin{'\n'}
Tel. (030) 13 88 90{'\n'}
mailbox@datenschutz-berlin.de{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Dauer der Datenspeicherung{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Die erhobenen Daten werden solange bei uns gespeichert, wie der Zweck der Verarbeitung es erfordert oder Du dein Recht auf Löschung oder dein Recht auf Datenübertragung an Dritte nicht ausgeübt hast.Sofern die Daten nicht gelöscht werden, weil sie für andere und gesetzlich zulässige Zwecke erforderlich sind, wird deren Verarbeitung eingeschränkt, d.h. die Daten werden gesperrt und nicht für andere Zwecke verarbeitet. Das gilt z.B. für Daten, die aus zuwendungs-, handels- oder steuerrechtlichen Gründen aufbewahrt werden müssen.  Nach gesetzlichen Vorgaben in Deutschland erfolgt die Aufbewahrung für 6 Jahre gemäß § 257 Abs. 1 HGB (Handelsbücher, Inventare, Eröffnungsbilanzen, Jahresabschlüsse, Handelsbriefe, Buchungsbelege, etc.) sowie für 10 Jahre gemäß § 147 Abs. 1 AO (Bücher, Aufzeichnungen, Lageberichte, Buchungsbelege, Handels- und Geschäftsbriefe, Für Besteuerung relevante Unterlagen, etc.).{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Verwendung von Cookies{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Unsere Webseite nutzt folgende Arten von Cookies, deren Umfang und Funktionsweise im Folgenden erläutert werden:{'\n'}
•         Transiente Cookies;{'\n'}
•         Persistente Cookies;{'\n\n'}
Transiente Cookies werden automatisiert gelöscht, wenn Du den Browser schließt. Dazu zählen insbesondere die Session-Cookies. Diese speichern eine sogenannte Session-ID, mit welcher sich verschiedene Anfragen deines Browsers der gemeinsamen Sitzung zuordnen lassen. Dadurch kann dein Rechner wiedererkannt werden, wenn Du auf unsere Website zurückkehrst.Die Session-Cookies werden gelöscht, wenn Du dich ausloggst oder den Browser schließt.Persistente Cookies werden automatisiert nach einer vorgegebenen Dauer gelöscht, die sich je nach Cookie unterscheiden kann. Du kannst die Cookies in den Sicherheitseinstellungen deines Browsers jederzeit löschen.Du hast die volle Kontrolle über die Verwendung von Cookies. Du kannst deine Browser-Einstellung entsprechend deinen Wünschen konfigurieren und z. B. die Annahme von Third-Party-Cookies oder allen Cookies ablehnen oder einschränken.Bereits gespeicherte Cookies können jederzeit gelöscht werden.Wir weisen dich darauf hin, dass Du eventuell nicht alle Funktionen dieser Website nutzen kannst, sofern die Third-Party-Cookies oder alle Cookies abgelehnt werden.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Online-Marketing/ Analyse-Tools{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Die von uns eingesetzten Tracking-Maßnahmen werden auf Grundlage des Art. 6 Abs. 1 S. 1 lit. f DSGVO, § 15 Abs. 3 TMG durchgeführt.Mit den zum Einsatz kommenden Tracking-Maßnahmen wollen wir eine bedarfsgerechte Gestaltung und die fortlaufende Optimierung unserer Webseite sicherstellen. Zum anderen setzen wir die Tracking-Maßnahmen ein, um die Nutzung unserer Webseite statistisch zu erfassen und zum Zwecke Auswertung und Optimierung unseres Internet-Auftrittes. Diese Interessen sind als berechtigt im Sinne der vorgenannten Vorschrift anzusehen.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Google Analytics und Google Tag Manager{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                      Zum Zwecke der bedarfsgerechten Gestaltung und fortlaufenden Optimierung unserer Webseiten nutzen wir Google Analytics, einen Webanalysedienst der Google Inc. (www.google.de/intl/de/about/) (1600 Amphitheatre Parkway, Mountain View, CA 94043, USA; im Folgenden „Google“).In diesem Zusammenhang werden pseudonymisierte Nutzungsprofile erstellt und Cookies (siehe auch unter „Verwendung von Cookies“) gesetzt.Die durch den Cookie erzeugten Informationen über deine Benutzung dieser Webseite, wie Browser-Typ/-Version, verwendetes Betriebssystem, Referrer-URL (die zuvor besuchte Seite), Hostname des zugreifenden Rechners (IP-Adresse) und die Uhrzeit der Serveranfrage werden an einen Server von Google in den USA übertragen und dort gespeichert.Die Informationen werden verwendet, um die Nutzung der Webseite auszuwerten, um Reports über die Webseitenaktivitäten zusammenzustellen und um weitere mit der Webseitennutzung und der Internetnutzung verbundene Dienstleistungen zu Zwecken der Marktforschung und bedarfsgerechten Gestaltung dieser Webseiten zu erbringen. In diesen Zwecken liegt auch unser berechtigtes Interesse an der Datenverarbeitung.Die Rechtsgrundlage für den Einsatz von Google Analytics und Google Tag Manager ist § 15 Abs. 3 TMG in Verbindung mit Art. 6 Abs. 1 lit. f DSGVO.Auch werden diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben ist oder soweit Dritte diese Daten im Auftrag verarbeiten. Es wird in keinem Fall deine IP-Adresse mit anderen Daten von Google zusammengeführt. Die IP-Adressen werden anonymisiert, so dass eine Zuordnung nicht möglich ist (sogenanntes IP-Masking).Die von uns gesendeten und mit Cookies, Nutzerkennungen (z.B. User-ID) oder Werbe-IDs verknüpften Daten werden nach 14 Monaten automatisch gelöscht. Die Löschung von Daten, deren Aufbewahrungsdauer erreicht ist, erfolgt automatisch einmal im Monat.Nähere Informationen zu Nutzungsbedingungen und Datenschutz von Google Analytics findest Du unter www.google.com/analytics/terms/de.html bzw. unter www.google.com/intl/de/analytics/privacyoverview.html.Du kannst die Installation und Speicherung von Cookies durch eine entsprechende Einstellung deiner Browser-Software verhindern; wir weisen jedoch darauf hin, dass Du in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen kannst.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Datensicherheit{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Wir sind um alle notwendigen technischen und organisatorischen Sicherheitsmaßnahmen bemüht, um deine personenbezogenen Daten so zu speichern, dass sie weder Dritten noch der Öffentlichkeit zugänglich sind.Solltest Du mit uns per E-Mail in Kontakt treten wollen, so weisen wir dich darauf hin, dass bei diesem Kommunikationsweg die Vertraulichkeit der übermittelten Informationen nicht vollständig gewährleistet werden kann. Wir empfehlen Dir daher, uns vertrauliche Informationen ausschließlich über den Postweg zukommen zu lassen.{'\n'}
                  </Text>
                  <Text style={styles.textSubtitle}>
                  Aktualität und Änderung dieser Datenschutzerklärung{'\n'}
                  </Text>
                  <Text style={styles.textStyle}>
                  Diese Datenschutzerklärung ist aktuell gültig. Durch die Weiterentwicklung unserer Webseite und Angebote darüber oder aufgrund geänderter gesetzlicher beziehungsweise behördlicher Vorgaben kann es notwendig werden, diese Datenschutzerklärung zu ändern.Die jeweils aktuelle Datenschutzerklärung kann jederzeit auf der Webseite unter www.picamory.com/datenschutzerklärung von Dir abgerufen und ausgedruckt werden.{'\n'}
                  </Text>
                </View>
              </View>
            </ScrollView>
          </View>
          <Image
            source={require('../assets/images/footerImageEdited.png')}
            style={styles.imageYellow}
          />

        </LinearGradient>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  header: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '3%',
    zIndex: 1,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerTitleText: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  headerTitle: {
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  imageYellow: {
    alignSelf: 'center',
    zIndex: 0,
    bottom: 0,
    position: 'absolute',
  },
  innerContentContainer: {
    borderRadius: 20,
    backgroundColor: '#FFFFFF',
    alignSelf: 'center',
    width: '90%',
    alignItems: 'center',
  },

  textTitle: {
    color: '#D38074',
    fontSize: 20,
    textAlign: 'center',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  textSubtitle: {
    color: '#000000',
    fontSize: 15,
    textAlign: 'left',
    fontWeight:"700",
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  textStyle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    marginVertical: 10,
    textAlign: "left"
  },
});