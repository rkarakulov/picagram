import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  BackHandler,
  Platform,
  SafeAreaView,
  TouchableNativeFeedback,
} from 'react-native';
import Header from '../components/Header';
import Footer from '../components/Footer';

export default class DetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    this.unsubscribe();
  }
  handleBackButtonClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => { });
  }
  onProductClick(album, item) {
    this.props.navigation.navigate('ProductsPage', {
      item: item,
      album: album,
    });
  }

  render() {
    const background = TouchableNativeFeedback.Ripple('#707070');
    const album = this.props.route.params.album;
    return (
      <SafeAreaView style={styles.container}>
        <Header />
        <Image
          style={{
            width: '100%',
            position: 'absolute',
            zIndex: 0,
            bottom: 0,
            left: 0,
          }}
          source={require('../assets/images/footerImageEdited.png')}
        />

        <ScrollView style={{ zIndex: 0 }}>
          {album.products.map((item, key) => {
            return (
              <View
                key={'detailpage' + key}
                style={styles.innerContainerImages}>
                <View style={{ borderRadius: 20, overflow: 'hidden' }}>
                  <TouchableNativeFeedback
                    background={background}
                    useForeground={true}
                    onPress={() => this.onProductClick(album, item)}>
                    <View style={{ height: 300, width: 354 }}>
                      <Image
                        source={item.image ? { uri: item.image } : album.image}
                        style={{
                          borderBottomRightRadius: 20,
                          width: 354,
                          height: 270,
                          borderBottomLeftRadius: 20,
                        }}
                      />
                      <View
                        style={[
                          styles.titleContainer,
                          { backgroundColor: album.color },
                        ]}>
                        <View style={{ width: '70%' }}>
                          <Text style={styles.textTitle}>
                            {item.productName}
                          </Text>
                          <Text style={styles.dimensions} > {item.customFields.pica_product_dimension} cm</Text>
                        </View>
                        <View style={{ width: '30%' }}>
                          <Text style={styles.textTitle}>
                            {item.price} EURO
                          </Text>


                        </View>
                      </View>
                    </View>
                  </TouchableNativeFeedback>
                </View>
              </View>
            );
          })
          }
        </ScrollView >

        <Footer activePage="DetailPage" navigation={this.props.navigation} />
      </SafeAreaView >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    // marginTop: Platform.OS === 'ios' ? 25 : 0,
  },

  innerContainerImages: {
    // width:'100%',
    alignSelf: 'center',
    marginBottom: 30,
    marginVertical: 20,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 20,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,

    elevation: 12,
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    position: 'absolute',
    alignSelf: 'center',
    bottom: 0,
    padding: '4%',
    width: 355,
  },
  textTitle: {
    color: 'white',
    fontSize: 18,
    width: '100%',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  dimensions: {
    color: 'white',
    fontFamily: 'Lato-Regular',
    fontSize: 13,
  },
  imageFooter: {
    width: '100%',
    height: '30%',
  },
});
