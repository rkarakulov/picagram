import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  BackHandler,
  TouchableOpacity,
  Image,
  FlatList,
  TextInput,
  Platform,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import FotobuchIcon from '../assets/icons/fotobuchIcon';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import AddImage from '../assets/icons/addImage.svg';
import GalerieIcon from '../assets/icons/galerieIcon.svg';
import LayoutIcon from '../assets/icons/layout.svg';
import TextIcon from '../assets/icons/textIcon.svg';
import DeleteIcon from '../assets/icons/deleteIcon.svg';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import ArrowBottom from '../assets/icons/arrowBottom.svg';
import GalerieIconWhite from '../assets/icons/galerieIconWhite.svg';
import AddImageFromLibrary from '../assets/icons/plusImage.svg';
import LayoutIconWhite from '../assets/icons/layoutIconWhite';
import TextEditIcon from '../assets/icons/textEditIcon.svg';
import DeleteIconWhite from '../assets/icons/deleteIconWhite.svg';
import CloseButton from '../assets/icons/modalClose.svg';

import { connect } from 'react-redux';

import { updateCartPhotos, updateCartOptions } from '../libs/card/domain/CartActions';
import { getPhotoIndexByPath, getPhotoCount, getPhotoCountSelection } from '../helpers/PhotoFunctions';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

class CreatePhotoPage extends Component {
  constructor(props) {
    super(props);

    this._flatList = null;
    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
    const selected =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];
    const pages =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].pages
        ? this.props.cart.cart[selectedIndex].pages
        : [];

    this.state = {
      selected: selected,
      count: 0,
      type: 'manual',
      modal: false,
      modalGalerie: false,
      modalLayout: false,
      modalText: false,
      modalEntfernen: false,
      modalSpeichern: false,
      viewableItems: [],
      pages: pages,
    };
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.unsubscribe();
  }

  handleBackButtonClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      this.loadPropsData();
    });
    this.loadPropsData();
  }

  loadPropsData = () => {
    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
    const selected =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];

    const pages =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].pages
        ? this.props.cart.cart[selectedIndex].pages
        : [];

    const font =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].font
        ? this.props.cart.cart[selectedIndex].font
        : 'CourierPrime-Regular';

    //const selected = this.props.route.params.selected;
    const type = this.props.route.params.selected;
    this.setState({
      modalGalerie: false,
      modal: false,
      modalLayout: false,
      modalText: false,
      modalEntfernen: false,
      modalSpeichern: false,
      selected: selected,
      pages: pages,
      selectedOptions: [],
      type: type,
      font: font,
    });
  };

  onBackClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  setPageLayout = (item) => {
    const viewable = [...this.state.viewableItems];
    const pages = [...this.state.pages];
    if (viewable && viewable.length > 0) {
      const index = pages.findIndex((obj) => obj.index === viewable[0].index);
      if (index != -1) {
        if (pages[index]) {
          pages[index].width = item.leftWidth;
          pages[index].height = item.leftHeight;
        }
      } else {
        pages.push({
          index: viewable[0].index,
          width: item.leftWidth,
          height: item.leftHeight,
        });
      }

      const index2 = pages.findIndex((obj) => obj.index === viewable[1].index);
      if (index2 != -1) {
        if (pages[index2]) {
          pages[index2].width = item.rightWidth;
          pages[index2].height = item.rightHeight;
        }
      } else {
        pages.push({
          index: viewable[1].index,
          width: item.rightWidth,
          height: item.rightHeight,
        });
      }
      this.setState({
        pages: pages,
        modalLayout: false,
      });
      this.updateAlbumData(pages);
    }
  };
  onViewableItemsChanged = ({ viewableItems, changed }) => {
    this.setState({
      viewableItems: [...viewableItems],
    });
  };

  onGesamtansichtClick = (item) => {
    this.props.navigation.navigate('OverallViewPage', {
      selected: this.state.selected,
      item: item,
    });
  };

  openPhoto = (photo, index = null) => {
    this.props.navigation.navigate('EditPhotoPage', {
      selected: this.state.selected,
      item: photo,
      index: index,
    });
  };

  onAddImageClick = () => {
    this.props.navigation.navigate('AlbumPage');
  };
  onButtonClick = () => {
    this.props.navigation.navigate('WarenkorbPage');
  };

  updateAlbumData = (data) => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    if (!cart[cartIndex]) cart[cartIndex] = {};
    cart[cartIndex].pages = data;
    this.props.updateCartOptions(cart);
  };
  updateCartPropsPhotos = (selected) => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    if (!cart[cartIndex].selected) cart[cartIndex].selected = [];
    cart[cartIndex].selected = selected;
    this.props.updateCartPhotos(cart);
  };

  setFont = (font) => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    if (!cart[cartIndex]) cart[cartIndex] = {};
    cart[cartIndex].font = font;
    this.props.updateCartOptions(cart);
    this.setState({ font: font });
  };
  speichernOptions = () => {
    //const selectedOptions = [...this.state.selectedOptions];
    const selectedItems = [...this.state.selected];

    //selectedOptions.map((item, index) => {
    //  console.log('item', item, index);
    ////  if (!selectedItems[index].options) selectedItems[index].options = {};
    //});
    /*
     this.state.selectedOptions.findIndex(
     obj => obj.index === index,
     )
     selectedItems[index].options = {
     text: e,
     selected: true,
     };
     */
  };

  renderViewable = (viewItem) => {
    const item = viewItem.item;
    return (
      <View style={styles.imageBoxWrap}>
        <View style={[styles.imageBox]} key={'viewable' + viewItem.index}>
          <View>
            <Image
              style={{
                width: item.width ? item.width : 114,
                height: item.height ? item.height : 114,
              }}
              source={{ uri: item.path }}
            />
          </View>
        </View>
      </View>
    );
  };

  renderLayouts(item) {
    const leftWidth = item.leftWidth - 25;
    const rightWidth = item.rightWidth - 25;
    const leftHeight = item.leftHeight - 25;
    const rightHeight = item.rightHeight - 25;
    return (
      <TouchableOpacity onPress={() => this.setPageLayout(item)}>
        <View style={{ flexDirection: 'row' }}>
          <View style={[styles.layoutBox, { alignSelf: 'center' }]}>
            <View style={[styles.innerLayoutBox, { height: leftHeight, width: leftWidth }]}>
              <GalerieIcon />
            </View>
          </View>
          <View style={[styles.layoutBox, { alignSelf: 'center' }]}>
            <View style={[styles.innerLayoutBox, { height: rightHeight, width: rightWidth }]}>
              <GalerieIcon />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const layouts = [
      { leftWidth: 100, leftHeight: 100, rightWidth: 100, rightHeight: 100 },
      { leftWidth: 50, leftHeight: 50, rightWidth: 100, rightHeight: 100 },
      { leftWidth: 100, leftHeight: 100, rightWidth: 50, rightHeight: 50 },
      { leftWidth: 50, leftHeight: 50, rightWidth: 50, rightHeight: 50 },
    ];

    const selectedPhotos =
      this.state.selected && this.state.selected.length > 0
        ? this.state.selected.filter(function (item) {
            return item.selected;
          })
        : [];

    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;

    // console.log(this.state.viewableItems, 'items');
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => this.onBackClick()}>
            <View style={{ padding: 10 }}>
              <ArrowBack />
            </View>
          </TouchableOpacity>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <FotobuchIcon />
            <Text style={styles.titleHeader}>FOTOBUCH</Text>
          </View>
          <TouchableOpacity onPress={() => this.setState({ modalSpeichern: true })}>
            <Text style={styles.speichern}>SPEICHERN</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.Buttons}>
          <TouchableOpacity style={{ width: '50%', backgroundColor: '#D38074' }}>
            <Text style={[styles.buttonText, { color: '#FFFFFF' }]}>ERSTELLEN</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{ width: '50%', backgroundColor: '#FFFFFF' }}
            onPress={() => this.onGesamtansichtClick()}
          >
            <Text style={[styles.buttonText, { color: '#D38074' }]}>GESAMTANSICHT</Text>
          </TouchableOpacity>
        </View>

        {this.state.modalLayout && (
          <FlatList
            numColumns={2}
            columnWrapperStyle={'row'}
            contentContainerStyle={{
              alignItems: 'center',
              width: '100%',
              height: '90%',
            }}
            keyExtractor={(item, index) => 'layoutViewable' + index.toString()}
            //keyExtractor={layout => "layout" + layout.height}
            data={this.state.viewableItems}
            renderItem={({ item }) => this.renderViewable(item)}
          />
        )}

        <FlatList
          style={{
            opacity: this.state.modalLayout ? 0 : 1,
          }}
          ref={(fl) => (this._flatList = fl)}
          keyExtractor={(item, index) => index.toString()}
          data={selectedPhotos}
          numColumns={2}
          columnWrapperStyle={'row'}
          contentContainerStyle={{
            flexGrow: 1,
            alignItems: 'center',
            width: '100%',
          }}
          onViewableItemsChanged={this.onViewableItemsChanged}
          viewabilityConfig={{ viewAreaCoveragePercentThreshold: 75 }}
          renderItem={(itemObj) => {
            const item = itemObj.item;
            const itemIndex = itemObj.index;
            //   console.log('item', item, itemIndex);
            //let selectedItem = false;

            let inView = false;
            let viewable = [...this.state.viewableItems];
            //  console.log('viewable', viewable);
            if (viewable && viewable.length > 0) {
              const viewIndex = viewable.findIndex((obj) => obj.index === itemIndex);

              if (viewIndex > -1) {
                inView = true;
              }
              //  console.log('inview', inView, viewIndex);
            }
            let selectedItem = item.options && item.options.selected ? true : false;
            const index = getPhotoIndexByPath([...this.state.selected], item.path);

            let pages = [...this.state.pages];

            let itemWidth = 130;
            let itemHeight = 130;

            let pageIndex = pages.findIndex((obj) => obj.index === itemIndex);
            if (pageIndex != -1) {
              itemWidth = pages[pageIndex].width + 60;
              itemHeight = pages[pageIndex].height + 60;
            }

            //console.log(pages, itemWidth, itemHeight, pageIndex);

            /*
             const optionIndex =
             this.state.selectedOptions &&
             this.state.selectedOptions.length > 0
             ? this.state.selectedOptions.findIndex(
             obj => obj.index === index,
             )
             : -1;

             if (optionIndex > -1) {
             selectedItem = true;
             }
             */
            return (
              <View
                style={inView && this.state.modalLayout ? styles.imageBoxFixed : styles.imageBoxRelative}
                key={item.path}
              >
                <View
                  style={[
                    styles.imageBoxWrap,
                    {
                      opacity: !inView && this.state.modalLayout ? 0 : 1,
                    },
                  ]}
                >
                  <View style={[styles.imageBox]}>
                    <TouchableOpacity onPress={() => this.openPhoto(item.path, index)}>
                      <Image
                        style={{
                          width: itemWidth,
                          height: itemHeight,
                        }}
                        source={{ uri: item.path }}
                      />
                    </TouchableOpacity>
                  </View>
                  {!selectedItem && (
                    <View style={styles.innerTextBox}>
                      <TouchableOpacity
                        onPress={() => {
                          if (index != -1) {
                            if (!selectedItem) {
                              const selectedItems = [...this.state.selected];
                              selectedItems[index].options = {
                                text: '',
                                selected: true,
                                index: index,
                              };
                              this.updateCartPropsPhotos(selectedItems);
                            }
                          }

                          if (!selectedItem) {
                            let selectedOptions = [...this.state.selectedOptions];
                            selectedOptions.push({
                              image: item.path,
                              text: '',
                              selected: true,
                              index: index,
                            });
                            // console.log('item add', selectedOptions);

                            this.setState({
                              selectedOptions: selectedOptions,
                            });
                          }
                        }}
                      >
                        <View>{!selectedItem && <AddImage style={{ marginVertical: 5 }} />}</View>
                      </TouchableOpacity>
                    </View>
                  )}
                  {selectedItem && (
                    <View style={styles.innerTextBox}>
                      <TextInput
                        style={{
                          fontWeight: 'normal',
                          fontFamily: this.state.font,
                        }}
                        value={
                          this.state.selected[index].options && this.state.selected[index].options.text
                            ? this.state.selected[index].options.text
                            : ''
                          //this.state.selectedOptions[optionIndex].text
                        }
                        /*
                         onBind={e => {
                         const selectedItems = [...this.state.selected];
                         selectedItems[index].options = {
                         text: e,
                         selected: true,
                         };
                         this.updateCartPropsPhotos(selectedItems);
                         }}
                         */
                        onChange={(event) => {
                          const { eventCount, target, text } = event.nativeEvent;
                          //console.log(text);

                          /*
                           let selectedOptions = [
                           ...this.state.selectedOptions,
                           ];
                           selectedOptions[optionIndex].text = text;
                           this.setState({
                           selectedOptions: selectedOptions,
                           });
                           */

                          const selectedItems = [...this.state.selected];
                          if (!selectedItems[index].options)
                            selectedItems[index].options = {
                              index: index,
                              image: item.path,
                              text: text,
                            };
                          else selectedItems[index].options.text = text;
                          this.updateCartPropsPhotos(selectedItems);

                          /*
                           let selectedOptions = [
                           ...this.state.selectedOptions,
                           ];
                           selectedOptions[index].text = e;
                           this.setState({
                           selectedOptions: selectedOptions,
                           });
                           */

                          /*
                           let selectedOptions = [
                           ...this.state.selectedOptions,
                           ];
                           selectedOptions[optionIndex].text = e;
                           this.setState({
                           selectedOptions: selectedOptions,
                           });
                           */
                        }}
                      />
                    </View>
                  )}
                </View>
              </View>
            );
          }}
        />

        <Modal
          transparent={true}
          isVisible={this.state.modal}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            bottom: -200,
          }}
        >
          <View style={styles.modalContent}>
            <View style={styles.innerModalContent}>
              <TouchableOpacity onPress={() => this.setState({ modal: false })} style={styles.closeButton}>
                <CloseButton />
              </TouchableOpacity>
              <Text style={styles.modalTitle}>
                Drücke länger auf die Seite,{'\n'}
                um sie verschieben zu können.
              </Text>

              <View>
                <TouchableOpacity style={styles.modalButton} onPress={() => this.setState({ modal: false })}>
                  <Text style={styles.modalButtonText}>OK</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        <Modal
          transparent={true}
          isVisible={this.state.modalSpeichern}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <View style={styles.modalContent}>
            <View style={styles.innerModalContent}>
              <TouchableOpacity onPress={() => this.setState({ modalSpeichern: false })} style={styles.closeButton}>
                <CloseButton />
              </TouchableOpacity>
              <Text style={styles.modalTitle}>SPEICHERN?</Text>
              <Text style={styles.modalSubTitle}>
                Bist du bereit, deinen Entwurf zu sichern,{'\n'}um ihn später zu beenden?
              </Text>

              <View>
                <TouchableOpacity style={styles.modalButton} onPress={() => this.speichernOptions()}>
                  <Text style={styles.modalButtonText}>SPEICHERN</Text>
                </TouchableOpacity>
              </View>

              <TouchableOpacity>
                <Text style={styles.Löschen}>Löschen</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal
          transparent={true}
          isVisible={this.state.modalGalerie}
          style={{
            position: 'absolute',
            bottom: 0,
            width: width,
            alignSelf: 'center',
            margin: 0,
          }}
        >
          <View style={[styles.footerModal, { height: 0.5 * height }]}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <GalerieIconWhite />
              <Text style={styles.footerTitle}>GALERIE</Text>
              <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ modalGalerie: false })}>
                <ArrowBottom />
              </TouchableOpacity>
            </View>
            <View style={styles.AddImageBox}>
              <TouchableOpacity style={{ padding: 15 }} onPress={() => this.onAddImageClick()}>
                <AddImageFromLibrary />
              </TouchableOpacity>
            </View>

            <FlatList
              keyExtractor={(item, index) => index.toString()}
              data={this.state.selected}
              numColumns={4}
              columnWrapperStyle={'row'}
              contentContainerStyle={{
                paddingBottom: 35,
              }}
              renderItem={({ item }) => {
                //  console.log('item', item);
                return (
                  <View key={item.path}>
                    <TouchableOpacity>
                      <Image style={{ width: 81, height: 81, margin: 5 }} source={{ uri: item.path }} />
                    </TouchableOpacity>
                  </View>
                );
              }}
            />
          </View>
        </Modal>
        <Modal
          transparent={true}
          isVisible={this.state.modalLayout}
          style={{
            position: 'absolute',
            bottom: 0,
            width: width,
            alignSelf: 'center',
            margin: 0,
          }}
        >
          <View style={[styles.footerModal, { height: 0.5 * height }]}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <LayoutIconWhite />
              <Text style={styles.footerTitle}>LAYOUT</Text>
              <TouchableOpacity
                style={{ padding: 10 }}
                onPress={() => {
                  this.setState({ modalLayout: false });
                }}
              >
                <ArrowBottom />
              </TouchableOpacity>
            </View>
            <FlatList
              numColumns={2}
              columnWrapperStyle={'row'}
              contentContainerStyle={{
                paddingBottom: 35,
                alignSelf: 'center',
                justifyContent: 'space-between',
              }}
              keyExtractor={(item, index) => 'layout' + index.toString()}
              //keyExtractor={layout => "layout" + layout.height}
              data={layouts}
              renderItem={({ item }) => this.renderLayouts(item)}
            />
          </View>
        </Modal>
        <Modal
          transparent={true}
          isVisible={this.state.modalText}
          style={{
            position: 'absolute',
            bottom: 0,
            width: width,
            alignSelf: 'center',
            margin: 0,
          }}
        >
          <View style={styles.footerModal}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TextEditIcon />
              <Text style={styles.footerTitle}>TEXT</Text>
              <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ modalText: false })}>
                <ArrowBottom />
              </TouchableOpacity>
            </View>
            <View style={styles.fonts}>
              <TouchableOpacity onPress={() => this.setFont('CourierPrime-Regular')}>
                <Text
                  style={{
                    fontSize: 20,
                    fontFamily: 'CourierPrime-Regular',
                    color: '#FFFFFF',
                  }}
                >
                  Hello
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.setFont('LinotypeZapfino')}>
                <Text
                  style={{
                    fontSize: 10,
                    fontFamily: 'LinotypeZapfino',
                    color: '#FFFFFF',
                  }}
                >
                  Hello
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.setFont('Lato-Bold')}>
                <Text
                  style={{
                    fontSize: 20,
                    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
                    color: '#FFFFFF',
                  }}
                >
                  Hello
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.setFont('Bodoni-DTC')}>
                <Text
                  style={{
                    fontSize: 20,
                    fontFamily: 'Bodoni-DTC',
                    color: '#FFFFFF',
                  }}
                >
                  Hello
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <Modal
          transparent={true}
          isVisible={this.state.modalEntfernen}
          style={{
            position: 'absolute',
            bottom: 0,
            width: width,
            alignSelf: 'center',
            margin: 0,
          }}
        >
          <View style={styles.footerModal}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <DeleteIconWhite />
              <Text style={styles.footerTitle}>ENTFERNEN</Text>
              <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ modalEntfernen: false })}>
                <ArrowBottom />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity style={styles.buttonFooter}>
                <Text style={styles.modalButtonText}>BUCH LEEREN</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <View style={styles.footer}>
          <TouchableOpacity style={styles.icons} onPress={() => this.setState({ modalGalerie: true })}>
            <View style={{ alignItems: 'center' }}>
              <GalerieIcon />
              <Text
                style={{
                  fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
                  fontSize: 12,
                }}
              >
                GALERIE
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.icons}
            onPress={() => {
              //const viewable = [...this.state.viewableItems];
              ///if (viewable && viewable.length > 0) {
              //  console.log('viewable', viewable[0]);
              // }
              //this._flatList.scrollToOffset(false, viewable[0].index);
              //              this._flatList.scrollToItem(false, viewable[0].index);
              this.setState({ modalLayout: true });
            }}
          >
            <View style={{ alignItems: 'center' }}>
              <LayoutIcon />
              <Text
                style={{
                  fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
                  fontSize: 12,
                }}
              >
                LAYOUT
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.icons} onPress={() => this.setState({ modalText: true })}>
            <View style={{ alignItems: 'center' }}>
              <TextIcon />
              <Text
                style={{
                  fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
                  fontSize: 12,
                }}
              >
                TEXT
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.icons} onPress={() => this.setState({ modalEntfernen: true })}>
            <View style={{ alignItems: 'center' }}>
              <DeleteIcon />
              <Text
                style={{
                  fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
                  fontSize: 12,
                }}
              >
                ENTFERNEN
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <TouchableOpacity activeOpacity={0.9} style={styles.Button} onPress={() => this.onButtonClick()}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={['#FFD06C', '#D78275', '#B42762']}
            style={styles.LinearGradient}
          >
            <Text style={styles.textButton}>ZUM WARENKORB{'\n'}HINZUFÜGEN</Text>
          </LinearGradient>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
    marginHorizontal: 10,
    backgroundColor: '#FFFFFF',
  },
  titleHeader: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  speichern: {
    color: '#12336B',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textDecorationLine: 'underline',
    fontSize: 13,
  },
  Buttons: {
    borderWidth: 1,
    borderColor: '#D38074',
    flexDirection: 'row',
    justifyContent: 'center',
    width: '90%',
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: 18,
    paddingVertical: 20,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
  },
  imageBoxRelative: {},
  imageBoxWrap: {
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#D38074',
    marginVertical: 10,
  },
  imageBox: {
    height: 185,
    width: 185,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '2%',
    alignItems: 'center',
  },
  icons: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  Button: {
    alignSelf: 'center',
    width: '100%',
    bottom: 0,
    marginBottom: 20,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 4,
    width: '90%',
    alignSelf: 'center',
  },
  textButton: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  footerModal: {
    backgroundColor: '#D38074',
    alignItems: 'center',
    paddingVertical: '5%',
  },
  footerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    color: '#FFFFFF',
    paddingHorizontal: 10,
  },
  AddImageBox: {
    borderWidth: 1,
    borderColor: '#FFFFFF',
    backgroundColor: '#D38074',
    height: 81,
    width: 81,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
    marginVertical: 10,
  },
  layoutBox: {
    borderWidth: 1,
    borderColor: '#D38074',
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  innerLayoutBox: {
    backgroundColor: '#D38074',
    justifyContent: 'center',
    alignItems: 'center',
  },
  fonts: {
    flexDirection: 'row',
    marginVertical: 20,
    justifyContent: 'space-between',
    width: '90%',
  },
  buttonFooter: {
    padding: 15,
    borderWidth: 1,
    width: 193,
    borderColor: '#FFFFFF',
    borderRadius: 20,
    marginVertical: 10,
    alignItems: 'center',
  },
  innerTextBox: {
    borderWidth: 1,
    borderColor: '#D38074',
    width: 150,
    height: 50,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
  },
  modalContent: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerModalContent: {
    backgroundColor: '#D38074',
    width: '90%',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalSubTitle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    paddingVertical: 10,
  },
  modalTitle: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  modalButton: {
    padding: 15,
    borderWidth: 1,
    width: 193,
    alignItems: 'center',
    borderColor: '#FFFFFF',
    borderRadius: 20,
    marginVertical: 10,
  },
  modalButtonText: {
    color: '#FFFFFF',
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  closeButton: {
    position: 'relative',
    alignSelf: 'flex-end',
    padding: 20,
  },
  Löschen: {
    textDecorationLine: 'underline',
    color: '#FFFFFF',
    marginBottom: 20,
    fontSize: 12,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
  };
}

export default connect(mapStateToProps, { updateCartPhotos, updateCartOptions })(CreatePhotoPage);
