import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
  FlatList,
  Dimensions,
  SafeAreaView,
  TouchableNativeFeedback,
  TouchableNativeFeedbackBase
} from 'react-native';
import EuroIcon from '../assets/icons/euroIcon.svg';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';
import Icon from '../assets/icons/icon.svg';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default class MyPaymentMethodPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  onBackClick = () => {
    this.props.navigation.navigate('ProfilPage');
  };

  onButtonClick = () => {
    this.props.navigation.navigate('PaymentMethodPage');
  };

  renderPaymentMethod(item) {
    return (
      <View style={styles.cardContainer}>
      <View style={styles.innerCardContainer}>
        <View style={{flexDirection: 'row'}}>
          <Icon />
          <Text style={styles.Information}>
            {item.nameCard}{'\n'}
            {item.cardNumber}{'\n'}
            {item.number}
          </Text>
        </View>
        <TouchableOpacity 
        onPress={() => this.onButtonClick()}
        style={{alignSelf:'flex-start'}}>
          <Text style={styles.bearbeiten}>BEARBEITEN</Text>
        </TouchableOpacity>
      </View>
    </View>
    );
  }

  render() {

    const background = TouchableNativeFeedback.Ripple('#fff',true)
    
    const paymentMethods = [
      {
        nameCard: 'KARTE',
        cardNumber:'XXXXXXXXXXXXX',
        number:'XXXXXXX'
      },    
    ];
    
    return (
      <SafeAreaView style={styles.container}>
         <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={{flex: 1}}>
        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => this.onBackClick()}
            style={styles.backButton}>
            <ArrowBack />
          </TouchableOpacity>
          <View style={styles.headerIconAndTitle}>
            <EuroIcon />
            <Text style={styles.headerTitle}>MEINE ZAHLUNGSMETHODE</Text>
          </View>
        </View>

        <View style={{flex: 1}}>
          <View style={{flex: 0.9}}>
            <FlatList
              style={{zIndex: 1}}
              contentContainerStyle={{paddingVertical: 20}}
              keyExtractor={paymentMethods => paymentMethods.index}
              data={paymentMethods}
              ListFooterComponent={() => (
                <View style={[styles.button,{width: '90%', alignSelf: 'center',borderRadius:20,overflow:'hidden'}]}>
                  <TouchableNativeFeedback
                    useForeground={true}
                    background={background}
                    onPress={() => this.onButtonClick()}>
                    <View style={{width:'100%',height:'100%',justifyContent:'center'}}>
                      <Text style={styles.buttonText}>
                        + FÜGE EINE{'\n'}ZAHLUNGSMETHODE HINZU
                      </Text>
                    </View>
                  </TouchableNativeFeedback>
                </View>
              )}
              renderItem={({item}) => this.renderPaymentMethod(item)}
            />
          </View>
          <View style={{flex: 0.1}} />
        </View>

        <Image
          source={require('../assets/images/footerImageEdited.png')}
          style={styles.imageYellow}
        />
<View style={[styles.footer,{borderRadius:20,overflow:'hidden',paddingBottom:5}]}>
        <TouchableNativeFeedback 
        useForeground={true}
        background={background}
        activeOpacity={0.9}
        >
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#FFD06C', '#D78275', '#B42762']}
            style={styles.LinearGradient}>
            <Text style={styles.textFooter}>BESTÄTIGEN</Text>
          </LinearGradient>
        </TouchableNativeFeedback>
      </View>

        <Text style={styles.whiteFooter} />
        </LinearGradient>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  header: {
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '3%',
    zIndex: 1,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  headerIconAndTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  imageYellow: {
    alignSelf: 'center',
    position: 'absolute',
    bottom:0
  },
  contentContainer: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flexDirection: 'column',
    zIndex: 1,
    width: width,
    paddingVertical: 10,
  },
  cardContainer: {
    width: '90%',
    backgroundColor: '#FFFFFF',
    borderRadius: 20,
    alignSelf:'center',
    marginVertical:10
  },
  innerCardContainer: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    borderWidth: 1,
    borderColor: '#FFFFFF',
    borderRadius: 20,
    height:52
  },
  buttonText: {
    color: '#FFFFFF',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    textAlign: 'center',
  },
  whiteFooter: {
    backgroundColor: 'white',
    position: 'absolute',
    padding: '5%',
    width: '100%',
    bottom: 0,
    zIndex: 1,
  },
  footer: {
    alignSelf: 'center',
    bottom: 30,
    width: '90%',
    zIndex: 99,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    bottom: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49, 
    elevation: 2,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  bearbeiten: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    color: '#12336B',
    textDecorationLine: 'underline',
  },
  Information: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    paddingLeft: 10,
    fontSize: 13,
    width:'70%'
  },
});
