import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Platform, BackHandler } from 'react-native';
import CheckOutImage from '../assets/images/checkoutImage';
import LinearGradient from 'react-native-linear-gradient';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';

export default class CheckOutPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentDidMount() { }

  handleBackButtonClick = () => {

    this.props.navigation.navigate('HomePage');
    return true;
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={{ flex: 1 }}>


          <Image
            source={require('../assets/images/blueBackground.png')}
            style={styles.blueImage}
          />
          <View style={styles.header}>
            <Text style={styles.textHeader}>CHECK OUT</Text>
          </View>

          <View style={styles.contentContainer}>
            <View>
              <CheckOutImage style={{ alignSelf: 'center' }} />
            </View>
            <View>
              <Text style={styles.textTitle}>
                DANKE FÜR DEINE{'\n'}BESTELLUNG
              </Text>
            </View>
            <View>
              <Text style={styles.textSubtitle}>
                Die Bestätigung deiner Bestellung wurde{'\n'}
                an deine E-Mailadresse geschickt.
              </Text>
            </View>
          </View>
          <Image
            source={require('../assets/images/footerImageEdited.png')}
            style={styles.imageYellow}
          />
        </LinearGradient>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#FFFFFF',
    width: '100%',
    alignItems: 'center',
    padding: 20,
  },
  textHeader: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  imageYellow: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: 0,
    zIndex: 1,
  },
  blueImage: {
    position: 'absolute',
    alignSelf: 'center',
    zIndex: 0,
  },
  contentContainer: {
    flexDirection: 'column',
    position: 'relative',
    alignSelf: 'center',
    flex: 1,
    zIndex: 9999,
    paddingTop: 30
  },
  textTitle: {
    fontSize: 20,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    marginVertical: 20,
    textAlign: 'center',
  },
  textSubtitle: {
    fontSize: 12,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    textAlign: 'center',
  },
});
