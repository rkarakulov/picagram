import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Alert,
  Platform,
  BackHandler,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  SafeAreaView,
  Image,
} from 'react-native';
import LogoHeader from '../assets/images/logo.svg';
import LinearGradient from 'react-native-linear-gradient';
import FooterImage from '../assets/images/FooterImageYellow.svg';
import SignInHeader from '../components/SignInHeader';

var height = Dimensions.get('window').height;

export default class ResetPasswordPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      confirmPassword: '',
    };
  }
  UNSAFE_componentWillMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    this.unsubscribe();
  }
  handleBackButtonClick = () => {
    const {goBack} = this.props.navigation;
    goBack();
    return true;
  };

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => {});
  }

  onButtonClick = () => {
    const password = this.state.password;
    if (password.length < 6) {
      Alert.alert('Passwort hat keine 6 Zeichen !!!');
    }
    const confirmPassword = this.state.confirmPassword;

    if (password != confirmPassword) {
      Alert.alert('Passwort does not match');
    } else {
      // console.log('Passwords ok');
    }
    // console.log('password:', password, 'confirmPassword:', confirmPassword);
  };

  onBackClick = () => {
    const {goBack} = this.props.navigation;
    goBack();
    return true;
  };

  render() {
    const {password, confirmPassword} = this.state;

    return (
      <SafeAreaView style={styles.container}>
        <SignInHeader onBackClick={this.onBackClick} />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          keyboardVerticalOffset={30}
          style={styles.container}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <ScrollView contentContainerStyle={{flexGrow: 1}}>
              <View style={{flex: 1, height: height}}>
                {/* <View style={{flex:1}}> */}
                <View style={styles.logo}>
                  <LogoHeader />
                </View>

                <View>
                  <Text style={styles.passworthilfe}>PASSWORTHILFE</Text>
                  <Text style={styles.textStyle}>
                    Wir fragen nach diesem Passwort,{'\n'}
                    wenn du dich anmeldest.
                  </Text>
                </View>

                <View style={{flex: 1}}>
                  <TextInput
                    style={styles.textInput}
                    placeholder="Neues Passwort eingeben"
                    placeholderTextColor="#707070"
                    autoCorrect={false}
                    returnKeyType="next"
                    autoCapitalize="none"
                    selectionColor="#000000"
                    onChangeText={password => this.setState({password})}
                    onSubmit={password}
                    onSubmitEditing={() => this.ConfirmPasswort.focus()}
                    blurOnSubmit={false}
                    minLength={6}
                  />
                  {/* </View> */}
                  {/* <View> */}
                  <TextInput
                    style={styles.textInput}
                    placeholder="Neues Passwort erneut eingeben"
                    placeholderTextColor="#707070"
                    autoCorrect={false}
                    autoCapitalize="none"
                    selectionColor="#000000"
                    onChangeText={confirmPassword =>
                      this.setState({confirmPassword})
                    }
                    onSubmit={confirmPassword}
                    minLength={6}
                    ref={ref => {
                      this.ConfirmPasswort = ref;
                    }}
                  />
                </View>

                <Text style={styles.passwortNote}>
                  Passwörter müssen mindestens 6 Zeichen lang sein.
                </Text>
                {/* </View> */}

                <View
                  style={{
                    width: '100%',
                  }}>
                  <Image
                    source={require('../assets/images/footerImageEdited.png')}
                    style={styles.imageYellow}
                  />

                  <View
                    style={{
                      position: 'absolute',
                      bottom: 0,
                      width: '100%',
                      height: 90,
                    }}>
                    <TouchableOpacity
                      activeOpacity={0.9}
                      style={styles.footer}
                      onPress={() => this.onSubmit()}>
                      <LinearGradient
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 0}}
                        colors={['#FFD06C', '#D78275', '#B42762']}
                        style={styles.LinearGradient}>
                        <Text style={styles.textFooter}>WEITER</Text>
                      </LinearGradient>
                    </TouchableOpacity>
                    <Text style={styles.whiteFooter} />
                  </View>
                </View>

                {/* <View>
          <Text style={styles.whiteFooter} />
        </View>

        <TouchableOpacity
          style={styles.footer}
          onPress={() => this.onButtonClick()}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#FFD06C', '#D78275', '#B42762']}
            style={styles.LinearGradient}>
            <Text style={styles.textFooter}>ÄNDERUNG SPEICHERN </Text>
          </LinearGradient>
        </TouchableOpacity>

        <FooterImage style={styles.image} /> */}
              </View>
            </ScrollView>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  backButton: {
    padding: 20,
    position: 'absolute',
    zIndex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
  },
  headerTitle: {
    color: '#FFFFFF',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  logo: {
    alignItems: 'center',
    marginVertical: 30,
    height: '10%',
  },
  passworthilfe: {
    fontSize: 25,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    alignSelf: 'center',
  },
  textStyle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    alignSelf: 'center',
    textAlign: 'center',
    marginVertical: 15,
    fontSize: 13,
  },
  textInput: {
    padding: '4%',
    color: '#000000',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
    marginHorizontal: 20,
    borderRadius: 20,
    borderWidth: 1,
    marginVertical: 10,
  },

  whiteFooter: {
    backgroundColor: 'white',
    position: 'absolute',
    padding: '5%',
    width: '100%',
    bottom: 0,
    zIndex: 1,
  },
  footer: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: 30,
    width: '100%',
    zIndex: 99,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 5,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  imageYellow: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    zIndex: 0,
  },
  passwortNote: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    alignSelf: 'center',
  },
  alert: {
    backgroundColor: '#FFFFFF',
    color: 'red',
  },
});
