import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
  Dimensions,
  SafeAreaView,
  TouchableNativeFeedback
} from 'react-native';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';
import BringAFriendImage from '../assets/images/bringAFriendImage';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class BringAFriendPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  onBackClick = () => {
    this.props.navigation.navigate('ProfilPage');
  };

  render() {
    const background = TouchableNativeFeedback.Ripple('#fff',true)
    return (
      <SafeAreaView style={styles.container}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={{flex: 1}}>

          <Image
            source={require('../assets/images/blueBackground.png')}
            style={styles.blueImage}
          />
           <Image
            source={require('../assets/images/footerImageEdited.png')}
            style={styles.imageYellow}
          />
   
          <View style={styles.contentContainer}>
            <View>
              <BringAFriendImage style={{alignSelf: 'center'}} />
            </View>
            <View style={{marginVertical:10}}>
              <Text style={styles.textTitle}>BRING A FRIEND</Text>
            </View>
            <View>
              <Text style={styles.textSubtitle}>
                Schenke deinen Freunden 5,00 Euro und bekomme{'\n'}
                5,00 Euro bei deren Erstbestellung!
              </Text>
            </View>
            <View>
              <Text style={[styles.textTitle, {color: '#F5C25B'}]}>
                PICAFRIEND05
              </Text>
            </View>
            <View style={[styles.button,{width: '90%', alignSelf: 'center',borderRadius:20,overflow:'hidden'}]}>
            <TouchableNativeFeedback 
            useForeground={true}
            background={background}>
              <View style={{height:'100%',width:'100%',justifyContent:'center'}}>
                <Text style={styles.buttonText}>KOPIEREN</Text>
              </View>
            </TouchableNativeFeedback>
            </View>
            <View>
              <Text style={styles.textSubtitle}>Guthaben: 0,00 Euro</Text>
            </View>          
          </View>

          <TouchableOpacity
            style={styles.arrowStyle}
            onPress={() => this.onBackClick()}>
            <ArrowBack />
          </TouchableOpacity>
          <Text style={styles.whiteFooter} />
          <View
           style={[styles.footer,{borderRadius:20,overflow:'hidden',paddingBottom:5}]}>
          
          <TouchableNativeFeedback 
          useForeground={true}
          background={background}>
            <View>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#FFD06C', '#D78275', '#B42762']}
              style={styles.LinearGradient}>
              <Text style={[styles.buttonText,{fontFamily: Platform.OS !== 'android' ? 'Lato-Heavy' : 'Lato-Heavy'}]}>FREUNDE EINLADEN</Text>
            </LinearGradient>
            </View>
          </TouchableNativeFeedback>
          </View>
        </LinearGradient>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageYellow: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    zIndex: 0,
  },
  whiteFooter: {
    backgroundColor: 'white',
    position: 'absolute',
    padding: '5%',
    width: '100%',
    bottom: 0,
    zIndex: 1,
  },
  footer: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: 30,
    width: '90%',
    zIndex: 99,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49, 
    elevation: 2,
  },
  arrowStyle: {
    position: 'absolute',
    padding: 20,
    zIndex:9999
  },
  blueImage: {
    position: 'absolute',
    alignSelf: 'center',
    position:'relative',
    zIndex:0
  },
  contentContainer: {
    alignSelf:'center',
    position: 'absolute',
    alignSelf:'center',
    width:'100%',
    paddingTop:60,
    zIndex:999,
  },
  textTitle: {
    fontSize: 20,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    color: '#FFFFFF',
    alignSelf: 'center',
  },
  textSubtitle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    textAlign: 'center',
    marginVertical:10
  },
  button: {
    borderWidth: 1,
    borderColor: '#FFFFFF',
    borderRadius: 20,
    height:52,
    marginVertical:10,
  },
  buttonText: {
    color: '#FFFFFF',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    textAlign: 'center',
  },
});
