import React, { Component } from 'react';
import {
  Alert,
  Dimensions,
  FlatList,
  Image,
  Picker,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableNativeFeedback,
  TouchableOpacity,
  View,
} from 'react-native';
import AdresseIcon from '../assets/icons/adresseIcon.svg';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';
import Icon from '../assets/icons/icon.svg';
import IconTransparent from '../assets/icons/icon-transparent.svg';
import Modal from 'react-native-modal';
import CloseButton from '../assets/icons/modalClose.svg';
import {
  createAddress,
  deleteAddress,
  getAddress,
  getCountries,
  getProfile,
  getStates,
  setDefaultAddress,
  updateAddress,
} from '../libs/auth/domain/LoginActions';
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

class AdressePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      nachname: '',
      name: '',
      zipcode: '',
      city: '',
      street: '',
      itemId: '',
      modalData: null,
      data: this.props.login.address && this.props.login.address.elements ? this.props.login.address.elements : [],
      countriesData:
        this.props.login.countries && this.props.login.countries.elements ? this.props.login.countries.elements : [],
      statesData: this.props.login.states && this.props.login.states.states ? this.props.login.states.states : [],
      land: '',
      countryState: '',
      //   [
      //     {
      //     id: 1,
      //     name: this.props.login.address.elements[0].firstName,
      //     nachname: "Mustermann",
      //     adresse:"Maxstraße 120",
      //     land:"00000 Hamburg, Deutschland"
      //     },
      // ]
    };
  }

  componentDidMount() {
    this.props.getAddress(this.props.login.results.contextToken);
    this.props.getCountries(this.props.login.results.contextToken);
    if (
      this.props.login.address &&
      this.props.login.address.elements &&
      this.props.login.address.elements[0].countryId
    ) {
      this.props.getStates(this.props.login.results.contextToken, this.props.login.address.elements[0].countryId);
    }
  }

  onBackClick = () => {
    this.props.navigation.navigate('ProfilPage');
  };

  onSubmit = () => {
    const customerId = this.props.login.profile.id;
    const salutationId = this.props.login.profile.salutationId;
    const nachname = this.state.nachname;
    const name = this.state.name;
    const land = this.state.land ? this.state.land : this.props.login.countries.elements[0].id;
    const zipcode = this.state.zipcode;
    const city = this.state.city;
    const street = this.state.street;
    const countryState = this.state.countryState ? this.state.countryState : this.props.login.states.states[0].id;
    let obj = {
      customerId: customerId,
      salutationId: salutationId,
      firstName: name,
      lastName: nachname,
      zipcode: zipcode,
      countryId: land,
      city: city,
      street: street,
      countryStateId: countryState,
    };

    if ((nachname == '') | (name == '') | (land == '') | (zipcode == '') | (city == '') | (street == '')) {
      Alert.alert('Field can not be empty');
    } else if (this.state.modalData) {
      this.props.updateAddress(this.props.login.results.contextToken, obj, this.state.itemId).then((resp) => {
        if (resp) {
          this.setState({ modal: false });
          this.props.getAddress(this.props.login.results.contextToken).then((res) => {
            if (res) {
              this.setState({ data: this.props.login.address.elements });

              this.setState({
                nachname: '',
                name: '',
                land: '',
                zipcode: '',
                city: '',
                street: '',
                countryState: '',
                modalData: null,
              });
            }
          });
        }
      });
    } else {
      this.props.createAddress(this.props.login.results.contextToken, obj).then((resp) => {
        if (resp) {
          this.setState({ modal: false });
          this.props.getAddress(this.props.login.results.contextToken).then((res) => {
            if (res) {
              this.setState({ data: this.props.login.address.elements });

              this.setState({
                nachname: '',
                name: '',
                land: '',
                zipcode: '',
                city: '',
                street: '',
                countryState: '',
                modalData: null,
              });
            }
          });
        }
      });
    }
  };
  onDelete = (items) => {
    this.props.deleteAddress(this.props.login.results.contextToken, items.id).then((resp) => {
      if (resp) {
        this.props.getAddress(this.props.login.results.contextToken).then((res) => {
          if (res) {
            this.setState({ data: this.props.login.address.elements });
            this.setState({ modal: false });
          }
        });
      }
    });
    this.setState({ modal: false });
  };
  onSetDefault = (items) => {
    this.props.setDefaultAddress(this.props.login.results.contextToken, items.id).then((resp) => {
      if (resp) {
        this.props.getProfile(this.props.login.results.contextToken).then((resp) => {
          if (resp) {
            this.props;
            this.props.getAddress(this.props.login.results.contextToken).then((res) => {
              if (res) {
                this.setState({ data: this.props.login.address.elements });
                this.setState({ modal: false });
              }
            });
          }
        });
        this.setState({ modal: false });
      }
    });
  };
  addNewAddress = () => {
    this.setState({
      nachname: '',
      name: '',
      land: '',
      zipcode: '',
      city: '',
      street: '',
      countryState: '',
      modalData: null,
      modal: true,
    });
  };

  passDataToModal = (item) => {
    this.setState({
      itemId: item.id,
      modalData: item,
      modal: true,
      nachname: item.lastName,
      name: item.firstName,
      land: item.countryId,
      zipcode: item.zipcode,
      city: item.city,
      countryState: item.countryStateId,
      street: item.street,
    });
  };

  renderAdresses(items) {
    return (
      <View style={styles.adresseContainer}>
        <View style={styles.innerAdresseContainer}>
          <View style={{ flexDirection: 'row' }}>
            {this.props.login.profile &&
            this.props.login.profile.defaultShippingAddress &&
            this.props.login.profile.defaultShippingAddress.id == items.id ? (
              <Icon />
            ) : (
              <TouchableOpacity
                style={{ overflow: 'hidden' }}
                onPress={() =>
                  Alert.alert('Bist du sicher?', 'Möchten Sie diese Adresse wirklich als Standard festlegen?', [
                    {
                      text: 'Cancel',
                      onPress: () => console.log('Canceled'),
                      style: 'cancel',
                    },
                    { text: 'YES', onPress: () => this.onSetDefault(items) },
                  ])
                }
              >
                <IconTransparent />
              </TouchableOpacity>
            )}
            <Text style={styles.adresseInformation}>
              {items.firstName} {items.lastName}
              {'\n'}
              {items.street}
              {'\n'}
              {items.zipcode} {items.city}
              {','}
              {'\n'}
              {items && items.country && items.country.name ? items.country.name : null}
            </Text>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => this.passDataToModal(items)}
              style={{ alignSelf: 'flex-start', marginBottom: 15 }}
            >
              <Text style={styles.bearbeiten}>BEARBEITEN</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                Alert.alert('Bist du sicher?', 'Möchten Sie diese Adresse wirklich löschen?', [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Canceled'),
                    style: 'cancel',
                  },
                  { text: 'OK', onPress: () => this.onDelete(items) },
                ])
              }
              style={{ alignSelf: 'flex-start' }}
            >
              <Text style={styles.bearbeiten}>LÖSCHEN</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  render() {
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    const value = this.state.value;
    const Orders = this.state.data;
    return (
      <SafeAreaView style={styles.container}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#ECAD75', '#D78275', '#B42762']}
          style={{ flex: 1 }}
        >
          <View style={styles.header}>
            <TouchableOpacity style={styles.backButton} onPress={() => this.onBackClick()}>
              <ArrowBack />
            </TouchableOpacity>
            <View style={styles.headerIconAndTitle}>
              <AdresseIcon />
              <Text style={styles.headerTitle}>MEINE ADRESSE</Text>
            </View>
          </View>

          <Image source={require('../assets/images/footerImageEdited.png')} style={styles.imageYellow} />

          <View style={{ flex: 1, zIndex: 9999 }}>
            <View style={{ flex: 0.9 }}>
              <FlatList
                style={{}}
                contentContainerStyle={{
                  justifyContent: 'space-between',
                  paddingVertical: 20,
                }}
                keyExtractor={(item, key) => key.toString()}
                data={Orders}
                ListFooterComponent={() => (
                  <View
                    style={[
                      styles.button,
                      {
                        width: '90%',
                        alignSelf: 'center',
                        borderRadius: 20,
                        overflow: 'hidden',
                      },
                    ]}
                  >
                    <TouchableNativeFeedback
                      useForeground={true}
                      background={background}
                      onPress={() => this.addNewAddress()}
                    >
                      <View
                        style={{
                          width: '100%',
                          height: '100%',
                          justifyContent: 'center',
                        }}
                      >
                        <Text style={styles.buttonText}>+ FÜGE EINE ADRESSE HINZU</Text>
                      </View>
                    </TouchableNativeFeedback>
                  </View>
                )}
                renderItem={({ item }) => this.renderAdresses(item)}
              />
            </View>
            <View style={{ flex: 0.1 }} />
          </View>
          <Text style={styles.whiteFooter} />
          <View style={[styles.footer, { borderRadius: 20, overflow: 'hidden', paddingBottom: 5 }]}>
            <TouchableNativeFeedback useForeground={true} background={background}>
              <View>
                <LinearGradient
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                  colors={['#FFD06C', '#D78275', '#B42762']}
                  style={styles.LinearGradient}
                >
                  <Text style={styles.textFooter}>BESTÄTIGEN</Text>
                </LinearGradient>
              </View>
            </TouchableNativeFeedback>
          </View>

          <Modal
            onRequestClose={() => this.setState({ modal: false })}
            transparent={true}
            isVisible={this.state.modal}
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              width: '100%',
            }}
          >
            <View style={styles.modalContent}>
              <KeyboardAwareScrollView
                contentContainerStyle={{
                  flexGrow: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                style={{ width: '100%' }}
              >
                <View style={styles.innerModalContent}>
                  <View style={styles.closeButton}>
                    <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ modal: false })}>
                      <CloseButton />
                    </TouchableOpacity>
                  </View>

                  <TextInput
                    style={styles.input}
                    placeholder="Name"
                    returnKeyType="next"
                    placeholderTextColor="#707070"
                    autoCorrect={false}
                    autoCapitalize="none"
                    selectionColor="#000000"
                    onSubmitEditing={() => this.NachnameInput.focus()}
                    blurOnSubmit={false}
                    onChangeText={(name) => this.setState({ name })}
                    value={this.state.name}
                  />
                  <TextInput
                    style={styles.input}
                    placeholder="Nachname"
                    placeholderTextColor="#707070"
                    autoCorrect={false}
                    autoCapitalize="none"
                    returnKeyType="next"
                    selectionColor="#000000"
                    blurOnSubmit={false}
                    ref={(ref) => {
                      this.NachnameInput = ref;
                    }}
                    onSubmitEditing={() => this.AdresseInput.focus()}
                    value={this.state.nachname}
                    onChangeText={(nachname) => this.setState({ nachname })}
                  />
                  <TextInput
                    style={styles.input}
                    placeholder="Straße"
                    placeholderTextColor="#707070"
                    autoCorrect={false}
                    returnKeyType="next"
                    autoCapitalize="none"
                    selectionColor="#000000"
                    blurOnSubmit={false}
                    ref={(ref) => {
                      this.AdresseInput = ref;
                    }}
                    onSubmitEditing={() => this.LandInput.focus()}
                    value={this.state.street}
                    onChangeText={(street) => this.setState({ street })}
                  />
                  <TextInput
                    style={styles.input}
                    placeholder="PLZ"
                    placeholderTextColor="#707070"
                    autoCorrect={false}
                    autoCapitalize="none"
                    blurOnSubmit={true}
                    selectionColor="#000000"
                    value={this.state.zipcode}
                    ref={(ref) => {
                      this.PLZInput = ref;
                    }}
                    onSubmitEditing={() => this.PLZInput.focus()}
                    onChangeText={(zipcode) => this.setState({ zipcode })}
                  />
                  <TextInput
                    style={styles.input}
                    placeholder="Stadt"
                    placeholderTextColor="#707070"
                    autoCorrect={false}
                    autoCapitalize="none"
                    blurOnSubmit={true}
                    selectionColor="#000000"
                    value={this.state.city}
                    ref={(ref) => {
                      this.StadtInput = ref;
                    }}
                    onSubmitEditing={() => this.StadtInput.focus()}
                    onChangeText={(city) => this.setState({ city })}
                  />
                  <View style={styles.inputPicker}>
                    <Picker
                      selectedValue={this.state.land}
                      style={{ height: 25 }}
                      onValueChange={(land) => this.setState({ land })}
                    >
                      {this.state.countriesData
                        ? this.state.countriesData.map((item, i) => {
                            return <Picker.Item key={i} label={item.name} value={item.id} />;
                          })
                        : null}
                    </Picker>
                  </View>
                  <View style={styles.inputPicker}>
                    <Picker
                      selectedValue={this.state.countryState}
                      style={{ height: 25 }}
                      onValueChange={(countryState) => this.setState({ countryState })}
                    >
                      {this.state.statesData
                        ? this.state.statesData.map((item, i) => {
                            return <Picker.Item key={i} label={item.name} value={item.id} />;
                          })
                        : null}
                    </Picker>
                  </View>
                  <View style={[styles.footerModal, { borderRadius: 20, overflow: 'hidden' }]}>
                    <TouchableNativeFeedback
                      useForeground={true}
                      background={background}
                      onPress={() => this.onSubmit()}
                    >
                      <View>
                        <LinearGradient
                          start={{ x: 0, y: 0 }}
                          end={{ x: 1, y: 0 }}
                          colors={['#FFD06C', '#D78275', '#B42762']}
                          style={styles.LinearGradient}
                        >
                          <Text style={styles.textFooter}>EINREICHEN</Text>
                        </LinearGradient>
                      </View>
                    </TouchableNativeFeedback>
                  </View>
                </View>
              </KeyboardAwareScrollView>
            </View>
          </Modal>
        </LinearGradient>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '3%',
    zIndex: 1,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  innerModalContent: {
    backgroundColor: '#FFFFFF',
    width: '90%',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerIconAndTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  imageYellow: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    zIndex: 0,
  },
  adresseContainer: {
    width: '90%',
    backgroundColor: '#FFFFFF',
    borderRadius: 20,
    alignSelf: 'center',
    marginVertical: 10,
  },
  innerAdresseContainer: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    borderWidth: 1,
    borderColor: '#FFFFFF',
    borderRadius: 20,
    height: 52,
  },
  buttonText: {
    color: '#FFFFFF',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    textAlign: 'center',
  },
  whiteFooter: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    padding: '5%',
    position: 'absolute',
    bottom: 0,
    zIndex: 1,
  },
  input: {
    padding: '4%',
    borderWidth: 1,
    borderRadius: 20,
    marginBottom: 20,
    color: 'black',
    fontSize: 13,
    width: '90%',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  inputPicker: {
    padding: '4%',
    borderWidth: 1,
    borderRadius: 20,
    marginBottom: 20,
    color: 'black',
    fontSize: 11,
    width: '90%',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  closeButton: {
    position: 'relative',
    alignSelf: 'flex-end',
    padding: 20,
  },
  footer: {
    alignSelf: 'center',
    width: '90%',
    zIndex: 3,
    bottom: 30,
  },
  footerModal: {
    alignSelf: 'center',
    width: '90%',
    paddingBottom: 20,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 2,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  bearbeiten: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    color: '#12336B',
    textDecorationLine: 'underline',
  },
  adresseInformation: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    paddingLeft: 10,
    fontSize: 13,
    width: '70%',
  },
  modalContent: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

export default connect(mapStateToProps, {
  getAddress,
  getProfile,
  getCountries,
  getStates,
  createAddress,
  deleteAddress,
  updateAddress,
  setDefaultAddress,
})(AdressePage);
