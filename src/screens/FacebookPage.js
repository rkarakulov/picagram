import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform,
  BackHandler,
  FlatList,
  Dimensions,
  SafeAreaView,
  TouchableNativeFeedback
} from 'react-native';

import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';
import FacebookLogo from '../assets/images/facebookLOGO.svg';
import InstagramIconBlack from '../assets/icons/instagramIconBlack.svg';
import OderLines from '../assets/images/oderLines.svg';
import AppleIcon from '../assets/icons/appleIcon.svg';
import GoogleIcon from '../assets/icons/googleIcon.svg';
import Apple from '../assets/icons/apple.svg';
import GalleryIcon from '../assets/icons/galleryIcon';
import {
  LoginManager,
  Profile,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk-next';
import SocialTabs from '../components/SocialTabs';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class FacebookPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false,
      user: null,
      authenticated: false,
      photos: [],
    };
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    this.unsubscribe();
  }

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      //  console.log('fokus');
      AccessToken.getCurrentAccessToken().then(accessToken => {
        //  console.log('token', accessToken);
        this.graphRequest();
        // this.setState({
        //   isLoggedIn: true,
        //   user: {...this.state.user, facebook: result, token: accessToken},
        // });
      });
    });
  }

  handleBackButtonClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  onBackClick = () => {
    this.props.navigation.navigate('ProductsPage');
  };
  renderFooterIcons() {
    if (Platform.OS === 'ios') {
      return (
        <View style={styles.footerIcons}>
          <TouchableOpacity>
            <InstagramIconBlack />
          </TouchableOpacity>
          <TouchableOpacity>
            <AppleIcon />
          </TouchableOpacity>
          <TouchableOpacity>
            <GoogleIcon />
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View style={styles.footerIcons}>
          <TouchableOpacity>
            <InstagramIconBlack />
          </TouchableOpacity>
          <TouchableOpacity>
            <GoogleIcon />
          </TouchableOpacity>
        </View>
      );
    }
  }

  _responseInfoCallback(error, result) {
    if (error) {
      // console.log('Error fetching data: ' + error.toString());
    } else {
      //   console.log(result);
      this.setState(
        {
          user: { ...this.state.user, facebook: result },
          isLoggedIn: true,
        },
        () => {
          //   console.log('get photos');
          //  console.log(this.state.user);
          this.graphRequestPhotos();
        },
      );
    }
  }

  _responseInfoCallbackPhotos(error, result) {
    if (error) {
      //   console.log('Error fetching photos data: ' + error.toString());
    } else {
      //  console.log('photos');
      //  console.log(result);
      this.setState({
        photos: result,
      });
    }
  }

  //'/photos?type=uploaded&fields=name,source,id',
  //
  //'albums?fields=id,name,cover_photo,photos{images{source}},description'
  async graphRequestPhotos() {
    const infoRequest = new GraphRequest(
      '/' +
      this.state.user.facebook.id +
      '/albums?fields=id,name,cover_photo,photos{images{source}},description',

      null,
      (err, res) => {
        this._responseInfoCallbackPhotos(err, res);
      },
    );
    const response = await new GraphRequestManager()
      .addRequest(infoRequest)
      .start();
  }
  async graphRequest() {
    const infoRequest = new GraphRequest(
      '/me?fields=name,picture,email',
      null,
      (err, res) => {
        this._responseInfoCallback(err, res);
      },
    );
    const response = await new GraphRequestManager()
      .addRequest(infoRequest)
      .start();
  }

  async signIn() {
    //  console.log('singin ');
    var result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
      'user_photos',
    ]);
    //  console.log(result);

    if (result.isCancelled) {
      //    console.log('Login cancelled');
    } else {
      this.graphRequest();
    }
  }

  onButtonClick = () => {
    this.signIn();
    // LoginManager.logInWithPermissions(['public_profile', 'user_photos']).then(
    //   function (result) {
    //     if (result.isCancelled) {
    //       console.log('Login cancelled');
    //     } else {
    //       console.log(result);
    //       console.log(
    //         'Login success with permissions: ' +
    //           result.grantedPermissions.toString(),
    //       );

    //       FBGraphRequest(
    //         'id, email, picture.type(large)',
    //         this.FBLoginCallback,
    //       );

    //       /*
    //       const infoRequest = new GraphRequest(
    //         '/me',
    //         {
    //           parameters: {
    //             fields: {
    //               string: 'email,name',
    //             },
    //           },
    //         },
    //         (err, res) => {
    //           if (res) {
    //             this.setState({
    //               user: {...this.state.user, facebook: res},
    //             });
    //           }
    //         },
    //       );
    //       new GraphRequestManager().addRequest(infoRequest).start();
    //       */
    //     }
    //   },
    //   function (error) {
    //     console.log('Login fail with error: ' + error);
    //   },
    // );
    // //this.props.navigation.navigate('FacebookSignInPage');
  };

  render() {
    const background = TouchableNativeFeedback.Ripple('#fff', true)
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity
            onPress={() => this.onBackClick()}
            style={styles.backButton}>
            <ArrowBack />
          </TouchableOpacity>
          <View style={styles.headerIconAndTitle}>
            <GalleryIcon />
            <Text style={styles.headerTitle}>BILDAUSWAHL</Text>
          </View>
        </View>

        <SocialTabs type="facebook" navigation={this.props.navigation} />
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          {!this.state.isLoggedIn && (
            <View style={{ flex: 1 }}>
              <View
                style={{
                  alignContent: 'center',
                  paddingTop: 50,
                  flex: 1,
                }}>
                <View style={{ alignSelf: 'center' }}>
                  <FacebookLogo />
                </View>
                <Text style={styles.textStyle}>
                  Lade deine schönsten Fotos von deinem{'\n'}
                  Facebook Account hoch.{'\n'}
                  Jetzt einloggen und loslegen!
                </Text>
                <View style={[styles.footer, { borderRadius: 20, overflow: 'hidden', paddingBottom: 5 }]}>
                  <TouchableNativeFeedback
                    useForeground={true}
                    background={background}
                    onPress={() => this.onButtonClick()}>
                    <View>
                      <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        colors={['#FFD06C', '#D78275', '#B42762']}
                        style={styles.LinearGradient}>
                        <Text style={styles.textFooter}>
                          ÜBER FACEBOOK ANMELDEN
                        </Text>
                      </LinearGradient>
                    </View>
                  </TouchableNativeFeedback>
                </View>
                <View style={[styles.buttonCenter, { borderRadius: 20, overflow: 'hidden', alignSelf: 'center', width: '90%', paddingBottom: 5 }]}>
                  <TouchableNativeFeedback
                    useForeground={true}
                    background={background}
                    onPress={() => {
                      this.props.navigation.navigate('FacebookPageWeb');
                    }}>
                    <View>
                      <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        colors={['#FFD06C', '#D78275', '#B42762']}
                        style={styles.LinearGradient}>
                        <Text style={styles.textFooter}>Find more on Facebook</Text>
                      </LinearGradient>
                    </View>
                  </TouchableNativeFeedback>
                </View>
              </View>
              <View style={{}}>
                <Image
                  style={styles.image}
                  source={require('../assets/images/footerImageEdited.png')}
                />

                <View style={styles.footerImageContent}>
                  <View style={styles.oder}>
                    <OderLines />
                    <Text
                      style={{
                        fontSize: 12,
                        paddingHorizontal: 20,
                        fontFamily: 'Lato-Heavy',
                      }}>
                      ODER
                    </Text>
                    <OderLines />
                  </View>

                  {this.renderFooterIcons()}
                </View>
              </View>
            </View>
          )}

          {this.state.isLoggedIn &&
            this.state.user &&
            this.state.user.facebook && (
              <View>
                {this.state.photos &&
                  this.state.photos.data &&
                  this.state.photos.data.length > 0 && (
                    <FlatList
                      style={{ alignSelf: 'center' }}
                      keyExtractor={album =>
                        album && album.id ? album.id : 'album0'
                      }
                      data={this.state.photos.data}
                      numColumns={2}
                      contentContainerStyle={{
                        paddingBottom: 30,
                        justifyContent: 'center',
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                      }}
                      renderItem={({ item }) => {
                        // console.log(item);
                        //  console.log('end item');
                        if (item && item.name) {
                          let img = null;
                          let count = 0;
                          if (
                            item &&
                            item.photos &&
                            item.photos.data.length > 0
                          ) {
                            if (item.photos.data && item.photos.data.length > 0)
                              img = item.photos.data[0].images[0].source;
                            count = item.photos.data.length;
                          }
                          //  console.log(img, count);
                          return (
                            <TouchableOpacity
                              style={{ margin: 10 }}
                              onPress={() =>
                                this.props.navigation.navigate(
                                  'ChoosePicturePage',
                                  {
                                    item: item,
                                    type: 'facebook',
                                  },
                                )
                              }>
                              <Image
                                style={styles.albumImage}
                                source={{
                                  uri: img ? img : '',
                                }}
                              />
                              <Text
                                numberOfLines={1}
                                ellipsizeMode="tail"
                                style={styles.imageDetail}>
                                {item.name}
                              </Text>
                              <Text style={styles.imageDetail}>{count}</Text>
                            </TouchableOpacity>
                          );
                        }
                      }}
                    />
                  )}
                <View>
                  <View style={{ padding: 20 }}>
                    <Text style={{ textAlign: 'center', width: '100%' }}>
                      {this.state.user.facebook.name}
                    </Text>
                  </View>

                  <View style={[styles.buttonCenter, { borderRadius: 20, overflow: 'hidden', paddingBottom: 5, width: '90%', alignSelf: 'center' }]}>
                    <TouchableNativeFeedback
                      useForeground={true}
                      background={background}
                      onPress={() => {
                        this.setState({ isLoggedIn: false, user: null });
                        // console.log('log out');
                      }}>
                      <View>
                        <LinearGradient
                          start={{ x: 0, y: 0 }}
                          end={{ x: 1, y: 0 }}
                          colors={['#FFD06C', '#D78275', '#B42762']}
                          style={styles.LinearGradient}>
                          <Text style={styles.textFooter}>
                            VON FACEBOOK ABMELDEN
                          </Text>
                        </LinearGradient>
                      </View>
                    </TouchableNativeFeedback>
                  </View>

                  <View style={[styles.buttonCenter, { borderRadius: 20, overflow: 'hidden', paddingBottom: 5, width: '90%', alignSelf: 'center' }]}>
                    <TouchableNativeFeedback
                      useForeground={true}
                      background={background}
                      onPress={() => {
                        this.props.navigation.navigate('FacebookPageWeb');
                      }}>
                      <View>
                        <LinearGradient
                          start={{ x: 0, y: 0 }}
                          end={{ x: 1, y: 0 }}
                          colors={['#FFD06C', '#D78275', '#B42762']}
                          style={styles.LinearGradient}>
                          <Text style={styles.textFooter}>
                            Find more on Facebook
                          </Text>
                        </LinearGradient>
                      </View>
                    </TouchableNativeFeedback>
                  </View>
                </View>
                {/* <View style={{padding: 20}}>
                  <Text style={{textAlign: 'center', width: '100%'}}>
                    {this.state.user.facebook.name}
                  </Text>
                </View>

                <TouchableOpacity
                  style={styles.buttonCenter}
                  onPress={() => {
                    this.setState({isLoggedIn: false, user: null});
                    console.log('log out');
                  }}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 0}}
                    colors={['#FFD06C', '#D78275', '#B42762']}
                    style={styles.LinearGradient}>
                    <Text style={styles.textFooter}>VON FACEBOOK ABMELDEN</Text>
                  </LinearGradient>
                </TouchableOpacity> */}
              </View>
            )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    // marginTop: Platform.OS === 'ios' ? 20 : 0,
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 15,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerIconAndTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  subHeader: {
    backgroundColor: '#D38074',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '70%',
    alignSelf: 'center',
  },
  icons: {
    position: 'absolute',
    alignSelf: 'center',
  },
  textStyle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
    fontSize: 13,
    margin: 10,
  },
  buttonCenter: {
    alignSelf: 'center',
    width: '100%',
    zIndex: 1,
    margin: 10,
  },
  footer: {
    alignSelf: 'center',
    width: '90%',
    zIndex: 1,
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 2,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  footerIcons: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footerImageContent: {
    alignSelf: 'center',
    position: 'absolute',
    paddingTop: 50,
  },
  oder: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
  },
  apple: {
    alignSelf: 'center',
    position: 'absolute',
  },
  image: {
    width: width,
    alignSelf: 'center',
    bottom: 0,
    zIndex: 0,
  },
  containerTitlesAlbum: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 20,
    marginHorizontal: 25,
  },
  textMeine: {
    color: '#D38074',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  textAlle: {
    color: '#12336B',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
    alignSelf: 'flex-end',
  },
  albumImage: {
    borderRadius: 20,
    marginBottom: 10,
    height: 172,
    width: 172,
  },
  imageDetail: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
    width: 150,
  },
});
