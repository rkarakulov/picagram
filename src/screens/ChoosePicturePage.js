import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  PermissionsAndroid,
  Platform,
  BackHandler,
  Image,
  SafeAreaView,
  Dimensions,
  TouchableNativeFeedback,
} from 'react-native';

import { connect } from 'react-redux';

import { updateCartPhotos } from '../libs/card/domain/CartActions';

import ArrowBack from '../assets/icons/arrowLeft.svg';
import LinearGradient from 'react-native-linear-gradient';
import GalleryIcon from '../assets/icons/galleryIcon';
import CameraRoll from '@react-native-community/cameraroll';
import Modal from 'react-native-modal';
import CloseButton from '../assets/icons/modalClose.svg';
import Icon from '../assets/icons/icon.svg';
import SocialTabs from '../components/SocialTabs';

import { getPhotoIndexByPath, getPhotoCount, getPhotoCountSelection } from '../helpers/PhotoFunctions';
/*
 async function hasAndroidPermission() {
 const permission = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;

 const hasPermission = await PermissionsAndroid.check(permission);
 if (hasPermission) {
 return true;
 }

 const status = await PermissionsAndroid.request(permission);
 return status === 'granted';
 }

 async function savePicture() {
 if (Platform.OS === 'android' && !(await hasAndroidPermission())) {
 return;
 }

 CameraRoll.save(tag, {type, album});
 }
 */
const { height, width } = Dimensions.get('window');

class ChoosePicturePage extends Component {
  constructor(props) {
    super(props);
    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
    const selected =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];

    let pictureSelectLimit = 10;
    let pictureSelectLimitMax = 999;
    if (
      this.props.cart &&
      this.props.cart.selectedAlbum &&
      this.props.cart.selectedAlbum.albumItem &&
      this.props.cart.selectedAlbum.albumItem.customFields
    ) {
      if (this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_min_upload) {
        pictureSelectLimit = this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_min_upload;
      }
      if (this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_max_upload) {
        pictureSelectLimitMax = this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_max_upload;
      }
    }
    this.state = {
      ButtonStateHolder: true,
      count: 1,
      data: [],
      selected: selected ? selected : [],
      modal: false,
      value: '',
      source: '',
      pictureSelectLimit: pictureSelectLimit,
      pictureSelectLimitMax: pictureSelectLimitMax,
      modalSpeichern: false,
      modalPreviewPicture: false,
      modalPreviewPictureSource: null,
      // pressed: false,
      // color: '#B42762'
    };
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.unsubscribe();
  }

  handleBackButtonClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  async componentDidMount() {
    if (Platform.OS === 'android') {
      const result = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, {
        title: 'Permission Explanation',
        message: 'ReactNativeForYou would like to access your photos!',
      });
      if (result !== 'granted') {
        //  console.log('Access to pictures was denied');
        return;
      }
    }
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getPhotoData();
    });
    this.getPhotoData();
  }

  updateCartPropsPhotos = (selected) => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    //if (cart[cartIndex]) {
    // console.log('cart index', cartIndex);
    // console.log(cart);
    if (cart && cart[cartIndex]) {
      if (!cart[cartIndex].selected) {
        cart[cartIndex].selected = [];
      }
      cart[cartIndex].selected = selected;
      this.props.updateCartPhotos(cart);
    }
    // }
  };
  getPhotoData = () => {
    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
    const selected =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];

    const item = this.props.route.params && this.props.route.params.item ? this.props.route.params.item : null;
    const type = this.props.route.params && this.props.route.params.type ? this.props.route.params.type : 'local';

    let pictureSelectLimit = 10;
    let pictureSelectLimitMax = 999;
    if (
      this.props.cart &&
      this.props.cart.selectedAlbum &&
      this.props.cart.selectedAlbum.albumItem &&
      this.props.cart.selectedAlbum.albumItem.customFields
    ) {
      if (this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_min_upload) {
        pictureSelectLimit = this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_min_upload;
      }

      if (this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_max_upload) {
        pictureSelectLimitMax = this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_max_upload;
      }
    }

    // this.setState({data: [], pictureSelectLimit: 24, selected: selected});
    // console.log(type, selected, 'slike');
    if (type === 'facebook') {
      this.setState({
        data: item && item.photos && item.photos.data ? item.photos.data : [],
        source: 'facebook',
        pictureSelectLimit: pictureSelectLimit,
        pictureSelectLimitMax: pictureSelectLimitMax,
        selected: selected,
      });
    } else if (type === 'instagram') {
      this.setState({
        data: item && item.data ? item.data : [],
        source: 'instagram',
        pictureSelectLimit: pictureSelectLimit,
        pictureSelectLimitMax: pictureSelectLimitMax,
        selected: selected,
      });
    } else {
      if (item) {
        // console.log(item);
        const fetchParams = {
          first: 200,
          //assetType: 'All',
          groupName: item.title,
          groupTypes: item.groupTypes,
          assetType: 'Photos',
          subType: item.subType,
        };

        if (Platform.OS === 'android') {
          // not supported in android
          delete fetchParams.groupTypes;
        } else {
        }
        // console.log('item', item.title);
        CameraRoll.getPhotos(fetchParams)

          .then((res) => {
            const edges = res && res.edges ? [...res.edges] : [];
            this.setState({
              data: edges,
              source: 'local',
              pictureSelectLimit: pictureSelectLimit,
              pictureSelectLimitMax: pictureSelectLimitMax,
              selected: selected,
            });
          })

          .catch((error) => {
            // console.log(error);
          });
      } else {
        this.setState({
          data: [],
          pictureSelectLimit: pictureSelectLimit,
          pictureSelectLimitMax: pictureSelectLimitMax,
          selected: selected,
        });
      }
    }
  };

  selectAllPhotos = () => {
    let data = [...this.state.data];
    let selected = [...this.state.selected];
    let numSelected = selected.length;
    const value = value;
    let firstLimitSelection = false;
    if (numSelected >= this.state.pictureSelectLimit) firstLimitSelection = true;
    data.map((item, key) => {
      if (
        (!firstLimitSelection && numSelected < this.state.pictureSelectLimit) ||
        (firstLimitSelection && numSelected < this.state.pictureSelectLimitMax)
      ) {
        var index = getPhotoIndexByPath(
          selected,
          this.state.source === 'facebook'
            ? item.images[0].source
            : this.state.source === 'instagram'
            ? item.media_url
            : item.node.image.uri,
        );

        if (index !== -1) {
        } else {
          selected.push({
            path:
              this.state.source === 'facebook'
                ? item.images[0].source
                : this.state.source === 'instagram'
                ? item.media_url
                : item.node.image.uri,
            source:
              this.state.source === 'facebook' ? 'facebook' : this.state.source === 'instagram' ? 'instagram' : 'local',
            selected: true,
            qty: 1,
          });
          numSelected++;
        }
      }
    });
    this.setState({ selected: selected });
    this.updateCartPropsPhotos(selected);
  };

  DeselectAllPhotos = () => {
    this.setState({ selected: [] });
    this.updateCartPropsPhotos([]);
  };

  onBackClick = () => {
    // console.log('on back click');
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  onImagePress = () => {
    this.setState(
      {
        count: this.state.count + 1,
      },
      () => {
        this.props.navigation.navigate('OverallViewPage', {
          selected: this.state.selected,
          type: null,
        });
      },
    );
  };

  nextPage = (type) => {
    this.setState(
      {
        modal: false,
      },
      () => {
        this.props.navigation.navigate('CreatePhotoPageV2', {
          selected: this.state.selected,
          type: type,
        });
      },
    );
    //   if(!this.state.pressed){
    //     this.setState({ pressed: true,color: '#fff'});
    //  } else {
    //    this.setState({ pressed: false, color: color});
    //  }
  };

  onNextClick = () => {
    this.onButtonClick();
  };

  onButtonClick = () => {
    this.setState({
      modal: true,
    });
  };

  render() {
    const value = this.state.value;
    //const count = this.state.selected.length;
    const count = getPhotoCountSelection(this.state.selected);
    let pictureSelectLimit = this.state.pictureSelectLimit;
    let pictureSelectLimitMax = this.state.pictureSelectLimitMax;
    if (count >= this.state.pictureSelectLimit) pictureSelectLimit = this.state.pictureSelectLimitMax;
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    const backgroundModalButtons = TouchableNativeFeedback.Ripple('#C7AEB8', true);

    let strLimit = '';
    let blnUnlimited = false;
    if (pictureSelectLimitMax === 999 || pictureSelectLimitMax === 0 || pictureSelectLimitMax > 999) {
      if (pictureSelectLimit === pictureSelectLimitMax) {
        strLimit = 300;
      }
      blnUnlimited = true;
    }
    if (strLimit == '') strLimit = pictureSelectLimit;

    let strCount = '';
    if (count > 300) {
      strCount = count + ' Selected Photo(s)';
    } else {
      if (blnUnlimited) {
        if (count < this.state.pictureSelectLimit) strCount = count + '/' + strLimit;
        else strCount = count + ' Selected Photo(s)';
      } else strCount = count + '/' + strLimit;
    }
    // console.log('selektovana slika ' + this.state.modalPreviewPictureSource);
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => this.onBackClick()}>
            <View style={{ padding: 10 }}>
              <ArrowBack />
            </View>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              width: '90%',
            }}
          >
            <GalleryIcon />
            <Text style={styles.headerTitle}>{strCount}</Text>
          </View>
          {/*}
           <TouchableOpacity
           onPress={() => this.setState({modalSpeichern: true})}>
           <Text style={styles.speichern}>SPEICHERN</Text>
           </TouchableOpacity>*/}
        </View>

        <SocialTabs type={this.state.source} navigation={this.props.navigation} />

        <View style={styles.containerTitlesAlbum}>
          <TouchableOpacity onPress={() => this.onButtonClick()}>
            <Text style={styles.textMeine}>ORDNER</Text>
          </TouchableOpacity>

          {/* {count < pictureSelectLimit && ( */}
          <TouchableOpacity
            onPress={() => {
              this.selectAllPhotos();
            }}
          >
            <Text style={styles.textAlle}>ALLE AUSWÄHLEN</Text>
          </TouchableOpacity>
          {/* )}
           {this.state.selected && count > 0 && (
           <TouchableOpacity
           onPress={() => {
           this.DeselectAllPhotos();
           }}>
           <Text style={styles.textAlle}>{'ABWÄHLEN'}</Text>
           </TouchableOpacity>
           )} */}
        </View>

        {this.state.source === 'local' && (
          <FlatList
            style={{ flex: 1 }}
            columnWrapperStyle={{ justifyContent: 'flex-start' }}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.data}
            numColumns={3}
            contentContainerStyle={{
              paddingBottom: 30,

              // alignSelf: 'center',
              // width: '100%',
              // flexDirection: 'row',
              // flexWrap: 'wrap',
            }}
            renderItem={({ item }) => {
              let blnSelected = false;

              var index = getPhotoIndexByPath(
                [...this.state.selected],
                this.state.source === 'facebook' ? item.images[0].source : item.node.image.uri,
              );
              //var index = this.state.selected.indexOf(item.node.image.uri);
              if (index !== -1) {
                blnSelected = true;
              }
              return (
                <View
                  style={{
                    flex: 1 / 3,
                  }}
                >
                  <TouchableOpacity
                    style={{ padding: 3 }}
                    /*
                     onLongPress={() => {
                     this.setState({
                     modalPreviewPictureSource: item.node.image.uri,
                     modalPreviewPicture: true,
                     });
                     }}
                     */
                    onPress={() => {
                      let selected = [...this.state.selected];

                      var index = getPhotoIndexByPath(selected, item.node.image.uri);
                      if (index !== -1) {
                        selected.splice(index, 1);
                      } else {
                        if (count >= pictureSelectLimitMax) {
                          alert('Max number of photos ' + pictureSelectLimit + ' is selected!');
                        } else {
                          selected.push({
                            path: item.node.image.uri,
                            source: 'local',
                            selected: true,
                            qty: 1,
                          });
                        }
                      }
                      this.setState({
                        selected: selected,
                        //   modalPreviewPicture: true,
                      });

                      this.updateCartPropsPhotos(selected);
                    }}
                  >
                    <View
                      style={{
                        position: 'relative',

                        justifyContent: 'center',
                      }}
                    >
                      {blnSelected && (
                        <View
                          style={{
                            position: 'absolute',
                            zIndex: 9998,
                            width: '100%',
                            height: '100%',
                            // margin: 5,
                            left: 0,
                            top: 0,
                            backgroundColor: 'rgba(255,255,255,0.4)',
                          }}
                        />
                      )}
                      {blnSelected && (
                        <Icon
                          style={{
                            position: 'absolute',
                            zIndex: 9999,
                            alignSelf: 'flex-end',
                            bottom: 0,
                          }}
                        />
                      )}
                      <Image
                        style={{
                          zIndex: 9,
                          width: '100%',
                          height: 114,
                          // margin: 5,
                        }}
                        source={{ uri: item.node.image.uri }}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        )}
        {this.state.source === 'facebook' && (
          <FlatList
            style={{ flex: 1 }}
            columnWrapperStyle={{ justifyContent: 'flex-start' }}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.data}
            numColumns={3}
            contentContainerStyle={{
              paddingBottom: 30,
              // alignSelf: 'center',
              // width: '100%',
              // flexDirection: 'row',
              // flexWrap: 'wrap',
            }}
            renderItem={({ item }) => {
              let blnSelected = false;

              var index = getPhotoIndexByPath(
                [...this.state.selected],
                this.state.source === 'facebook' ? item.images[0].source : item.node.image.uri,
              );
              //var index = this.state.selected.indexOf(item.images[0].source);
              if (index !== -1) {
                blnSelected = true;
              }
              return (
                <View
                  style={{
                    flex: 1 / 3,
                    flexDirection: 'column',
                    padding: 3,
                  }}
                >
                  <TouchableOpacity
                    /*
                     onLongPress={() => {
                     this.setState({
                     modalPreviewPictureSource: item.images[0].source,
                     modalPreviewPicture: true,
                     });
                     }}
                     */
                    onPress={() => {
                      let selected = [...this.state.selected];

                      var indexPhoto = getPhotoIndexByPath(
                        selected,
                        this.state.source === 'facebook' ? item.images[0].source : item.node.image.uri,
                      );
                      //var index = selected.indexOf(item.images[0].source);
                      if (indexPhoto !== -1) {
                        selected.splice(indexPhoto, 1);
                      } else {
                        if (count >= pictureSelectLimitMax) {
                          alert('Max number of photos ' + pictureSelectLimit + ' is selected!');
                        } else {
                          selected.push({
                            path: item.images[0].source,
                            source: 'facebook',
                            selected: true,
                            qty: 1,
                          });
                        }
                      }
                      this.setState({ selected: selected });
                      this.updateCartPropsPhotos(selected);
                    }}
                  >
                    {/* <View
                     style={{
                     position: 'relative',
                     justifyContent: 'center',
                     }}> */}
                    {blnSelected && (
                      <View
                        style={{
                          position: 'absolute',
                          zIndex: 9998,
                          width: '100%',
                          height: '100%',
                          // margin: 5,
                          left: 0,
                          top: 0,
                          backgroundColor: 'rgba(255,255,255,0.4)',
                        }}
                      ></View>
                    )}
                    {blnSelected && (
                      <Icon
                        style={{
                          position: 'absolute',
                          zIndex: 9999,
                          alignSelf: 'flex-end',
                          bottom: 0,
                        }}
                      />
                    )}

                    <Image
                      style={{
                        zIndex: 9,
                        width: '100%',
                        height: 114,
                        // margin: 5,
                      }}
                      source={{ uri: item.images[0].source }}
                    />
                    {/* </View> */}
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        )}
        {this.state.source === 'instagram' && (
          <FlatList
            style={{ flex: 1 }}
            columnWrapperStyle={{ justifyContent: 'flex-start' }}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.data}
            numColumns={3}
            contentContainerStyle={{
              paddingBottom: 30,
              // alignSelf: 'center',
              // width: '100%',
              // flexDirection: 'row',
              // flexWrap: 'wrap',
            }}
            renderItem={({ item }) => {
              let blnSelected = false;

              var index = getPhotoIndexByPath([...this.state.selected], item.media_url);
              //var index = this.state.selected.indexOf(item.images[0].source);
              if (index !== -1) {
                blnSelected = true;
              }
              return (
                <View
                  style={{
                    flex: 1 / 3,
                    flexDirection: 'column',
                    padding: 3,
                  }}
                >
                  <TouchableOpacity
                    /*
                     onLongPress={() => {
                     this.setState({
                     modalPreviewPictureSource: item.media_url,
                     modalPreviewPicture: true,
                     });
                     }}
                     */
                    onPress={() => {
                      let selected = [...this.state.selected];

                      var indexPhoto = getPhotoIndexByPath(selected, item.media_url);
                      //var index = selected.indexOf(item.images[0].source);
                      if (indexPhoto !== -1) {
                        selected.splice(indexPhoto, 1);
                      } else {
                        if (count >= pictureSelectLimitMax) {
                          alert('Max number of photos ' + pictureSelectLimit + ' is selected!');
                        } else {
                          selected.push({
                            path: item.media_url,
                            source: 'instagram',
                            selected: true,
                            qty: 1,
                          });
                        }
                      }
                      this.setState({ selected: selected });
                      this.updateCartPropsPhotos(selected);
                    }}
                  >
                    {/* <View
                     style={{
                     position: 'relative',
                     justifyContent: 'center',
                     }}> */}
                    {blnSelected && (
                      <View
                        style={{
                          position: 'absolute',
                          zIndex: 9998,
                          width: '100%',
                          height: '100%',
                          // margin: 5,
                          left: 0,
                          top: 0,
                          backgroundColor: 'rgba(255,255,255,0.4)',
                        }}
                      ></View>
                    )}
                    {blnSelected && (
                      <Icon
                        style={{
                          position: 'absolute',
                          zIndex: 9999,
                          alignSelf: 'flex-end',
                          bottom: 0,
                        }}
                      />
                    )}

                    <Image
                      style={{
                        zIndex: 9,
                        width: '100%',
                        height: 114,
                        // margin: 5,
                      }}
                      source={{ uri: item.media_url }}
                    />
                    {/* </View> */}
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        )}
        <Modal
          onRequestClose={() => this.setState({ modalSpeichern: false })}
          transparent={true}
          isVisible={this.state.modalSpeichern}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <View style={styles.modalContent}>
            <View style={styles.innerModalContent}>
              <TouchableOpacity onPress={() => this.setState({ modalSpeichern: false })} style={styles.closeButton}>
                <CloseButton />
              </TouchableOpacity>
              <Text style={styles.modalTitle}>SPEICHERN?</Text>
              <Text style={styles.modalSubTitle}>
                Bist du bereit, deinen Entwurf zu sichern,{'\n'}um ihn später zu beenden?
              </Text>

              <View style={[styles.modalButton, { borderRadius: 20, overflow: 'hidden' }]}>
                <TouchableNativeFeedback
                  background={background}
                  useForeground={true}
                  onPress={() => this.setState({ modalSpeichern: false })}
                >
                  <View style={{ width: '100%', padding: 15 }}>
                    <Text style={styles.modalButtonText}>NACH DATUM</Text>
                  </View>
                </TouchableNativeFeedback>
              </View>

              <TouchableOpacity>
                <Text style={styles.Löschen}>Löschen</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        {/*<Modal
         onRequestClose={() => this.setState({ modal: false })}
         transparent={true}
         isVisible={this.state.modal}
         style={{
         flex: 1,
         margin: 0,
         justifyContent: 'center',
         alignItems: 'center',
         }}>
         <View style={styles.modalContent}>
         <View style={styles.innerModalContentFotoreihenfolge}>
         <View style={styles.modalTitleContainer}>
         <Text style={styles.modalTitle}>FOTOREIHENFOLGE</Text>

         <TouchableOpacity
         style={{ padding: 10 }}
         onPress={() => this.setState({ modal: false })}>
         <CloseButton />
         </TouchableOpacity>
         </View>

         <Text style={styles.modalSubtitle}>
         Wie möchtest du deine Fotos anordnen?
         </Text>
         <View style={{ paddingVertical: 10 }}>
         <View
         style={[
         styles.modalButtons,
         { overflow: 'hidden', borderRadius: 20 },
         ]}>
         <TouchableNativeFeedback
         background={backgroundModalButtons}
         useForeground={true}
         onPress={() => this.nextPage('data')}>
         <View style={{ width: '100%', padding: 15 }}>
         <Text style={styles.buttonText}>NACH DATUM</Text>
         </View>
         </TouchableNativeFeedback>
         </View>
         <View
         style={[
         styles.modalButtons,
         { overflow: 'hidden', borderRadius: 20 },
         ]}>
         <TouchableNativeFeedback
         useForeground={true}
         background={backgroundModalButtons}
         onPress={() => this.nextPage('manual')}>
         <View style={{ width: '100%', padding: 15 }}>
         <Text style={styles.buttonText}>
         REINFOLGE{'\n'}BEIBEHALTEN
         </Text>
         </View>
         </TouchableNativeFeedback>
         </View>
         </View>
         </View>
         </View>
         </Modal>
         */}
        <Modal
          onRequestClose={() => this.setState({ modalPreviewPicture: false })}
          transparent={true}
          isVisible={this.state.modalPreviewPicture}
          style={{
            flex: 1,
            margin: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <TouchableOpacity
            onPress={() => this.setState({ modalPreviewPicture: false })}
            style={styles.closeButtonCustom}
          >
            <CloseButton />
          </TouchableOpacity>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
            }}
          >
            {this.state.modalPreviewPictureSource !== null && (
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <Image
                  style={{
                    width: 400,
                    height: '75%',
                    resizeMode: 'contain',
                  }}
                  // source={require('../assets/images/geschenkkarte.jpg')}
                  source={{ uri: this.state.modalPreviewPictureSource }}
                />
              </View>
            )}
          </View>
        </Modal>
        <View
          style={{
            paddingTop: this.state.data && this.state.data.length > 0 ? 0 : 20,
          }}
        >
          <View style={[styles.footer, { borderRadius: 20, overflow: 'hidden', paddingBottom: 5 }]}>
            <TouchableNativeFeedback
              onPress={() => this.nextPage('data')}
              background={background}
              useForeground={true}
              disabled={this.state.pictureSelectLimit > count}
            >
              <View>
                <LinearGradient
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                  colors={['#FFD06C', '#D78275', '#B42762']}
                  style={[styles.LinearGradient, this.state.pictureSelectLimit > count ? styles.buttonDisabled : '']}
                >
                  <Text style={styles.textFooter}>WEITER</Text>
                </LinearGradient>
              </View>
            </TouchableNativeFeedback>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 15,
    backgroundColor: '#FFFFFF',
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  subHeader: {
    backgroundColor: '#D38074',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '70%',
    alignSelf: 'center',
  },
  smartphoneIcon: {
    position: 'absolute',
    alignSelf: 'center',
  },
  containerTitlesAlbum: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 20,
    marginHorizontal: 25,
  },
  textMeine: {
    color: '#D38074',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  textAlle: {
    color: '#12336B',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
    alignSelf: 'flex-end',
  },
  footer: {
    alignSelf: 'center',
    bottom: 30,
    width: '90%',
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.17,
    shadowRadius: 2.49,
    elevation: 2,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  whiteFooter: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    padding: '5%',
  },

  speichern: {
    color: '#12336B',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
    textDecorationLine: 'underline',
  },
  buttonDisabled: {
    opacity: 0.5,
  },
  modalContent: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerModalContentFotoreihenfolge: {
    backgroundColor: '#FFFFFF',
    width: '90%',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalTitleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    width: '70%',
    justifyContent: 'space-between',
    marginHorizontal: 10,
    alignSelf: 'flex-end',
    // backgroundColor:'green',
    paddingVertical: 10,
  },
  modalSubtitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 13,
  },
  modalButtons: {
    // padding: 15,
    width: 193,
    // paddingHorizontal: 40,
    borderWidth: 1,
    borderColor: '#B42762',
    borderRadius: 20,
    margin: 5,
    //  marginVertical: 10,
  },
  buttonText: {
    color: '#B42762',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
    fontSize: 13,
  },
  innerModalContent: {
    backgroundColor: '#D38074',
    width: '90%',
    // height: '30%',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalSubTitle: {
    fontSize: 12,
    textAlign: 'center',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    paddingVertical: 10,
  },
  modalTitle: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  modalButton: {
    borderWidth: 1,
    width: 193,
    alignItems: 'center',
    borderColor: '#FFFFFF',
    borderRadius: 20,
    marginVertical: 10,
  },
  modalButtonText: {
    color: '#FFFFFF',
    paddingHorizontal: '20%',
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  closeButton: {
    position: 'relative',
    alignSelf: 'flex-end',
    padding: 20,
  },
  Löschen: {
    textDecorationLine: 'underline',
    color: '#FFFFFF',
    marginBottom: 20,
    fontSize: 12,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  closeButtonCustom: {
    position: 'absolute',
    top: 40,
    right: 20,
    zIndex: 9999,

    borderRadius: 40,
    padding: 15,
    backgroundColor: 'rgba(255,255,255,0.5)',
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
  };
}

export default connect(mapStateToProps, { updateCartPhotos })(ChoosePicturePage);
