import axios from 'axios';

// Global Axios Config
export const axiosInstance = axios.create({
  baseURL: 'https://picamory.devla.dev/store-api/',
  headers: {name: 'Content-Type', value: 'application/json'},
});
