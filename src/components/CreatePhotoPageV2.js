import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  BackHandler,
  TouchableOpacity,
  Image,
  FlatList,
  TextInput,
  Platform,
  Dimensions,
  SafeAreaView,
  KeyboardAvoidingView,
  ImageBackground,
  TouchableNativeFeedback,
  TouchableNativeFeedbackBase,
  Pressable,
  Alert,
} from 'react-native';
import FotobuchIcon from '../assets/icons/fotobuchIcon';
import ArrowBack from '../assets/icons/arrowLeft.svg';
import AddText from '../assets/icons/addText.svg';
import GalerieIcon from '../assets/icons/galerieIcon.svg';
import LayoutIcon from '../assets/icons/layout.svg';
import TextIcon from '../assets/icons/textIcon.svg';
import DeleteIcon from '../assets/icons/deleteIcon.svg';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import ArrowBottom from '../assets/icons/arrowBottom.svg';
import GalerieIconWhite from '../assets/icons/galerieIconWhite.svg';
import AddImageFromLibrary from '../assets/icons/plusImage.svg';
import LayoutIconWhite from '../assets/icons/layoutIconWhite';
import TextEditIcon from '../assets/icons/textEditIcon.svg';
import DeleteIconWhite from '../assets/icons/deleteIconWhite.svg';
import CloseButton from '../assets/icons/modalClose.svg';
import AddPage from '../assets/icons/plusIcon.svg';
import ProductLayout from '../components/ProductLayout';
import ProductSingleLayout from './ProductSingleLayout';
import ProductLayoutHochformat from '../components/ProductLayoutHochformat';
import ProductPostcardLayout from './ProductPostcardLayout';
import ProductLayoutSquare from './ProductLayoutSquare';
import ProductLayoutQuare from '../components/ProductLayoutQuare';
import { connect } from 'react-redux';
import { showAlert } from '../helpers/AlertManager';
import { updateCartPhotos, updateCartOptions } from '../libs/card/domain/CartActions';
import { getPhotoIndexByPath } from '../helpers/PhotoFunctions';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

class CreatePhotoPageV2 extends Component {
  constructor(props) {
    super(props);

    this._flatList = null;
    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;

    const pageLayout =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].album
        ? this.props.cart.cart[selectedIndex].album.layout
        : 'square';

    const pageName =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].albumItem
        ? this.props.cart.cart[selectedIndex].albumItem.productName
        : 'FOTO';

    const selected =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];
    const pages =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].pages
        ? this.props.cart.cart[selectedIndex].pages
        : [];

    this.state = {
      value: '',
      height: 0,
      showCharacters: false,
      showCharactersMessage: false,
      selected: selected,
      count: 0,
      type: 'manual',
      modalPostcardTitle: false,
      modalTitle: false,
      modalTextIndex: null,
      input1: false,
      input2: false,
      modalTextValue: '',
      modalMessageIndex: null,
      modalMessageValue: '',
      modalSelectedItem: null,
      modal: false,
      pageName: pageName,
      modalGalerie: false,
      modalLayout: false,
      modalText: false,
      modalEntfernen: false,
      modalSpeichern: false,
      viewableItems: [],
      pages: pages,
      pageLayout: '',
      galleryOpenedWithImage: null,
    };

    this.onPress = this.onPress.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onPressMessage = this.onPressMessage.bind(this);
    this.onBlurMessage = this.onBlurMessage.bind(this);
  }

  updateSize = (height) => {
    this.setState({
      height,
    });
  };

  // Render_Square_Layout_header = () => {
  //   const Square_header_View = (
  //     <View style={{
  //       marginVertical: 10, alignSelf: 'center', width: 200, height: 200, overflow: 'hidden',
  //       shadowOffset: { width: 10, height: 10 },
  //       shadowColor: '#000',
  //       shadowOpacity: 1,
  //       elevation: 20,
  //       backgroundColor: '#000',
  //     }}>
  //       <Image source={require('../assets/images/PicamorySquare.png')}
  //         style={{
  //           width: 200, height: 200
  //         }} />
  //     </View>
  //   );
  //   return Square_header_View;
  // };

  // Render_Hochformat_Layout_header = () => {
  //   const Hochformat_header_View = (
  //     <View style={{
  //       marginVertical: 10, alignSelf: 'center', width: 200, height: 200, overflow: 'hidden',
  //       shadowOffset: { width: 10, height: 10 },
  //       shadowColor: '#000',
  //       shadowOpacity: 1,
  //       elevation: 20,
  //       backgroundColor: '#000',
  //     }}>
  //       <Image source={require('../assets/images/PicamoryHochformat.png')}
  //         style={{
  //           width: 200, height: 200
  //         }} />
  //     </View>
  //   );
  //   return Hochformat_header_View;
  // };

  onPress() {
    this.setState({ showCharacters: true });
    this.setState({ showCharactersMessage: false });
  }

  onBlur() {
    this.setState({ showCharacters: this.state.showCharacters });
  }

  onPressMessage() {
    this.setState({ showCharactersMessage: true });
    this.setState({ showCharacters: false });
  }

  onBlurMessage() {
    this.setState({ showCharactersMessage: this.state.showCharactersMessage });
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.unsubscribe();
  }

  handleBackButtonClick = () => {
    const { goBack } = this.props.navigation;
    goBack(null);
    return true;
  };

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      this.loadPropsData();
    });
    this.loadPropsData();
  }

  loadPropsData = () => {
    const selectedIndex = this.props.cart.selectedAlbumIndex ? this.props.cart.selectedAlbumIndex : 0;
    const selected =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];

    const pageLayout =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].album
        ? this.props.cart.cart[selectedIndex].album.layout
        : 'square';

    const pageName =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].albumItem
        ? this.props.cart.cart[selectedIndex].albumItem.productName
        : 'FOTO';
    const pages =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].pages
        ? this.props.cart.cart[selectedIndex].pages
        : [];

    const font =
      this.props.cart.cart[selectedIndex] && this.props.cart.cart[selectedIndex].font
        ? this.props.cart.cart[selectedIndex].font
        : 'CourierPrime-Regular';

    //const selected = this.props.route.params.selected;
    // console.log('ONFOCUS CALLED', this.props.cart.cart[selectedIndex].selected);
    const type = this.props.route.params.selected;
    this.setState({
      modalGalerie: false,
      modal: false,
      modalLayout: false,
      modalText: false,
      modalEntfernen: false,
      modalSpeichern: false,
      modalSelectedItem: null,
      modalTextIndex: null,
      modalTextValue: '',
      modalMessageValue: '',
      selected: selected,
      pages: pages,
      selectedOptions: [],
      type: type,
      font: font,
      pageName: pageName,
      galleryOpenedWithImage: null,
      singleLayout: false,
      pageLayout: '',
    });
  };

  onBackClick = () => {
    const { goBack } = this.props.navigation;
    goBack(null);
    return true;
  };

  setPageLayout = (item) => {
    const viewable = [...this.state.viewableItems];
    const pages = [...this.state.pages];
    if (viewable && viewable.length > 0) {
      const index = pages.findIndex((obj) => obj.index === viewable[0].index);
      if (index !== -1) {
        if (pages[index]) {
          pages[index].width = item.leftWidth;
          pages[index].height = item.leftHeight;
          pages[index].leftFull = item.leftFull;
          pages[index].rightFull = item.rightFull;
        }
      } else {
        pages.push({
          index: viewable[0].index,
          width: item.leftWidth,
          height: item.leftHeight,
          leftFull: item.leftFull,
          rightFull: item.rightFull,
        });
      }

      const index2 = pages.findIndex((obj) => obj.index === viewable[1].index);
      if (index2 !== -1) {
        if (pages[index2]) {
          pages[index2].width = item.rightWidth;
          pages[index2].height = item.rightHeight;
          pages[index2].leftFull = item.leftFull;
          pages[index2].rightFull = item.rightFull;
        }
      } else {
        pages.push({
          index: viewable[1].index,
          width: item.rightWidth,
          height: item.rightHeight,
          leftFull: item.leftFull,
          rightFull: item.rightFull,
        });
      }
      this.setState({ modalLayout: false });
      this.setState({
        pages: pages,
        modalLayout: false,
      });
      this.updateAlbumData(pages);
    }
  };
  setSingleLayout = (item) => {
    const viewable = [...this.state.viewableItems];
    const pages = [...this.state.pages];
    if (viewable && viewable.length > 0) {
      const index = pages.findIndex((obj) => obj.index === viewable[0].index);
      if (index !== -1) {
        if (pages[index]) {
          pages[index].width = item.leftWidth;
          pages[index].height = item.leftHeight;
          pages[index].leftFull = item.leftFull;
          pages[index].rightFull = item.rightFull;
        }
      } else {
        pages.push({
          index: viewable[0].index,
          width: item.leftWidth,
          height: item.leftHeight,
          leftFull: item.leftFull,
          rightFull: item.rightFull,
        });
      }
      this.setState({ modalLayout: false });
      this.setState({
        pages: pages,
        modalLayout: false,
      });
      this.updateAlbumData(pages);
    }
  };
  onViewableItemsChanged = ({ viewableItems, changed }) => {
    this.setState({
      viewableItems: [...viewableItems],
    });
  };

  onGesamtansichtClick = (item) => {
    this.props.navigation.navigate('OverallViewPage', {
      selected: this.state.selected,
      item: item,
    });
  };

  openPhoto = (photo, index = null) => {
    if (photo.includes('null-')) {
      this.setState({ modalGalerie: true, galleryOpenedWithImage: photo });
    } else {
      this.props.navigation.navigate('EditPhotoPage', {
        selected: this.state.selected,
        item: photo,
        index: index,
      });
    }
  };

  onAddImageClick = () => {
    this.props.navigation.navigate('AlbumPage');
  };
  onButtonClick = () => {
    this.updateAlbumDataActive();
    this.props.navigation.navigate('WarenkorbPage');
  };

  updateAlbumData = (data) => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    if (!cart[cartIndex]) {
      cart[cartIndex] = {};
    }
    cart[cartIndex].pages = data;
    this.props.updateCartOptions(cart);
  };

  updateAlbumDataActive = () => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    if (!cart[cartIndex]) {
      cart[cartIndex] = {};
    }
    cart[cartIndex].finished = true;
    this.props.updateCartOptions(cart);
  };

  updateCartPropsPhotos = (selected) => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    if (!cart[cartIndex].selected) {
      cart[cartIndex].selected = [];
    }
    cart[cartIndex].selected = selected;
    this.props.updateCartPhotos(cart);
  };
  clearCartPhotosForSelectedAlbum = () => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    cart[cartIndex].selected = [];
    this.props.updateCartPhotos(cart);
    this.setState({ selected: [] });
    this.setState({ modalEntfernen: false });
    // console.log('CLEAR');
  };
  addRowToPhotos = (selected) => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    // console.log('SELECTED', selected);
    // console.log('CART INDEX', cart[cartIndex].selected);
    // console.log('STATE AT START', this.state.selected);
    let indexToInsert = null;
    for (let i = 0; i < cart[cartIndex].selected.length; i += 2) {
      if (
        cart[cartIndex].selected[i].path === selected[0].path &&
        cart[cartIndex].selected[i + 1].path === selected[1].path
      ) {
        indexToInsert = i;
        //('INDEX FOUND', i);
        break;
      }
    }
    let emptyObj = {
      path: `null-${Date.now()}`,
      selected: true,
      source: 'local',
    };

    let newArr = [
      ...cart[cartIndex].selected.slice(0, indexToInsert + 2),
      { ...emptyObj },
      { ...emptyObj, path: `null-${Date.now() + 1}` },
      ...cart[cartIndex].selected.slice(indexToInsert + 2, cart[cartIndex].selected.length),
    ];

    // console.log('SELECTED', newArr);
    cart[cartIndex].selected = [...newArr];
    // if (!cart[cartIndex].selected) cart[cartIndex].selected = [];
    // cart[cartIndex].selected = selected;
    // console.log('POZIVA SE UPDATE CART PHOTOS', selected);
    this.props.updateCartPhotos(cart);
    // console.log('STATE AT END', newArr);

    this.setState({ selected: [...newArr] });
  };
  setFont = (font) => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    if (!cart[cartIndex]) {
      cart[cartIndex] = {};
    }
    cart[cartIndex].font = font;
    this.props.updateCartOptions(cart);
    this.setState({ font: font });
    this.setState({ modalText: false });
  };
  changeImagePath = (img) => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    cart[cartIndex].selected.splice(
      cart[cartIndex].selected.findIndex((e) => e.path === img),
      1,
    );
    cart[cartIndex].selected.find((e) => e.path === this.state.galleryOpenedWithImage).path = img;
    this.props.updateCartPhotos(cart);
    this.setState({
      selected: [...cart[cartIndex].selected],
      modalGalerie: false,
      galleryOpenedWithImage: null,
    });
  };
  speichernOptions = () => {
    //const selectedOptions = [...this.state.selectedOptions];
    //const selectedItems = [...this.state.selected];
    //selectedOptions.map((item, index) => {
    //  console.log('item', item, index);
    ////  if (!selectedItems[index].options) selectedItems[index].options = {};
    //});
    /*
     this.state.selectedOptions.findIndex(
     obj => obj.index === index,
     )
     selectedItems[index].options = {
     text: e,
     selected: true,
     };
     */
    this.setState({ modalSpeichern: false });
  };
  setStateValues = (props) => {
    let values = { ...props };
    this.setState({ ...values });
  };
  removeOneImage = (path) => {
    let cart = [...this.props.cart.cart];
    const cartIndex = this.props.cart.selectedAlbumIndex;
    cart[cartIndex].selected.splice(
      cart[cartIndex].selected.findIndex((e) => e.path == path),
      1,
    );
    this.props.updateCartPhotos(cart);

    this.setState({ selected: [...cart[cartIndex].selected] });
  };

  renderMoreImagesBox(showFooterAddMoreImages) {
    if (showFooterAddMoreImages) {
      return (
        <View style={styles.touchableNativeFeedbackButton}>
          <TouchableNativeFeedback useForeground={true} onPress={() => this.openPhoto('null-', null)}>
            <View style={styles.AddImagesButton}>
              <Text style={styles.textButtonAddImages}>ADD MORE IMAGES!</Text>
            </View>
          </TouchableNativeFeedback>
        </View>
      );
    } else {
      return <View />;
    }
  }

  renderLayouts(item) {
    const leftWidth = item.leftWidth - 25;
    const rightWidth = item.rightWidth - 25;
    const leftHeight = item.leftHeight - 25;
    const rightHeight = item.rightHeight - 25;
    return (
      <TouchableOpacity onPress={() => this.setPageLayout(item)}>
        <View style={{ flexDirection: 'row', paddingHorizontal: 3 }}>
          <View style={[styles.layoutBox, { alignSelf: 'center' }]}>
            <View style={[styles.innerLayoutBox, { height: leftHeight, width: leftWidth }]} />
          </View>
          <View style={[styles.layoutBox, { alignSelf: 'center' }]}>
            <View style={[styles.innerLayoutBox, { height: rightHeight, width: rightWidth }]} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  renderSingleLayouts(item) {
    const leftWidth = item.leftWidth - 25;
    const rightWidth = item.rightWidth - 25;
    const leftHeight = item.leftHeight - 25;
    const rightHeight = item.rightHeight - 25;
    return (
      <TouchableOpacity onPress={() => this.setSingleLayout(item)}>
        <View style={{ flexDirection: 'row', paddingHorizontal: 3 }}>
          <View style={[styles.layoutBox, { alignSelf: 'center' }]}>
            <View style={[styles.innerLayoutBox, { height: leftHeight, width: leftWidth }]}>
              {/* <GalerieIcon /> */}
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    let newStyle = {
      height,
    };
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    const backgroundButtonsSubheader = TouchableNativeFeedback.Ripple('#D38074', true);
    const layouts = [
      {
        leftWidth: 100,
        leftHeight: 100,
        rightWidth: 100,
        rightHeight: 100,
        leftFull: true,
        rightFull: true,
      },
      {
        leftWidth: 100,
        leftHeight: 100,
        rightWidth: 70,
        rightHeight: 70,
        leftFull: true,
        rightFull: false,
      },
      {
        leftWidth: 70,
        leftHeight: 70,
        rightWidth: 100,
        rightHeight: 100,
        leftFull: false,
        rightFull: true,
      },
      {
        leftWidth: 50,
        leftHeight: 50,
        rightWidth: 50,
        rightHeight: 50,
        leftFull: false,
        rightFull: false,
      },
    ];

    const selectedPhotos =
      this.state.selected && this.state.selected.length > 0
        ? this.state.selected.filter(function (item) {
            return item.selected;
          })
        : [];

    /*
     const selectedIndex = this.props.cart.selectedAlbumIndex
     ? this.props.cart.selectedAlbumIndex
     : 0;
     */

    let modalTextValue = '';
    if (this.state.modalTextIndex && this.state.modalTextIndex > -1) {
      modalTextValue =
        this.state.selected[this.state.modalTextIndex].options &&
        this.state.selected[this.state.modalTextIndex].options.text
          ? this.state.selected[this.state.modalTextIndex].options.text
          : '';
    }
    // modalMessageValue =
    //   this.state.selected[this.state.modalTextIndex].options &&
    //     this.state.selected[this.state.modalTextIndex].options.textMessage
    //     ? this.state.selected[this.state.modalTextIndex].options.textMessage
    //     : '';
    let modalMessageValue = '';
    if (this.state.modalMessageIndex && this.state.modalMessageIndex > -1) {
      modalMessageValue =
        this.state.selected[this.state.modalMessageIndex].options &&
        this.state.selected[this.state.modalMessageIndex].options.textMessage
          ? this.state.selected[this.state.modalMessageIndex].options.textMessage
          : '';
    }
    // console.log(this.state.viewableItems, 'items');
    // const Item = ({item}) => {
    //   return (

    //     <View style={{flex: 1,alignItems:
    // 'center',alignSelf:'center',width:'100%',backgroundColor:'yellow',justifyContent:'flex-start'}}>

    //       <View style={{flex:1,alignSelf:'flex-start'}}>
    //         <TouchableOpacity style={{backgroundColor:'green',}} onPress={() => {}}>
    //           <Text>jedan</Text>
    //            </TouchableOpacity>
    //       </View>
    //     </View>
    //   )
    // }

    let showFooterOptions = true;
    let showFooterTextOption = false;
    let showFooterLayoutOption = false;
    let showFooterRemoveOption = false;
    let showFooterGalleryOption = false;
    let numberOfPages = 1;

    let cfLayoutType = '';
    let cfProductType = '';
    if (
      this.props.cart &&
      this.props.cart.selectedAlbum &&
      this.props.cart.selectedAlbum.albumItem &&
      this.props.cart.selectedAlbum.albumItem.customFields
    ) {
      if (
        this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_page_type &&
        this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_page_type === 'double'
      ) {
        numberOfPages = 2;
      }

      if (
        this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_text &&
        this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_text === true
      ) {
        showFooterTextOption = true;
      } else {
        showFooterTextOption = false;
      }

      if (
        this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_footer_options &&
        this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_footer_options === true
      ) {
        showFooterOptions = true;
      } else {
        showFooterOptions = false;
      }

      if (
        this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_footer_options_layout &&
        this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_footer_options_layout === true
      ) {
        showFooterLayoutOption = true;
      } else {
        showFooterLayoutOption = false;
      }

      if (
        this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_footer_options_remove &&
        this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_footer_options_remove === true
      ) {
        showFooterRemoveOption = true;
      } else {
        showFooterRemoveOption = false;
      }

      if (
        this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_footer_options_gallery &&
        this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_footer_options_gallery === true
      ) {
        showFooterGalleryOption = true;
      } else {
        showFooterGalleryOption = false;
      }

      if (this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_layout) {
        cfLayoutType = this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_layout;
      }
      if (this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_product_type) {
        cfProductType = this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_product_type;
      }
    }

    let aryMoreImagesTypes = ['box_print_minivintage', 'print_vintage', 'print_minivintage', 'print_10x15'];

    let showFooterAddMoreImages = false;

    if (aryMoreImagesTypes.includes(cfProductType)) {
      showFooterAddMoreImages = true;
    }

    let layoutMainType = 'portrait';

    if (
      this.props.cart &&
      this.props.cart.selectedAlbum &&
      this.props.cart.selectedAlbum.albumItem &&
      this.props.cart.selectedAlbum.albumItem.customFields
    ) {
      if (this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_layout) {
        layoutMainType = this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_layout;
      }
    }

    /*
     console.log(
     'text',
     this.props.cart.selectedAlbum.albumItem.customFields
     .pm_product_designer_text,
     showFooterTextOption,
     );
     */

    console.log('layout ' + this.state.pageLayout + ' - cf: ' + cfLayoutType);

    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => this.onBackClick()}>
            <View style={{ padding: 10 }}>
              <ArrowBack />
            </View>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              width: '90%',
            }}
          >
            <FotobuchIcon />
            <Text style={styles.titleHeader}>{this.state.pageName}</Text>
          </View>
        </View>

        <FlatList
          style={{
            opacity: this.state.modalLayout ? 0.8 : 1,
          }}
          ref={(fl) => (this._flatList = fl)}
          keyExtractor={(item, index) => index.toString()}
          data={selectedPhotos}
          numColumns={numberOfPages}
          contentContainerStyle={{
            alignSelf: 'center',
            justifyContent: 'center',
            flexGrow: 1,
            alignItems: 'center',
            width: '100%',
            paddingVertical: 10,
          }}
          onViewableItemsChanged={this.onViewableItemsChanged}
          viewabilityConfig={{ viewAreaCoveragePercentThreshold: 75 }}
          ListFooterComponent={this.renderMoreImagesBox(showFooterAddMoreImages)}
          renderItem={(itemObj, gridKey) => {
            const item = itemObj.item;
            const itemIndex = itemObj.index;

            let inView = false;
            let viewable = [...this.state.viewableItems];
            if (viewable && viewable.length > 0) {
              const viewIndex = viewable.findIndex((obj) => obj.index === itemIndex);

              if (viewIndex > -1) {
                inView = true;
              }
            }

            const index = getPhotoIndexByPath([...this.state.selected], item.path);

            let pages = [...this.state.pages];

            let itemWidth = 130;
            let itemHeight = 130;

            //console.log('type: ' + layoutMainType);
            //layoutMainType = 'others';
            let leftFull = false;
            let rightFull = false;
            let pageIndex = pages.findIndex((obj) => obj.index === itemIndex);
            if (pageIndex !== -1) {
              itemWidth = pages[pageIndex].width + 60;
              itemHeight = pages[pageIndex].height + 60;
              leftFull = pages[pageIndex].leftFull;
              rightFull = pages[pageIndex].rightFull;
            }
            return (
              <View
                style={[inView && this.state.modalLayout ? styles.imageBoxFixed : styles.imageBoxRelative, {}]}
                key={item.path}
              >
                <ProductLayout
                  cart={this.props.cart}
                  index={index}
                  index2={index}
                  item={item}
                  layoutMainType={layoutMainType}
                  modalTextIndex={this.state.modalTextIndex}
                  font={this.state.font}
                  itemWidth={itemWidth}
                  itemHeight={itemHeight}
                  leftFull={leftFull}
                  rightFull={rightFull}
                  numberOfPages={numberOfPages}
                  openPhoto={this.openPhoto}
                  removeOneImage={this.removeOneImage}
                  setStateValues={this.setStateValues}
                  updateCartPropsPhotos={this.updateCartPropsPhotos}
                  showFooterOptions={showFooterOptions}
                  showFooterGalleryOption={showFooterGalleryOption}
                  showFooterLayoutOption={showFooterLayoutOption}
                  showFooterRemoveOption={showFooterRemoveOption}
                  showFooterTextOption={showFooterTextOption}
                  showFooterAddMoreImages={showFooterAddMoreImages}
                />
              </View>
            );
          }}
        />

        {this.state.pageLayout === 'single' && (
          <FlatList
            style={{
              opacity: this.state.modalLayout ? 0.8 : 1,
            }}
            ref={(fl) => (this._flatList = fl)}
            keyExtractor={(item, index) => index.toString()}
            data={selectedPhotos}
            numColumns={1}
            contentContainerStyle={{
              alignSelf: 'center',
              justifyContent: 'center',
              flexGrow: 1,
              alignItems: 'center',
              width: '100%',
              paddingVertical: 10,
            }}
            onViewableItemsChanged={this.onViewableItemsChanged}
            viewabilityConfig={{ viewAreaCoveragePercentThreshold: 75 }}
            ListFooterComponent={this.renderMoreImagesBox(showFooterAddMoreImages)}
            renderItem={(itemObj, gridKey) => {
              const item = itemObj.item;
              const itemIndex = itemObj.index;
              //   console.log('item', item, itemIndex);
              //let selectedItem = false;

              let inView = false;
              let viewable = [...this.state.viewableItems];
              //  console.log('viewable', viewable);
              if (viewable && viewable.length > 0) {
                const viewIndex = viewable.findIndex((obj) => obj.index === itemIndex);

                if (viewIndex > -1) {
                  inView = true;
                }
                //  console.log('inview', inView, viewIndex);
              }
              let selectedItem = item.options && item.options.selected ? true : false;
              const index = getPhotoIndexByPath([...this.state.selected], item.path);

              let pages = [...this.state.pages];

              let itemWidth = 130;
              let itemHeight = 130;

              let layoutMainType = 'portrait';

              if (
                this.props.cart &&
                this.props.cart.selectedAlbum &&
                this.props.cart.selectedAlbum.albumItem &&
                this.props.cart.selectedAlbum.albumItem.customFields
              ) {
                if (this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_layout) {
                  layoutMainType = this.props.cart.selectedAlbum.albumItem.customFields.pm_product_designer_layout;
                }
              }

              if (layoutMainType !== 'portrait' && layoutMainType !== 'landscape') {
                layoutMainType = 'others';
              }
              console.log('type: ' + layoutMainType);
              //layoutMainType = 'others';
              let leftFull = false;
              let rightFull = false;
              let pageIndex = pages.findIndex((obj) => obj.index === itemIndex);
              if (pageIndex !== -1) {
                itemWidth = pages[pageIndex].width + 60;
                itemHeight = pages[pageIndex].height + 60;
                leftFull = pages[pageIndex].leftFull;
                rightFull = pages[pageIndex].rightFull;
              }

              //console.log(pages, itemWidth, itemHeight, pageIndex);

              /*
               const optionIndex =
               this.state.selectedOptions &&
               this.state.selectedOptions.length > 0
               ? this.state.selectedOptions.findIndex(
               obj => obj.index === index,
               )
               : -1;

               if (optionIndex > -1) {
               selectedItem = true;
               }
               */
              // console.log('gridkey ' + itemIndex);

              return (
                <View
                  style={[inView && this.state.modalLayout ? styles.imageBoxFixed : styles.imageBoxRelative, {}]}
                  key={item.path}
                >
                  <ProductSingleLayout
                    cart={this.props.cart}
                    index={index}
                    index2={index}
                    item={item}
                    layoutMainType={layoutMainType}
                    modalTextIndex={this.state.modalTextIndex}
                    font={this.state.font}
                    itemWidth={itemWidth}
                    itemHeight={itemHeight}
                    leftFull={leftFull}
                    rightFull={rightFull}
                    openPhoto={this.openPhoto}
                    removeOneImage={this.removeOneImage}
                    setStateValues={this.setStateValues}
                    updateCartPropsPhotos={this.updateCartPropsPhotos}
                    showFooterOptions={showFooterOptions}
                    showFooterGalleryOption={showFooterGalleryOption}
                    showFooterLayoutOption={showFooterLayoutOption}
                    showFooterRemoveOption={showFooterRemoveOption}
                    showFooterTextOption={showFooterTextOption}
                    showFooterAddMoreImages={showFooterAddMoreImages}
                  />
                </View>
              );
            }}
          />
        )}
        {this.state.pageLayout === 'hochformat_book' && (
          <FlatList
            style={{
              opacity: this.state.modalLayout ? 0.8 : 1,
            }}
            ref={(fl) => (this._flatList = fl)}
            keyExtractor={(item, index) => index.toString()}
            data={selectedPhotos}
            numColumns={2}
            columnWrapperStyle={'column'}
            contentContainerStyle={{
              alignSelf: 'center',
              justifyContent: 'center',
              flexGrow: 1,
              alignItems: 'center',
              width: '100%',
              paddingVertical: 10,
            }}
            ListHeaderComponent={this.Render_Hochformat_Layout_header}
            stickyHeaderIndices={[0]}
            ItemSeparatorComponent={({ leadingItem }) => (
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <View style={{ borderRadius: 20, overflow: 'hidden' }}>
                  <TouchableNativeFeedback
                    useForeground={true}
                    background={backgroundButtonsSubheader}
                    onPress={() => {
                      this.addRowToPhotos(leadingItem);
                    }}
                    style={{
                      padding: 10,
                      height: 50,
                      justifyContent: 'center',
                    }}
                  >
                    <Image source={require('../assets/icons/plusIcon.png')} style={{ width: 30, height: 30 }} />
                  </TouchableNativeFeedback>
                </View>
              </View>
            )}
            onViewableItemsChanged={this.onViewableItemsChanged}
            viewabilityConfig={{ viewAreaCoveragePercentThreshold: 75 }}
            renderItem={(itemObj, gridKey) => {
              const item = itemObj.item;
              const itemIndex = itemObj.index;
              //   console.log('item', item, itemIndex);
              //let selectedItem = false;

              let inView = false;
              let viewable = [...this.state.viewableItems];
              //  console.log('viewable', viewable);
              if (viewable && viewable.length > 0) {
                const viewIndex = viewable.findIndex((obj) => obj.index === itemIndex);

                if (viewIndex > -1) {
                  inView = true;
                }
                //  console.log('inview', inView, viewIndex);
              }
              let selectedItem = item.options && item.options.selected ? true : false;
              const index = getPhotoIndexByPath([...this.state.selected], item.path);

              let pages = [...this.state.pages];

              let itemWidth = 130;
              let itemHeight = 130;
              let leftFull = false;
              let rightFull = false;
              let pageIndex = pages.findIndex((obj) => obj.index === itemIndex);
              if (pageIndex !== -1) {
                itemWidth = pages[pageIndex].width + 60;
                itemHeight = pages[pageIndex].height + 60;
                leftFull = pages[pageIndex].leftFull;
                rightFull = pages[pageIndex].rightFull;
              }

              //console.log(pages, itemWidth, itemHeight, pageIndex);

              /*
               const optionIndex =
               this.state.selectedOptions &&
               this.state.selectedOptions.length > 0
               ? this.state.selectedOptions.findIndex(
               obj => obj.index === index,
               )
               : -1;

               if (optionIndex > -1) {
               selectedItem = true;
               }
               */
              // console.log('gridkey ' + itemIndex);

              return (
                <View
                  style={[inView && this.state.modalLayout ? styles.imageBoxFixed : styles.imageBoxRelative, {}]}
                  key={item.path}
                >
                  <ProductLayoutHochformat
                    cart={this.props.cart}
                    index={index}
                    item={item}
                    modalTextIndex={this.state.modalTextIndex}
                    font={this.state.font}
                    itemWidth={itemWidth}
                    itemHeight={itemHeight}
                    leftFull={leftFull}
                    rightFull={rightFull}
                    openPhoto={this.openPhoto}
                    removeOneImage={this.removeOneImage}
                    setStateValues={this.setStateValues}
                    updateCartPropsPhotos={this.updateCartPropsPhotos}
                  />
                </View>
              );
            }}
          />
        )}
        {this.state.pageLayout === 'quare_book' && (
          <FlatList
            style={{
              opacity: this.state.modalLayout ? 0.8 : 1,
            }}
            ref={(fl) => (this._flatList = fl)}
            keyExtractor={(item, index) => index.toString()}
            data={selectedPhotos}
            numColumns={2}
            columnWrapperStyle={'column'}
            contentContainerStyle={{
              alignSelf: 'center',
              justifyContent: 'center',
              flexGrow: 1,
              alignItems: 'center',
              width: '100%',
              paddingVertical: 10,
            }}
            ListHeaderComponent={this.Render_Hochformat_Layout_header}
            stickyHeaderIndices={[0]}
            ItemSeparatorComponent={({ leadingItem }) => (
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <View style={{ borderRadius: 20, overflow: 'hidden' }}>
                  <TouchableNativeFeedback
                    useForeground={true}
                    background={backgroundButtonsSubheader}
                    onPress={() => {
                      this.addRowToPhotos(leadingItem);
                    }}
                    style={{
                      padding: 10,
                      height: 50,
                      justifyContent: 'center',
                    }}
                  >
                    <Image source={require('../assets/icons/plusIcon.png')} style={{ width: 30, height: 30 }} />
                  </TouchableNativeFeedback>
                </View>
              </View>
            )}
            onViewableItemsChanged={this.onViewableItemsChanged}
            viewabilityConfig={{ viewAreaCoveragePercentThreshold: 75 }}
            renderItem={(itemObj, gridKey) => {
              const item = itemObj.item;
              const itemIndex = itemObj.index;
              //   console.log('item', item, itemIndex);
              //let selectedItem = false;

              let inView = false;
              let viewable = [...this.state.viewableItems];
              //  console.log('viewable', viewable);
              if (viewable && viewable.length > 0) {
                const viewIndex = viewable.findIndex((obj) => obj.index === itemIndex);

                if (viewIndex > -1) {
                  inView = true;
                }
                //  console.log('inview', inView, viewIndex);
              }
              let selectedItem = item.options && item.options.selected ? true : false;
              const index = getPhotoIndexByPath([...this.state.selected], item.path);

              let pages = [...this.state.pages];

              let itemWidth = 130;
              let itemHeight = 130;
              let leftFull = false;
              let rightFull = false;
              let pageIndex = pages.findIndex((obj) => obj.index === itemIndex);
              if (pageIndex !== -1) {
                itemWidth = pages[pageIndex].width + 60;
                itemHeight = pages[pageIndex].height + 60;
                leftFull = pages[pageIndex].leftFull;
                rightFull = pages[pageIndex].rightFull;
              }

              //console.log(pages, itemWidth, itemHeight, pageIndex);

              /*
               const optionIndex =
               this.state.selectedOptions &&
               this.state.selectedOptions.length > 0
               ? this.state.selectedOptions.findIndex(
               obj => obj.index === index,
               )
               : -1;

               if (optionIndex > -1) {
               selectedItem = true;
               }
               */
              // console.log('gridkey ' + itemIndex);

              return (
                <View
                  style={[inView && this.state.modalLayout ? styles.imageBoxFixed : styles.imageBoxRelative, {}]}
                  key={item.path}
                >
                  <ProductLayoutQuare
                    cart={this.props.cart}
                    index={index}
                    item={item}
                    modalTextIndex={this.state.modalTextIndex}
                    font={this.state.font}
                    itemWidth={itemWidth}
                    itemHeight={itemHeight}
                    leftFull={leftFull}
                    rightFull={rightFull}
                    openPhoto={this.openPhoto}
                    removeOneImage={this.removeOneImage}
                    setStateValues={this.setStateValues}
                    updateCartPropsPhotos={this.updateCartPropsPhotos}
                  />
                </View>
              );
            }}
          />
        )}
        {this.state.pageLayout === 'square_book' && (
          <FlatList
            style={{
              opacity: this.state.modalLayout ? 0.8 : 1,
            }}
            ref={(fl) => (this._flatList = fl)}
            keyExtractor={(item, index) => index.toString()}
            data={selectedPhotos}
            numColumns={2}
            columnWrapperStyle={'column'}
            contentContainerStyle={{
              alignSelf: 'center',
              justifyContent: 'center',
              flexGrow: 1,
              alignItems: 'center',
              width: '100%',
              paddingVertical: 10,
            }}
            ItemSeparatorComponent={({ leadingItem }) => (
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <View style={{ borderRadius: 20, overflow: 'hidden' }}>
                  <TouchableNativeFeedback
                    useForeground={true}
                    background={backgroundButtonsSubheader}
                    onPress={() => {
                      this.addRowToPhotos(leadingItem);
                    }}
                    style={{
                      padding: 10,
                      height: 50,
                      justifyContent: 'center',
                    }}
                  >
                    <Image source={require('../assets/icons/plusIcon.png')} style={{ width: 30, height: 30 }} />
                  </TouchableNativeFeedback>
                </View>
              </View>
            )}
            onViewableItemsChanged={this.onViewableItemsChanged}
            viewabilityConfig={{ viewAreaCoveragePercentThreshold: 75 }}
            // ListHeaderComponent={this.Render_Square_Layout_header}
            // stickyHeaderIndices={[0]}
            // stickyHeaderHiddenOnScroll={true}
            renderItem={(itemObj, gridKey) => {
              const item = itemObj.item;
              const itemIndex = itemObj.index;
              //let selectedItem = false;

              let inView = false;
              let viewable = [...this.state.viewableItems];
              //  console.log('viewable', viewable);
              if (viewable && viewable.length > 0) {
                const viewIndex = viewable.findIndex((obj) => obj.index === itemIndex);

                if (viewIndex > -1) {
                  inView = true;
                }
                //  console.log('inview', inView, viewIndex);
              }
              let selectedItem = item.options && item.options.selected ? true : false;
              const index = getPhotoIndexByPath([...this.state.selected], item.path);

              let pages = [...this.state.pages];

              let itemWidth = 130;
              let itemHeight = 130;
              let leftFull = false;
              let rightFull = false;

              let pageIndex = pages.findIndex((obj) => obj.index === itemIndex);
              if (pageIndex !== -1) {
                itemWidth = pages[pageIndex].width + 60;
                itemHeight = pages[pageIndex].height + 60;
                leftFull = pages[pageIndex].leftFull;
                rightFull = pages[pageIndex].rightFull;
              }

              //console.log(pages, itemWidth, itemHeight, pageIndex);

              /*
               const optionIndex =
               this.state.selectedOptions &&
               this.state.selectedOptions.length > 0
               ? this.state.selectedOptions.findIndex(
               obj => obj.index === index,
               )
               : -1;

               if (optionIndex > -1) {
               selectedItem = true;
               }
               */
              // console.log('gridkey ' + itemIndex);

              return (
                <View
                  style={[inView && this.state.modalLayout ? styles.imageBoxFixed : styles.imageBoxRelative, {}]}
                  key={item.path}
                >
                  <ProductLayoutSquare
                    cart={this.props.cart}
                    index={index}
                    item={item}
                    modalTextIndex={this.state.modalTextIndex}
                    font={this.state.font}
                    itemWidth={itemWidth}
                    itemHeight={itemHeight}
                    leftFull={leftFull}
                    rightFull={rightFull}
                    openPhoto={this.openPhoto}
                    removeOneImage={this.removeOneImage}
                    setStateValues={this.setStateValues}
                    updateCartPropsPhotos={this.updateCartPropsPhotos}
                  />
                </View>
              );
            }}
          />
        )}

        {this.state.pageLayout === 'postkarten' && (
          <FlatList
            style={{
              opacity: this.state.modalLayout ? 0.8 : 1,
            }}
            ref={(fl) => (this._flatList = fl)}
            keyExtractor={(item, index) => index.toString()}
            data={selectedPhotos}
            renderItem={(itemObj, gridKey) => {
              const item = itemObj.item;
              const itemIndex = itemObj.index;
              //let selectedItem = false;

              let inView = false;
              let viewable = [...this.state.viewableItems];
              //  console.log('viewable', viewable);
              if (viewable && viewable.length > 0) {
                const viewIndex = viewable.findIndex((obj) => obj.index === itemIndex);

                if (viewIndex > -1) {
                  inView = true;
                }
                //  console.log('inview', inView, viewIndex);
              }
              let selectedItem = item.options && item.options.selected ? true : false;
              const index = getPhotoIndexByPath([...this.state.selected], item.path);

              let pages = [...this.state.pages];
              let itemWidth = 130;
              let itemHeight = 130;
              let leftFull = false;
              let rightFull = false;
              let pageIndex = pages.findIndex((obj) => obj.index === itemIndex);

              if (pageIndex !== -1) {
                itemWidth = pages[pageIndex].width + 60;
                itemHeight = pages[pageIndex].height + 60;
                leftFull = pages[pageIndex].leftFull;
                rightFull = pages[pageIndex].rightFull;
              }

              //console.log(pages, itemWidth, itemHeight, pageIndex);

              /*
               const optionIndex =
               this.state.selectedOptions &&
               this.state.selectedOptions.length > 0
               ? this.state.selectedOptions.findIndex(
               obj => obj.index === index,
               )
               : -1;

               if (optionIndex > -1) {
               selectedItem = true;
               }
               */
              // console.log('gridkey ' + itemIndex);

              return (
                <View
                  style={[inView && this.state.modalLayout ? styles.imageBoxFixed : styles.imageBoxRelative, {}]}
                  key={item.path}
                >
                  <ProductPostcardLayout
                    cart={this.props.cart}
                    index={index}
                    item={item}
                    modalTextIndex={this.state.modalTextIndex}
                    font={this.state.font}
                    itemWidth={itemWidth}
                    itemHeight={itemHeight}
                    leftFull={leftFull}
                    rightFull={rightFull}
                    openPhoto={this.openPhoto}
                    removeOneImage={this.removeOneImage}
                    setStateValues={this.setStateValues}
                    updateCartPropsPhotos={this.updateCartPropsPhotos}
                  />
                </View>
              );
            }}
          />
        )}

        <Modal
          onRequestClose={() => this.setState({ modalSpeichern: false })}
          transparent={true}
          isVisible={this.state.modalSpeichern}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <View style={styles.modalContent}>
            <View style={styles.innerModalContent}>
              <TouchableOpacity onPress={() => this.setState({ modalSpeichern: false })} style={styles.closeButton}>
                <CloseButton />
              </TouchableOpacity>
              <Text style={styles.modalTitle}>SPEICHERN?</Text>
              <Text style={styles.modalSubTitle}>
                Bist du bereit, deinen Entwurf zu sichern,{'\n'}um ihn später zu beenden?
              </Text>

              <View style={[styles.modalButton, { borderRadius: 20, overflow: 'hidden' }]}>
                <TouchableNativeFeedback
                  useForeground={true}
                  background={background}
                  onPress={() => this.speichernOptions()}
                >
                  <View style={{ width: '100%', padding: 15 }}>
                    <Text style={styles.modalButtonText}>NACH DATUM</Text>
                  </View>
                </TouchableNativeFeedback>
              </View>

              <TouchableOpacity>
                <Text style={styles.Löschen}>Löschen</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal
          onRequestClose={() => this.setState({ modalGalerie: false })}
          transparent={true}
          isVisible={this.state.modalGalerie}
          style={{
            position: 'absolute',
            bottom: 0,
            width: width,
            alignSelf: 'center',
            margin: 0,
          }}
        >
          <View style={[styles.footerModal, { height: 0.5 * height }]}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <GalerieIconWhite />
              <Text style={styles.footerTitle}>GALERIE</Text>
              <TouchableOpacity
                style={{ padding: 10 }}
                onPress={() =>
                  this.setState({
                    modalGalerie: false,
                    galleryOpenedWithImage: null,
                  })
                }
              >
                <ArrowBottom />
              </TouchableOpacity>
            </View>
            <View style={styles.AddImageBox}>
              <TouchableOpacity style={{ padding: 15 }} onPress={() => this.onAddImageClick()}>
                <AddImageFromLibrary />
              </TouchableOpacity>
            </View>

            <FlatList
              keyExtractor={(item, index) => index.toString()}
              data={this.state.selected}
              numColumns={4}
              columnWrapperStyle={'row'}
              contentContainerStyle={{
                paddingBottom: 35,
              }}
              renderItem={({ item, index }) => {
                // console.log('item', item);
                return (
                  <View key={item?.path || index?.toString()}>
                    <TouchableOpacity
                      onPress={() => {
                        if (item?.path) {
                          this.changeImagePath(item.path);
                        }
                      }}
                    >
                      <Image style={{ width: 81, height: 81, margin: 5 }} source={{ uri: item.path }} />
                    </TouchableOpacity>
                  </View>
                );
              }}
            />
          </View>
        </Modal>
        <Modal
          onRequestClose={() => this.setState({ modalLayout: false })}
          transparent={true}
          isVisible={this.state.modalLayout}
          style={{
            position: 'absolute',
            bottom: 0,
            width: width,
            alignSelf: 'center',
            margin: 0,
          }}
        >
          <View style={[styles.footerModal]}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <LayoutIconWhite />
              <Text style={styles.footerTitle}>LAYOUT</Text>
              <TouchableOpacity
                style={{ padding: 10 }}
                onPress={() => {
                  this.setState({ modalLayout: false });
                }}
              >
                <ArrowBottom />
              </TouchableOpacity>
            </View>

            {this.state.pageLayout === 'square_book' && (
              <FlatList
                pagingEnabled={true}
                style={{ width: '100%' }}
                numColumns={2}
                columnWrapperStyle={'row'}
                contentContainerStyle={{
                  alignSelf: 'center',
                }}
                keyExtractor={(item, index) => 'layout' + index.toString()}
                //keyExtractor={layout => "layout" + layout.height}
                data={layouts}
                renderItem={({ item }) => this.renderLayouts(item)}
              />
            )}
            {this.state.pageLayout === 'hochformat_book' && (
              <FlatList
                pagingEnabled={true}
                style={{ width: '100%' }}
                numColumns={2}
                columnWrapperStyle={'row'}
                contentContainerStyle={{
                  alignSelf: 'center',
                }}
                keyExtractor={(item, index) => 'layout' + index.toString()}
                //keyExtractor={layout => "layout" + layout.height}
                data={layouts}
                renderItem={({ item }) => this.renderLayouts(item)}
              />
            )}
            {this.state.pageLayout === 'single' && (
              <FlatList
                pagingEnabled={true}
                style={{ width: '100%' }}
                numColumns={1}
                contentContainerStyle={{
                  alignSelf: 'center',
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}
                keyExtractor={(item, index) => 'layout' + index.toString()}
                //keyExtractor={layout => "layout" + layout.height}
                data={layouts}
                renderItem={({ item }) => this.renderSingleLayouts(item)}
              />
            )}
          </View>
        </Modal>
        <Modal
          onRequestClose={() => this.setState({ modalText: false })}
          // avoidKeyboard={true}
          transparent={true}
          isVisible={this.state.modalText}
          style={{
            position: 'absolute',
            bottom: 0,
            width: width,
            alignSelf: 'center',
            margin: 0,
          }}
        >
          <View style={styles.footerModal}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TextEditIcon />
              <Text style={styles.footerTitle}>TEXT</Text>
              <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ modalText: false })}>
                <ArrowBottom />
              </TouchableOpacity>
            </View>
            <View style={styles.fonts}>
              <TouchableOpacity onPress={() => this.setFont(Platform.OS === 'ios' ? 'AmaticSC-Bold' : 'AmaticSC-Bold')}>
                <Text
                  style={{
                    fontSize: 20,
                    fontFamily: Platform.OS === 'ios' ? 'AmaticSC-Bold' : 'AmaticSC-Bold',
                    color: '#FFFFFF',
                  }}
                >
                  Hello
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setFont(Platform.OS === 'ios' ? 'DancingScript-Bold' : 'DancingScript-Bold')}
              >
                <Text
                  style={{
                    fontSize: 20,
                    fontFamily: Platform.OS === 'ios' ? 'DancingScript-Bold' : 'DancingScript-Bold',
                    color: '#FFFFFF',
                  }}
                >
                  Hello
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  this.setFont(Platform.OS === 'ios' ? 'Lora-VariableFont_wght' : 'Lora-VariableFont_wght')
                }
              >
                <Text
                  style={{
                    fontSize: 20,
                    fontFamily: Platform.OS === 'ios' ? 'Lora-VariableFont_wght' : 'Lora-VariableFont_wght',
                    color: '#FFFFFF',
                  }}
                >
                  Hello
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.setFont(Platform.OS === 'ios' ? 'Roboto-Medium' : 'Roboto-Medium')}>
                <Text
                  style={{
                    fontSize: 20,
                    fontFamily: Platform.OS === 'ios' ? 'Roboto-Medium' : 'Roboto-Medium',
                    color: '#FFFFFF',
                  }}
                >
                  Hello
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <Modal
          onRequestClose={() => this.setState({ modalEntfernen: false })}
          transparent={true}
          isVisible={this.state.modalEntfernen}
          style={{
            position: 'absolute',
            bottom: 0,
            width: width,
            alignSelf: 'center',
            margin: 0,
          }}
        >
          <View style={styles.footerModal}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <DeleteIconWhite />
              <Text style={styles.footerTitle}>ENTFERNEN</Text>
              <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ modalEntfernen: false })}>
                <ArrowBottom />
              </TouchableOpacity>
            </View>
            <View style={[styles.buttonFooter, { borderRadius: 20, overflow: 'hidden' }]}>
              <TouchableNativeFeedback
                onPress={this.clearCartPhotosForSelectedAlbum}
                useForeground={true}
                background={background}
              >
                <View style={{ padding: 15, width: '100%' }}>
                  <Text style={styles.modalButtonText}>BUCH LEEREN</Text>
                </View>
              </TouchableNativeFeedback>
            </View>
          </View>
        </Modal>
        <Modal
          onRequestClose={() => this.setState({ modalTitle: false })}
          transparent={false}
          isVisible={this.state.modalTitle}
          style={{
            marginBottom: 0,
            justifyContent: 'flex-end',
            alignItems: 'center',
            paddingTop: Platform.OS === 'ios' ? 20 : 0,
          }}
        >
          <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : ''}>
            <View
              style={{
                width: width,
                flex: 1,
                backgroundColor: '#FFFFFF',
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}
            >
              <View style={{ padding: 20 }}>
                <TextInput
                  pointerEvents="none"
                  style={{
                    fontSize: 25,
                    fontWeight: 'normal',
                    fontFamily: this.state.font,
                  }}
                  autoFocus={true}
                  selectionColor="#000"
                  // multiline={true}
                  maxLength={27}
                  onFocus={this.onPress}
                  // onBlur={this.onBlur}
                  placeholder="Titel"
                  value={this.state.modalTextValue}
                  onChange={(event) => {
                    const { text } = event.nativeEvent;
                    this.setState({ modalTextValue: text });
                  }}
                />
              </View>

              <View style={{ position: 'absolute', bottom: 0, width: width }}>
                {this.state.showCharacters ? (
                  <View
                    style={{
                      borderBottomWidth: 1,
                      borderBottomColor: '#707070',
                      justifyContent: 'flex-end',
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: 'Lato-Bold',
                        fontSize: 12,
                        textAlign: 'right',
                        padding: 5,
                      }}
                    >
                      {this.state.modalTextValue.length}/27 Zeichen übrig
                    </Text>
                  </View>
                ) : null}
                <View
                  style={{
                    paddingVertical: 20,
                    borderRadius: 20,
                    overflow: 'hidden',
                    width: '90%',
                    alignSelf: 'center',
                  }}
                >
                  <TouchableNativeFeedback
                    background={background}
                    useForeground={true}
                    onPress={() => {
                      const text = this.state.modalTextValue;
                      const selectedItems = [...this.state.selected];
                      if (!selectedItems[this.state.modalTextIndex].options) {
                        selectedItems[this.state.modalTextIndex].options = {
                          index: this.state.modalTextIndex,
                          image: this.state.modalSelectedItem.path,
                          text: text,
                        };
                      } else {
                        selectedItems[this.state.modalTextIndex].options.text = text;
                      }
                      this.updateCartPropsPhotos(selectedItems);

                      this.setState({
                        modalTitle: false,
                        modalTextIndex: null,
                        modalTextValue: '',
                        modalSelectedItem: null,
                      });
                    }}
                  >
                    <View>
                      <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        colors={['#FFD06C', '#D78275', '#B42762']}
                        style={styles.LinearGradientModal}
                      >
                        <Text style={styles.textFooter}>WEITER</Text>
                      </LinearGradient>
                    </View>
                  </TouchableNativeFeedback>
                </View>
              </View>
            </View>
          </KeyboardAvoidingView>
        </Modal>
        <Modal
          onRequestClose={() => this.setState({ modalPostcardTitle: false })}
          transparent={false}
          isVisible={this.state.modalPostcardTitle}
          style={{
            marginBottom: 0,
            justifyContent: 'flex-end',
            alignItems: 'center',
            paddingTop: Platform.OS === 'ios' ? 20 : 0,
          }}
        >
          <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : ''}>
            <SafeAreaView
              style={{
                width: width,
                flex: 1,
                backgroundColor: '#FFFFFF',
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}
            >
              <View style={{ padding: 20, flex: 1 }}>
                <View style={{}}>
                  <TextInput
                    onFocus={(event) => {
                      // `bind` the function if you're using ES6 classes
                      this._scrollToInput(findNodeHandle(event.target));
                    }}
                    onSubmitEditing={() => this.modalMessageValue.focus()}
                    pointerEvents="none"
                    style={{
                      fontSize: 25,
                      fontWeight: 'normal',
                      fontFamily: this.state.font,
                      borderBottomWidth: 1,
                    }}
                    autoFocus={true}
                    selectionColor="#000"
                    maxLength={40}
                    onFocus={this.onPress}
                    //onBlur={this.onBlur}
                    placeholder="Titel"
                    value={this.state.modalTextValue}
                    onChange={(event) => {
                      const { text } = event.nativeEvent;
                      this.setState({ modalTextValue: text });
                      this.setState({ showCharacters: true });
                      this.setState({ showCharactersMessage: false });
                      // console.log("tekst", text)
                    }}
                  />
                </View>
                <View style={{ height: '50%' }}>
                  <TextInput
                    pointerEvents="none"
                    style={{
                      fontSize: 15,
                      fontWeight: 'normal',
                      fontFamily: this.state.font,
                    }}
                    ref={(ref) => {
                      this.modalMessageValue = ref;
                    }}
                    selectionColor="#000"
                    multiline={true}
                    maxLength={400}
                    onFocus={this.onPressMessage}
                    //onBlur={this.onBlurMessage}
                    placeholder="Message"
                    value={this.state.modalMessageValue}
                    onChange={(event) => {
                      const { text } = event.nativeEvent;
                      this.setState({ modalMessageValue: text });
                      this.setState({ showCharacters: false });
                      this.setState({ showCharactersMessage: true });
                      // console.log("message", text)
                    }}
                  />
                </View>
              </View>

              <View style={{ position: 'absolute', bottom: 0, width: width }}>
                {this.state.showCharacters ? (
                  <View
                    style={{
                      borderBottomWidth: 1,
                      borderBottomColor: '#707070',
                      justifyContent: 'flex-end',
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: 'Lato-Bold',
                        fontSize: 12,
                        textAlign: 'right',
                        padding: 5,
                      }}
                    >
                      {this.state.modalTextValue.length}/40 Zeichen übrig
                    </Text>
                  </View>
                ) : (
                  <View
                    style={{
                      borderBottomWidth: 1,
                      borderBottomColor: '#707070',
                      justifyContent: 'flex-end',
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: 'Lato-Bold',
                        fontSize: 12,
                        textAlign: 'right',
                        padding: 5,
                      }}
                    >
                      {this.state.modalMessageValue.length}/400 Zeichen übrig
                    </Text>
                  </View>
                )}

                <View
                  style={{
                    paddingVertical: 20,
                    borderRadius: 20,
                    overflow: 'hidden',
                    width: '90%',
                    alignSelf: 'center',
                  }}
                >
                  <TouchableNativeFeedback
                    background={background}
                    useForeground={true}
                    onPress={() => {
                      const text = this.state.modalTextValue;
                      const textMessage = this.state.modalMessageValue;
                      const selectedItems = [...this.state.selected];
                      if (!selectedItems[this.state.modalTextIndex].options) {
                        selectedItems[this.state.modalTextIndex].options = {
                          index: this.state.modalTextIndex,
                          image: this.state.modalSelectedItem.path,
                          text: text,
                          textMessage: textMessage,
                        };
                      } else {
                        selectedItems[this.state.modalTextIndex].options.text = text;
                      }
                      selectedItems[this.state.modalTextIndex].options.textMessage = textMessage;
                      this.updateCartPropsPhotos(selectedItems);

                      this.setState({
                        modalPostcardTitle: false,
                        modalTextIndex: null,
                        modalTextValue: '',
                        modalMessageValue: '',
                        modalSelectedItem: null,
                      });
                      //   console.log("titel", text, "message", text)
                    }}
                  >
                    <View>
                      <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        colors={['#FFD06C', '#D78275', '#B42762']}
                        style={styles.LinearGradientModal}
                      >
                        <Text style={styles.textFooter}>WEITER</Text>
                      </LinearGradient>
                    </View>
                  </TouchableNativeFeedback>
                </View>
              </View>
            </SafeAreaView>
          </KeyboardAvoidingView>
        </Modal>
        <View style={{ paddingBottom: 20 }}>
          {showFooterOptions && (
            <View style={styles.footer}>
              {showFooterGalleryOption && (
                <TouchableOpacity style={styles.icons} onPress={() => this.setState({ modalGalerie: true })}>
                  <View style={{ alignItems: 'center' }}>
                    <GalerieIcon />
                    <Text
                      style={{
                        fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
                        fontSize: 12,
                      }}
                    >
                      GALERIE
                    </Text>
                  </View>
                </TouchableOpacity>
              )}

              {showFooterLayoutOption && (
                <TouchableOpacity
                  style={styles.icons}
                  onPress={() => {
                    //const viewable = [...this.state.viewableItems];
                    ///if (viewable && viewable.length > 0) {
                    //  console.log('viewable', viewable[0]);
                    // }
                    //this._flatList.scrollToOffset(false, viewable[0].index);
                    //              this._flatList.scrollToItem(false, viewable[0].index);
                    this.setState({ modalLayout: true });
                  }}
                >
                  <View style={{ alignItems: 'center' }}>
                    <LayoutIcon />
                    <Text
                      style={{
                        fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
                        fontSize: 12,
                      }}
                    >
                      LAYOUT
                    </Text>
                  </View>
                </TouchableOpacity>
              )}

              {showFooterTextOption && (
                <TouchableOpacity style={styles.icons} onPress={() => this.setState({ modalText: true })}>
                  <View style={{ alignItems: 'center' }}>
                    <TextIcon />
                    <Text
                      style={{
                        fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
                        fontSize: 12,
                      }}
                    >
                      TEXT
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
              {showFooterRemoveOption && (
                <TouchableOpacity style={styles.icons} onPress={() => this.setState({ modalEntfernen: true })}>
                  <View style={{ alignItems: 'center' }}>
                    <DeleteIcon />
                    <Text
                      style={{
                        fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
                        fontSize: 12,
                      }}
                    >
                      ENTFERNEN
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
            </View>
          )}
          <View
            style={{
              borderRadius: 20,
              overflow: 'hidden',
              alignSelf: 'center',
              width: '90%',
              paddingBottom: 5,
            }}
          >
            <TouchableNativeFeedback background={background} useForeground={true} onPress={() => this.onButtonClick()}>
              <View>
                <LinearGradient
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                  colors={['#FFD06C', '#D78275', '#B42762']}
                  style={styles.LinearGradient}
                >
                  <Text style={styles.textButton}>ZUM WARENKORB HINZUFÜGEN</Text>
                </LinearGradient>
              </View>
            </TouchableNativeFeedback>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 15,
    backgroundColor: '#FFFFFF',
  },
  titleHeader: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  speichern: {
    color: '#12336B',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textDecorationLine: 'underline',
    fontSize: 13,
  },
  Buttons: {
    borderWidth: 1,
    borderColor: '#D38074',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    alignSelf: 'center',
  },
  innerButtons: {
    width: '50%',
    height: 40,
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
  },
  imageBoxRelative: {
    //marginLeft: 20,
  },
  imageBoxFixed: {
    //marginLeft: 20,
  },
  imageBoxWrap: {
    // backgroundColor:"#ECECEC",
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
    // margin:1,
    // borderRadius:20,
    // shadowColor: "#000",
    // shadowOffset: {
    // width: 0,
    // height: 5,
    // },
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 10,
    // borderWidth: 1,
    // borderColor: '#D38074',
    //  marginVertical: 10,
  },
  imageBox: {
    height: 185,
    width: 185,
    justifyContent: 'center',
    alignItems: 'center',
    // marginVertical:20
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    padding: 10,
    alignItems: 'center',
  },
  icons: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  LinearGradientModal: {
    borderRadius: 20,
    padding: '4%',
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 2,
  },
  LinearGradient: {
    borderRadius: 20,
    paddingVertical: 15,
    paddingHorizontal: 10,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.17,
    shadowRadius: 2.49,
    elevation: 2,
  },
  touchableNativeFeedbackButton: {
    borderRadius: 20,
    overflow: 'hidden',
  },
  AddImagesButton: {
    borderRadius: 20,
    backgroundColor: 'white',
    borderWidth: 2,
    borderColor: '#D38074',
    paddingVertical: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.17,
    shadowRadius: 2.49,
    elevation: 2,
  },
  textButton: {
    alignItems: 'center',
    color: 'white',
    fontSize: 14,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
    paddingLeft: 25,
    paddingRight: 25,
  },
  textButtonAddImages: {
    alignItems: 'center',
    color: '#D38074',
    fontSize: 14,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
    paddingLeft: 25,
    paddingRight: 25,
  },
  footerModal: {
    backgroundColor: '#D38074',
    alignItems: 'center',
    paddingVertical: '5%',
  },
  footerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    color: '#FFFFFF',
    paddingHorizontal: 10,
  },
  AddImageBox: {
    borderWidth: 1,
    borderColor: '#FFFFFF',
    backgroundColor: '#D38074',
    height: 81,
    width: 81,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
    marginVertical: 10,
  },
  layoutBox: {
    borderWidth: 1,
    borderColor: '#D38074',
    backgroundColor: '#fff',
    height: 85,
    width: 85,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  innerLayoutBox: {
    backgroundColor: '#D38074',
    justifyContent: 'center',
    alignItems: 'center',
  },
  fonts: {
    flexDirection: 'row',
    marginVertical: 20,
    justifyContent: 'space-between',
    width: '90%',
  },
  buttonFooter: {
    borderWidth: 1,
    width: 193,
    borderColor: '#FFFFFF',
    borderRadius: 20,
    marginVertical: 10,
    alignItems: 'center',
  },
  innerTextBox: {
    borderWidth: 1,
    borderColor: '#D38074',
    width: 141,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
    backgroundColor: '#FFFFFF',
  },
  innerTextBoxOption: {
    borderWidth: 1,
    borderColor: '#D38074',
    width: 141,
    // height: 30,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
    backgroundColor: '#FFFFFF',
  },
  addMorePagesOption: {
    borderWidth: 1,
    borderColor: '#D38074',
    width: 141,
    // height: 30,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
    backgroundColor: '#FFFFFF',
  },
  modalContent: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerModalContent: {
    backgroundColor: '#D38074',
    width: '90%',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalSubTitle: {
    fontSize: 12,
    textAlign: 'center',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    paddingVertical: 10,
  },
  modalTitle: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  modalButton: {
    borderWidth: 1,
    width: 193,
    alignItems: 'center',
    borderColor: '#FFFFFF',
    borderRadius: 20,
    marginVertical: 10,
  },
  modalButtonText: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  closeButton: {
    position: 'relative',
    alignSelf: 'flex-end',
    padding: 20,
  },
  Löschen: {
    textDecorationLine: 'underline',
    color: '#FFFFFF',
    marginBottom: 20,
    fontSize: 12,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
  };
}

export default connect(mapStateToProps, { updateCartPhotos, updateCartOptions })(CreatePhotoPageV2);
