import React, {Component} from 'react';
import { View, TouchableOpacity, Text, StyleSheet, LayoutAnimation, Platform, UIManager, ScrollView} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import LinearGradient from 'react-native-linear-gradient';


export default class Accordian extends Component{

    constructor(props) {
        super(props);
        this.state = { 
          data: props.data,
          expanded : false,
        }

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }
  
  render() {

    return (
       <View>
           
            <TouchableOpacity ref={this.accordian} style={styles.row} onPress={()=>this.toggleExpand()}>
                <Text style={styles.innerContainersText}>{this.props.title}</Text>
                <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30}/>
            </TouchableOpacity>
            <View/>
            {
                this.state.expanded &&              
                    <LinearGradient 
                    style={styles.child}
                    colors={['#B42762','#ECAD75']}
                    start={{ x: 1, y: 0 }}
                    end={{ x: 0, y: 1 }}>
                    <Text style={styles.innerContainersText}>{this.props.data}</Text>  
                    </LinearGradient>              
            }
            
       </View>
    )
  }

  toggleExpand=()=>{
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded : !this.state.expanded})
  }

}

const styles = StyleSheet.create({
    row:{
        flexDirection: 'row',
        justifyContent:'space-between',
        padding:16,
        paddingLeft:25,
        paddingRight:18,
        alignItems:'center',
        backgroundColor: '#FFFFFF',
        borderBottomWidth:1,
        borderColor:'#B42762'
    },
    parentHr:{
        color: 'white',
        width:'100%',
    },
    child:{
        padding:16,
        paddingLeft:25,
        paddingRight:18,
        borderBottomLeftRadius:20,
        borderBottomRightRadius:20,
        flex:1,
        width:'100%'
    },
    innerContainersText: {
        fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
        fontSize: 13,
        width:'90%',
      }, 
});