import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ImageBackground,
  Alert,
  TouchableNativeFeedback,
} from 'react-native';

import AddText from '../assets/icons/addText.svg';

export default class ProductLayoutHochformat extends Component {
  render() {
    const background = TouchableNativeFeedback.Ripple('#fff', true);

    const item = this.props.item;

    const index = this.props.index;
    const selectedIndex = this.props.cart.selectedAlbumIndex
      ? this.props.cart.selectedAlbumIndex
      : 0;
    const selected =
      this.props.cart.cart[selectedIndex] &&
      this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];

    let modalTextValue = '';
    if (this.props.modalTextIndex && this.props.modalTextIndex > -1) {
      modalTextValue =
        selected[this.props.modalTextIndex].options &&
        selected[this.props.modalTextIndex].options.text
          ? selected[this.props.modalTextIndex].options.text
          : '';
    }

    let leftFull = this.props.leftFull;
    let rightFull = this.props.rightFull;

    let fullImage = false;

    if (index % 2 === 0 && leftFull) {
      fullImage = true;
    }
    if (!(index % 2 === 0) && rightFull) {
      fullImage = true;
    }
    return (
      <View
        style={[
          styles.imageBoxFixed,
          {
            shadowColor: '#000',
            shadowOffset: {
              width: 10,
              height: 10,
            },
            shadowOpacity: 0.8,
            shadowRadius: 15,
            zIndex: 999,
          },
        ]}>
        <ImageBackground
          source={
            index % 2
              ? require('../assets/images/PicamoryHochformatRight.png')
              : require('../assets/images/PicamoryHochformatLeft.png')
          }
          style={{
            alignSelf: 'center',
            justifyContent: 'center',
            marginVertical: 15,
            overflow: 'hidden',
            elevation: 20,
            backgroundColor: '#fff',
            height: 241,
          }}>
          <View
            style={fullImage ? styles.imageBoxWrapFull : styles.imageBoxWrap}>
            <View style={[fullImage ? styles.imageBoxFull : styles.imageBox]}>
              <TouchableOpacity
                onLongPress={() => {
                  Alert.alert(
                    'Test',
                    'Are you sure you want to delete this book page',
                    [
                      {
                        text: 'Ok',
                        onPress: () => {
                          this.props.removeOneImage(item.path);
                        },
                      },
                      {
                        text: 'Cancel',
                      },
                    ],
                    {cancelable: true},
                  );
                }}
                onPress={() => this.props.openPhoto(item.path, index)}>
                {!item?.path?.includes('null-') ? (
                  <Image
                    style={{
                      width: fullImage ? 185 : this.props.itemWidth,
                      height: fullImage ? 241 : this.props.itemHeight,
                    }}
                    source={{uri: item.path}}
                  />
                ) : (
                  <View
                    style={{
                      //flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <AddText />
                  </View>
                )}
              </TouchableOpacity>
            </View>

            {!fullImage && (
              <TouchableOpacity
                onPress={() => {
                  this.props.setStateValues({
                    modalTitle: true,
                    modalTextIndex: index,
                    modalTextValue:
                      selected[index].options && selected[index].options.text
                        ? selected[index].options.text
                        : '',
                    modalSelectedItem: item,
                    showCharacters: false,
                  });
                }}>
                <View style={{width: 141, alignItems: 'center'}}>
                  {selected[index] &&
                  selected[index].options &&
                  selected[index].options.text ? (
                    <Text
                      numberOfLines={1}
                      style={{
                        padding: 5,
                        fontWeight: 'normal',
                        fontFamily: this.props.font,
                        justifyContent: 'center',
                        marginBottom: 30,

                        alignItems: 'center',
                      }}>
                      {selected[index].options && selected[index].options.text
                        ? selected[index].options.text
                        : ''}
                    </Text>
                  ) : (
                    <View style={styles.innerTextBoxOption}>
                      <AddText />
                    </View>
                  )}
                </View>
              </TouchableOpacity>
            )}
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
  },

  imageBoxRelative: {
    //marginLeft: 20,
  },
  imageBoxFixed: {
    //marginLeft: 20,
  },
  imageBoxWrap: {
    // backgroundColor:"#ECECEC",
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
    // margin:1,
    // borderRadius:20,
    // shadowColor: "#000",
    // shadowOffset: {
    // width: 0,
    // height: 5,
    // },
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 10,
    // borderWidth: 1,
    // borderColor: '#D38074',
    //  marginVertical: 10,
  },
  imageBoxWrapFull: {
    // backgroundColor:"#ECECEC",
    justifyContent: 'center',
    alignItems: 'center',
    //marginVertical:20
  },
  imageBoxFull: {
    height: 200,
    width: 185,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 25,
  },
  imageBox: {
    height: 185,
    width: 185,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  AddImageBox: {
    borderWidth: 1,
    borderColor: '#FFFFFF',
    backgroundColor: '#D38074',
    height: 81,
    width: 81,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
    marginVertical: 10,
  },
  layoutBox: {
    borderWidth: 1,
    borderColor: '#D38074',
    backgroundColor: '#fff',
    height: 85,
    width: 85,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  innerLayoutBox: {
    backgroundColor: '#D38074',
    justifyContent: 'center',
    alignItems: 'center',
  },

  innerTextBox: {
    borderWidth: 1,
    borderColor: '#D38074',
    width: 141,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
    backgroundColor: '#FFFFFF',
  },
  innerTextBoxOption: {
    borderWidth: 1,
    borderColor: '#D38074',
    width: 141,
    // height: 30,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
    backgroundColor: '#FFFFFF',
  },
  addMorePagesOption: {
    borderWidth: 1,
    borderColor: '#D38074',
    width: 141,
    // height: 30,
    marginBottom: 25,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
    backgroundColor: '#FFFFFF',
  },
});
