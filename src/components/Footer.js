import React, {Component} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import ICONHOME from '../assets/icons/home.svg';
import HOMEACTIVE from '../assets/icons/homeActive.svg';
import ICONENTWURF from '../assets/icons/entwurf.svg';
import ICONENTWURFACTIVE from '../assets/icons/entwurfActive.svg';
import ICONPROFIL from '../assets/icons/profil.svg';
import ICONPROFILEACTIVE from '../assets/icons/profilActive.svg';
import ICONWARENKORB from '../assets/icons/warenkorb.svg';
import ICONWARENKORBACTIVE from '../assets/icons/warenkorbActive.svg';

import {connect} from 'react-redux';
class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const activePage = this.props.activePage;
    return (
      <View style={styles.footer}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('HomePage')}
          style={styles.icons}>
          {activePage === 'HomePage' ? (
            <HOMEACTIVE />
          ) : (
            <ICONHOME style={{color: this.props.color}} />
          )}
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            if (this.props.removeHandlerBack) this.props.removeHandlerBack();
            this.props.navigation.navigate('EntwurftPage');
          }}
          style={styles.icons}>
          <ICONENTWURF style={{color: this.props.color}} />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            if (this.props.removeHandlerBack) this.props.removeHandlerBack();
            this.props.navigation.navigate('ProfilPage');
          }}
          style={styles.icons}>
          {activePage === 'ProfilPage' ? (
            <ICONPROFILEACTIVE />
          ) : (
            <ICONPROFIL style={{color: this.props.color}} />
          )}
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            if (this.props.removeHandlerBack) this.props.removeHandlerBack();
            this.props.navigation.navigate('WarenkorbPage');
          }}
          style={styles.icons}>
          <ICONWARENKORB />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '2%',
    backgroundColor: '#FFFFFF',
  },
  icons: {
    paddingLeft: 10,
    paddingRight: 10,
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
  };
}

export default connect(mapStateToProps, {})(Footer);
