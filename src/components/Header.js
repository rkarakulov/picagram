import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import LogoHeader from '../assets/images/logo.svg';

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() { }

  render() {
    return (
      <View style={{
        shadowColor: '#000',
        shadowOffset: {
          width: 5,
          height: 10,
        },
        shadowOpacity: 0.1,
        shadowRadius: 8,
      }}>
        <View style={styles.header}>
          <LogoHeader style={{ maxHeight: '70%' }} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    overflow: 'hidden',
    elevation: 10,
    backgroundColor: '#fff',
    paddingTop: 5,
    width: '100%',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center'
  },
});
