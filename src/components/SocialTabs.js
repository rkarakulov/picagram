import React, {Component} from 'react';
import {View, StyleSheet, TouchableOpacity, Platform} from 'react-native';

import SmartphoneIcon from '../assets/icons/ACTIVESMARTPHONE.svg';
import SmartphoneIconInactive from '../assets/icons/INACTIVESMARTPHONE.svg';
import InstagramIconInactive from '../assets/icons/INACTIVEINSTAGRAM.svg';
import InstagramIcon from '../assets/icons/ACTIVEINSTAGRAM.svg';
import FacebookIcon from '../assets/icons/ACTIVEFACEBOOK.svg';
import FacebookIconInactive from '../assets/icons/INACTIVEFACEBOOK.svg';

export default class SocialTabs extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const type = this.props.type;
    return (
      <View
        style={{
          width: '100%',
          backgroundColor: '#D38074',
          alignSelf: 'center',
        }}>
        <View style={styles.subHeader}>
          <View style={styles.iconWrap}>
            <TouchableOpacity
              onPress={() => {
                if (type !== 'local') {
                  this.props.navigation.navigate('AlbumPage');
                }
              }}>
              {type === 'local' ? (
                <SmartphoneIcon
                  style={
                    Platform.OS === 'ios'
                      ? styles.activeShadow
                      : styles.activeShadowAndroid
                  }
                />
              ) : (
                <SmartphoneIconInactive />
              )}
            </TouchableOpacity>
          </View>
          <View style={styles.iconWrap}>
            <TouchableOpacity
              onPress={() => {
                if (type !== 'instagram') {
                  this.props.navigation.navigate('InstagramPage');
                }
              }}>
              {type === 'instagram' ? (
                <InstagramIcon
                  style={
                    Platform.OS === 'ios'
                      ? styles.activeShadow
                      : styles.activeShadowAndroid
                  }
                />
              ) : (
                <InstagramIconInactive />
              )}
            </TouchableOpacity>
          </View>
          <View style={styles.iconWrap}>
            <TouchableOpacity
              onPress={() => {
                if (type !== 'facebook') {
                  this.props.navigation.navigate('FacebookPage');
                }
              }}>
              {type === 'facebook' ? (
                <FacebookIcon
                  style={
                    Platform.OS === 'ios'
                      ? styles.activeShadow
                      : styles.activeShadowAndroid
                  }
                />
              ) : (
                <FacebookIconInactive />
              )}
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '2%',
    backgroundColor: '#FFFFFF',
  },
  iconWrap: {
    paddingTop: 5,
  },
  icons: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  activeShadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 5,
    },
    shadowOpacity: 0.2,
    shadowRadius: 9,
    zIndex: 999999,
  },
  activeShadowAndroid: {
    borderWidth: 0,
    borderRadius: 30,
    elevation: 10,
    shadowColor: 'rgba(0,0,0,0.5)',
    height: 60,
    width: 60,
  },
  subHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '70%',
    alignSelf: 'center',
  },
  icons: {
    position: 'absolute',
    alignSelf: 'center',
  },
  smartphoneIcon: {
    position: 'absolute',
    alignSelf: 'center',
  },
});
