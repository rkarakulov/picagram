import React, { Component } from 'react';
import { Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import BackIcon from '../assets/icons/ArrowBack.svg';

export default class SignInHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.header}>
        <TouchableOpacity onPress={() => this.props.onBackClick()} style={styles.backButton}>
          <BackIcon />
        </TouchableOpacity>
        <View style={styles.headerTitle}>
          <Text style={styles.signin}>SIGN IN</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    marginTop: 0,
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: '5%',
    backgroundColor: '#D38074',
  },
  backButton: {
    padding: 20,
    position: 'absolute',
    zIndex: 1,
  },
  headerTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
  },
  signin: {
    color: '#FFFFFF',
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
});
