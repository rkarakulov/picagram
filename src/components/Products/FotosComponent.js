import React, { Component } from 'react';
import {
  Dimensions,
  Image,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  TouchableOpacity,
  View,
} from 'react-native';
import ArrowBack from '../../assets/icons/arrowLeft.svg';
import FotobuchImage from '../../assets/images/fotobuchImage.svg';
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import CloseButton from '../../assets/icons/buttonX.svg';
import FotosIcon from '../../assets/icons/fotosIcon';
import Swiper from 'react-native-custom-swiper';
import { connect } from 'react-redux';
import InfoIcon from '../../assets/icons/infoIcon';
import { SvgUri } from 'react-native-svg';
import BouncyCheckbox from 'react-native-bouncy-checkbox';

import { updateCart, updateCartSelected } from '../../libs/card/domain/CartActions';

var width = Dimensions.get('window').width;

class FotosComponent extends Component {
  constructor(props) {
    super(props);
    const item = this.props.item;
    const album = this.props.album;

    let glosy = false;
    let mat = false;

    const cart = this.props.cart.cart;
    let selectedItem = null;
    let selectedIndex = null;
    const searchName = album.title + ' ' + item.productName;
    const index = cart.findIndex((obj) => obj.albumName === searchName);
    if (index != -1) {
      selectedItem = cart[index];
      if (selectedItem && selectedItem.finishingTypes) {
        glosy = selectedItem.finishingTypes.glossy ? selectedItem.finishingTypes.glossy : false;
        mat = selectedItem.finishingTypes.mat ? selectedItem.finishingTypes.mat : false;
      }
    }
    this.state = {
      selectedSwipeIndex: 0,
      modalImageOptions: false,
      albumItem: item,
      album: album,
      albumName: album.title + ' ' + item.productName,
      albumSize: 24,
      isCheckedGlosy: glosy,
      isCheckedMat: mat,
      imgArray: [
        require('../../assets/images/fotobuchImage.svg'),
        require('../../assets/images/fotobuchImage.svg'),
        require('../../assets/images/fotobuchImage.svg'),
      ],
      currentIndex: 0,
      modal: false,
    };
  }

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      this.loadPropsData();
    });
    this.loadPropsData();
  }

  loadPropsData = () => {
    const item = this.props.item;
    const album = this.props.album;

    let glosy = false;
    let mat = false;

    const cart = this.props.cart.cart;
    let selectedItem = null;
    let selectedIndex = null;
    const searchName = album.title + ' ' + item.productName;
    const index = cart.findIndex((obj) => obj.albumName === searchName);
    if (index != -1) {
      selectedItem = cart[index];
      if (selectedItem && selectedItem.finishingTypes) {
        glosy = selectedItem.finishingTypes.glossy ? selectedItem.finishingTypes.glossy : false;
        mat = selectedItem.finishingTypes.mat ? selectedItem.finishingTypes.mat : false;
      }
    }
    this.setState({
      modalImageOptions: false,
      modal: false,
      albumItem: item,
      isCheckedGlosy: glosy,
      isCheckedMat: mat,
      selectedSwipeIndex: 0,
      album: album,
      albumName: album.title + ' ' + item.productName,
      albumSize: 24,
      imgArray: [
        require('../../assets/images/fotobuchImage.svg'),
        require('../../assets/images/fotobuchImage.svg'),
        require('../../assets/images/fotobuchImage.svg'),
      ],
      currentIndex: 0,
    });
  };

  onBackClick = () => {
    this.setState({ selectedSwipeIndex: 0, currentIndex: 0 }, () => {
      this.props.onBack();
    });
  };

  onGestaltungStartenClick = () => {
    if (this.state.album && this.state.album.photoOptions) this.setState({ modalImageOptions: true });
    else {
      this.onButtonClick();
    }
  };

  onButtonClick = () => {
    //  console.log('cart');
    //  console.log(this.props.cart);
    if (this.state.album && this.state.album.photoOptions && !this.state.isCheckedGlosy && !this.state.isCheckedMat) {
      alert('Please select print type!');
    } else {
      this.setState({ modalImageOptions: false }, () => {
        const item = this.props.item;
        const album = this.props.album;

        const cart = this.props.cart.cart;
        let selectedItem = null;
        let selectedIndex = null;

        const searchName = album.title + ' ' + item.productName;

        let glossy = false;
        let mat = false;

        if (this.state.isCheckedGlosy) {
          glossy = this.state.isCheckedGlosy;
        } else if (this.state.isCheckedMat) {
          mat = this.state.isCheckedMat;
        } else {
          if (item.customFields) {
            if (item.customFields.pm_product_designer_mate) mat = true;
            if (item.customFields.pm_product_designer_glossy) glossy = true;
          }
        }
        const index = cart.findIndex((obj) => obj.albumName === searchName);
        if (index != -1) {
          selectedItem = cart[index];
          selectedItem.finishingTypes = {
            glossy: glossy,
            mat: mat,
          };
          selectedIndex = index;
        }

        if (cart.length > 0 && selectedIndex === null) {
          selectedIndex = cart.length;
        }

        if (!selectedItem) {
          let cartData = [...cart];
          selectedItem = {
            albumSize: this.state.albumSize,
            albumName: this.state.albumName,
            albumItem: {
              price: item.price,
              description: item.description,
              shortDescription: item.shortDescription,
              productName: item.productName,
              image: item.image,
              id: item.id,
              customFields: item.customFields,
            },
            album: {
              color: album.color,
              description: album.description,
              price: album.price,
              title: album.title,
              productName: album.productName,
              id: album.id,
              layout: album.layout,
            },
            selected: [],
            finishingTypes: {
              glossy: glossy,
              mat: mat,
            },
          };
          cartData.push(selectedItem);
          this.props.updateCart(cartData);
          this.props.updateCartSelected(selectedItem, selectedIndex ? selectedIndex : 0);
        } else {
          this.props.updateCartSelected(selectedItem, selectedIndex ? selectedIndex : 0);
        }

        this.props.openPage('AlbumPage');
      });
    }
  };
  renderImageSwipeItem = (item) => {
    const originalWidth = 390;
    const originalHeight = 359;
    const aspectRatio = originalWidth / originalHeight;
    const windowWidth = Dimensions.get('window').width;

    return (
      <View style={{ width: windowWidth, aspectRatio, alignItems: 'center' }}>
        {!item && <FotobuchImage width="100%" height="100%" viewBox={`0 0 ${originalWidth} ${originalHeight}`} />}
        {item && <Image style={{ width: windowWidth, height: windowWidth, margin: 5 }} source={{ uri: item }} />}
      </View>
    );
  };

  screenChange = (index) => {
    //console.log('index when change :=> \n', index);
    this.setState({ selectedSwipeIndex: index, currentIndex: index });
  };

  render() {
    const originalWidth = 390;
    const originalHeight = 359;
    const aspectRatio = originalWidth / originalHeight;
    const windowWidth = Dimensions.get('window').width;
    const item = this.props.item;
    const album = this.props.album;
    const title = album && album.title ? album.title : '';
    const background = TouchableNativeFeedback.Ripple('#fff', true);

    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => this.onBackClick()} style={styles.backButton}>
            <ArrowBack />
          </TouchableOpacity>
          <View style={styles.headerIconAndTitle}>
            <FotosIcon />
            <Text style={styles.headerTitle}>{title}</Text>
          </View>
        </View>

        <ScrollView>
          {item.swipe && item.swipe.length > 0 && item.swipe.length >= this.state.selectedSwipeIndex && (
            <View>
              <Swiper
                style={{ flex: 1, height: 359, width: 390 }}
                currentSelectIndex={this.state.selectedSwipeIndex}
                showSwipeBtn={true}
                leftButtonImage={require('../../assets/images/arrowLeft.png')}
                rightButtonImage={require('../../assets/images/arrowRight.png')}
                swipeData={item.swipe}
                renderSwipeItem={this.renderImageSwipeItem}
                onScreenChange={this.screenChange}
              />
            </View>
          )}
          <View>
            <View style={styles.titleContainer}>
              <View style={{ width: '70%' }}>
                <Text style={[styles.textTitlesContainer, { color: album.color }]}>{item.productName}</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingBottom: 10,
                  }}
                >
                  <Text style={styles.textTitlesContainer}>AB {item.price} EURO</Text>
                  <TouchableOpacity onPress={() => this.setState({ modal: true })} style={{ padding: 10 }}>
                    <InfoIcon />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{ width: '30%' }}>
                <Text style={[styles.textTitlesContainer, { color: album.color }]}>{item.shortDescription}</Text>
              </View>
            </View>
            <View style={{ paddingBottom: 20 }}>
              <Text style={styles.textStyle}>{item.description}</Text>
            </View>
            <View style={styles.perfektContainer}>
              <Text style={styles.perfektTitle}>PERFEKT ALS...</Text>

              {item.features && item.features.length > 0 && (
                <View style={styles.containerLoveandVacation}>
                  {item.features.map((itemIm, key) => {
                    //   console.log('item im', itemIm);
                    let imageType = 'svg';
                    //console.log(itemIm.image);
                    if (itemIm.image && !(itemIm.image.indexOf('.svg') > -1)) {
                      imageType = 'png';
                      //console.log('obo je pnfg');
                    } else {
                      // console.log('nije usao');
                    }

                    //console.log(imageType);
                    return (
                      <View
                        key={'feature' + key}
                        style={{
                          flexDirection: 'row',
                          width: '50%',
                        }}
                      >
                        {itemIm.image && imageType === 'svg' && (
                          <SvgUri style={{ width: 165, height: 165 }} uri={itemIm.image} />
                        )}

                        {itemIm.image && imageType === 'png' && (
                          <Image
                            style={{
                              resizeMode: 'cover',
                              width: 50,
                              height: 50,
                            }}
                            source={{ uri: itemIm.image }}
                          />
                        )}

                        <Text style={styles.textLoveandVacation}>{itemIm.title}</Text>
                      </View>
                    );
                  })}
                </View>
              )}
            </View>

            {item.featuresBottom && item.featuresBottom.length > 0 && (
              <View style={{ paddingBottom: 30, width: '90%', alignSelf: 'center' }}>
                {item.featuresBottom.map((itemIm, key) => {
                  return (
                    <View key={'featuresBottom' + key} style={styles.iconContainer}>
                      {itemIm.image && (
                        <View
                          style={{
                            width: 60,
                            height: 60,
                          }}
                        >
                          <SvgUri style={{ width: 60, height: 60 }} uri={itemIm.image} />
                        </View>
                      )}

                      <Text style={styles.textStyleIcon}>{itemIm.title}</Text>
                    </View>
                  );
                })}
              </View>
            )}
          </View>
        </ScrollView>
        {/* <View>
         <Text style={styles.whiteFooter} />
         </View> */}

        <View
          style={{
            borderRadius: 20,
            overflow: 'hidden',
            width: '90%',
            alignSelf: 'center',
            bottom: 30,
            paddingBottom: 5,
          }}
        >
          <TouchableNativeFeedback
            background={background}
            useForeground={true}
            onPress={() => this.onGestaltungStartenClick()}
          >
            <View>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={['#FFD06C', '#D78275', '#B42762']}
                style={styles.LinearGradient}
              >
                <Text style={styles.textFooter}>GESTALTUNG STARTEN</Text>
              </LinearGradient>
            </View>
          </TouchableNativeFeedback>
        </View>

        <Modal
          onRequestClose={() => this.setState({ modal: false })}
          transparent={true}
          isVisible={this.state.modal}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <View style={styles.modalContent}>
            <View style={styles.innerModalContent}>
              <TouchableOpacity onPress={() => this.setState({ modal: false })} style={styles.closeButtonPreis}>
                <CloseButton />
              </TouchableOpacity>
              <Text style={styles.modalTitle}>PREIS?{'\n'}</Text>
              <Text style={styles.modalSubTitle}>
                {item.price} €{'\n'}Bis 24 Seiten + 0,89 € pro zusätzlicher Seite
              </Text>

              <View style={[styles.footerModal, { borderRadius: 20, overflow: 'hidden', paddingBottom: 5 }]}>
                <TouchableNativeFeedback
                  useForeground={true}
                  background={background}
                  onPress={() => this.setState({ modal: false })}
                >
                  <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    colors={['#FFD06C', '#D78275', '#B42762']}
                    style={styles.LinearGradient}
                  >
                    <Text style={styles.textFooter}>OK</Text>
                  </LinearGradient>
                </TouchableNativeFeedback>
              </View>
            </View>
          </View>
        </Modal>

        <Modal
          onRequestClose={() => this.setState({ modalImageOptions: false })}
          transparent={false}
          isVisible={this.state.modalImageOptions}
          style={{
            margin: 0,
            paddingTop: Platform.OS === 'ios' ? 0 : 0,
          }}
        >
          <SafeAreaView
            style={{
              flex: 1,
              backgroundColor: '#FFFFFF',
            }}
          >
            <ScrollView>
              <View>
                <View style={{ alignItems: 'center' }}>
                  {this.state.isCheckedGlosy ? (
                    <Image
                      source={require('../../assets/images/glossy.jpg')}
                      style={{ height: originalHeight, width: width }}
                    />
                  ) : (
                    <Image
                      source={require('../../assets/images/mate.jpg')}
                      style={{ height: originalHeight, width: width }}
                    />
                  )}
                </View>
                <View
                  style={{
                    position: 'absolute',
                    alignSelf: 'flex-end',
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.setState({ modalImageOptions: false })}
                    style={styles.closeButton}
                  >
                    <CloseButton />
                  </TouchableOpacity>
                </View>

                <View>
                  <Text
                    style={{
                      paddingVertical: 20,
                      textAlign: 'center',
                      color: '#000',
                      fontFamily: 'Lato-Bold',
                      fontWeight: '700',
                      fontSize: 25,
                    }}
                  >
                    Wähle eine Option für deine Fotos
                  </Text>
                </View>

                <View
                  style={{
                    width: 200,
                    shadowColor: '#000',
                    shadowOffset: {
                      width: 10,
                      height: 10,
                    },
                    shadowOpacity: 0.8,
                    shadowRadius: 15,
                    alignSelf: 'center',
                    paddingBottom: 50,
                  }}
                >
                  <View
                    style={{
                      backgroundColor: '#fff',
                      overflow: 'hidden',
                      elevation: 15,
                    }}
                  >
                    <Text
                      style={{
                        paddingVertical: 10,
                        textAlign: 'center',
                        color: '#000',
                        fontFamily: 'Lato-Bold',
                        fontWeight: '700',
                        fontSize: 15,
                      }}
                    >
                      Oberfläche
                    </Text>
                    <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                      <View
                        style={{
                          marginHorizontal: 5,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <BouncyCheckbox
                          fillColor="#D38074"
                          disableText={true}
                          disableBuiltInState={true}
                          onPress={() =>
                            this.setState({
                              isCheckedGlosy: true,
                              isCheckedMat: false,
                            })
                          }
                          isChecked={this.state.isCheckedGlosy}
                        />
                        <Text
                          style={{
                            paddingVertical: 10,
                            textAlign: 'center',
                            color: '#000',
                            fontFamily: 'Lato-Bold',
                            fontWeight: '700',
                            fontSize: 15,
                          }}
                        >
                          Glänzend
                        </Text>
                      </View>
                      <View
                        style={{
                          marginHorizontal: 5,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <BouncyCheckbox
                          fillColor="#D38074"
                          disableText={true}
                          disableBuiltInState={true}
                          onPress={() =>
                            this.setState({
                              isCheckedGlosy: false,
                              isCheckedMat: true,
                            })
                          }
                          isChecked={this.state.isCheckedMat}
                        />
                        <Text
                          style={{
                            paddingVertical: 10,
                            textAlign: 'center',
                            color: '#000',
                            fontFamily: 'Lato-Bold',
                            fontWeight: '700',
                            fontSize: 15,
                          }}
                        >
                          Matt
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>

            <View
              style={{
                borderRadius: 20,
                overflow: 'hidden',
                width: '90%',
                alignSelf: 'center',
                bottom: 30,
                paddingBottom: 5,
              }}
            >
              <TouchableNativeFeedback
                background={background}
                useForeground={true}
                onPress={() => this.onButtonClick()}
              >
                <View>
                  <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    colors={['#FFD06C', '#D78275', '#B42762']}
                    style={styles.LinearGradient}
                  >
                    <Text style={styles.textFooter}>GESTALTUNG STARTEN</Text>
                  </LinearGradient>
                </View>
              </TouchableNativeFeedback>
            </View>
          </SafeAreaView>
        </Modal>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  LinearGradientModal: {
    borderRadius: 20,
    padding: '4%',
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 2,
  },
  headerContainer: {
    // marginTop: Platform.OS === 'ios' ? 20 : 0,
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 15,
    backgroundColor: '#FFFFFF',

    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 10,
    },
    shadowOpacity: 0.2,
    shadowRadius: 8,
    zIndex: 999999,
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  headerIconAndTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 20,
    width: '90%',
    alignSelf: 'center',
  },
  textTitleColorPink: {
    color: '#D38074',
  },
  textTitlesContainer: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  textStyle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    width: '90%',
    alignSelf: 'center',
  },
  textStyleIcon: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    width: '85%',
    alignSelf: 'center',
  },
  iconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  perfektContainer: {
    position: 'relative',
    paddingBottom: 40,
    // backgroundColor:'lightblue'
  },
  perfektTitle: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    alignSelf: 'center',
    // backgroundColor:'black',
    marginBottom: 20,
  },
  containerLoveandVacation: {
    flexDirection: 'row',
    width: '90%',
    justifyContent: 'space-between',
    alignSelf: 'center',
    alignItems: 'flex-start',
  },
  textLoveandVacation: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    width: '67%',
    paddingLeft: 10,
    // backgroundColor:'green',
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.17,
    shadowRadius: 2.49,
    elevation: 2,
  },
  footer: {
    position: 'absolute',
    alignSelf: 'center',
    width: '100%',
    // backgroundColor:'black',
  },
  whiteFooter: {
    width: '100%',
    padding: '5%',
  },

  modalContent: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerModalContent: {
    backgroundColor: '#FFFFFF',
    width: '90%',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  // modalTitle: {
  //   fontSize: 13,
  //   fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  //   textAlign: 'center',
  // },
  // modalButton: {
  //   padding: 15,
  //   borderWidth: 1,
  //   width: '100%',
  //   borderColor: '#FFFFFF',
  //   borderRadius: 20,

  // },
  // modalButtonText: {
  //   color: '#FFFFFF',
  //   // paddingHorizontal: '20%',
  //   fontSize: 13,
  //   fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  // },
  // closeButton: {
  //   position: 'relative',
  //   alignSelf: 'flex-end',
  //   padding: 20,
  //   bottom: '15%',
  // },
  modalTitle: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  closeButton: {
    margin: 10,
    borderRadius: 40,
    padding: 15,
    backgroundColor: 'rgba(255,255,255,0.5)',
  },
  closeButtonPreis: {
    position: 'relative',
    alignSelf: 'flex-end',
    padding: 20,
  },
  modalSubTitle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
  },
  footerModal: {
    alignSelf: 'center',
    width: 193,
    marginVertical: 20,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
  };
}

export default connect(mapStateToProps, { updateCart, updateCartSelected })(FotosComponent);
