import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Platform,
} from 'react-native';
import ArrowBack from '../../assets/icons/arrowLeft.svg';
import FotobuchImage from '../../assets/images/fotobuchImage.svg';
import DesignIcon from '../../assets/icons/designIcon';
import LayoutIcon from '../../assets/icons/layoutIcon';
import FormatIcon from '../../assets/icons/formatIcon';
import ShippingIcon from '../../assets/icons/shippingIcon';
import LoveIcon from '../../assets/icons/loveIcon';
import VacationIcon from '../../assets/icons/vacationIcon';
import LinearGradient from 'react-native-linear-gradient';
import FotoboxIcon from '../../assets/icons/fotoboxIcon';
import Swiper from 'react-native-custom-swiper';

export default class FotoBoxComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgArray: [
        require('../../assets/images/fotobuchImage.svg'),
        require('../../assets/images/fotobuchImage.svg'),
        require('../../assets/images/fotobuchImage.svg'),
      ],
      currentIndex: 0,
    };
  }

  componentDidMount() {}

  onBackClick = () => {
    this.props.onBack();
  };
  onButtonClick = () => {
    this.props.openPage('AlbumPage');
  };
  renderImageSwipeItem = item => {
    const originalWidth = 390;
    const originalHeight = 359;
    const aspectRatio = originalWidth / originalHeight;
    const windowWidth = Dimensions.get('window').width;
    return (
      <View style={{width: windowWidth, aspectRatio, alignItems: 'center'}}>
        <FotobuchImage
          width="100%"
          height="100%"
          viewBox={`0 0 ${originalWidth} ${originalHeight}`}
        />
      </View>
    );
  };

  screenChange = index => {
    //console.log('index when change :=> \n', index);
    this.setState({currentIndex: index});
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity
            style={styles.backButton}
            onPress={() => this.onBackClick()}
            style={styles.backButton}>
            <ArrowBack />
          </TouchableOpacity>

          <View style={styles.headerIconAndTitle}>
            <FotoboxIcon />
            <Text style={styles.headerTitle}>FOTOBOX</Text>
          </View>
        </View>

        <ScrollView>
          <View style={{position: 'relative'}}>
            <Swiper
              style={{flex: 1, height: 359, width: 390}}
              currentSelectIndex={0}
              swipeData={this.state.imgArray}
              renderSwipeItem={this.renderImageSwipeItem}
              onScreenChange={this.screenChange}
            />
            <View>
              <View style={styles.titleContainer}>
                <View>
                  <Text
                    style={[styles.textTitlesContainer, styles.textTitleColor]}>
                    FOTOBUCH „IT´S MY LIFE“
                  </Text>
                  <Text style={styles.textTitlesContainer}>
                    AB XX,XX EURO{'\n'}
                  </Text>
                </View>
                <Text
                  style={[styles.textTitlesContainer, styles.textTitleColor]}>
                  21X21 CM
                </Text>
              </View>
              <Text style={styles.textStyle}>
                Hariberia vendit as moluption ni vel maximet mo eatibus{'\n'}aut
                eicieni ut exerem rero ipit officti comnit, ut hillia{'\n'}
                alitiis eum estio volore porit, eos distis rercillor si int
                {'\n'}officiam vendant, ullaborum sitem corum la quia quam,
                {'\n'}odiatem repedipiciis sapiden dunti.
                {'\n\n'}
              </Text>

              <View style={styles.perfektContainer}>
                <Text style={styles.perfektTitle}>PERFEKT ALS...</Text>

                <View style={styles.containerLoveandVacation}>
                  <TouchableOpacity>
                    <LoveIcon />
                  </TouchableOpacity>
                  <Text style={styles.textLoveandVacation}>
                    Schenke deine/n{'\n'}Liebste/n eure{'\n'}schönste Foto-
                    {'\n'}sammlung
                  </Text>
                  <TouchableOpacity>
                    <VacationIcon />
                  </TouchableOpacity>
                  <Text style={styles.textLoveandVacation}>
                    Deine schönen{'\n'}
                    Urlaubserinner-{'\n'}ungen zum{'\n'}Durchblättern
                  </Text>
                </View>
              </View>

              <View
                style={{paddingBottom: 30, width: '90%', alignSelf: 'center'}}>
                <View style={styles.iconContainer}>
                  <TouchableOpacity>
                    <DesignIcon />
                  </TouchableOpacity>
                  <Text style={styles.textStyle}>
                    Individuelle Gestaltung, große Auswahl von{'\n'}
                    Farben und Schriftarten und grafischen{'\n'}Elementen
                  </Text>
                </View>

                <View style={styles.iconContainer}>
                  <TouchableOpacity>
                    <LayoutIcon />
                  </TouchableOpacity>
                  <Text style={styles.textStyle}>
                    XX bis XX Seiten mit Soft- oder Hardcover in{'\n'}
                    verschiedenen Layouts, Innenseiten aus{'\n'}Fotopapier oder
                    Hochglanzpapier
                  </Text>
                </View>

                <View style={styles.iconContainer}>
                  <TouchableOpacity>
                    <FormatIcon />
                  </TouchableOpacity>
                  <Text style={styles.textStyle}>
                    Wähle zwischen zwei verschiedenen Formaten{'\n'}aus: 21x21
                    cm oder 14x14 cm
                  </Text>
                </View>

                <View style={styles.iconContainer}>
                  <TouchableOpacity>
                    <ShippingIcon />
                  </TouchableOpacity>
                  <Text style={styles.textStyle}>
                    Vesand aus Deutschland,{'\n'}
                    Internationaler- und Expressversand möglich
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
        <View>
          <View>
            <Text style={styles.whiteFooter} />
          </View>

          <TouchableOpacity
            style={styles.footer}
            onPress={() => this.onButtonClick()}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#FFD06C', '#D78275', '#B42762']}
              style={styles.LinearGradient}>
              <Text style={styles.textFooter}>GESTALTUNG STARTEN</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    marginTop: Platform.OS === 'ios' ? 20 : 0,
    flex: 0,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 15,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    padding: 10,
    zIndex: 1,
  },
  headerTitle: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  headerIconAndTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    justifyContent: 'center',
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
    width: '90%',
    alignSelf: 'center',
  },
  textTitleColor: {
    color: '#12336B',
  },
  textTitlesContainer: {
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  textStyle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    width: '90%',
    alignSelf: 'center',
  },
  iconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  perfektContainer: {
    marginBottom: 20,
    position: 'relative',
  },
  perfektTitle: {
    fontSize: 18,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    alignSelf: 'center',
    marginBottom: '5%',
  },
  containerLoveandVacation: {
    flexDirection: 'row',
    width: '90%',
    justifyContent: 'space-between',
    alignSelf: 'center',
    alignItems: 'center',
  },
  textLoveandVacation: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  LinearGradient: {
    borderRadius: 22,
    padding: 15,
    width: '90%',
    alignSelf: 'center',
  },
  footer: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: 30,
    width: '100%',
  },
  whiteFooter: {
    width: '100%',
    backgroundColor: 'white',
    padding: '5%',
  },
});
