import React, {Component} from 'react';
import {
  Alert,
  Image,
  StyleSheet,
  TouchableNativeFeedback,
  TouchableOpacity,
  View,
} from 'react-native';
import AddText from '../assets/icons/addText.svg';

export default class ProductPostcardLayout extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const background = TouchableNativeFeedback.Ripple('#C0C0C0', true);

    const item = this.props.item;
    const index = this.props.index;
    const selectedIndex = this.props.cart.selectedAlbumIndex
      ? this.props.cart.selectedAlbumIndex
      : 0;
    const selected =
      this.props.cart.cart[selectedIndex] &&
      this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];
    const count = item && item.qty ? item.qty : 1;

    let modalTextValue = '';
    if (this.props.modalTextIndex && this.props.modalTextIndex > -1) {
      modalTextValue =
        selected[this.props.modalTextIndex].options &&
        selected[this.props.modalTextIndex].options.text
          ? selected[this.props.modalTextIndex].options.text
          : '';
    }

    // let modalMessageValue = '';
    // if (this.props.modalMessageIndex && this.props.modalMessageIndex > -1)
    //     modalMessageValue =
    //         selected[this.props.modalMessageIndex].options &&
    //             selected[this.props.modalMessageIndex].options.textMessage
    //             ? selected[this.props.modalMessageIndex].options.textMessage
    //             : '';

    return (
      <View style={styles.container}>
        <View
          style={[
            styles.containerContent,
            {
              elevation: 20,
              shadowColor: '#000',
              shadowOffset: {
                width: 10,
                height: 10,
              },
              shadowOpacity: 0.2,
              shadowRadius: 40,
            },
          ]}>
          <View>
            <View
              style={[
                styles.layoutContainer,
                {
                  marginBottom: 10,
                  overflow: 'hidden',
                  backgroundColor: '#fff',
                  elevation: 20,
                },
              ]}>
              <TouchableNativeFeedback
                onLongPress={() => {
                  Alert.alert(
                    'Test',
                    'Are you sure you want to delete this book page',
                    [
                      {
                        text: 'Ok',
                        onPress: () => {
                          this.props.removeOneImage(item.path);
                        },
                      },
                      {
                        text: 'Cancel',
                      },
                    ],
                    {cancelable: true},
                  );
                }}
                onPress={() => this.props.openPhoto(item.path, index)}
                useForeground={false}
                background={background}>
                <View style={styles.innerImageContainer}>
                  {!item?.path?.includes('null-') ? (
                    <Image
                      style={{
                        resizeMode: 'cover',
                        width: '95%',
                        height: '95%',
                      }}
                      source={{uri: item.path}}
                    />
                  ) : (
                    <TouchableOpacity style={{padding: 10}}>
                      <AddText />
                    </TouchableOpacity>
                  )}
                </View>
              </TouchableNativeFeedback>
            </View>
          </View>
          <View
            style={[
              styles.layoutContainer,
              {overflow: 'hidden', backgroundColor: '#fff', elevation: 20},
            ]}>
            <View
              style={{
                width: '100%',
                height: '100%',
                padding: 20,
                justifyContent: 'space-between',
                flexDirection: 'row',
              }}>
              <View style={styles.messageContainer} />
              <View style={styles.messageContainerText}>
                <View
                  style={{
                    width: 40,
                    height: 50,
                    borderWidth: 2,
                    alignSelf: 'flex-end',
                  }}
                />
                <View
                  style={{
                    width: '85%',
                    alignSelf: 'flex-end',
                    height: 120,
                    justifyContent: 'space-between',
                    position: 'absolute',
                    bottom: 0,
                  }}>
                  <View style={{borderBottomWidth: 2}} />
                  <View style={{borderBottomWidth: 2}} />
                  <View style={{borderBottomWidth: 2}} />
                  <View style={{borderBottomWidth: 2}} />
                  <View style={{borderBottomWidth: 2}} />
                  <View />
                </View>
              </View>
              {/* <View style={styles.messageContainer}>
               <TouchableNativeFeedback
               onPress={() => {
               this.props.setStateValues({
               modalPostcardTitle: true,
               modalTextIndex: index,
               modalTextValue:
               selected[index].options && selected[index].options.text
               ? selected[index].options.text
               : '',
               modalMessageValue:
               selected[index].options &&
               selected[index].options.textMessage
               ? selected[index].options.textMessage
               : '',
               modalSelectedItem: item,
               showCharacters: false,
               });
               }}
               useForeground={true}
               background={background}>
               <View
               style={{
               height: '100%',
               width: '100%',
               justifyContent: 'center',
               }}>
               {selected[index] &&
               selected[index].options &&
               selected[index].options.text ? (
               <View
               style={{
               height: '100%',
               width: '100%',
               justifyContent: 'flex-start',
               }}>
               <View style={{}}>
               <Text
               numberOfLines={2}
               adjustsFontSizeToFit
               style={[
               styles.textMessageStyle,
               { fontFamily: this.props.font },
               ]}>
               {selected[index].options &&
               selected[index].options.text
               ? selected[index].options.text
               : ''}
               </Text>
               </View>
               <View style={{}}>
               <Text
               numberOfLines={10}
               adjustsFontSizeToFit
               style={[
               styles.textMessageStyle,
               { fontFamily: this.props.font },
               ]}>
               {selected[index].options &&
               selected[index].options.textMessage
               ? selected[index].options.textMessage
               : ''}
               </Text>
               </View>
               </View>
               ) : (
               <View>
               <Text
               style={{
               textAlign: 'center',
               color: '#D38074',
               fontSize: 20,
               fontFamily:
               Platform.OS !== 'android'
               ? 'Courier Prime'
               : 'Courier-Prime',
               fontWeight: '600',
               }}>
               Aa
               </Text>
               <Text
               style={{
               textAlign: 'center',
               color: '#D38074',
               fontSize: 10,
               fontFamily:
               Platform.OS !== 'android'
               ? 'Courier Prime'
               : 'Courier-Prime',
               }}>
               {' '}
               Your message
               </Text>
               </View>
               )}
               </View>
               </TouchableNativeFeedback>
               </View>
               <View style={{ width: '40%' }}>
               <Text style={styles.informationStyle}>
               The postcard will be shipped to the address selected at
               checkout
               </Text>
               </View> */}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerContent: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flex: 1,
    marginVertical: 20,
  },
  layoutContainer: {
    width: 350,
    height: 250,
    marginBottom: 10,
    // overflow: 'hidden',
  },
  innerImageContainer: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  messageContainer: {
    borderRightWidth: 5,
    height: '100%',
    width: '50%',
    // alignSelf: 'flex-start',
    justifyContent: 'center',
  },
  messageContainerText: {
    height: '100%',
    width: '50%',

    // alignSelf: 'flex-start',
  },
  textMessageStyle: {
    padding: 5,
    textAlign: 'left',
    fontWeight: 'normal',
    marginBottom: 10,
  },
  informationStyle: {
    fontSize: 12,
    textDecorationLine: 'underline',
    textAlign: 'left',
    color: '#A0A0A0',
    fontFamily: 'Lato-Bold',
  },
});
