import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ImageBackground,
  Platform,
  Alert,
  TouchableNativeFeedback,
} from 'react-native';
import AddText from '../assets/icons/addText.svg';

export default class ProductSingleLayout extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {};
  }
  renderLayoutTypes() {}
  onPlusPress = index => {
    this.setState({count: this.state.count + 1});
  };

  onMinusPress = index => {
    this.setState({count: this.state.count - 1});
  };
  render() {
    const item = this.props.item;
    const count = item && item.qty ? item.qty : 1;

    const index = this.props.index;
    const selectedIndex = this.props.cart.selectedAlbumIndex
      ? this.props.cart.selectedAlbumIndex
      : 0;
    const selected =
      this.props.cart.cart[selectedIndex] &&
      this.props.cart.cart[selectedIndex].selected
        ? this.props.cart.cart[selectedIndex].selected
        : [];

    let modalTextValue = '';
    if (this.props.modalTextIndex && this.props.modalTextIndex > -1)
      modalTextValue =
        selected[this.props.modalTextIndex].options &&
        selected[this.props.modalTextIndex].options.text
          ? selected[this.props.modalTextIndex].options.text
          : '';

    let leftFull = this.props.leftFull;
    let rightFull = this.props.rightFull;

    let fullImage = leftFull;

    let layoutMainType = this.props.layoutMainType;

    let showFooterTextOption = this.props.showFooterTextOption;

    let showFooterAddMoreImages = this.props.showFooterAddMoreImages;
    //console.log('footer text ' + showFooterTextOption);
    console.log('layoutmaintype: ' + layoutMainType);
    return (
      <View>
        {layoutMainType === 'portrait' && (
          <View
            style={{
              shadowColor: '#000',
              shadowOffset: {
                width: 10,
                height: 10,
              },
              shadowOpacity: 0.5,
              shadowRadius: 15,
              zIndex: 999,
              height: 230,
            }}>
            <ImageBackground
              source={require('../assets/images/PicamoryHochformatLeft.png')}
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                marginBottom: 15,
                backgroundColor: '#fff',
                overflow: 'hidden',
                elevation: 15,
                height: 230,
              }}>
              <View
                style={
                  fullImage ? styles.imageBoxWrapFull : styles.imageBoxWrap
                }>
                <View
                  style={[fullImage ? styles.imageBoxFull : styles.imageBox]}>
                  <TouchableOpacity
                    onLongPress={() => {
                      Alert.alert(
                        'Test',
                        'Are you sure you want to delete this book page',
                        [
                          {
                            text: 'Ok',
                            onPress: () => {
                              this.props.removeOneImage(item.path);
                            },
                          },
                          {
                            text: 'Cancel',
                          },
                        ],
                        {cancelable: true},
                      );
                    }}
                    onPress={() => this.props.openPhoto(item.path, index)}>
                    {!item?.path?.includes('null-') ? (
                      <Image
                        style={{
                          width: fullImage ? 185 : this.props.itemWidth,
                          height: fullImage ? 230 : this.props.itemHeight,
                        }}
                        source={{uri: item.path}}
                      />
                    ) : (
                      <View
                        style={{
                          // flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <AddText />
                      </View>
                    )}
                  </TouchableOpacity>
                  {!fullImage && (
                    <TouchableOpacity
                      onPress={() => {
                        this.props.setStateValues({
                          modalTitle: true,
                          modalTextIndex: index,
                          modalTextValue:
                            selected[index].options &&
                            selected[index].options.text
                              ? selected[index].options.text
                              : '',
                          modalSelectedItem: item,
                          showCharacters: false,
                        });
                      }}>
                      <View style={{width: 141, alignItems: 'center'}}>
                        {selected[index] &&
                        selected[index].options &&
                        selected[index].options.text ? (
                          <Text
                            numberOfLines={1}
                            style={{
                              //padding: 5,
                              fontWeight: 'normal',
                              fontFamily: this.props.font,
                              justifyContent: 'center',
                              marginVertical: 8,
                              alignItems: 'center',
                            }}>
                            {selected[index].options &&
                            selected[index].options.text
                              ? selected[index].options.text
                              : ''}
                          </Text>
                        ) : (
                          <View style={styles.innerTextBoxOption}>
                            <AddText />
                          </View>
                        )}
                      </View>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </ImageBackground>
          </View>
        )}
        {layoutMainType === 'landscape' && (
          <View
            style={{
              shadowColor: '#000',
              shadowOffset: {
                width: 10,
                height: 10,
              },
              shadowOpacity: 0.5,
              shadowRadius: 15,
              zIndex: 999,
              height: 170,
              width: 230,
            }}>
            <ImageBackground
              source={require('../assets/images/PicamoryQuareLeft.png')}
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                marginVertical: 15,
                backgroundColor: '#fff',
                overflow: 'hidden',
                elevation: 15,
                height: 170,
                width: 230,
              }}>
              <View
                style={
                  fullImage ? styles.imageBoxWrapFull : styles.imageBoxWrap
                }>
                <View
                  style={[fullImage ? styles.imageBoxFull : styles.imageBox]}>
                  <TouchableOpacity
                    onLongPress={() => {
                      Alert.alert(
                        'Test',
                        'Are you sure you want to delete this book page',
                        [
                          {
                            text: 'Ok',
                            onPress: () => {
                              this.props.removeOneImage(item.path);
                            },
                          },
                          {
                            text: 'Cancel',
                          },
                        ],
                        {cancelable: true},
                      );
                    }}
                    onPress={() => this.props.openPhoto(item.path, index)}>
                    {!item?.path?.includes('null-') ? (
                      <Image
                        style={{
                          width: fullImage ? 230 : this.props.itemWidth,
                          height: fullImage ? 230 : this.props.itemHeight,
                        }}
                        source={{uri: item.path}}
                      />
                    ) : (
                      <View
                        style={{
                          // flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <AddText />
                      </View>
                    )}
                  </TouchableOpacity>
                </View>
              </View>
            </ImageBackground>
          </View>
        )}
        {layoutMainType === 'polaroid' && (
          <View
            style={{
              shadowColor: '#000',
              shadowOffset: {
                width: 10,
                height: 10,
              },
              shadowOpacity: 0.5,
              shadowRadius: 15,
              zIndex: 999,
            }}>
            <ImageBackground
              source={require('../assets/images/PicamoryPolaroidLeft.png')}
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                marginVertical: 15,
                backgroundColor: '#fff',
                overflow: 'hidden',
                elevation: 15,
              }}>
              <View
                style={
                  fullImage ? styles.imageBoxWrapFull : styles.imageBoxWrap
                }>
                <View
                  style={[
                    fullImage ? styles.imageBoxFull : styles.imagePolaroidBox,
                  ]}>
                  <TouchableOpacity
                    onLongPress={() => {
                      Alert.alert(
                        'Test',
                        'Are you sure you want to delete this book page',
                        [
                          {
                            text: 'Ok',
                            onPress: () => {
                              this.props.removeOneImage(item.path);
                            },
                          },
                          {
                            text: 'Cancel',
                          },
                        ],
                        {cancelable: true},
                      );
                    }}
                    onPress={() => this.props.openPhoto(item.path, index)}>
                    {!item?.path?.includes('null-') ? (
                      <Image
                        style={{
                          width: fullImage ? 185 : this.props.itemWidth,
                          height: fullImage ? 185 : this.props.itemHeight,
                        }}
                        source={{uri: item.path}}
                      />
                    ) : (
                      <View
                        style={{
                          // flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <AddText />
                      </View>
                    )}
                  </TouchableOpacity>
                  {!fullImage && (
                    <TouchableOpacity
                      onPress={() => {
                        this.props.setStateValues({
                          modalTitle: true,
                          modalTextIndex: index,
                          modalTextValue:
                            selected[index].options &&
                            selected[index].options.text
                              ? selected[index].options.text
                              : '',
                          modalSelectedItem: item,
                          showCharacters: false,
                        });
                      }}>
                      {showFooterTextOption && (
                        <View style={{width: 141, alignItems: 'center'}}>
                          {selected[index] &&
                          selected[index].options &&
                          selected[index].options.text ? (
                            <Text
                              numberOfLines={1}
                              style={{
                                //padding: 5,
                                fontWeight: 'normal',
                                fontFamily: this.props.font,
                                justifyContent: 'center',
                                marginVertical: 8,
                                alignItems: 'center',
                              }}>
                              {selected[index].options &&
                              selected[index].options.text
                                ? selected[index].options.text
                                : ''}
                            </Text>
                          ) : (
                            <View style={styles.innerTextBoxOption}>
                              <AddText />
                            </View>
                          )}
                        </View>
                      )}
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </ImageBackground>
          </View>
        )}
        {layoutMainType === 'others' && (
          <View
            style={{
              shadowColor: '#000',
              shadowOffset: {
                width: 10,
                height: 10,
              },
              shadowOpacity: 0.5,
              shadowRadius: 15,
              zIndex: 999,
            }}>
            <ImageBackground
              source={require('../assets/images/PicamorySquareLeft.png')}
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                marginVertical: 15,
                backgroundColor: '#fff',
                overflow: 'hidden',
                elevation: 15,
              }}>
              <View
                style={
                  fullImage ? styles.imageBoxWrapFull : styles.imageBoxWrap
                }>
                <View
                  style={[
                    fullImage ? styles.imageBoxFull : styles.imageLandscapeBox,
                  ]}>
                  <TouchableOpacity
                    onLongPress={() => {
                      Alert.alert(
                        'Test',
                        'Are you sure you want to delete this book page',
                        [
                          {
                            text: 'Ok',
                            onPress: () => {
                              this.props.removeOneImage(item.path);
                            },
                          },
                          {
                            text: 'Cancel',
                          },
                        ],
                        {cancelable: true},
                      );
                    }}
                    onPress={() => this.props.openPhoto(item.path, index)}>
                    {!item?.path?.includes('null-') ? (
                      <Image
                        style={{
                          //width: fullImage ? 185 : this.props.itemWidth,
                          //height: fullImage ? 185 : this.props.itemHeight,
                          width: 200,
                          height: 155,
                        }}
                        source={{uri: item.path}}
                      />
                    ) : (
                      showFooterTextOption && (
                        <View
                          style={{
                            // flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <AddText />
                        </View>
                      )
                    )}
                  </TouchableOpacity>
                  {!fullImage && (
                    <TouchableOpacity
                      onPress={() => {
                        this.props.setStateValues({
                          modalTitle: true,
                          modalTextIndex: index,
                          modalTextValue:
                            selected[index].options &&
                            selected[index].options.text
                              ? selected[index].options.text
                              : '',
                          modalSelectedItem: item,
                          showCharacters: false,
                        });
                      }}>
                      {showFooterTextOption && (
                        <View style={{width: 141, alignItems: 'center'}}>
                          {selected[index] &&
                          selected[index].options &&
                          selected[index].options.text ? (
                            <Text
                              numberOfLines={1}
                              style={{
                                //padding: 5,
                                fontWeight: 'normal',
                                fontFamily: this.props.font,
                                justifyContent: 'center',
                                marginVertical: 8,
                                alignItems: 'center',
                              }}>
                              {selected[index].options &&
                              selected[index].options.text
                                ? selected[index].options.text
                                : ''}
                            </Text>
                          ) : (
                            <View style={styles.innerTextBoxOption}>
                              <AddText />
                            </View>
                          )}
                        </View>
                      )}
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </ImageBackground>
          </View>
        )}

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 25,
            paddingTop: 10,
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              onPress={() => {
                if (
                  selected &&
                  selected[index] &&
                  selected[index].qty &&
                  selected[index].qty > 1
                ) {
                  selected[index].qty = selected[index].qty - 1;
                  this.props.updateCartPropsPhotos(selected);
                }
              }}>
              <Image
                source={require('../assets/icons/minusIcon.png')}
                style={{width: 25, height: 25}}
              />
            </TouchableOpacity>
            <Text
              style={{
                marginHorizontal: 10,
                fontFamily: 'Lato-Regular',
                fontSize: 20,
              }}>
              {count}
            </Text>
            <TouchableOpacity
              onPress={() => {
                if (selected && selected[index] && selected[index].qty) {
                  selected[index].qty = selected[index].qty + 1;
                  this.props.updateCartPropsPhotos(selected);
                }
              }}>
              <Image
                source={require('../assets/icons/plusIcon.png')}
                style={{width: 25, height: 25}}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imageBoxRelative: {
    //marginLeft: 20,
  },
  imageBoxFixed: {
    //marginLeft: 20,
  },
  imageBoxWrapFull: {
    // backgroundColor:"#ECECEC",
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  imageBoxWrap: {
    // backgroundColor:"#ECECEC",
    justifyContent: 'center',
    alignItems: 'center',
    //marginVertical: 5,
    // margin:1,
    // borderRadius:20,
    // shadowColor: "#000",
    // shadowOffset: {
    // width: 0,
    // height: 5,
    // },
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 10,
    // borderWidth: 1,
    // borderColor: '#D38074',
    //  marginVertical: 10,
  },
  imageBoxFull: {
    height: 165,
    width: 185,
    justifyContent: 'center',
    alignItems: 'center',
    // marginVertical: 5
  },
  imageBox: {
    height: 185,
    width: 185,
    justifyContent: 'center',
    alignItems: 'center',
    //marginVertical: 20
  },
  imagePolaroidBox: {
    height: 185,
    width: 155,
    justifyContent: 'center',
    alignItems: 'center',
    //marginVertical: 20
  },

  imageLandscapeBox: {
    height: 155,
    width: 200,
    justifyContent: 'center',
    alignItems: 'center',
    //marginVertical: 20
  },

  AddImageBox: {
    borderWidth: 1,
    borderColor: '#FFFFFF',
    backgroundColor: '#D38074',
    height: 81,
    width: 81,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
    marginVertical: 10,
  },
  layoutBox: {
    borderWidth: 1,
    borderColor: '#D38074',
    backgroundColor: '#fff',
    height: 85,
    width: 85,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  innerLayoutBox: {
    backgroundColor: '#D38074',
    justifyContent: 'center',
    alignItems: 'center',
  },

  innerTextBox: {
    borderWidth: 1,
    borderColor: '#D38074',
    width: 141,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
    backgroundColor: '#FFFFFF',
  },
  innerTextBoxOption: {
    borderWidth: 1,
    borderColor: '#D38074',
    width: 141,
    marginTop: 10,
    // height: 30,
    //marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
    backgroundColor: '#FFFFFF',
  },
  addMorePagesOption: {
    borderWidth: 1,
    borderColor: '#D38074',
    width: 141,
    // height: 30,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: 1,
    backgroundColor: '#FFFFFF',
  },
  modalContent: {
    flex: 1,
    width: '90%',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
  innerModalContent: {
    backgroundColor: '#D38074',
    width: '90%',
    height: '30%',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalTitle: {
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    textAlign: 'center',
  },
  modalButton: {
    borderWidth: 1,
    width: 193,
    borderColor: '#FFFFFF',
    borderRadius: 20,
    marginVertical: 10,
  },
  modalButtonText: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  closeButton: {
    position: 'relative',
    alignSelf: 'flex-end',
    padding: 20,
    bottom: '15%',
  },
});
