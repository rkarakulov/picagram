import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';
import Header from '../components/Header';

import { connect } from 'react-redux';

class SideMenu extends Component {
  navigateToScreen = route => () => {
    // console.log("navigate to" + route);
    this.props.navigation.navigate(route);
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View>
            <Header />
          </View>
          <View>
            <TouchableOpacity onPress={this.navigateToScreen('HomePage')}>
              <View style={styles.navSection}>
                <Text style={styles.navItems}>Home</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.navigateToScreen('AlbumPage')}>
              <View style={styles.navSection}>
                <Text style={styles.navItems}>Entwurf</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.navigateToScreen('ProfilPage')}>
              <View style={styles.navSection}>
                <Text style={styles.navItems}>Profil</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.navigateToScreen('WarenkorbPage')}>
              <View style={styles.navSection}>
                <Text style={styles.navItems}>Warenkorb</Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  navItems: {
    padding: 10,
    color: '#fff',
  },
  navSection: {
    backgroundColor: '#333',
    borderRadius: 10,
    marginHorizontal: 15,
    marginVertical: 5,
    padding: 5,
  },
  sectionHeading: {
    padding: 15,
    fontWeight: '700',
    color: '#000000',
    fontSize: 14,
  },
  footerContainer: {
    padding: 20,
    backgroundColor: '#333',
  },
  footerItem: {
    color: 'white',
    fontWeight: '700',
  },
});

SideMenu.propTypes = {
  navigation: PropTypes.object,
};

//export default SideMenu;

function mapStateToProps(state) {
  return {
    cart: state.cart,
  };
}

export default connect(mapStateToProps, {})(SideMenu);
