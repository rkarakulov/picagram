import { applyMiddleware, createStore } from 'redux';
import { persistStore } from 'redux-persist';
import { rootReducer } from './root.reducer';
import { default as thunk } from 'redux-thunk';
import type { IState } from './state.interface';
import type { IThunkDependencies } from './thunk-dependencies/thunk-dependencies.interface';
import { thunkDependencies } from './thunk-dependencies/thunk-dependencies';
import { defaultState } from './default-store';
import { IStore } from '../libs/utils/redux/thunk/redux-thunk.interface';
import { Persistor } from 'redux-persist/es/types';
import { composeWithDevTools } from '@redux-devtools/extension';

function isProd() {
  return false;
}

export function configureStore(): {
  store: IStore<IState>;
  persistor: Persistor;
} {
  const baseMiddlewares = applyMiddleware(thunk.withExtraArgument<IThunkDependencies>(thunkDependencies));

  const middleware = isProd() ? baseMiddlewares : composeWithDevTools(baseMiddlewares);

  const store = createStore(rootReducer, defaultState, middleware);
  const persistor = persistStore(store);

  return { store, persistor };
}
