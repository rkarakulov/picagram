import AsyncStorage from '@react-native-async-storage/async-storage';
import { combineReducers } from 'redux';
import { loginReducer } from '../libs/auth/domain/LoginReducer';
import { requestReducer } from '../libs/request/domain/RequestReducer';
import { cartReducer } from '../libs/card/domain/CartReducer';
import { productReducer } from '../libs/product/domain/ProductReducer';
import { settingsReducer } from '../libs/settings/domain/SettingsReducer';
import { persistReducer } from 'redux-persist';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['product'],
};

const combinedReducer = combineReducers({
  request: requestReducer,
  login: loginReducer,
  cart: cartReducer,
  product: productReducer,
  settings: settingsReducer,
});

export const rootReducer = persistReducer(persistConfig, combinedReducer);
