import { IState } from './state.interface';
import { LOGIN_INITIAL_STATE } from '../libs/auth/domain';
import { REQUEST_INITIAL_STATE } from '../libs/request/domain/request.init-state';
import { CART_INITIAL_STATE } from '../libs/card/domain/card.init-state';
import { PRODUCT_INITIAL_STATE } from '../libs/product/domain/product.init-state';
import { SETTINGS_INITIAL_STATE } from '../libs/settings/domain/settings.init-state';

export const defaultState: IState = {
  login: LOGIN_INITIAL_STATE,
  request: REQUEST_INITIAL_STATE,
  cart: CART_INITIAL_STATE,
  product: PRODUCT_INITIAL_STATE,
  settings: SETTINGS_INITIAL_STATE,
};
