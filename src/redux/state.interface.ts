import { ILoginStoreSegment } from '../libs/auth/domain/login.segment-interface';

export type IState = ILoginStoreSegment & {
  request: any;
  cart: any;
  product: any;
  settings: any;
};
