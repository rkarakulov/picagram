export type IConfig = {
  baseUrl: string;
  apiUrl: string;
  headers: {
    swAccessKey: string;
    swContextToken: string;
  },
};
