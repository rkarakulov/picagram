import { config as innerConfig } from './env';
import type { IConfig } from './config.interface';

export function getAppConfig(): IConfig {
  return innerConfig;
}
