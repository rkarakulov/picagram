import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'typescript-fsa';
import { Meta } from 'typescript-fsa/src/index';
import { Store } from 'redux';

export type IThunk<P, S = void, D = any, R = void> = (
  params?: P,
) => (dispatch: IDispatch<S, D>, getState?: () => S, dependencies?: D) => R;

export type IThunkAsync<P, S = void, D = any> = (
  params?: P,
) => (dispatch: IDispatch<S, D>, getState?: () => S, dependencies?: D) => Promise<any>;

export type IDispatch<S, D = void> = ThunkDispatch<S, D, AnyAction>;

export type IAction<Payload = any> = {
  type: string;
  payload?: Payload;
  error?: boolean;
  meta?: Meta;
};

export interface IStore<S = any, P = any> extends Store<S, IAction<P>> {
  dispatch: IDispatch<S>;
}
