import { IThunk } from '../../../utils';

interface ILogThunkParams {
  message: string;
}

export const logThunk: IThunk<ILogThunkParams> = (params) => {
  return async (_dispatch) => {
    console.log(params.message);
  };
};
