import * as types from '../../../redux/types';
import { CART_INITIAL_STATE } from './card.init-state';

export function cartReducer(state = CART_INITIAL_STATE, action) {
  switch (action.type) {
    case types.CART_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        loading: false,
        cart: [],
        error: null,
      };
    case types.CART_SUCCESS:
      return {
        ...state,
        cart: action.payload,
        isFailed: false,
        requesting: false,
      };
    case types.CART_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        error: 'Error while trying to cart',
      };
    case types.CART_SELECTED_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        loading: false,
        selectedAlbum: null,
        selectedAlbumIndex: null,
        error: null,
      };
    case types.CART_SELECTED_UPDATE:
      return {
        ...state,
        cart: action.payload,
        isFailed: false,
        requesting: false,
      };
    case types.CART_OPTIONS_UPDATE:
      return {
        ...state,
        cart: action.payload,
        isFailed: false,
        requesting: false,
      };
    case types.CART_SELECTED_SUCCESS:
      return {
        ...state,
        selectedAlbum: action.item,
        selectedAlbumIndex: action.key,
        isFailed: false,
        requesting: false,
      };
    case types.CART_SELECTED_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        error: 'Error while trying to cart',
      };

    default:
      return state;
  }
}
