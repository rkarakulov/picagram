import * as types from '../../../redux/types';
import { axiosInstance } from '../../../api/axios';

const FormData = require('form-data');

function cartRequest() {
  return {
    type: types.CART_REQUEST,
  };
}

function cartSuccess(response) {
  return {
    type: types.CART_SUCCESS,
    payload: response,
  };
}

function cartFailure(error) {
  return {
    type: types.CART_FAIL,
    payload: error,
  };
}

function cartSelectedRequest() {
  return {
    type: types.CART_SELECTED_REQUEST,
  };
}

function cartSelectedSuccess(item, key) {
  return {
    type: types.CART_SELECTED_SUCCESS,
    item: item,
    key: key,
  };
}

function cartUpdatePhotos(response) {
  return {
    type: types.CART_SELECTED_UPDATE,
    payload: response,
  };
}

function cartUpdateOptions(response) {
  return {
    type: types.CART_OPTIONS_UPDATE,
    payload: response,
  };
}

function cartSelectedFailure(error) {
  return {
    type: types.CART_SELECTED_FAIL,
    payload: error,
  };
}

function updateSuccess(response) {
  return {
    type: types.UPDATE_SUCCESS,
    payload: response.data,
  };
}

function requestStart() {
  return {
    type: types.REQUEST_START,
  };
}

function requestEnd() {
  return {
    type: types.REQUEST_END,
  };
}

export function request(start) {
  return async (dispatch) => {
    if (start == true) {
      dispatch(requestStart());
    } else {
      dispatch(requestEnd());
    }
  };
}

export function updateCart(response) {
  return async (dispatch) => {
    dispatch(cartRequest());

    try {
      dispatch(cartSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      //  console.log(error);
      dispatch(cartFailure(error.message));
      return Promise.reject('error greska');

      //dispatch(cartFailure(error.message));
    }
  };
}

export function updateCartPhotos(item) {
  return async (dispatch) => {
    //dispatch(cartSelectedRequest());

    try {
      // console.log('POZIVA SE CART UPDATE PHOTOS');
      dispatch(cartUpdatePhotos(item));
      return Promise.resolve(true);
    } catch (error) {
      // console.log(error);
      //  dispatch(cartSelectedFailure(error.message));
      return Promise.reject('error greska');

      //dispatch(cartFailure(error.message));
    }
  };
}

export function updateCartOptions(item) {
  return async (dispatch) => {
    //dispatch(cartSelectedRequest());

    try {
      dispatch(cartUpdateOptions(item));
      return Promise.resolve(true);
    } catch (error) {
      // console.log(error);
      //  dispatch(cartSelectedFailure(error.message));
      return Promise.reject('error greska');

      //dispatch(cartFailure(error.message));
    }
  };
}

export function updateCartSelected(item, key) {
  return async (dispatch) => {
    dispatch(cartSelectedRequest());

    try {
      dispatch(cartSelectedSuccess(item, key));
      return Promise.resolve(true);
    } catch (error) {
      // console.log(error);
      dispatch(cartSelectedFailure(error.message));
      return Promise.reject('error greska');

      //dispatch(cartFailure(error.message));
    }
  };
}

export function cart(email, password) {
  return async (dispatch) => {
    dispatch(cartRequest());

    try {
      const response = await axiosInstance.post('auth/cart', {
        email,
        password,
      });
      // TODO store token here
      await new Promise((resolve, reject) => setTimeout(() => resolve(), 800));
      dispatch(cartSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      //   console.log(error);
      dispatch(cartFailure(error.message));
      return Promise.reject('error greska');

      //dispatch(cartFailure(error.message));
    }
  };
}

export function update(fullName, email, phone, token, profileImage, accessToken) {
  return async (dispatch) => {
    try {
      const formData = new FormData();
      formData.append('name', fullName);
      formData.append('email', email);
      formData.append('phone', phone);

      if (profileImage.length > 3) {
        formData.append('profilePicture', {
          uri: profileImage,
          name: 'userProfile.jpg',
          type: 'image/jpg',
        });
      }

      const response = await axiosInstance.post('user', formData, token);
      //  console.log(response);

      var res = { user: response.data, accessToken: accessToken };
      response.data = res;
      await new Promise((resolve, reject) => setTimeout(() => resolve(), 800));
      dispatch(updateSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      // console.log(error);
      return Promise.reject('error greska');

      //dispatch(cartFailure(error.message));
    }
  };
}
