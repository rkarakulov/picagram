export const CART_INITIAL_STATE = {
  loading: false,
  requesting: false,
  isFailed: false,
  error: null,
  cart: [],
  selectedAlbumIndex: null,
  selectedAlbum: null,
};
