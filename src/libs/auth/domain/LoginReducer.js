import * as types from '../../../redux/types';
import { LOGIN_INITIAL_STATE } from './login.init-state';

export function loginReducer(state = LOGIN_INITIAL_STATE, action) {
  switch (action.type) {
    case types.PROFILE_SAVE_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        error: null,
      };
    case types.PROFILE_SAVE_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        isFailed: false,
        requesting: false,
      };
    case types.PROFILE_SAVE_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        error: 'Bitte überprüfen Sie das Formular und versuchen Sie es erneut!',
      };
    case types.LOGIN_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        results: [],
        error: null,
      };
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        results: action.payload,
        isAuthenticated: true,
        isFailed: false,
        requesting: false,
      };
    case types.LOGIN_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        error: 'Error while trying to login',
      };
    case types.LOGOUT_REQUEST:
      return {};
    case types.PROFILE_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        profile: {},
        error: null,
      };
    case types.PROFILE_SUCCESS:
      return {
        ...state,
        profile: action.payload,
        isFailed: false,
        requesting: false,
      };
    case types.PROFILE_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        error: 'Error while trying to fetch profile data',
      };
    case types.ADDRESS_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        address: [],
        error: null,
      };
    case types.ADDRESS_SUCCESS:
      return {
        ...state,
        address: action.payload,
        isFailed: false,
        requesting: false,
      };
    case types.ADDRESS_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        error: 'Error while trying to fetch addresses',
      };
    case types.COUNTRIES_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        countries: [],
        error: null,
      };
    case types.COUNTRIES_SUCCESS:
      return {
        ...state,
        countries: action.payload,
        isFailed: false,
        requesting: false,
      };
    case types.COUNTRIES_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        error: 'Error while trying to fetch countries',
      };
    case types.STATES_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        states: [],
        error: null,
      };
    case types.STATES_SUCCESS:
      return {
        ...state,
        states: action.payload,
        isFailed: false,
        requesting: false,
      };
    case types.STATES_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        error: 'Error while trying to fetch states',
      };
    case types.ADDRESS_CREATE_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        error: null,
      };
    case types.ADDRESS_CREATE_SUCCESS:
      return {
        ...state,
        isFailed: false,
        requesting: false,
      };
    case types.ADDRESS_CREATE_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        error: 'Error while trying to create address',
      };
    case types.ADDRESS_UPDATE_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        error: null,
      };
    case types.ADDRESS_UPDATE_SUCCESS:
      return {
        ...state,
        isFailed: false,
        requesting: false,
      };
    case types.ADDRESS_UPDATE_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        error: 'Error while trying to update address',
      };
    case types.ADDRESS_DELETE_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        error: null,
      };
    case types.ADDRESS_DELETE_SUCCESS:
      return {
        ...state,
        isFailed: false,
        requesting: false,
      };
    case types.ADDRESS_DELETE_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        error: 'Error while trying to fetch addresses',
      };
    case types.ADDRESS_DEFAULT_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        error: null,
      };
    case types.ADDRESS_DEFAULT_SUCCESS:
      return {
        ...state,
        isFailed: false,
        requesting: false,
      };
    case types.ADDRESS_DEFAULT_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        error: 'Error while trying to set default addresses',
      };
    case types.REGISTER_SUCCESS:
      return {
        ...state,
        results: action.payload,
        isAuthenticated: true,
        isFailed: false,
        requesting: false,
      };
    case types.BOARD_SUCCESS:
      return {
        ...state,
        results: action.payload,
        isAuthenticated: true,
        isFailed: false,
        requesting: false,
      };
    case types.UPDATE_SUCCESS:
      return {
        ...state,
        results: action.payload,
        isAuthenticated: true,
        isFailed: false,
        requesting: false,
      };
    case types.REFRESH_TOKEN:
      return {
        ...state,
        results: action.payload,
      };

    default:
      return state;
  }
}
