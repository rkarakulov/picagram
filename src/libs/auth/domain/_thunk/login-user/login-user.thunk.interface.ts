import { INavigation, Page } from '../../../../common';

export interface ILoginUserThunkParams {
  email: string;
  token: string;
  navigation: INavigation;
  password?: string;
  fromPage?: Page;
}
