import axios, { Method } from 'axios';
import { LoginAction } from '../../login.action';
import { IThunk } from '../../../../utils';
import { addNotificationThunk } from '../../../../notification/domain';
import { logThunk } from '../../../../logger/domain';
import { t } from '../../../../i18n/domain';
import { ILoginUserThunkParams } from './login-user.thunk.interface';
import { getProfileThunk } from '../get-profile';
import { getAppConfig } from '../../../../config/domain';

export const loginUserThunk: IThunk<ILoginUserThunkParams> = (params) => {
  return async (dispatch) => {
    const { token, navigation, fromPage, email, password } = params;
    const data = {
      username: email,
      password: password,
    };
    const config = {
      method: 'POST' as Method,
      url: `${getAppConfig().apiUrl}/login`,
      headers: {
        'sw-access-key': getAppConfig().headers.swAccessKey,
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'sw-context-token': getAppConfig().headers.swContextToken,
      },
      data,
    };

    try {
      dispatch(LoginAction.Login.started());

      const response = await axios(config);

      dispatch(getProfileThunk({ token, navigation, fromPage }));

      dispatch(LoginAction.Login.done({ result: response.data }));
    } catch (error) {
      dispatch(
        logThunk({
          message: error.message,
        }),
      );

      dispatch(
        addNotificationThunk({
          message: t('$CheckFormAndTryAgain'),
        }),
      );
    }
  };
};
