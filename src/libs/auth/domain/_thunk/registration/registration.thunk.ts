import axios, { Method } from 'axios';
import { IThunk } from '../../../../utils';
import { LoginAction } from '../../login.action';
import { loginUserThunk } from '../../index';
import { t } from '../../../../i18n/domain';
import { addNotificationThunk } from '../../../../notification/domain';
import { logThunk } from '../../../../logger/domain';
import { IRegisterThunkParams } from './registration.thunk.interface';
import { getAppConfig } from '../../../../config/domain';

export const registrationThunk: IThunk<IRegisterThunkParams> = (params) => {
  return async (dispatch) => {
    const { firstName, email, lastName, password, navigation, token } = params;
    const data = {
      email,
      firstName,
      lastName,
      acceptedDataProtection: true,
      salutationId: 'c0126800fce641dbae0aacfe2247ca03',
      password,
      storefrontUrl: getAppConfig().baseUrl,
      active: true,
      billingAddress: {
        city: 'City',
        zipcode: 'Zip',
        street: 'Street',
        countryId: '5d19bf82d7ff47cc9923fd0577965cae',
      },
    };
    const config = {
      method: 'post' as Method,
      url: `${getAppConfig().apiUrl}/register`,
      headers: {
        'sw-access-key': getAppConfig().headers.swAccessKey,
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'sw-context-token': getAppConfig().headers.swContextToken,
      },
      data,
    };

    try {
      dispatch(LoginAction.Register.started());

      const response = await axios(config);
      dispatch(
        loginUserThunk({
          email,
          password,
          navigation,
          token,
        }),
      );

      dispatch(LoginAction.Register.done({ result: response.data }));
    } catch (error) {
      dispatch(
        logThunk({
          message: error.message,
        }),
      );

      dispatch(
        addNotificationThunk({
          message: t('$CheckFormAndTryAgain'),
        }),
      );
    }
  };
};
