import { ILoginUserThunkParams } from '../../index';

export type IRegisterThunkParams = ILoginUserThunkParams & {
  lastName?: string;
  firstName?: string;
  city?: string;
  zip?: string;
  street?: string;
};
