import { INavigation, Page } from '../../../../common';

export interface IGetProfileThunkParams {
  token: string;
  navigation: INavigation;
  fromPage?: Page;
}
