import axios, { Method } from 'axios';
import { IThunk } from '../../../../utils';
import { LoginAction } from '../../login.action';
import { addNotificationThunk } from '../../../../notification/domain';
import { logThunk } from '../../../../logger/domain';
import { t } from '../../../../i18n/domain';
import { IGetProfileThunkParams } from './get-profile.thunk.interface';
import { Page, Screen } from '../../../../common';
import { getAppConfig } from '../../../../config/domain';

export const getProfileThunk: IThunk<IGetProfileThunkParams> = (params) => {
  return async (dispatch) => {
    const { token, navigation, fromPage } = params;

    const data = {
      page: 1,
      limit: 25,
      associations: {
        defaultBillingAddress: {
          'total-count-mode': 1,
        },
        defaultShippingAddress: {
          'total-count-mode': 1,
        },
      },
      'total-count-mode': 1,
    };

    const config = {
      method: 'POST' as Method,
      url: `${getAppConfig().apiUrl}/customer`,
      headers: {
        'sw-access-key': getAppConfig().headers.swAccessKey,
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'sw-context-token': token,
      },
      data,
    };

    try {
      dispatch(LoginAction.GetProfile.started());

      const response = await axios(config);

      dispatch(LoginAction.GetProfile.done({ result: response.data }));

      if (fromPage === Page.Cart) {
        navigation.goBack();
      } else {
        navigation.navigate(Screen.ProfilePage);
      }
    } catch (error) {
      dispatch(
        logThunk({
          message: error.message,
        }),
      );

      dispatch(
        addNotificationThunk({
          message: t('$CheckFormAndTryAgain'),
        }),
      );
    }
  };
};
