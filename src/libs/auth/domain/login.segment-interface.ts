export interface ILoginStoreSegment {
  login: ILoginStore;
}

export interface ILoginStore {
  isAuthenticated: boolean;
  results: {
    contextToken: any;
  };
  loading: boolean;
  requesting: boolean;
  profile: any;
  address: any[];
  countries: any[];
  states: any[];
  isFailed: boolean;
}
