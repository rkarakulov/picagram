import { ILoginStore } from './login.segment-interface';

export const LOGIN_INITIAL_STATE: ILoginStore = {
  isAuthenticated: false,
  results: { contextToken: undefined },
  loading: false,
  requesting: false,
  profile: {},
  address: [],
  countries: [],
  states: [],
  isFailed: false,
};
