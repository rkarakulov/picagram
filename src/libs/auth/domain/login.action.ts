import typescriptFsa from 'typescript-fsa';

export namespace LoginAction {
  const actionCreator = typescriptFsa('LoginAction');

  export const Login = actionCreator.async<void, any, string>('Login');
  export const Register = actionCreator.async<void, any, string>('Register');
  export const GetProfile = actionCreator.async<void, any, string>('GetProfile');
}
