import { ILoginStoreSegment } from './login.segment-interface';

export const loginSegmentSelector = (store: ILoginStoreSegment) => store.login;
