import * as types from '../../../redux/types';
import { axiosInstance } from '../../../api/axios';
import axios, { Method } from 'axios';
import { LoginAction } from './index';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const FormData = require('form-data');

function addressRequest() {
  return {
    type: types.ADDRESS_REQUEST,
  };
}

function addressSuccess(response) {
  return {
    type: types.ADDRESS_SUCCESS,
    payload: response.data,
  };
}

function addressFailure(error) {
  return {
    type: types.ADDRESS_FAIL,
    payload: error,
  };
}

function countriesRequest() {
  return {
    type: types.COUNTRIES_REQUEST,
  };
}

function countriesSuccess(response) {
  return {
    type: types.COUNTRIES_SUCCESS,
    payload: response.data,
  };
}

function countriesFailure(error) {
  return {
    type: types.COUNTRIES_FAIL,
    payload: error,
  };
}

function statesRequest() {
  return {
    type: types.STATES_REQUEST,
  };
}

function statesSuccess(response) {
  return {
    type: types.STATES_SUCCESS,
    payload: response.data,
  };
}

function statesFailure(error) {
  return {
    type: types.STATES_FAIL,
    payload: error,
  };
}

function addressCreateRequest() {
  return {
    type: types.ADDRESS_CREATE_REQUEST,
  };
}

function addressCreateSuccess(response) {
  return {
    type: types.ADDRESS_CREATE_SUCCESS,
    payload: response.data,
  };
}

function addressCreateFailure(error) {
  return {
    type: types.ADDRESS_CREATE_FAIL,
    payload: error,
  };
}

function addressUpdateRequest() {
  return {
    type: types.ADDRESS_UPDATE_REQUEST,
  };
}

function addressUpdateSuccess(response) {
  return {
    type: types.ADDRESS_UPDATE_SUCCESS,
    payload: response.data,
  };
}

function addressUpdateFailure(error) {
  return {
    type: types.ADDRESS_UPDATE_FAIL,
    payload: error,
  };
}

function addressDeleteRequest() {
  return {
    type: types.ADDRESS_DELETE_REQUEST,
  };
}

function addressDeleteSuccess(response) {
  return {
    type: types.ADDRESS_DELETE_SUCCESS,
    payload: response.data,
  };
}

function addressDeleteFailure(error) {
  return {
    type: types.ADDRESS_DELETE_FAIL,
    payload: error,
  };
}

function addressDefaultRequest() {
  return {
    type: types.ADDRESS_DEFAULT_REQUEST,
  };
}

function addressDefaultSuccess(response, response2) {
  return {
    type: types.ADDRESS_DEFAULT_SUCCESS,
    payload: response.data + response2.data,
  };
}

function addressDefaultFailure(error) {
  return {
    type: types.ADDRESS_DEFAULT_FAIL,
    payload: error,
  };
}

function loginSuccess(response) {
  return {
    type: LoginAction.Login.done.type,
    payload: response.data,
  };
}

function loginFailure(error) {
  return {
    type: LoginAction.Login.failed.type,
    payload: error,
  };
}

function updateSuccess(response) {
  return {
    type: types.UPDATE_SUCCESS,
    payload: response.data,
  };
}

function profileSaveRequest() {
  return {
    type: types.PROFILE_SAVE_REQUEST,
  };
}

function profileSaveSuccess(response) {
  return {
    type: types.PROFILE_SAVE_SUCCESS,
    payload: response.data,
  };
}

function profileSaveFailure(error) {
  return {
    type: types.PROFILE_SAVE_FAIL,
    payload: error,
  };
}

function requestStart() {
  return {
    type: types.REQUEST_START,
  };
}

function requestEnd() {
  return {
    type: types.REQUEST_END,
  };
}

function selectMoreIndex(item) {
  return {
    type: types.SELECT_MORE_INDEX,
    payload: item,
  };
}

function moreModalStart() {
  return {
    type: types.MORE_MODAL_START,
  };
}

function moreModalEnd() {
  return {
    type: types.MORE_MODAL_END,
  };
}

function logoutRequest() {
  return {
    type: types.LOGOUT_REQUEST,
  };
}

function refreshTokenUpdate(response) {
  return {
    type: types.UPDATE_SUCCESS,
    payload: response,
  };
}

export function refreshToken(token) {
  return async (dispatch) => {
    dispatch(refreshTokenUpdate(token));
  };
}

export function request(start) {
  return async (dispatch) => {
    if (start == true) {
      dispatch(requestStart());
    } else {
      dispatch(requestEnd());
    }
  };
}

export function moreModal(start) {
  // console.log("moreModal ",start)
  return async (dispatch) => {
    if (start == true) {
      dispatch(moreModalStart());
    } else {
      dispatch(moreModalEnd());
    }
  };
}

export function selectMore(item) {
  return async (dispatch) => {
    dispatch(selectMoreIndex(item));
  };
}

export function getAddress(token) {
  return async (dispatch) => {
    dispatch(addressRequest());

    try {
      const data = {
        page: 1,
        limit: 25,
        'total-count-mode': 1,
      };
      const config = {
        method: 'post' as Method,
        url: 'https://picamory.devla.dev/store-api/account/list-address',
        headers: {
          'sw-access-key': 'SWSCWD-ERPQI_RMH3GPEY-WBKA',
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'sw-context-token': token,
        },
        data: data,
      };
      const response = await axios(config);

      dispatch(addressSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      dispatch(addressFailure(error.response.data));
      return Promise.reject('error greska');
    }
  };
}

export function getCountries(token) {
  return async (dispatch) => {
    dispatch(countriesRequest());

    try {
      const data = {
        page: 1,
        limit: 50,
        'total-count-mode': 1,
      };
      const config = {
        method: 'post' as Method,
        url: 'https://picamory.devla.dev/store-api/country',
        headers: {
          'sw-access-key': 'SWSCWD-ERPQI_RMH3GPEY-WBKA',
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'sw-context-token': token,
        },
        data: data,
      };
      const response = await axios(config);
      dispatch(countriesSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      // console.log('response catch');
      // console.log(error);
      dispatch(countriesFailure(error.response.data));
      return Promise.reject('error greska');

      //dispatch(loginFailure(error.message));
    }
  };
}

export function getStates(token, countryId) {
  return async (dispatch) => {
    dispatch(statesRequest());

    try {
      const data = {
        countryId: countryId,
        page: 1,
        limit: 50,
        'total-count-mode': 1,
      };
      const config = {
        method: 'post' as Method,
        url: 'https://picamory.devla.dev/country/country-state-data',
        headers: {
          'sw-access-key': 'SWSCWD-ERPQI_RMH3GPEY-WBKA',
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'sw-context-token': token,
        },
        data: data,
      };
      const response = await axios(config);
      dispatch(statesSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      dispatch(statesFailure(error.response.data));
      return Promise.reject('error greska');
    }
  };
}

export function createAddress(token, values) {
  return async (dispatch) => {
    dispatch(addressCreateRequest());

    try {
      const data = values;
      const config = {
        method: 'post' as Method,
        url: 'https://picamory.devla.dev/store-api/account/address',
        headers: {
          'sw-access-key': 'SWSCWD-ERPQI_RMH3GPEY-WBKA',
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'sw-context-token': token,
        },
        data,
      };
      const response = await axios(config);
      dispatch(addressCreateSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      dispatch(addressCreateFailure(error.response.data));
      return Promise.reject('error greska');
    }
  };
}

export function updateAddress(token, values, id) {
  return async (dispatch) => {
    dispatch(addressUpdateRequest());

    try {
      const data = values;
      const config = {
        method: 'patch' as Method,
        url: `https://picamory.devla.dev/store-api/account/address/${id}`,
        headers: {
          'sw-access-key': 'SWSCWD-ERPQI_RMH3GPEY-WBKA',
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'sw-context-token': token,
        },
        data: data,
      };
      const response = await axios(config);
      dispatch(addressUpdateSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      dispatch(addressUpdateFailure(error.response.data));
      return Promise.reject('error greska');
    }
  };
}

export function deleteAddress(token, values) {
  return async (dispatch) => {
    dispatch(addressDeleteRequest());

    try {
      const config = {
        method: 'delete' as Method,
        url: `https://picamory.devla.dev/store-api/account/address/${values}`,
        headers: {
          'sw-access-key': 'SWSCWD-ERPQI_RMH3GPEY-WBKA',
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'sw-context-token': token,
        },
      };
      const response = await axios(config);
      dispatch(addressDeleteSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      dispatch(addressDeleteFailure(error.response.data));
      return Promise.reject('error greska');
    }
  };
}

export function setDefaultAddress(token, values) {
  return async (dispatch) => {
    dispatch(addressDefaultRequest());

    try {
      const configBill = {
        method: 'patch' as Method,
        url: `https://picamory.devla.dev/store-api/account/address/default-billing/${values}`,
        headers: {
          'sw-access-key': 'SWSCWD-ERPQI_RMH3GPEY-WBKA',
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'sw-context-token': token,
        },
      };
      const configShip = {
        method: 'patch' as Method,
        url: `https://picamory.devla.dev/store-api/account/address/default-shipping/${values}`,
        headers: {
          'sw-access-key': 'SWSCWD-ERPQI_RMH3GPEY-WBKA',
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'sw-context-token': token,
        },
      };
      const responseBill = await axios(configBill);
      const responseShip = await axios(configShip);
      dispatch(addressDefaultSuccess(responseBill, responseShip));
      return Promise.resolve(true);
    } catch (error) {
      dispatch(addressDefaultFailure(error.response.data));
      return Promise.reject('error greska');
    }
  };
}

export function loginGoogle(token) {
  return async (dispatch) => {
    try {
      const response = await axiosInstance.get(`oauth/google/callback?access_token=${token}`);
      dispatch(loginSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      dispatch(loginFailure(error.message));
      return Promise.reject('error greska');
    }
  };
}

export function loginFacebook(token) {
  return async (dispatch) => {
    try {
      const response = await axiosInstance.get(`oauth/facebook/callback?access_token=${token}`);
      dispatch(loginSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      dispatch(loginFailure(error.message));
      return Promise.reject('error greska');
    }
  };
}

export function saveProfile(nachname, name, email, _city, _street, _country, _zip, token) {
  return async (dispatch) => {
    dispatch(profileSaveRequest());

    try {
      const data = {
        email: email,
        firstName: name,
        lastName: nachname,
        acceptedDataProtection: true,
        salutationId: 'c0126800fce641dbae0aacfe2247ca03',
        /*
         storefrontUrl: 'https://picamory.devla.dev',
         active: true,
         billingAddress: {
         city: city,
         zipcode: zip,
         street: street,
         countryId: '5d19bf82d7ff47cc9923fd0577965cae',
         },
         */
      };

      const config = {
        method: 'post' as Method,
        url: 'https://picamory.devla.dev/store-api/account/change-profile',
        headers: {
          'sw-access-key': 'SWSCWD-ERPQI_RMH3GPEY-WBKA',
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'sw-context-token': token,
        },
        data: data,
      };
      const response = await axios(config);
      dispatch(profileSaveSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      dispatch(profileSaveFailure(error.message));
      return Promise.reject(error.message);
    }
  };
}

export function update(fullName, email, phone, token, profileImage, accessToken) {
  return async (dispatch) => {
    try {
      const formData = new FormData();
      formData.append('name', fullName);
      formData.append('email', email);
      formData.append('phone', phone);

      if (profileImage.length > 3) {
        formData.append('profilePicture', {
          uri: profileImage,
          name: 'userProfile.jpg',
          type: 'image/jpg',
        });
      }

      const response = await axiosInstance.post('user', formData, token);

      const res = { user: response.data, accessToken: accessToken };
      response.data = res;
      dispatch(updateSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      return Promise.reject('error greska');
    }
  };
}

export function updateMe(response, accessToken) {
  return async (dispatch) => {
    const res = { user: response.data, accessToken: accessToken };
    response.data = res;
    dispatch(updateSuccess(response));
    return Promise.resolve(true);
  };
}

export function logOutServer(token) {
  return async (dispatch) => {
    try {
      await axiosInstance.post('auth/logout', {}, token);
      dispatch(logoutRequest());
      return Promise.resolve(true);
    } catch (error) {
      return Promise.reject('error greska');
    }
  };
}

export function logOut() {
  return async (dispatch) => {
    try {
      dispatch(logoutRequest());
      return Promise.resolve(true);
    } catch (error) {
      return Promise.reject('error logout greska');
    }
  };
}
