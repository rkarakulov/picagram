import { createSelector } from 'reselect';
import { loginSegmentSelector } from '../domain/login.segment-selector';
import { ISignInPageStateProps } from './sign-in-page.interface';
import { createPathSelector } from 'reselect-utils';

export const signInPagePropsSelector = createSelector(
  [
    createPathSelector(loginSegmentSelector).isAuthenticated(),
    createPathSelector(loginSegmentSelector).results.contextToken(),
  ],
  (isAuthenticated, contextToken): ISignInPageStateProps => {
    return {
      isAuthenticated,
      contextToken,
    };
  },
);
