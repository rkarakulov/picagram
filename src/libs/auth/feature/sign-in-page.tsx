import * as React from 'react';
import {
  BackHandler,
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  ScrollView,
  Text,
  TextInput,
  TouchableNativeFeedback,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';

import LogoHeader from '../../../assets/images/logo.svg';
import OderLines from '../../../assets/images/oderLines.svg';
import FacebookIconBlack from '../../../assets/icons/facebookIconBlack.svg';
import AppleIcon from '../../../assets/icons/appleIcon.svg';
import GoogleIcon from '../../../assets/icons/googleIcon.svg';
import LinearGradient from 'react-native-linear-gradient';
import SignInHeader from '../../../components/SignInHeader';
import { connect } from 'react-redux';
import {
  ISignInPageDispatchProps,
  ISignInPageOwnProps,
  ISignInPageProps,
  ISignInPageState,
  ISignInPageStateProps,
} from './sign-in-page.interface';
import { signInPagePropsSelector } from './sign-in-page.props-selector';
import { styles } from './sign-in-page.css';
import { loginUserThunk } from '../domain/_thunk/login-user/login-user.thunk';

class SignInPageComponent extends React.Component<ISignInPageProps, ISignInPageState> {
  public state = {
    user: '',
    password: '',
    emailError: false,
    passwordError: false,
  };
  passwordInputRef: TextInput;

  private unsubscribe: any;

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.unsubscribe();
  }

  handleBackButtonClick = () => {
    if (this.props.isAuthenticated) {
      const { goBack } = this.props.navigation;
      goBack();
    } else {
      this.props.navigation.navigate('HomePage');
    }
    return true;
  };

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => undefined);
  }

  renderFooterIcons() {
    if (Platform.OS === 'ios') {
      return (
        <View style={styles.footerIcons}>
          <TouchableOpacity>
            <FacebookIconBlack />
          </TouchableOpacity>
          <TouchableOpacity>
            <AppleIcon />
          </TouchableOpacity>
          <TouchableOpacity>
            <GoogleIcon />
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View style={styles.footerIcons}>
          <TouchableOpacity>
            <FacebookIconBlack />
          </TouchableOpacity>
          <TouchableOpacity>
            <GoogleIcon />
          </TouchableOpacity>
        </View>
      );
    }
  }

  onBackClick = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  };

  onAnmeldenClick = () => {
    const reg = /^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/;

    let error = false;
    let errorMsg = '';
    if (this.state.user == '' || reg.test(this.state.user) == false) {
      this.setState({ emailError: true });
      error = true;
      errorMsg += 'Enter your email! ';
    }

    if (this.state.password == '') {
      this.setState({ passwordError: true });
      error = true;

      errorMsg += 'Enter your password! ';
    } else if (this.state.password.length < 8) {
      this.setState({ passwordError: true });
      error = true;
      errorMsg += 'Password must be longer then 7 charachters! ';
    }

    if (error) {
      console.log(errorMsg);
      return;
    }

    const fromPage =
      this.props.route && this.props.route.params && this.props.route.params.fromPage
        ? this.props.route.params.fromPage
        : undefined;

    this.props.loginUser({
      email: this.state.user,
      password: this.state.password,
      navigation: this.props.navigation,
      token: this.props.contextToken,
      fromPage,
    });
  };

  onRegistrierenClick = () => {
    const from =
      this.props.route && this.props.route.params && this.props.route.params.from ? this.props.route.params.from : null;
    this.props.navigation.navigate('RegistrationPage', { from: from });
  };

  render() {
    const background = TouchableNativeFeedback.Ripple('#fff', true);
    const backgroundPurple = TouchableNativeFeedback.Ripple('#B42762', true);

    console.log('render');

    return (
      <>
        <SafeAreaView style={styles.container}>
          <SignInHeader onBackClick={this.onBackClick} />
          <Image style={styles.image} source={require('../../../assets/images/footerImageEdited.png')} />

          <KeyboardAvoidingView
            behavior={'height'}
            keyboardVerticalOffset={Platform.OS !== 'android' ? 0 : 30}
            style={{ flex: 1 }}
          >
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <ScrollView style={{ flex: 1 }} contentContainerStyle={{ zIndex: 9999, flexGrow: 1 }}>
                <View style={styles.logo}>
                  <LogoHeader />
                </View>

                <View style={styles.textWelcomeBlock}>
                  <Text style={styles.textWelcome}>SCHÖN, DASS DU DA BIST!</Text>
                </View>

                <View style={styles.inputsBlock}>
                  <TextInput
                    style={styles.input}
                    selectionColor="#000000"
                    placeholderTextColor="#707070"
                    placeholder="E-Mail"
                    autoCorrect={false}
                    autoCapitalize="none"
                    returnKeyType="next"
                    blurOnSubmit={false}
                    onSubmitEditing={() => this.passwordInputRef.focus()}
                    onChangeText={(user) => this.setState({ user })}
                  />
                  <TextInput
                    style={styles.input}
                    placeholder="Passwort"
                    selectionColor="#000000"
                    placeholderTextColor="#707070"
                    autoCorrect={false}
                    autoCapitalize="none"
                    secureTextEntry={true}
                    returnKeyType="done"
                    ref={(ref) => {
                      this.passwordInputRef = ref;
                    }}
                    onChangeText={(password) => this.setState({ password })}
                  />
                </View>

                <View style={{ zIndex: 99999 }}>
                  <View style={[styles.anmeldenButton, { borderRadius: 20, overflow: 'hidden', paddingBottom: 5 }]}>
                    <TouchableNativeFeedback
                      useForeground={true}
                      background={background}
                      onPress={() => this.onAnmeldenClick()}
                    >
                      <View>
                        <LinearGradient
                          start={{ x: 0, y: 0 }}
                          end={{ x: 1, y: 0 }}
                          colors={['#FFD06C', '#D78275', '#B42762']}
                          style={styles.LinearGradient}
                        >
                          <Text style={styles.textAnmelden}>ANMELDEN</Text>
                        </LinearGradient>
                      </View>
                    </TouchableNativeFeedback>
                  </View>

                  <View style={{ alignItems: 'center', paddingVertical: 20 }}>
                    <Text style={{ fontSize: 12, fontFamily: 'Lato-Regular' }}>Du hast noch kein Konto?</Text>
                  </View>
                  <View style={[styles.registrierenButton, { borderRadius: 20, overflow: 'hidden' }]}>
                    <TouchableNativeFeedback
                      useForeground={true}
                      background={backgroundPurple}
                      onPress={() => this.onRegistrierenClick()}
                    >
                      <View
                        style={{
                          height: '100%',
                          width: '100%',
                          justifyContent: 'center',
                        }}
                      >
                        <Text style={styles.textRegistrieren}>REGISTRIEREN</Text>
                      </View>
                    </TouchableNativeFeedback>
                  </View>

                  <View>
                    <View style={{ alignSelf: 'center', paddingTop: 20 }}>
                      <View style={styles.oder}>
                        <OderLines />
                        <Text
                          style={{
                            fontSize: 12,
                            paddingHorizontal: 20,
                            fontFamily: 'Lato-Heavy',
                          }}
                        >
                          ODER
                        </Text>
                        <OderLines />
                      </View>

                      {this.renderFooterIcons()}
                    </View>
                  </View>
                </View>
              </ScrollView>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </SafeAreaView>
      </>
    );
  }
}

export const SignInPage = connect<ISignInPageStateProps, ISignInPageDispatchProps, ISignInPageOwnProps>(
  signInPagePropsSelector,
  {
    loginUser: loginUserThunk,
  },
)(SignInPageComponent);
