import { Platform, StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  backButton: {
    padding: 20,
    position: 'absolute',
    zIndex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
  },
  headerTitle: {
    color: '#FFFFFF',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 18,
  },
  logo: {
    alignItems: 'center',
    marginVertical: 20,
  },
  textWelcomeBlock: {
    alignItems: 'center',
    paddingBottom: 20,
  },
  textWelcome: {
    fontSize: 25,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  inputsBlock: {
    width: '90%',
    alignSelf: 'center',
  },
  input: {
    padding: '4%',
    margin: 10,
    borderWidth: 1,
    borderRadius: 20,
    backgroundColor: '#FFFFFF',
    color: '#000000',
    fontSize: 13,
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
  },
  anmeldenButton: {
    alignSelf: 'center',
    width: '90%',
  },
  LinearGradient: {
    borderRadius: 20,
    padding: 15,
    width: '100%',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 2,
  },
  textAnmelden: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Lato-Heavy',
  },
  registrierenButton: {
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#B42762',
    borderRadius: 20,
    width: '90%',
    alignSelf: 'center',
    height: 52,
  },
  textRegistrieren: {
    alignItems: 'center',
    textAlign: 'center',
    color: '#B42762',
    fontFamily: Platform.OS !== 'android' ? 'Lato-Bold' : 'Lato-Bold',
    fontSize: 15,
  },
  oder: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  apple: {
    alignSelf: 'center',
    position: 'absolute',
  },
  footerIcons: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  image: {
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
    width: '100%',
    zIndex: 0,
  },
});
