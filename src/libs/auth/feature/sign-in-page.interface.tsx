import { loginUserThunk } from '../domain/_thunk/login-user/login-user.thunk';
import { INavigation, IRoute } from '../../common';

export type ISignInPageProps = ISignInPageOwnProps & ISignInPageStateProps & ISignInPageDispatchProps;

export interface ISignInPageOwnProps {
  navigation: INavigation;
  route: IRoute;
}

export interface ISignInPageStateProps {
  isAuthenticated: boolean;
  contextToken: any;
}

export interface ISignInPageDispatchProps {
  loginUser: typeof loginUserThunk;
}

export interface ISignInPageState {
  user: string;
  password: string;
  emailError: boolean;
  passwordError: boolean;
}
