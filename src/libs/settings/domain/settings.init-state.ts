export const SETTINGS_INITIAL_STATE = {
  loading: false,
  requesting: false,
  results: [],
  isFailed: null,
};
