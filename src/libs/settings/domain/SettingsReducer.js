import * as types from '../../../redux/types';
import { SETTINGS_INITIAL_STATE } from './settings.init-state';

export function settingsReducer(state = SETTINGS_INITIAL_STATE, action) {
  switch (action.type) {
    case types.SETTINGS_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        results: [],
        error: null,
      };
    case types.SETTINGS_SUCCESS:
      return {
        ...state,
        results: action.payload,
        isFailed: false,
        requesting: false,
      };
    case types.SETTINGS_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        error: 'Error while trying to settings',
      };

    default:
      return state;
  }
}
