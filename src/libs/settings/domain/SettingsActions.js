import * as types from '../../../redux/types';
import { axiosInstance } from '../../../api/axios';
import axios from 'axios';

const FormData = require('form-data');

function settingsRequest() {
  return {
    type: types.SETTINGS_REQUEST,
  };
}

function settingsSuccess(response) {
  return {
    type: types.SETTINGS_SUCCESS,
    payload: response,
  };
}

function settingsFailure(error) {
  return {
    type: types.SETTINGS_FAIL,
    payload: error,
  };
}

export function settingsInstagram(item) {
  return async (dispatch) => {
    dispatch(settingsRequest());

    try {
      dispatch(settingsSuccess(item));
      return Promise.resolve(true);
    } catch (error) {
      //  console.log('response catch');

      // console.log(error);
      dispatch(settingsFailure(error.message));
      return Promise.reject(error.message);

      //dispatch(settingsFailure(error.message));
    }
  };
}

export function settingsGoogle(token) {
  return async (dispatch) => {
    dispatch(settingsRequest());
    try {
      const response = await axiosInstance.get(`oauth/google/callback?access_token=${token}`);
      // TODO store token here
      await new Promise((resolve, reject) => setTimeout(() => resolve(), 800));
      dispatch(settingsSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      //  console.log(error);
      dispatch(settingsFailure(error.message));
      return Promise.reject('error greska');

      //dispatch(settingsFailure(error.message));
    }
  };
}

export function settingsFacebook(token) {
  return async (dispatch) => {
    dispatch(settingsRequest());

    try {
      const response = await axiosInstance.get(`oauth/facebook/callback?access_token=${token}`);
      //   console.log('response sa servera', response);
      // TODO store token here
      await new Promise((resolve, reject) => setTimeout(() => resolve(), 800));
      dispatch(settingsSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      //   console.log('ovde err');
      // console.log(error);
      dispatch(settingsFailure(error.message));
      return Promise.reject('error greska');

      //dispatch(settingsFailure(error.message));
    }
  };
}
