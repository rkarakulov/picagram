export type INavigation = {
  goBack: () => void;
  navigate: (screen: string, params?: any) => void;
  addListener: (key: string, callback: () => any) => any;
};

export type IRoute = {
  params: {
    from: any;
    fromPage: any;
  };
};

export enum Screen {
  ProfilePage = 'ProfilPage',
  RegistrationPage = 'RegistrationPage',
}

export enum Page {
  Cart = 'cart',
}
