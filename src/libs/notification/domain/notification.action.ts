import typescriptFsa from 'typescript-fsa';

export namespace NotificationAction {
  const actionCreator = typescriptFsa('NotificationAction');

  export const Temp = actionCreator<any>('Temp');
}
