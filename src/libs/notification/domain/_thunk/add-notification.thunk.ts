import { IThunk } from '../../../utils';
import { Alert } from 'react-native';

interface IAddNotificationThunkParams {
  message: string;
}

export const addNotificationThunk: IThunk<IAddNotificationThunkParams> = (params) => {
  return async (_dispatch) => {
    Alert.alert(params.message);
  };
};
