import * as types from '../../../redux/types';
import { PRODUCT_INITIAL_STATE } from './product.init-state';

export function productReducer(state = PRODUCT_INITIAL_STATE, action) {
  switch (action.type) {
    case types.PRODUCT_REQUEST:
      return {
        ...state,
        isFailed: false,
        requesting: true,
        loading: true,
        albums: [],
        error: null,
      };
    case types.PRODUCT_SUCCESS:
      return {
        ...state,
        albums: action.payload,
        isFailed: false,
        requesting: false,
        loading: false,
      };
    case types.PRODUCT_FAIL:
      return {
        ...state,
        isFailed: true,
        requesting: false,
        loading: false,
        error: 'Error while trying to product',
      };

    default:
      return state;
  }
}
