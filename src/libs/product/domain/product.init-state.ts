export const PRODUCT_INITIAL_STATE = {
  loading: false,
  requesting: false,
  isFailed: false,
  error: null,
  albums: [],
};
