import * as types from '../../../redux/types';
import { axiosInstance } from '../../../api/axios';
import axios from 'axios';

const FormData = require('form-data');

function productRequest() {
  return {
    type: types.PRODUCT_REQUEST,
  };
}

function productSuccess(response) {
  return {
    type: types.PRODUCT_SUCCESS,
    payload: response,
  };
}

function productFailure(error) {
  return {
    type: types.PRODUCT_FAIL,
    payload: error,
  };
}

function requestStart() {
  return {
    type: types.REQUEST_START,
  };
}

function requestEnd() {
  return {
    type: types.REQUEST_END,
  };
}

export function request(start) {
  return async (dispatch) => {
    if (start == true) {
      dispatch(requestStart());
    } else {
      dispatch(requestEnd());
    }
  };
}

export function updateProduct(response) {
  return async (dispatch) => {
    dispatch(productRequest());

    try {
      dispatch(productSuccess(response));
      return Promise.resolve(true);
    } catch (error) {
      //  console.log(error);
      dispatch(productFailure(error.message));
      return Promise.reject('error greska');

      //dispatch(productFailure(error.message));
    }
  };
}

export function getProducts(aryCategory) {
  return async (dispatch) => {
    dispatch(productRequest());

    try {
      const regex = /(<([^>]+)>)/gi;
      const categories = [...aryCategory];
      const data = {
        filter: [],
        associations: {
          categories: {
            limit: 1,
          },
          media: {},
        },
        includes: {},
        aggregations: [
          {
            name: 'product-categories',
            type: 'terms',
            field: 'categories.name',
          },
        ],
      };
      var config = {
        method: 'post',
        url: 'https://picamory.devla.dev/store-api/product',
        headers: {
          'sw-access-key': 'SWSCWD-ERPQI_RMH3GPEY-WBKA',
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        data: data,
      };
      const response = await axios(config);
      if (response && response.data && response.data.elements) {
        response.data.elements.map((item, key) => {
          let blnHasCat = false;

          if (!item.parentId && item.categories && item.categories.length > 0) {
            item.categories.map((catItem, catKey) => {
              const index = categories.findIndex(
                (obj) => obj.title.toLowerCase() === catItem.translated.name.toLowerCase(),
              );

              if (index > -1) {
                const indexItem = categories[index].products.findIndex((obj) => obj.id === item.id);
                if (indexItem === -1) {
                  const medias = [];
                  let imageUrl = '';
                  if (item.media && item.media.length > 0) {
                    item.media.map((mediaItem, mediaKey) => {
                      medias.push(mediaItem.media.url);
                      if (imageUrl === '') imageUrl = mediaItem.media.url;
                    });
                  }
                  if (item.cover && item.cover.media && item.cover.media.url) imageUrl = item.cover.media.url;

                  let features = [];
                  let featuresBottom = [];

                  let customFields = {};
                  let i = 1;
                  if (item.customFields) {
                    if (item.customFields.custom_product_info_1) {
                      i++;
                      features.push({
                        title: item.customFields.custom_product_info_1,
                        image: item.customFields.custom_product_info_icon_1
                          ? item.customFields.custom_product_info_icon_1
                          : null,
                      });
                    }
                    if (item.customFields.custom_product_info_2) {
                      i++;
                      features.push({
                        title: item.customFields.custom_product_info_2,
                        image: item.customFields.custom_product_info_icon_2
                          ? item.customFields.custom_product_info_icon_2
                          : null,
                      });
                    }

                    for (i; i <= 10; i++) {
                      if (item.customFields['custom_product_info_' + i]) {
                        featuresBottom.push({
                          title: item.customFields['custom_product_info_' + i],
                          image: item.customFields['custom_product_info_icon_' + i]
                            ? item.customFields['custom_product_info_icon_' + i]
                            : null,
                        });
                      }
                    }

                    customFields = item.customFields;
                  }
                  blnHasCat = true;
                  let description = item.translated.description ? item.translated.description : '';
                  description = description.replace('&nbsp;', ' ');
                  description = description.replace(regex, '');

                  let product = {
                    id: item.id,
                    productName: item.translated.name,
                    description: description,
                    price: item.calculatedPrice.totalPrice,
                    shortDescription: '',
                    swipe: medias,
                    features: features,
                    featuresBottom: featuresBottom,
                    image: imageUrl,
                    customFields: customFields,
                  };

                  categories[index].products.push(product);
                }
              }

              //console.log(item);
            });
          }
        });
      }
      // console.log(categories);
      //console.log(response);
      //return false;
      // TODO store token here
      await new Promise((resolve, reject) => setTimeout(() => resolve(), 800));
      dispatch(productSuccess(categories));
      return Promise.resolve(true);
    } catch (error) {
      //  console.log('response catch');
      //  console.log(error);
      dispatch(productFailure(error.message));
      return Promise.reject('error greska');

      //dispatch(loginFailure(error.message));
    }
  };
}
