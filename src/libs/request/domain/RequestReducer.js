import * as types from '../../../redux/types';
import { REQUEST_INITIAL_STATE } from './request.init-state';

export function requestReducer(state = REQUEST_INITIAL_STATE, action) {
  switch (action.type) {
    case types.REQUEST_START:
      return {
        ...state,
        requesting: true,
      };
    case types.REQUEST_END:
      return {
        ...state,
        requesting: false,
      };
    case types.MORE_MODAL_START:
      return {
        ...state,
        modal: true,
      };
    case types.MORE_MODAL_END:
      return {
        ...state,
        modal: false,
      };
    case types.SELECT_MORE_INDEX:
      return {
        ...state,
        item: action.payload,
      };

    default:
      return state;
  }
}
