import { ILocale } from './locale/locale.interface';
import { locale } from './locale/de';
import { parameterize } from './_helper/parameterize';

export function t(key: keyof ILocale, ...params: (number | string)[]): string {
  return params.length ? (locale[key] ? parameterize(t(key), ...params) : '') : locale[key];
}
