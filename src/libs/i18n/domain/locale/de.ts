import { ILocale } from './locale.interface';

export const locale: ILocale = {
  $CheckFormAndTryAgain: 'Bitte überprüfen Sie das Formular und versuchen Sie es erneut!',
  $FillAllFields: 'Bitte fülle alle Felder aus!',
  $WrongPassword: 'Passwort stimmt nicht!',
};
