import { ILocale } from './locale.interface';

export const locale: ILocale = {
  $CheckFormAndTryAgain: 'Please check the form and try again!',
  $FillAllFields: 'Please fill in all fields!',
  $WrongPassword: 'Password wrong!',
};
