export interface ILocale {
  $CheckFormAndTryAgain: string;
  $FillAllFields: string;
  $WrongPassword: string;
}
