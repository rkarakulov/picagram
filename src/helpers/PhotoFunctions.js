import React, {Component} from 'react';

export function getPhotoIndexByPath(arr, path, type = null) {
  if (type)
    return arr.findIndex(obj => obj.path === path && obj.source === type);
  else return arr.findIndex(obj => obj.path === path);
}

export function getPhotoCountSelection(selected, type = null) {
  let count = 0;
  if (selected) {
    selected.map((item, key) => {
      if (type) {
        if (item.selected && item.source === type) count++;
      } else {
        if (item.selected) count++;
      }
    });
  }
  return count;
}

export function getPhotoCount(selected, type = null) {
  let count = 0;
  if (selected) {
    selected.map((item, key) => {
      if (type) {
        if (item.source === type) count++;
      } else {
        count++;
      }
    });
  }
  return count;
}

import {
  COLOR_PRIMARY_FOTOS,
  COLOR_PRIMARY_FOTOBUCH,
  COLOR_PRIMARY_FOTOBOX,
  COLOR_PRIMARY_POSTKARTEN,
  COLOR_PRIMARY_FANBOOK,
} from '../assets/colors/colors';

import LayoutIcon from '../assets/icons/layoutIcon';
import ShippingIcon from '../assets/icons/shippingIcon';
import LoveIcon from '../assets/icons/loveIcon';
import VacationIcon from '../assets/icons/vacationIcon';
import DesignIcon from '../assets/icons/designIcon';
import FormatIcon from '../assets/icons/formatIcon';

export function getItems() {
  const items = [
    {
      id: 1,
      title: 'FOTOS',
      productName: 'PRODUKT 1',
      image: require('../assets/images/fotos.png'),
      price: 100,
      description: '00 x 00 cm',
      color: COLOR_PRIMARY_FOTOS,
      products: [],
      layout: 'single',
      photoOptions: true,
      type: 'album',
    },
    {
      id: 2,
      title: 'FOTOBUCH',
      productName: 'PRODUKT 2',
      image: require('../assets/images/fotobuch.png'),
      price: 100,
      description: '00 x 00 cm',
      color: COLOR_PRIMARY_FOTOBUCH,
      products: [],
      layout: 'square_book',
      type: 'album',
      photoOptions: false,
    },
    {
      id: 3,
      title: 'FOTOBOX',
      productName: 'PRODUKT 3',
      image: require('../assets/images/fotobox.png'),
      price: 100,
      description: '00 x 00 cm',
      color: COLOR_PRIMARY_FOTOBOX,
      products: [],
      layout: 'square_book',
      type: 'album',
      photoOptions: true,
    },
    {
      id: 4,
      title: 'POSTKARTEN',
      productName: 'PRODUKT 4',
      image: require('../assets/images/postkarten.png'),
      price: 100,
      description: '00 x 00 cm',
      color: COLOR_PRIMARY_POSTKARTEN,
      products: [],
      layout: 'postkarten',
      type: 'album',
      photoOptions: false,
    },
    {
      id: 5,
      title: 'FANBOOK',
      productName: 'PRODUKT 5',
      image: require('../assets/images/fanbook.png'),
      price: 100,
      description: '00 x 00 cm',
      color: COLOR_PRIMARY_FANBOOK,
      layout: 'square_book',
      products: [],
      type: 'album',
      photoOptions: false,
    },
    {
      id: 6,
      title: 'GESCHENKKARTE',
      productName: 'PRODUKT 5',
      image: require('../assets/images/geschenkkarte.jpg'),
      price: 100,
      description: '00 x 00 cm',
      color: COLOR_PRIMARY_FOTOBUCH,
      layout: 'square_book',
      products: [],
      photoOptions: false,
      type: 'gift',
    },
  ];
  const itemsOld = [
    {
      id: 1,
      title: 'FOTOS',
      productName: 'PRODUKT 1',
      image: require('../assets/images/fotos.png'),
      price: 100,
      description: '00 x 00 cm',
      color: COLOR_PRIMARY_FOTOS,
      products: [
        {
          id: 1,
          productName: 'Produkt 1',
          price: 100,
          shortDescription: '00 x 00cm',
          description:
            'Hariberia vendit as moluption ni vel maximet mo eatibus\naut eicieni ut exerem rero ipit officti comnit, ut hillia\nalitiis eum estio volore porit, eos distis rercillor si int\nofficiam vendant, ullaborum sitem corum la quia quam,\nodiatem repedipiciis sapiden dunti.\n\n',
          swipe: [
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
          ],

          features: [
            {
              title:
                'Schenke deine/n\nLiebste/n eure\nschönste Foto-\nsammlung',
              image: <LoveIcon />,
            },
            {
              title: 'Deine schönen\nUrlaubserinner-\nungen zum\nDurchblättern',
              image: <VacationIcon />,
            },
          ],
          featuresBottom: [
            {
              title:
                'Vesand aus Deutschland,\nInternationaler- und Expressversand möglich',
              image: <ShippingIcon />,
            },
            {
              title: 'Premiumpapier mit 200 g/m2',
              image: <LayoutIcon />,
            },
          ],
        },
        {
          id: 2,
          productName: 'Produkt 2',
          price: 100,
          shortDescription: '00 x 00cm',
          description:
            'Hariberia vendit as moluption ni vel maximet mo eatibus\naut eicieni ut exerem rero ipit officti comnit, ut hillia\nalitiis eum estio volore porit, eos distis rercillor si int\nofficiam vendant, ullaborum sitem corum la quia quam,\nodiatem repedipiciis sapiden dunti.\n\n',
          swipe: [
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
          ],

          features: [
            {
              title:
                'Schenke deine/n\nLiebste/n eure\nschönste Foto-\nsammlung',
              image: <LoveIcon />,
            },
            {
              title: 'Deine schönen\nUrlaubserinner-\nungen zum\nDurchblättern',
              image: <VacationIcon />,
            },
          ],
          featuresBottom: [
            {
              title:
                'Vesand aus Deutschland,\nInternationaler- und Expressversand möglich',
              image: <ShippingIcon />,
            },
            {
              title: 'Premiumpapier mit 200 g/m2',
              image: <LayoutIcon />,
            },
          ],
        },
      ],
    },
    {
      id: 2,
      title: 'FOTOBUCH',
      productName: 'PRODUKT 2',
      image: require('../assets/images/fotobuch.png'),
      price: 100,
      description: '00 x 00 cm',
      color: COLOR_PRIMARY_FOTOBUCH,
      products: [
        {
          id: 3,
          productName: 'Produkt 1',
          price: 100,
          shortDescription: '00 x 00cm',
          description:
            'Hariberia vendit as moluption ni vel maximet mo eatibus\naut eicieni ut exerem rero ipit officti comnit, ut hillia\nalitiis eum estio volore porit, eos distis rercillor si int\nofficiam vendant, ullaborum sitem corum la quia quam,\nodiatem repedipiciis sapiden dunti.\n\n',
          swipe: [
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
          ],

          features: [
            {
              title:
                'Schenke deine/n\nLiebste/n eure\nschönste Foto-\nsammlung',
              image: <LoveIcon />,
            },
            {
              title: 'Deine schönen\nUrlaubserinner-\nungen zum\nDurchblättern',
              image: <VacationIcon />,
            },
          ],
          featuresBottom: [
            {
              title:
                'Individuelle Gestaltung, große Auswahl von\nFarben und Schriftarten und grafischen\nElementen',
              image: <DesignIcon />,
            },
            {
              title:
                'XX bis XX Seiten mit Soft- oder Hardcover in\nverschiedenen Layouts, Innenseiten aus\nFotopapier oder Hochglanzpapier',
              image: <LayoutIcon />,
            },
            {
              title:
                'Wähle zwischen zwei verschiedenen Formaten\naus: 21x21cm oder 14x14 cm',
              image: <FormatIcon />,
            },
            {
              title:
                'Vesand aus Deutschland,\nInternationaler- und Expressversand möglich',
              image: <ShippingIcon />,
            },
          ],
        },
        {
          id: 4,
          productName: 'Produkt 2',
          price: 100,
          shortDescription: '00 x 00cm',
          description:
            'Hariberia vendit as moluption ni vel maximet mo eatibus\naut eicieni ut exerem rero ipit officti comnit, ut hillia\nalitiis eum estio volore porit, eos distis rercillor si int\nofficiam vendant, ullaborum sitem corum la quia quam,\nodiatem repedipiciis sapiden dunti.\n\n',
          swipe: [
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
          ],

          features: [
            {
              title:
                'Schenke deine/n\nLiebste/n eure\nschönste Foto-\nsammlung',
              image: <LoveIcon />,
            },
            {
              title: 'Deine schönen\nUrlaubserinner-\nungen zum\nDurchblättern',
              image: <VacationIcon />,
            },
          ],
          featuresBottom: [
            {
              title:
                'Individuelle Gestaltung, große Auswahl von\nFarben und Schriftarten und grafischen\nElementen',
              image: <DesignIcon />,
            },
            {
              title:
                'XX bis XX Seiten mit Soft- oder Hardcover in\nverschiedenen Layouts, Innenseiten aus\nFotopapier oder Hochglanzpapier',
              image: <LayoutIcon />,
            },
            {
              title:
                'Wähle zwischen zwei verschiedenen Formaten\naus: 21x21cm oder 14x14 cm',
              image: <FormatIcon />,
            },
            {
              title:
                'Vesand aus Deutschland,\nInternationaler- und Expressversand möglich',
              image: <ShippingIcon />,
            },
          ],
        },
      ],
    },
    {
      id: 3,
      title: 'FOTOBOX',
      productName: 'PRODUKT 3',
      image: require('../assets/images/fotobox.png'),
      price: 100,
      description: '00 x 00 cm',
      color: COLOR_PRIMARY_FOTOBOX,
      products: [
        {
          id: 5,
          productName: 'Produkt 1',
          price: 100,
          shortDescription: '00 x 00cm',
          description:
            'Hariberia vendit as moluption ni vel maximet mo eatibus\naut eicieni ut exerem rero ipit officti comnit, ut hillia\nalitiis eum estio volore porit, eos distis rercillor si int\nofficiam vendant, ullaborum sitem corum la quia quam,\nodiatem repedipiciis sapiden dunti.\n\n',

          swipe: [
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
          ],
          features: [
            {
              title:
                'Schenke deine/n\nLiebste/n eure\nschönste Foto-\nsammlung',
              image: <LoveIcon />,
            },
            {
              title: 'Deine schönen\nUrlaubserinner-\nungen zum\nDurchblättern',
              image: <VacationIcon />,
            },
          ],
          featuresBottom: [
            {
              title:
                'Individuelle Gestaltung, große Auswahl von\nFarben und Schriftarten und grafischen\nElementen',
              image: <DesignIcon />,
            },
            {
              title:
                'XX bis XX Seiten mit Soft- oder Hardcover in\nverschiedenen Layouts, Innenseiten aus\nFotopapier oder Hochglanzpapier',
              image: <LayoutIcon />,
            },
            {
              title:
                'Wähle zwischen zwei verschiedenen Formaten\naus: 21x21cm oder 14x14 cm',
              image: <FormatIcon />,
            },
            {
              title:
                'Vesand aus Deutschland,\nInternationaler- und Expressversand möglich',
              image: <ShippingIcon />,
            },
          ],
        },
        {
          id: 6,
          productName: 'Produkt 2',
          price: 100,
          shortDescription: '00 x 00cm',
          description:
            'Hariberia vendit as moluption ni vel maximet mo eatibus\naut eicieni ut exerem rero ipit officti comnit, ut hillia\nalitiis eum estio volore porit, eos distis rercillor si int\nofficiam vendant, ullaborum sitem corum la quia quam,\nodiatem repedipiciis sapiden dunti.\n\n',
          swipe: [
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
          ],

          features: [
            {
              title:
                'Schenke deine/n\nLiebste/n eure\nschönste Foto-\nsammlung',
              image: <LoveIcon />,
            },
            {
              title: 'Deine schönen\nUrlaubserinner-\nungen zum\nDurchblättern',
              image: <VacationIcon />,
            },
          ],
          featuresBottom: [
            {
              title:
                'Individuelle Gestaltung, große Auswahl von\nFarben und Schriftarten und grafischen\nElementen',
              image: <DesignIcon />,
            },
            {
              title:
                'XX bis XX Seiten mit Soft- oder Hardcover in\nverschiedenen Layouts, Innenseiten aus\nFotopapier oder Hochglanzpapier',
              image: <LayoutIcon />,
            },
            {
              title:
                'Wähle zwischen zwei verschiedenen Formaten\naus: 21x21cm oder 14x14 cm',
              image: <FormatIcon />,
            },
            {
              title:
                'Vesand aus Deutschland,\nInternationaler- und Expressversand möglich',
              image: <ShippingIcon />,
            },
          ],
        },
      ],
    },
    {
      id: 4,
      title: 'POSTKARTEN',
      productName: 'PRODUKT 4',
      image: require('../assets/images/postkarten.png'),
      price: 100,
      description: '00 x 00 cm',
      color: COLOR_PRIMARY_POSTKARTEN,

      products: [
        {
          id: 7,
          productName: 'Produkt 1',
          price: 100,
          shortDescription: '00 x 00cm',
          description:
            'Hariberia vendit as moluption ni vel maximet mo eatibus\naut eicieni ut exerem rero ipit officti comnit, ut hillia\nalitiis eum estio volore porit, eos distis rercillor si int\nofficiam vendant, ullaborum sitem corum la quia quam,\nodiatem repedipiciis sapiden dunti.\n\n',

          swipe: [
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
          ],
          features: [
            {
              title:
                'Schenke deine/n\nLiebste/n eure\nschönste Foto-\nsammlung',
              image: <LoveIcon />,
            },
            {
              title: 'Deine schönen\nUrlaubserinner-\nungen zum\nDurchblättern',
              image: <VacationIcon />,
            },
          ],
          featuresBottom: [
            {
              title:
                'Individuelle Gestaltung, große Auswahl von\nFarben und Schriftarten und grafischen\nElementen',
              image: <DesignIcon />,
            },
            {
              title:
                'XX bis XX Seiten mit Soft- oder Hardcover in\nverschiedenen Layouts, Innenseiten aus\nFotopapier oder Hochglanzpapier',
              image: <LayoutIcon />,
            },
            {
              title:
                'Wähle zwischen zwei verschiedenen Formaten\naus: 21x21cm oder 14x14 cm',
              image: <FormatIcon />,
            },
            {
              title:
                'Vesand aus Deutschland,\nInternationaler- und Expressversand möglich',
              image: <ShippingIcon />,
            },
          ],
        },
        {
          id: 8,
          productName: 'Produkt 2',
          price: 100,
          shortDescription: '00 x 00cm',
          description:
            'Hariberia vendit as moluption ni vel maximet mo eatibus\naut eicieni ut exerem rero ipit officti comnit, ut hillia\nalitiis eum estio volore porit, eos distis rercillor si int\nofficiam vendant, ullaborum sitem corum la quia quam,\nodiatem repedipiciis sapiden dunti.\n\n',

          swipe: [
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
          ],
          features: [
            {
              title:
                'Schenke deine/n\nLiebste/n eure\nschönste Foto-\nsammlung',
              image: <LoveIcon />,
            },
            {
              title: 'Deine schönen\nUrlaubserinner-\nungen zum\nDurchblättern',
              image: <VacationIcon />,
            },
          ],
          featuresBottom: [
            {
              title:
                'Individuelle Gestaltung, große Auswahl von\nFarben und Schriftarten und grafischen\nElementen',
              image: <DesignIcon />,
            },
            {
              title:
                'XX bis XX Seiten mit Soft- oder Hardcover in\nverschiedenen Layouts, Innenseiten aus\nFotopapier oder Hochglanzpapier',
              image: <LayoutIcon />,
            },
            {
              title:
                'Wähle zwischen zwei verschiedenen Formaten\naus: 21x21cm oder 14x14 cm',
              image: <FormatIcon />,
            },
            {
              title:
                'Vesand aus Deutschland,\nInternationaler- und Expressversand möglich',
              image: <ShippingIcon />,
            },
          ],
        },
      ],
    },
    {
      id: 5,
      title: 'FANBOOK',
      productName: 'PRODUKT 5',
      image: require('../assets/images/fanbook.png'),
      price: 100,
      description: '00 x 00 cm',
      color: COLOR_PRIMARY_FANBOOK,

      products: [
        {
          id: 9,
          productName: 'Produkt 1',
          price: 100,
          shortDescription: '00 x 00cm',
          description:
            'Hariberia vendit as moluption ni vel maximet mo eatibus\naut eicieni ut exerem rero ipit officti comnit, ut hillia\nalitiis eum estio volore porit, eos distis rercillor si int\nofficiam vendant, ullaborum sitem corum la quia quam,\nodiatem repedipiciis sapiden dunti.\n\n',

          swipe: [
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
          ],
          features: [
            {
              title:
                'Schenke deine/n\nLiebste/n eure\nschönste Foto-\nsammlung',
              image: <LoveIcon />,
            },
            {
              title: 'Deine schönen\nUrlaubserinner-\nungen zum\nDurchblättern',
              image: <VacationIcon />,
            },
          ],
          featuresBottom: [
            {
              title:
                'Individuelle Gestaltung, große Auswahl von\nFarben und Schriftarten und grafischen\nElementen',
              image: <DesignIcon />,
            },
            {
              title:
                'XX bis XX Seiten mit Soft- oder Hardcover in\nverschiedenen Layouts, Innenseiten aus\nFotopapier oder Hochglanzpapier',
              image: <LayoutIcon />,
            },
            {
              title:
                'Wähle zwischen zwei verschiedenen Formaten\naus: 21x21cm oder 14x14 cm',
              image: <FormatIcon />,
            },
            {
              title:
                'Vesand aus Deutschland,\nInternationaler- und Expressversand möglich',
              image: <ShippingIcon />,
            },
          ],
        },
        {
          id: 10,
          productName: 'Produkt 2',
          price: 100,
          shortDescription: '00 x 00cm',
          description:
            'Hariberia vendit as moluption ni vel maximet mo eatibus\naut eicieni ut exerem rero ipit officti comnit, ut hillia\nalitiis eum estio volore porit, eos distis rercillor si int\nofficiam vendant, ullaborum sitem corum la quia quam,\nodiatem repedipiciis sapiden dunti.\n\n',
          swipe: [
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
            require('../assets/images/fotobuchImage.svg'),
          ],

          features: [
            {
              title:
                'Schenke deine/n\nLiebste/n eure\nschönste Foto-\nsammlung',
              image: <LoveIcon />,
            },
            {
              title: 'Deine schönen\nUrlaubserinner-\nungen zum\nDurchblättern',
              image: <VacationIcon />,
            },
          ],
          featuresBottom: [
            {
              title:
                'Individuelle Gestaltung, große Auswahl von\nFarben und Schriftarten und grafischen\nElementen',
              image: <DesignIcon />,
            },
            {
              title:
                'XX bis XX Seiten mit Soft- oder Hardcover in\nverschiedenen Layouts, Innenseiten aus\nFotopapier oder Hochglanzpapier',
              image: <LayoutIcon />,
            },
            {
              title:
                'Wähle zwischen zwei verschiedenen Formaten\naus: 21x21cm oder 14x14 cm',
              image: <FormatIcon />,
            },
            {
              title:
                'Vesand aus Deutschland,\nInternationaler- und Expressversand möglich',
              image: <ShippingIcon />,
            },
          ],
        },
      ],
    },
  ];
  return items;
}
