import React, {Component} from 'react';
import {View, ActivityIndicator} from 'react-native';

export default AppLoader = ({isVisible}) => (
  <View
    style={{
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#0000004D',
      zIndex: 2,
      elevation: 2,
    }}>
    <ActivityIndicator isVisible={isVisible} size="large" color={'white'} />
  </View>
);
