import {showMessage} from 'react-native-flash-message';

const chooseColor = type => {
  switch (type) {
    case 'success':
      return '#6D7C3E';
    case 'error':
      return '#BE2335';
    case 'warning':
      return '#BC9633';
    default:
      return '#23BC76';
  }
};
export const showAlert = (message, type = 'error') => {
  showMessage({
    message,
    type: 'info',
    backgroundColor: chooseColor(type),
    color: '#FFF',
    duration: 2000,
    animationDuration: 500,
    floating: true,
    textStyle: {fontFamily: 'Poppins-Medium', fontWeight: 16},
  });
};
