import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomePage from '../screens/HomePage';
import ProfilPage from '../screens/ProfilPage';
import React from 'react';

const Tab = createBottomTabNavigator();

export default function MyTabs() {
  return (
    <Tab.Navigator
    initialRouteName="Home"
    screenOptions={{
        tabBarStyle: { position: 'absolute' }
      }}>
       <Tab.Screen
              name="Home"
              component={HomePage}
              options={{
                tabBarLabel: 'Home',
              }}
            />
            <Tab.Screen
              name="Profil"
              component={ProfilPage}           
            />
    </Tab.Navigator>
  );
}
