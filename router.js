import 'react-native-gesture-handler';
import React from 'react';
import { connect } from 'react-redux';
import { axiosInstance } from './src/api/axios';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import HomePage from './src/screens/HomePage';
import ProductsPage from './src/screens/ProductsPage';
import AlbumPage from './src/screens/AlbumPage';
import InstagramPage from './src/screens/InstagramPage';
import FacebookPage from './src/screens/FacebookPage';
import FacebookPageWeb from './src/screens/FacebookPageWeb';
import InstagramPageWeb from './src/screens/InstagramPageWeb';
import { SignInPage } from './src/libs/auth/feature/sign-in-page';
import RegistrationPage from './src/screens/registration/registration-page';
import InstagramSignInPage from './src/screens/InstagramSignInPage';
import FacebookSignInPage from './src/screens/FacebookSignInPage';
import DetailPage from './src/screens/DetailPage';
import ResetPasswordEmailPage from './src/screens/ResetPasswordEmailPage';
import ResetPasswordCodePage from './src/screens/ResetPasswordCodePage';
import ResetPasswordPage from './src/screens/ResetPasswordPage';
import ChoosePicturePage from './src/screens/ChoosePicturePage';
import ProfilPage from './src/screens/ProfilPage';
import SettingsPage from './src/screens/SettingsPage';
import HelpAndContactPage from './src/screens/HelpAndContactPage';
import HelpAndContactFaqPage from './src/screens/HelpAndContactFaqPage';
import HelpAndContactSendMessagePage from './src/screens/HelpAndContactSendMessagePage';
import OrdersPage from './src/screens/OrdersPage';
import BringAFriendPage from './src/screens/BringAFriendPage';
import GiftCardPage from './src/screens/GiftCardPage';
import AdressePage from './src/screens/AdressePage';
import MyPaymentMethodPage from './src/screens/MyPaymentMethodPage';
import CheckOutPage from './src/screens/CheckOutPage';
import CreatePhotoPageV2 from './src/components/CreatePhotoPageV2';
import CreatePhotoPage from './src/screens/CreatePhotoPage';
import EditPhotoPage from './src/screens/EditPhotoPage';
import PaymentMethodPage from './src/screens/PaymentMethodPage';
import SummaryPage from './src/screens/SummaryPage';
import WarenkorbPage from './src/screens/WarenkorbPage';
import EntwurftPage from './src/screens/EntwurftPage';
import UploadPage from './src/screens/UploadPage';
import OverallViewPage from './src/screens/OverallViewPage';
import UberUnsPage from './src/screens/UberUnsPage';
import DatenerklarungPage from './src/screens/DatenerklarungPage';
import UnsereAgbsPage from './src/screens/UnsereAgbsPage';
import GeschenkkartePage from './src/screens/GeschenkkartePage';
import AnmeldeMethodePage from './src/screens/login-method/login-method-page';

import AppLoader from './src/helpers/AppLoader';
import { showAlert } from './src/helpers/AlertManager';

const MainStack = createStackNavigator();

function Router(props) {
  global.accessToken = props.login && props.login.results ? props.login.results.accessToken : '';

  if (global.interceptors == undefined || global.interceptors == false) {
    if (props.moreModal) {
      props.moreModal(false);
    }
    if (props.request) {
      props.request(false);
    }

    global.interceptors = true;

    axiosInstance.interceptors.request.use(function (config) {
      if (props.request) {
        props.request(true);
      }

      return config;
    });

    axiosInstance.interceptors.response.use(
      (response) => {
        //  console.log('router interceptors response');
        if (props.request) {
          props.request(false);
        }
        return response;
      },
      (error) => {
        // console.log('err msg', error.message);
        // console.log('data', error?.response?.data);
        // console.log('status', error?.response?.status);
        if (error?.response?.data?.message) {
          showAlert(error.response.data.message);
        }
        // console.log('router interceptors error');
        if (props.request) {
          props.request(false);
        }

        return Promise.reject(error);
      },
    );
  }

  return (
    <NavigationContainer>
      <MainStack.Navigator
        //drawerContent={props => <SideMenu {...props} />}
        screenOptions={{
          headerShown: false,
        }}
      >
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="HomePage"
          component={HomePage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="SignInPage"
          component={SignInPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="ProductsPage"
          component={ProductsPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="AlbumPage"
          component={AlbumPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="InstagramPage"
          component={InstagramPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="FacebookPage"
          component={FacebookPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="FacebookPageWeb"
          component={FacebookPageWeb}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="InstagramPageWeb"
          component={InstagramPageWeb}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="RegistrationPage"
          component={RegistrationPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="InstagramSignInPage"
          component={InstagramSignInPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="FacebookSignInPage"
          component={FacebookSignInPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="DetailPage"
          component={DetailPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="ResetPasswordEmailPage"
          component={ResetPasswordEmailPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="ResetPasswordCodePage"
          component={ResetPasswordCodePage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="ResetPasswordPage"
          component={ResetPasswordPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="ChoosePicturePage"
          component={ChoosePicturePage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="ProfilPage"
          component={ProfilPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="SettingsPage"
          component={SettingsPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="HelpAndContactPage"
          component={HelpAndContactPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="HelpAndContactFaqPage"
          component={HelpAndContactFaqPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="HelpAndContactSendMessagePage"
          component={HelpAndContactSendMessagePage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="OrdersPage"
          component={OrdersPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="BringAFriendPage"
          component={BringAFriendPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="GiftCardPage"
          component={GiftCardPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="AdressePage"
          component={AdressePage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="MyPaymentMethodPage"
          component={MyPaymentMethodPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="CheckOutPage"
          component={CheckOutPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="CreatePhotoPage"
          component={CreatePhotoPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, unmountOnBlur: true }}
          name="CreatePhotoPageV2"
          component={CreatePhotoPageV2}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="EditPhotoPage"
          component={EditPhotoPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="PaymentMethodPage"
          component={PaymentMethodPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="SummaryPage"
          component={SummaryPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="WarenkorbPage"
          component={WarenkorbPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="EntwurftPage"
          component={EntwurftPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="UploadPage"
          component={UploadPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="OverallViewPage"
          component={OverallViewPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="UberUnsPage"
          component={UberUnsPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="DatenerklarungPage"
          component={DatenerklarungPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="UnsereAgbsPage"
          component={UnsereAgbsPage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="GeschenkkartePage"
          component={GeschenkkartePage}
        />
        <MainStack.Screen
          options={{ swipeEnabled: true, ...TransitionPresets.SlideFromRightIOS }}
          name="AnmeldeMethodePage"
          component={AnmeldeMethodePage}
        />
      </MainStack.Navigator>
      {(props.login.requesting || props.product.requesting) && <AppLoader isVisible={true} />}
    </NavigationContainer>
  );
}

function mapStateToProps(state) {
  return {
    login: state.login,
    product: state.product,
    requesting: state.request,
  };
}

export default connect(mapStateToProps, {})(Router);
